@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Personal</h5>
                        <span>Editar Personal</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <div class="card-block">
                            <form id="main" method="POST" action="{{ route('admin.actividad_edit_data',['id' => $actividades->id] ) }}" >
                                @csrf
                                @method('PATCH')
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Descripción</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="descripcion" name="descripcion" required value="{{ isset($actividades->descripcion)?$actividades->descripcion:''}}">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Estado</label>
                                            <div class="col-sm-10">
                                                <select name="estado" class="form-control" id="estado">
                                                    <option value="1" {{ $actividades->estado == 1 ? 'selected' :''}} >ACTIVO</option>
                                                    <option value="2" {{ $actividades->estado == 2 ? 'selected' :''}} >DESACTIVADO</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <hr>
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-sm btn-primary m-b-0">
                                            <i class="fa fa-save"></i> Editar
                                        </button>
                                        <a href="{{ route('admin.actividad_list') }}" class="btn btn-info waves-effect waves-light btn-sm m-b-0"><i class="fa fa-reply"></i> Volver</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <script>
        // $('body').addClass('wysihtml5-supported');
    </script>
    <script src="{{ asset('js/titular.js') }}"></script>
@stop