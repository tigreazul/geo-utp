@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Actividades</h5>
                        <span>Creación Actividades</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- card -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Registro de Actividades</h5>
                            <!-- <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
                        </div>
                        <div class="card-block">
                            <form id="main" method="POST" action="{{ route('admin.actividad_add') }}" >
                                @csrf
                                <div class="col-lg-12 col-xl-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Descripción</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="descripcion" name="descripcion" required>
                                                    <span class="messages"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Estado</label>
                                                <div class="col-sm-10">
                                                    <select name="estado" class="form-control" id="UsuEstado">
                                                        <option value="1">ACTIVO</option>
                                                        <option value="2">DESACTIVADO</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <hr>
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-sm btn-primary m-b-0">
                                            <i class="fa fa-save"></i> Registrar
                                        </button>
                                        <a href="{{ route('admin.actividad_list') }}" class="btn btn-sm btn-default m-b-0"><i class="fa fa-reply"></i> Volver</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <script>
        // $('body').addClass('wysihtml5-supported');
        $('#nombre').on('keyup',function(){
            let vr = $(this).val();
            let ap = $('#apaterno').val();
            let aa = vr.substring(0,1).toUpperCase();
            console.log(ap);
            if($(ap == "")){
                $('#usuario2').val(aa);
                $('#usuario').val(aa);
            }else{
                $('#usuario2').val(aa+ap);
                $('#usuario').val(aa+ap);
            }

        });

        $('#apaterno').on('keyup',function(){
            let vr = $(this).val().toUpperCase();
            let aa = $('#usuario').val();
            let name = $("#nombre").val().substring(0,1).toUpperCase();
            $('#usuario2').val(name+vr);
            $('#usuario').val(name+vr);
        });
    </script>
    <script src="{{ asset('js/titular.js') }}"></script>
@stop