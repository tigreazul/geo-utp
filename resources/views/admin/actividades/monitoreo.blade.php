@extends('admin.layouts.admin')
@section('content')
    <!-- <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Monitoreo</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class=" breadcrumb breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="#"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Dashboard</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div> -->

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- card -->
                    <div class="row">
                        <div class="card panel-modulo col " style="margin-right: 15px;">
                            <!-- <div class="card-header">
                                <div class="row">
                                    <div class="col">
                                        <h5>Lista de Actividades</h5>
                                    </div>
                                    <div class="col-42">
                                        <a href="{{ route('admin.actividad_create') }}" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus-circle"></i> NUEVO ACTIVIDAD</a>
                                    </div>
                                </div>
                            </div> -->
                            <div class="card-block">
                                <div id="map"></div>
                            </div>
                        </div>

                    </div>
                    <div class="md-overlay"></div>
                    <!-- card -->
                </div>
            </div>
        </div>
    </div>


    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" />
    <script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>
    <script type="text/javascript">
        
        var locations = [
            <?php foreach ($puntosreferencia as $makera) { ?>
                ["<?php echo $makera->nombre.' '.$makera->apellidoPaterno.' '.$makera->apellidoMaterno. ', ha realizado '.$makera->actividades.' actividades' ?>", 
                <?php echo $makera->latitud ?>, 
                <?php echo $makera->longitud ?>
                ],
            <?php }?>
            
        ];

        var map = L.map('map').setView([-10.577545, -75.40076], 8);
        mapLink ='<a href="http://openstreetmap.org">OpenStreetMap</a>';
        L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; ' + mapLink + ' Contributors UTP',
            maxZoom: 20,
        }).addTo(map);

        for (var i = 0; i < locations.length; i++) {
            marker = new L.marker([locations[i][1], locations[i][2]])
            .bindPopup(locations[i][0])
            .addTo(map);
        }
    </script>

    <style>
        #map {
            width: 100%;
            height: 700px;
            border:1px solid
        }
    </style>
@stop