@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Prospecto</h5>
                        <span>Recarga de saldo</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class=" breadcrumb breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="#"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Dashboard</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>INFORMACIÓN PERSONAL</h5>
                                </div>
                                <div class="card-block" style="">
                                    
                                    <div class="row">
                                        <div class="col-lg-4 col-xl-3 col-sm-12">
                                            <div class="badge-box">
                                                <div class="sub-title">Datos completos</div>
                                                <input type="hidden" value="{{$prospecto->id}}" id="id_prospecto">
                                                <input type="hidden" value="{{ route('admin.estado_pendientes', ['id'=>$prospecto->id]) }}" id="url_proceso">
                                                <p> {{ $prospecto->apellidoPaterno.' '.$prospecto->apellidoMaterno.' '.$prospecto->nombre}} </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-xl-3 col-sm-6">
                                            <div class="badge-box">
                                                <div class="sub-title">Email</div>
                                                <p> {{ $prospecto->email }} </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-xl-3 col-sm-6">
                                            <div class="badge-box">
                                                <div class="sub-title">Tipo de documento</div>
                                                <p> {{ $prospecto->Descripcion }} </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-xl-3 col-sm-6">
                                            <div class="badge-box">
                                                <div class="sub-title">Numero de documento</div>
                                                <p> {{ $prospecto->nro_documento }} </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    <h5>HISTORIAL DE SALDO</h5>
                                </div>
                                <div class="card-block" style="">
                                    <div class="dt-responsive table-responsive">
                                        <!-- lista de tablas -->
                                        <table class="table table-hover m-b-0 table-sm">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Monto</th>
                                                    <th>Voucher</th>
                                                    <th>Validado</th>
                                                    <!-- <th>Acción</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php $i = 1; $valida = 0; $pendientes=0; @endphp
                                                @foreach($historial as $history)
                                                    <tr class="table-verify selector-{{ $history->id }}">
                                                        <td scope="row">{{ $i }}</td>
                                                        <td>S/. {{ $history->montos }}</td>
                                                        <td>
                                                            <a href="{{ asset('voucher/'.$history->voucher) }}" target="_blank">
                                                                <img src="{{ isset($history->voucher)?asset('voucher/'.$history->voucher):'' }}" class="rounded img-60" >
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <label class="label label-{{ ($history->validado == 1)? 'success': 'danger' }}">{{ ($history->validado == 1)? 'VALIDADO': 'PENDIENTE EN VALIDACIÓN' }}</label>
                                                        </td>
                                                    </tr>

                                                    @if($history->validado == 1)
                                                        @php $valida = $valida + $history->montos; @endphp
                                                    @else
                                                        @php $pendientes = $pendientes + $history->montos; @endphp
                                                    @endif

                                                    @php $i++; @endphp
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="btn btn-success waves-effect waves-light">
                                        <strong>VALIDADO </strong> <span class="badge">S/. {{$valida}}</span>
                                    </div>
                                    <div class="btn btn-danger waves-effect waves-light">
                                        <strong>PENDIENTE </strong> <span class="badge">S/. {{$pendientes}}</span>
                                    </div>

                                </div>
                            </div>

                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/prospecto.js') }}"></script>
@stop