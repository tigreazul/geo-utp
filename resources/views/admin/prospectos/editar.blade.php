@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Prospecto</h5>
                        <span>Editar Prospecto</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- card -->
                    <div class="card">
                        <!-- <div class="card-header"> -->
                            <!-- <h5>Editar de Usuario</h5> -->
                            <!-- <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
                        <!-- </div> -->
                        
                        <div class="card-block">
                            
                            <form id="main" method="POST" action="{{ route('admin.prospecto_edit_data',['id' => $prospecto->id] ) }}" enctype="multipart/form-data">
                                @csrf
                                @method('PATCH')
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Nombre</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="nombre" name="nombre" required onkeypress="return soloLetras(event)" value="{{ isset($prospecto->nombre)?$prospecto->nombre:''}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Apellido Paterno</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="apaterno" name="apaterno" required onkeypress="return soloLetras(event)" value="{{ isset($prospecto->apellidoPaterno)?$prospecto->apellidoPaterno:''}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Apellido Materno</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="amaterno" name="amaterno" required onkeypress="return soloLetras(event)" value="{{ isset($prospecto->apellidoMaterno)?$prospecto->apellidoMaterno:''}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-10">
                                                <input type="email" class="form-control" id="email" name="email" required value="{{ isset($prospecto->email)?$prospecto->email:''}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Tipo de Documento</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="form-stacked-select" name="tdocumento" required>
                                                    <option value="">[SELECCIONE]</option>
                                                    @foreach($documento as $tipo)
                                                        <option value="{{ $tipo->Valor }}" {{ ($tipo->Valor == $prospecto->tipo_documento)?'selected':'' }} >{{ $tipo->Descripcion}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Numero Documento</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="nrodocumento" name="nrodocumento" required value="{{ isset($prospecto->nro_documento)?$prospecto->nro_documento:'' }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Telefono</label>
                                            <div class="col-sm-10">
                                                <input type="hidden" id="historial" name="historial" value="{{ isset($historial->id)?$historial->id:'' }}">
                                                <input type="text" class="form-control" id="telefono" name="telefono" required value="{{ isset($prospecto->telefono)?$prospecto->telefono:'' }}">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Compra de Saldo</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="comprasaldo" name="comprasaldo" required>
                                                    <option value="">[SELECCIONE]</option>
                                                    <option value="1" {{ ($prospecto->compra_saldo == '1')?'selected':'' }}>SI</option>
                                                    <option value="2" {{ ($prospecto->compra_saldo == '2')?'selected':'' }}>NO</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row voucher" style="display:none">
                                            <label class="col-sm-2 col-form-label">Monto</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="form-stacked-select" name="saldos" >
                                                    <option value="">[SELECCIONE]</option>
                                                    @foreach($montos as $monto)
                                                        <option value="{{ $monto->id }}" {{ isset($historial->monto)?($monto->id == $historial->monto)?'selected':'' :'' }}>S/ {{ $monto->montos}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row voucher" style="display:none">
                                            <label class="col-sm-2 col-form-label">Voucher</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control" id="voucher" name="voucher" value="{{ isset($historial->voucher)?$historial->voucher:'' }}">
                                                <br>
                                                @if( isset($historial->voucher) && $historial->voucher!= '' )
                                                    <img src="{{ isset($historial->voucher)?asset('voucher/'.$historial->voucher):'' }}" class="rounded" >
                                                @endif
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Dirección</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="direccion" name="direccion" value="{{ isset($prospecto->direccion)?$prospecto->direccion:'' }}" required>
                                                <span class="messages"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Referencia</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="referencia" name="referencia" value="{{ isset($prospecto->referencia)?$prospecto->referencia:'' }}" required>
                                                <span class="messages"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Latitud</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="lat" name="txtLat" value="{{ $prospecto->latitud }}" required>
                                                <span class="messages"></span>
                                            </div>
                                            <label class="col-sm-1 col-form-label">Longitud</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="lon" name="txtLng" value="{{ $prospecto->longitud }}" required>
                                                <span class="messages"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <div id="map"></div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Estado</label>
                                            <div class="col-sm-10">
                                                <select name="estado" class="form-control" id="UsuEstado">
                                                    <option value="1" {{ ($prospecto->estado == '1')?'selected':'' }}>ACTIVO</option>
                                                    <option value="2" {{ ($prospecto->estado == '2')?'selected':'' }}>DESACTIVADO</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <hr>
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-sm btn-primary m-b-0">
                                                <i class="fa fa-save"></i> Editar
                                        </button>
                                        <a href="{{ route('admin.prospecto_list') }}" class="btn btn-info waves-effect waves-light btn-sm m-b-0"><i class="fa fa-reply"></i> Volver</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <script>

            let tipo = $('#comprasaldo').val();
            if(tipo == 1){
                $('.voucher').show();
            }else{
                $('.voucher').hide();
            }

        // $('body').addClass('wysihtml5-supported');
        $('#comprasaldo').change(function(e){
            e.preventDefault();
            let tipo = $(this).val();
            console.log(tipo);
            if(tipo == 1){
                $('.voucher').show();
            }else{
                $('.voucher').hide();
            }
        });
    </script>

    @include('admin.prospectos.modal')
@stop