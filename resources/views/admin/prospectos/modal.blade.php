<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" />
<script>
    function success(position) {

        var lati  = position.coords.latitude.toFixed(8);
        var long = position.coords.longitude.toFixed(8);

        mapa(lati,long);
        // let lati = document.getElementById('lat').value;
        // let long = document.getElementById('lon').value
        // console.log(lati,long);
        
    };

    function error() {
        // swal("Unable to retrieve your location");
        mapa('','');
    };

    function mapa(lati, long){
        let startlat;
        let startlon;

        if(lati == '' && long == ''){
            // var startlat  = position.coords.latitude.toFixed(8);
            // var startlon = position.coords.longitude.toFixed(8);
            startlat = document.getElementById('lat').value;
            startlon = document.getElementById('lon').value
        }else{
            startlat  = lati;
            startlon = long;
        }

        console.log(startlat,startlon,'ssss');
        
        var options = {
            center: [startlat, startlon],
            zoom: 18
        }

        document.getElementById('lat').value = startlat;
        document.getElementById('lon').value = startlon;
        
        var map = L.map('map', options);
        var nzoom = 12;

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: 'OSM'
        }).addTo(map);

        var myMarker = L.marker([startlat, startlon], {
            title: "Coordinates",
            alt: "Coordinates",
            draggable: true
        }).addTo(map).on('dragend', function() {
            var lat = myMarker.getLatLng().lat.toFixed(8);
            var lon = myMarker.getLatLng().lng.toFixed(8);
            var czoom = map.getZoom();
            if (czoom < 18) {
                nzoom = czoom + 2;
            }
            if (nzoom > 18) {
                nzoom = 18;
            }
            if (czoom != 18) {
                map.setView([lat, lon], nzoom);
            } else {
                map.setView([lat, lon]);
            }
            document.getElementById('lat').value = lat;
            document.getElementById('lon').value = lon;
            myMarker.bindPopup("Lat " + lat + "<br />Lon " + lon).openPopup();
        });

        function chooseAddr(lat1, lng1) {
            myMarker.closePopup();
            map.setView([lat1, lng1], 18);
            myMarker.setLatLng([lat1, lng1]);
            lat = lat1.toFixed(8);
            lon = lng1.toFixed(8);
            document.getElementById('lat').value = lat;
            document.getElementById('lon').value = lon;
            myMarker.bindPopup("Lat " + lat + "<br />Lon " + lon).openPopup();
        }

        function myFunction(arr) {
            var out = "<br />";
            var i;

            if (arr.length > 0) {
                for (i = 0; i < arr.length; i++) {
                out += "<div class='address' title='Show Location and Coordinates' onclick='chooseAddr(" + arr[i].lat + ", " + arr[i].lon + ");return false;'>" + arr[i].display_name + "</div>";
                }
                document.getElementById('results').innerHTML = out;
            } else {
                document.getElementById('results').innerHTML = "Sorry, no results...";
            }
        }
    }



    navigator.geolocation.getCurrentPosition(success, error);
</script>


<div class="modal fade" id="default-Modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Editar pagina</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
            </div>
        </div>
    </div>
</div>

<style>
    #map {
        width: 100%;
        height: 500px;
        /* padding: 0;
        margin: 0; */
        border:1px solid
    }
</style>