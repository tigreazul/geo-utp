@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Prospectos</h5>
                        <span>Administración de Prospectos</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class=" breadcrumb breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="#"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Dashboard</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- card -->
                    <div class="row">
                        <div class="card panel-modulo col " style="margin-right: 15px;">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col">
                                        <h5>Lista de Prospectos</h5>
                                    </div>
                                    <div class="col-42">
                                        <a href="{{ route('admin.prospecto_create') }}" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus-circle"></i> NUEVO PROSPECTO</a>
                                    </div>                                
                                </div>
                            </div>
                            <div class="card-block">
                                @if (Session::has('message'))
                                    <div class="alert alert-success background-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <i class="icofont icofont-close-line-circled"></i>
                                        </button>
                                        {!! session('message') !!}
                                    </div>
                                @endif
                                <div class="dt-responsive table-responsive">
                                    <!-- lista de tablas -->
                                    <table class="table table-hover m-b-0 table-sm">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nombre Completo</th>
                                                <th>Email</th>
                                                <th>Tipo Documento</th>
                                                <th>Numero Documento</th>
                                                <th>Validado</th>
                                                <!-- <th>Recarga</th> -->
                                                <th>Estado</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i = 1; @endphp
                                            @foreach($prospecto as $prospect)
                                                <tr class="table-verify selector-{{ $prospect->id }}">
                                                    <td scope="row">{{ $i }}</td>
                                                    <td>{{ $prospect->apellidoPaterno.' '.$prospect->apellidoMaterno.', '.$prospect->nombre }}</td>
                                                    <td>{{ $prospect->email }}</td>
                                                    <td>{{ $prospect->Descripcion }}</td>
                                                    <td>{{ $prospect->nro_documento }}</td>
                                                    <td>
                                                        <label class="label label-{{ ($prospect->validado == 1)? 'success': 'danger' }}">{{ ($prospect->validado == 1)? 'VALIDADO': 'PENDIENTE EN VALIDACIÓN' }}</label>
                                                    </td>
                                                    <!-- <td>
                                                        <label class="badge bg-{{ ($prospect->pendiente == 0)? 'success': 'danger' }}">{{$prospect->pendiente}}</label>
                                                        <label class="label label-{{ ($prospect->pendiente == 0)? 'success': 'danger' }}">{{ ($prospect->pendiente == 0)? 'VALIDADO': 'PENDIENTE EN VALIDACIÓN' }}</label>
                                                    </td> -->
                                                    <td><label class="label label-{{ ($prospect->estado == 1)? 'success': 'danger' }}">{{ ($prospect->estado == 1)? 'ACTIVO': 'DESACTIVADO' }} </label></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                Acciones
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                <a class="dropdown-item alert-delete-prospect" href="#" data-id="{{ $prospect->id }}" style="background: #ff5370;color: #fff;">
                                                                    <i class="fa fa-trash-alt"></i>
                                                                    Eliminar
                                                                </a>
                                                                <a class="dropdown-item" href="{{ route('admin.prospecto_edit', ['id'=>$prospect->id]) }}">
                                                                    <i class="fa fa-edit"></i>
                                                                    Editar
                                                                </a>
                                                                <!-- <a class="dropdown-item alert-recargar-prospecdt" href="{{ route('admin.prospecto_recarga',$prospect->id) }}" >
                                                                    <i class="fa fa-server"></i>
                                                                    Historial Recarga
                                                                </a>
                                                                <a class="dropdown-item" id="recargar-prospect" data-id="{{ $prospect->id }}" href="#" >
                                                                    <i class="fa fa-sync-alt"></i>
                                                                    Solicitud de Recarga
                                                                </a> -->
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @php $i++; @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <!-- lista de tablas -->
                                    <div class="table-responsive">
                                    </div>
                                    <div class="card-block p-b-0">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="md-overlay"></div>
                    <!-- card -->
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modal-solicitud" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Solicitud de recarga de saldo</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-4 col-xl-4 col-sm-12">
                            <div class="badge-box">
                                <div class="sub-title"><strong style="font-weight: bold;">Datos completos</strong></div>
                                <p class="datos_completos"></p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4 col-sm-12">
                            <div class="badge-box">
                                <div class="sub-title"><strong style="font-weight: bold;">Email</strong></div>
                                <p class="datos_email"></p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4 col-sm-12">
                            <div class="badge-box">
                                <div class="sub-title"><strong style="font-weight: bold;">Documento</strong></div>
                                <p class="datos_documento"></p>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row voucher">
                                <label class="col-sm-2 col-form-label">Monto</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="select-saldo" name="saldos" required>
                                        <option value="">[SELECCIONE]</option>
                                        @foreach($montos as $monto)
                                            <option value="{{ $monto->id }}">S/ {{ $monto->montos}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row voucher">
                                <label class="col-sm-2 col-form-label">Voucher</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" id="voucher" name="voucher" value="">
                                    <input type="hidden" class="codigo_recarga" name="codigo_recarga" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Cerrar</button>
                    <a href="#" class="btn btn-primary waves-effect waves-light" id="enviar-prospect"><i class="fa fa-save"></i> Enviar recarga</a>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(document).on('click','.alert-delete-prospect',function(e){
            e.preventDefault();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var id = $(this).attr('data-id');
            swal({
                title: "Eliminar?",
                text: "Esta seguro que desea eliminar el registro?",
                type: "error",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Eliminar",
                cancelButtonText: "Cancelar",
            }, 
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: local.base+'/admin/prospectos/delete/'+id,
                        type: 'post',
                        dataType: 'json',
                        data: {_token: CSRF_TOKEN},
                        success: function(data){
                            if(data.status == true){
                                swal("Eliminado", "Se ha eliminado correctamente.", "success");
                                $('.selector-'+id).remove();
                            }else{
                                // swal('Ocurrio un error vuelva a intentarlo');
                                swal("Eliminado", "Ocurrio un error vuelva a intentarlo", "error");
                            }
                        }
                    });
                }
            });
        });

        $(document).on('click','#recargar-prospect',function(e){
            e.preventDefault();
            let code = $(this).data('id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url     : "{{ route('admin.datos_personal') }}",
                type    : 'POST',
                dataType: 'JSON',
                data    : {id:code},
                success : function(data){
                    $('.datos_completos').text(data.apellidoPaterno + ' ' + data.apellidoMaterno + ' ' + data.nombre)
                    $('.datos_email').text(data.email)
                    $('.codigo_recarga').val(data.id)
                    $('.datos_documento').text(data.Descripcion + ' - ' + data.nro_documento)
                    $('#modal-solicitud').modal('show'); 
                },
                error   : function(jqxhr, textStatus, error){
                    console.log(jqxhr.responseText);
                }
            });
        });

        $(document).on('click','#enviar-prospect',function(e){
            e.preventDefault();
            let code    = $('.codigo_recarga').val();
            let saldos  = $('#select-saldo').val();
            let voucher = $('#voucher').prop('files')[0];

            var formData = new FormData();
            formData.append('saldo', saldos);
            formData.append('voucher', voucher);
            formData.append('code', code);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url     : "{{ route('admin.enviar_recarga') }}",
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                type: 'post',
                success : function(data){
                    $('.codigo_recarga').val('');
                    $('#select-saldo').val('');
                    $('#voucher').val('');
                    $('#modal-solicitud').modal('hide'); 
                    swal('Se envio la solicitud de recarga correctamente')
                },
                error   : function(jqxhr, textStatus, error){
                    console.log(jqxhr.responseText);
                }
            });
            return false;
        });

        

    </script>
@stop