<div class="row">

    <div class="col-sm-8">
        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Copia DNI</label>
            <div class="col-sm-9">
                <!-- {{ isset($titular->aDNI)?'':'required' }} -->
                <input type="file" class="btn btn-primary btn-sm" name="copiaDni" id="copiaDni"  >
                <br>
                @if(isset($titular->aDNI) && $titular->aDNI != '')
                    <span class="messages"><a href="{{ isset($titular->aDNI)?asset('upload/titular/'.$titular->aDNI):'' }}" class="btn btn-default" target="_blank"><i class="fa fa-file"></i> DNI</a></span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Ficha Padrón</label>
            <div class="col-sm-9">
                <input type="file" name="fichaPadron" id="fichaPadron" class="btn btn-primary btn-sm" >
                <br>
                @if(isset($titular->aExpediente) && $titular->aExpediente != '')
                    <span class="messages"><a href="{{ isset($titular->aExpediente)?asset('upload/titular/'.$titular->aExpediente):'' }}" class="btn btn-default" target="_blank"><i class="fa fa-file"></i> FICHA PADRÓN</a></span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Constancia de Viviencia</label>
            <div class="col-sm-9">
                <input type="file" name="constanciaVivencia" id="constanciaVivencia" class="btn btn-primary btn-sm" >
                <br>
                @if(isset($titular->constanciaVivencia) && $titular->constanciaVivencia != '')
                    <span class="messages"><a href="{{ isset($titular->constanciaVivencia)?asset('upload/titular/'.$titular->constanciaVivencia):'' }}" class="btn btn-default" target="_blank"><i class="fa fa-file"></i> CONSTANCIA VIVENCIA</a></span>
                @endif
            </div>
        </div>

        <div id="documento_adicional">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Carta de renuncia</label>
                <div class="col-sm-9">
                    <!-- {{ isset($titular->aDNI)?'':'required' }} -->
                    <input type="file" class="btn btn-primary btn-sm" name="cartaRenuncia" id="cartaRenuncia"  >
                    <br>
                    @if(isset($titular->aCartaRenuncia) && $titular->aCartaRenuncia != '')
                        <span class="messages"><a href="{{ isset($titular->aCartaRenuncia)?asset('upload/titular/'.$titular->aCartaRenuncia):'' }}" class="btn btn-default" target="_blank"><i class="fa fa-file"></i> CARTA DE RENUNCIA</a></span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Contrato de Transferencia</label>
                <div class="col-sm-9">
                    <!-- {{ isset($titular->aDNI)?'':'required' }} -->
                    <input type="file" class="btn btn-primary btn-sm" name="contratoTransferencia" id="contratoTransferencia"  >
                    <br>
                    @if(isset($titular->contratoTransferencia) && $titular->contratoTransferencia != '')
                        <span class="messages"><a href="{{ isset($titular->contratoTransferencia)?asset('upload/titular/'.$titular->contratoTransferencia):'' }}" class="btn btn-default" target="_blank"><i class="fa fa-file"></i> CONTRATO DE TRANSFERENCIA</a></span>
                    @endif
                </div>
            </div>
            
        </div>


        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Entrego Carnet</label>
            <div class="col-sm-9">
                <select class="form-control" id="carnet" name="ecarnet" required>
                    <option value="">[SELECCIONE]</option>
                    <option value="SI" {{ isset($titular->entregoCarnet)?($titular->entregoCarnet == 'SI'?'selected':''):''}} >SI</option>
                    <option value="NO" {{ isset($titular->entregoCarnet)?($titular->entregoCarnet == 'NO'?'selected':''):''}} >NO</option>
                </select>
                <span class="messages"></span>
            </div>
        </div>

        <div id="panel_adicional">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Codigo Tarjeta</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="ctarjeta" name="ctarjeta"  value="{{ isset($titular->codigoTarjeta)?$titular->codigoTarjeta:''}}">
                    <span class="messages"></span>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">ID EQUIPO</label>
                <div class="col-sm-9">
                    <input type="number" onKeyPress="return soloNumeros(event)" class="form-control" id="idEquipo" name="idEquipo"  value="{{ isset($titular->idEquipo)?$titular->idEquipo:''}}">
                    <span class="messages"></span>
                </div>
            </div>
        </div>
        

    </div>

</div>

<script type="text/javascript">

    $(document).ready(function(e){
        let cod = $('#carnet').val();
        console.log(cod);
        if(cod == 'SI'){
            $('#panel_adicional').show();
        }else{
            $('#panel_adicional').hide();
        }
    });

    $(document).on('change','#carnet',function(e){
        e.preventDefault();
        let cod = $(this).val();
        if(cod == 'SI'){
            $('#panel_adicional').show();
        }else{
            $('#panel_adicional').hide();
        }
    });
</script>