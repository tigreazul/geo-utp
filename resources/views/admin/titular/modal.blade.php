<div class="modal fade" id="default-Modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Datos del Subtitular</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="guardar-subtitular">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nombre</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nombre" id="Snombre" onkeypress="return soloLetras(event)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Apellido Paterno</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="apaterno" id="Sapaterno" onkeypress="return soloLetras(event)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Apellido Materno</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="amaterno" id="Samaterno" onkeypress="return soloLetras(event)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">DNI</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="dni" id="Sdni" onKeyPress="return soloNumeros(event)" maxlength="8" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Sexo</label>
                        <div class="col-sm-9">
                            <select name="sexo" class="form-control" id="Ssexo" required>
                                <option value="">[SELECCIONE]</option>
                                <option value="F">FEMENINO</option>
                                <option value="M">MASCULINO</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Instrucción</label>
                        <div class="col-sm-9">
                            <select name="grado" class="form-control" id="Sgrado" required>
                                <option value="">[SELECCIONE]</option>
                                @foreach($institucion as $inst)
                                    <option value="{{ $inst->codigo }}" >{{ $inst->valor }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Ocupación</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="ocupacion" id="Socupacion" onkeypress="return soloLetras(event)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Civil</label>
                        <div class="col-sm-9">
                            <select name="civil" class="form-control"  id="Scivils" required>
                                <option value="">[SELECCIONE]</option>
                                @foreach($civil as $civi)
                                    <option value="{{ $civi->codigo }}">{{ $civi->valor }}</option>
                                @endforeach
                            </select>
                            <span class="messages"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Relación</label>
                        <div class="col-sm-9">
                            <select name="relacion" class="form-control"  id="Se_page_estado" required>
                                <option value="">[SELECCIONE]</option>
                                @foreach($relacion as $rela)
                                    <option value="{{ $rela->codigo }}">{{ $rela->valor }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Telefono</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="telefono" id="Stelefono" onKeyPress="return soloNumeros(event)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Departamento</label>
                        <div class="col-sm-9">
                            <select name="dptoSub" class="form-control"  id="Sdep" required>
                                <option value="">[SELECCIONE]</option>
                                @foreach($departamento as $dep)
                                    <option value="{{ $dep->id }}" data-id="{{ $dep->id }}">{{ $dep->descripcion }}</option>
                                @endforeach
                            </select>
                            <span class="messages"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Provincia</label>
                        <div class="col-sm-9">
                            <select name="provSub" class="form-control"  id="Sprov" required>
                                <option value="">[SELECCIONE]</option>
                            </select>
                            <span class="messages"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Distrito</label>
                        <div class="col-sm-9">
                            <select name="distSub" class="form-control"  id="Sdist" required>
                                <option value="">[SELECCIONE]</option>
                            </select>
                            <span class="messages"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect" id="btnSub">Guardar</button>
                    <button type="button" class="btn btn-default waves-effect md-close" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>


<div class="modal fade" id="modal-habitan" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">DATOS DE PERSONAS QUE HABITEN HOGAR</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="guardar-habitan">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nombre</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nombre" id="Hnombre" onkeypress="return soloLetras(event)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Apellido Paterno</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="apaterno" id="Hapaterno" onkeypress="return soloLetras(event)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Apellido Materno</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="amaterno" id="Hamaterno" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">DNI</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="dni" id="Hdni" onKeyPress="return soloNumeros(event)"  maxlength="8"  required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Sexo</label>
                        <div class="col-sm-9">
                            <select name="sexo" class="form-control" id="Hsexo" required>
                                <option value="">[SELECCIONE]</option>
                                <option value="F">FEMENINO</option>
                                <option value="M">MASCULINO</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Instrucción</label>
                        <div class="col-sm-9">
                            <select name="grado" class="form-control" id="Hgrado" required>
                                <option value="">[SELECCIONE]</option>
                                @foreach($institucion as $inst)
                                    <option value="{{ $inst->codigo }}" >{{ $inst->valor }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Relación</label>
                        <div class="col-sm-9">
                            <select name="relacion" class="form-control" id="He_page_estado" required>
                                <option value="">[SELECCIONE]</option>
                                @foreach($relacion as $rela)
                                    <option value="{{ $rela->codigo }}">{{ $rela->valor }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Departamento</label>
                        <div class="col-sm-9">
                            <select name="dptoSub" class="form-control" id="HdepH" required>
                            HprovH
                                <option value="">[SELECCIONE]</option>
                                @foreach($departamento as $dep)
                                    <option value="{{ $dep->id }}" data-id="{{ $dep->id }}">{{ $dep->descripcion }}</option>
                                @endforeach
                            </select>
                            <span class="messages"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Provincia</label>
                        <div class="col-sm-9">
                            <select name="provSub" class="form-control" id="HprovH" required>
                                <option value="">[SELECCIONE]</option>
                            </select>
                            <span class="messages"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Distrito</label>
                        <div class="col-sm-9">
                            <select name="distSub" class="form-control" id="HdistH" required>
                                <option value="">[SELECCIONE]</option>
                            </select>
                            <span class="messages"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect" id="btnHabita">Guardar</button>
                    <button type="button" class="btn btn-default waves-effect md-close" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>



<div class="modal fade" id="modal-hijos" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">DATOS DE LOS HIJOS QUE VIVEN EN EL HOGAR</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="guardar-subtitular">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nombre</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nombre" required onkeypress="return soloLetras(event)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Apellido Paterno</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="apaterno" required onkeypress="return soloLetras(event)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Apellido Materno</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="amaterno" required onkeypress="return soloLetras(event)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">DNI</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="dni" onKeyPress="return soloNumeros(event)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Sexo</label>
                        <div class="col-sm-9">
                            <select name="sexo" class="form-control" id="sexo" required>
                                <option value="">[SELECCIONE]</option>
                                <option value="F">FEMENINO</option>
                                <option value="M">MASCULINO</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Instrucción</label>
                        <div class="col-sm-9">
                            <select name="grado" class="form-control" id="grado" required>
                                <option value="">[SELECCIONE]</option>
                                @foreach($institucion as $inst)
                                    <option value="{{ $inst->codigo }}" >{{ $inst->valor }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Ocupación</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="ocupacion" onkeypress="return soloLetras(event)" required >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Civil</label>
                        <div class="col-sm-9">
                            <select name="civil" class="form-control" id="civils" required>
                                <option value="">[SELECCIONE]</option>
                                @foreach($civil as $civi)
                                    <option value="{{ $civi->codigo }}">{{ $civi->valor }}</option>
                                @endforeach
                            </select>
                            <span class="messages"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Telefono</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="telefono" required onKeyPress="return soloNumeros(event)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Departamento</label>
                        <div class="col-sm-9">
                            <select name="dptoSub" class="form-control" id="dep" required>
                                <option value="">[SELECCIONE]</option>
                                @foreach($departamento as $dep)
                                    <option value="{{ $dep->id }}" data-id="{{ $dep->id }}">{{ $dep->descripcion }}</option>
                                @endforeach
                            </select>
                            <span class="messages"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Provincia</label>
                        <div class="col-sm-9">
                            <select name="provSub" class="form-control" id="prov" required>
                                <option value="">[SELECCIONE]</option>
                            </select>
                            <span class="messages"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Distrito</label>
                        <div class="col-sm-9">
                            <select name="distSub" class="form-control" id="dist" required>
                                <option value="">[SELECCIONE]</option>
                            </select>
                            <span class="messages"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">Guardar</button>
                    <button type="button" class="btn btn-default waves-effect md-close" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>


<script>
    $(document).on('submit','#guardar-subtitular',function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var serial = $(this).serialize();
        let idTitu = $('#idTitu').val();

        $.ajax({
            url: "{{ route('admin.subtitular_add')}}",
            method:'post',
            dataType: 'json',
            data: serial+"&idTitular="+idTitu,
            beforeSend:function(){
                $('.loader-cards').parents('.card').addClass("card-load");
                $('.loader-cards').parents('.card').append('<div class="card-loader"><i class="feather icon-radio rotate-refresh"></div>');
            },
            success: function(data){
                console.log(data.status);
                if(data.status == true){
                    $('#subtitular-body-table').html("");
                    $('#default-Modal').modal('hide');
                    let vhtml = "";
                    let i = 0;
                    $( data.lst ).each(function( index, element ){
                        i++;
                        vhtml += '<tr><td>'+i+'</td>';
                        vhtml += '<td>'+element.nombre+'</td>';
                        vhtml += '<td>'+element.apellidoPaterno+'</td>';
                        vhtml += '<td>'+element.dni+'</td>';
                        vhtml += '<td>'+element.ocupacion+'</td>';
                        vhtml += '<td>'+element.departamento+' / '+element.provincia+' / '+element.distrito+'</td>';
                        vhtml += '<td><div class="btn-group">';
                        vhtml += '<button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Acciones</button>';
                        vhtml += '<div class="dropdown-menu">'
                        vhtml += '<a class="dropdown-item editSubtitular" href="#" data-id="'+element.idSubtitular+'">';
                        vhtml += '<i class="fa fa-edit"></i>Editar</a>';
                        vhtml += '</div></div></td>';
                        vhtml += '<tr>';
                        // sub->departamento.' / '.$sub->provincia.' / '.$sub->distrito
                    });

                    $('#subtitular-body-table').html(vhtml);
                }
            },
            complete: function(){
                $('.loader-cards').parents('.card').children(".card-loader").remove();
                $('.loader-cards').parents('.card').removeClass("card-load");
            }
        });
        $('#guardar-subtitular')[0].reset();
        return false;
    });

    $(document).on('submit','#editar-subtitular',function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var serial = $(this).serialize();
        let idTitu = $('#idTitu').val();
        let id = $(this).data('id');
        $.ajax({
            url: "{{ route('admin.subtitular_update')}}",
            method:'patch',
            dataType: 'json',
            data: {idTitular:idTitu,id:id},
            data: serial+"&idTitular="+idTitu+"&id="+id,
            beforeSend:function(){
                $('.loader-cards').parents('.card').addClass("card-load");
                $('.loader-cards').parents('.card').append('<div class="card-loader"><i class="feather icon-radio rotate-refresh"></div>');
            },
            success: function(data){
                console.log(data.status);
                if(data.status == true){
                    $('#subtitular-body-table').html("");
                    $('#default-Modal').modal('hide');
                    let vhtml = "";
                    let i = 0;
                    $( data.lst ).each(function( index, element ){
                        i++;
                        vhtml += '<tr><td>'+i+'</td>';
                        vhtml += '<td>'+element.nombre+'</td>';
                        vhtml += '<td>'+element.apellidoPaterno+'</td>';
                        vhtml += '<td>'+element.dni+'</td>';
                        vhtml += '<td>'+element.ocupacion+'</td>';
                        vhtml += '<td>'+element.departamento+' / '+element.provincia+' / '+element.distrito+'</td>';
                        vhtml += '<td><div class="btn-group">';
                        vhtml += '<button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Acciones</button>';
                        vhtml += '<div class="dropdown-menu">'
                        vhtml += '<a class="dropdown-item editSubtitular" href="#" data-id="'+element.idSubtitular+'">';
                        vhtml += '<i class="fa fa-edit"></i>Editar</a>';
                        vhtml += '</div></div></td>';
                        vhtml += '<tr>';
                        // sub->departamento.' / '.$sub->provincia.' / '.$sub->distrito
                    });

                    $('#subtitular-body-table').html(vhtml);
                }
            },
            complete: function(){
                $('.loader-cards').parents('.card').children(".card-loader").remove();
                $('.loader-cards').parents('.card').removeClass("card-load");
            }
        });
        $('#guardar-subtitular')[0].reset();
        return false;
    });
    

    $(document).on('click','.editSubtitular',function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var serial = $(this).serialize();
        let idTitu = $('#idTitu').val();
        let id = $(this).data('id');
        $.ajax({
            url: "{{ route('admin.subtitular_view')}}",
            method:'post',
            dataType: 'json',
            data: {id:id},
            beforeSend:function(){
                $('.loader-cards').parents('.card').addClass("card-load");
                $('.loader-cards').parents('.card').append('<div class="card-loader"><i class="feather icon-radio rotate-refresh"></div>');
            },
            success: function(data){
                if(data.status == true){
                    
                    
                    let vprov = '';
                    vprov += '<option value="">[SELECCIONE]</option>';
                    $( data.provincia ).each(function( index, ele ){
                        vprov += '<option value="'+ele.provID+'">';
                        vprov += ele.descripcion;
                        vprov += '</option>';
                    });

                    $('#Sprov').html(vprov);


                    let vdist = '';
                    vdist += '<option value="">[SELECCIONE]</option>';
                    $( data.distrito ).each(function( index, ele ){
                        vdist += '<option value="'+ele.distID+'">';
                        vdist += ele.descripcion;
                        vdist += '</option>';
                    });

                    $('#Sdist').html(vdist);



                    $('#default-Modal').modal('show');
                    $('#default-Modal').find('#guardar-subtitular').removeAttr('id');
                    $('#default-Modal').find('form').attr('id','editar-subtitular');
                    $('#default-Modal').find('form').attr('data-id',id);
                    
                    let i = 0;
                    $('#Snombre').val(data.lst.nombre);
                    $('#Sapaterno').val(data.lst.apellidoPaterno);
                    $('#Samaterno').val(data.lst.apellidoMaterno);
                    $('#Sdni').val(data.lst.dni);
                    $('#Ssexo').val(data.lst.sexo);
                    $('#Sgrado').val(data.lst.idInstruccion);
                    $('#Socupacion').val(data.lst.ocupacion);
                    $('#Scivils').val(data.lst.idCivil);
                    $('#Se_page_estado').val(data.lst.idRelacion);
                    $('#Stelefono').val(data.lst.telefono);

                    $('#Sdep').val(data.lst.idUbigeo.split('.')[0]);

                    $('#Sprov').val(data.lst.idUbigeo.split('.')[1]);
                    $('#Sdist').val(data.lst.idUbigeo.split('.')[2]);

                    $('#btnSub').text('Editar');
                    $('#btnSub').attr('data-id',id);
                    $('#btnSub').addClass('.editarSubtitular');
                    // console.log(data.lst);
                }
            },
            complete: function(){
                $('.loader-cards').parents('.card').children(".card-loader").remove();
                $('.loader-cards').parents('.card').removeClass("card-load");
            }
        });
        // $('#guardar-subtitular')[0].reset();
        return false;
    });

    // --------------------------------------------------------
    $(document).on('submit','#guardar-habitan',function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var serial = $(this).serialize();
        let idTitu = $('#idTitu').val();

        $.ajax({
            url: "{{ route('admin.habitan_add')}}",
            method:'post',
            dataType: 'json',
            data: serial+"&idTitular="+idTitu,
            beforeSend:function(){
                $('.loader-cards').parents('.card').addClass("card-load");
                $('.loader-cards').parents('.card').append('<div class="card-loader"><i class="feather icon-radio rotate-refresh"></div>');
            },
            success: function(data){
                console.log(data.status);
                if(data.status == true){
                    $('#habitan-body-table').html("");
                    $('#modal-habitan').modal('hide');
                    let vhtml = "";
                    let i = 0;
                    $( data.lst ).each(function( index, element ){
                        i++;
                        vhtml += '<tr><td>'+i+'</td>';
                        vhtml += '<td>'+element.nombre+'</td>';
                        vhtml += '<td>'+element.apellidoPaterno+'</td>';
                        vhtml += '<td>'+element.dni+'</td>';
                        vhtml += '<td>'+element.relacion+'</td>';
                        vhtml += '<td><div class="btn-group">';
                        vhtml += '<button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Acciones</button>';
                        vhtml += '<div class="dropdown-menu">'
                        vhtml += '<a class="dropdown-item editHabitan" href="" data-id="'+element.idMiembro+'">';
                        vhtml += '<i class="fa fa-edit"></i>Editar</a>';
                        vhtml += '</div></div></td>';
                        vhtml += '<tr>';
                    });

                    $('#habitan-body-table').html(vhtml);
                }
            },
            complete: function(){
                $('.loader-cards').parents('.card').children(".card-loader").remove();
                $('.loader-cards').parents('.card').removeClass("card-load");
            }
        });
        $('#guardar-habitan')[0].reset();
        return false;
    });

    $(document).on('submit','#editar-habitan',function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var serial = $(this).serialize();
        let idTitu = $('#idTitu').val();
        let id = $(this).data('id');
        $.ajax({
            url: "{{ route('admin.habitan_update')}}",
            method:'patch',
            dataType: 'json',
            data: {idTitular:idTitu,id:id},
            data: serial+"&idTitular="+idTitu+"&id="+id,
            beforeSend:function(){
                $('.loader-cards').parents('.card').addClass("card-load");
                $('.loader-cards').parents('.card').append('<div class="card-loader"><i class="feather icon-radio rotate-refresh"></div>');
            },
            success: function(data){
                console.log(data.status);
                if(data.status == true){
                    $('#habitan-body-table').html("");
                    $('#modal-habitan').modal('hide');
                    let vhtml = "";
                    let i = 0;
                    $( data.lst ).each(function( index, element ){
                        i++;
                        vhtml += '<tr><td>'+i+'</td>';
                        vhtml += '<td>'+element.nombre+'</td>';
                        vhtml += '<td>'+element.apellidoPaterno+'</td>';
                        vhtml += '<td>'+element.dni+'</td>';
                        vhtml += '<td>'+element.relacion+'</td>';
                        vhtml += '<td><div class="btn-group">';
                        vhtml += '<button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Acciones</button>';
                        vhtml += '<div class="dropdown-menu">'
                        vhtml += '<a class="dropdown-item editHabitan" href="" data-id="'+element.idMiembro+'">';
                        vhtml += '<i class="fa fa-edit"></i>Editar</a>';
                        vhtml += '</div></div></td>';
                        vhtml += '<tr>';
                    });
                    $('#habitan-body-table').html(vhtml);
                }
            },
            complete: function(){
                $('.loader-cards').parents('.card').children(".card-loader").remove();
                $('.loader-cards').parents('.card').removeClass("card-load");
            }
        });
        $('#editar-habitan')[0].reset();
        return false;
    });
    

    $(document).on('click','.editHabitan',function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var serial = $(this).serialize();
        let idTitu = $('#idTitu').val();
        let id = $(this).data('id');
        $.ajax({
            url: "{{ route('admin.habitan_view')}}",
            method:'post',
            dataType: 'json',
            data: {id:id},
            beforeSend:function(){
                $('.loader-cards').parents('.card').addClass("card-load");
                $('.loader-cards').parents('.card').append('<div class="card-loader"><i class="feather icon-radio rotate-refresh"></div>');
            },
            success: function(data){
                if(data.status == true){
                    console.log(data);
                    let vprov = '';
                    vprov += '<option value="">[SELECCIONE]</option>';
                    $( data.provincia ).each(function( index, ele ){
                        vprov += '<option value="'+ele.provID+'">';
                        vprov += ele.descripcion;
                        vprov += '</option>';
                    });

                    $('#HprovH').html(vprov);


                    let vdist = '';
                    vdist += '<option value="">[SELECCIONE]</option>';
                    $( data.distrito ).each(function( index, ele ){
                        vdist += '<option value="'+ele.distID+'">';
                        vdist += ele.descripcion;
                        vdist += '</option>';
                    });

                    $('#HdistH').html(vdist);
                    
                    $('#modal-habitan').find('#guardar-habitan').removeAttr('id');
                    $('#modal-habitan').find('form').attr('id','editar-habitan');
                    $('#modal-habitan').find('form').attr('data-id',id);
                    
                    $('#Hnombre').val(data.lst.nombre);
                    $('#Hapaterno').val(data.lst.apellidoPaterno);
                    $('#Hamaterno').val(data.lst.apellidoMaterno);
                    $('#Hdni').val(data.lst.dni);
                    $('#Hsexo').val(data.lst.sexo);
                    $('#Hgrado').val(data.lst.idInstruccion);
                    $('#He_page_estado').val(data.lst.idRelacion);
                    // $('#HdepH').val(data.lst.idUbigeo.split('.')[0]);

                    $('#HdepH').val(data.lst.idUbigeo.split('.')[0]);
                    $('#HprovH').val(data.lst.idUbigeo.split('.')[1]);
                    $('#HdistH').val(data.lst.idUbigeo.split('.')[2]);


                    $('#btnHabita').text('Editar');
                    $('#btnHabita').attr('data-id',id);
                    $('#btnHabita').addClass('.editarSubtitular');
                    // console.log(data.lst);
                    $('#modal-habitan').modal('show');
                }
            },
            complete: function(){
                $('.loader-cards').parents('.card').children(".card-loader").remove();
                $('.loader-cards').parents('.card').removeClass("card-load");
            }
        });
        // $('#editar-habitan')[0].reset();
        return false;
    });
</script>