@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Solicitudes</h5>
                        <span>Administración de Solicitudes</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class=" breadcrumb breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="#"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Dashboard</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">

                        <div class="col-sm-{{ ($prospecto->compra_saldo == 1)? '8': '12' }}">
                            <div class="card">
                                <div class="card-header">
                                    <h5>FICHA DEL PROSPECTO</h5>
                                    <label class="label label-{{ ($prospecto->validado == 1)? 'success': 'danger' }}">{{ ($prospecto->validado == 1)? 'VALIDADO': ( ($prospecto->validado == 2) ? 'SOLICITUD RECHAZADO':'PENDIENTE EN VALIDACIÓN') }}</label>
                                </div>
                                <div class="card-block" style="">
                                    
                                    <div class="row">
                                        <div class="col-lg-4 col-xl-3 col-sm-12">
                                            <div class="badge-box">
                                                <div class="sub-title">Datos completos</div>
                                                <input type="hidden" value="{{$prospecto->id}}" id="id_prospecto">
                                                <input type="hidden" value="{{ route('admin.estado_pendientes', ['id'=>$prospecto->id]) }}" id="url_proceso">
                                                <p> {{ $prospecto->apellidoPaterno.' '.$prospecto->apellidoMaterno.' '.$prospecto->nombre}} </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-xl-3 col-sm-6">
                                            <div class="badge-box">
                                                <div class="sub-title">Email</div>
                                                <p> {{ $prospecto->email }} </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-xl-3 col-sm-6">
                                            <div class="badge-box">
                                                <div class="sub-title">Documento</div>
                                                <p> {{ $prospecto->Descripcion }} - {{ $prospecto->nro_documento }} </p>
                                            </div>
                                        </div>
                                        <!-- <div class="col-lg-4 col-xl-3 col-sm-6">
                                            <div class="badge-box">
                                                <div class="sub-title">Compra de saldo</div>
                                                <p> {{ $prospecto->compra_saldo == 1 ? 'SI':'NO' }} </p>
                                            </div>
                                        </div> -->
                                        <!-- <div class="col-lg-4 col-xl-3 col-sm-6">
                                            <div class="badge-box">
                                                <div class="sub-title">Validación</div>
                                                <p><label class="label label-{{ ($prospecto->validado == 1)? 'success': 'danger' }}">{{ ($prospecto->validado == 1)? 'VALIDADO': ( ($prospecto->validado == 2) ? 'SOLICITUD RECHAZADO':'PENDIENTE EN VALIDACIÓN') }}</label></p>
                                            </div>
                                        </div> -->
                                        @if($prospecto->compra_saldo == 1)
                                            <!-- <div class="col-lg-4 col-xl-3 col-sm-6">
                                                <div class="badge-box">
                                                    <div class="sub-title">Estado Voucher</div>
                                                    <p><label class="label label-{{ ($prospecto->estado_voucher == 1)? 'success': 'danger' }}">{{ ($prospecto->estado_voucher == 1)? 'VOUCHER APROBADO Y PAGADO': ( ($prospecto->estado_voucher == 2) ? 'VOUCHER RECHAZADO':'PENDIENTE EN VALIDACIÓN') }}</label></p>
                                                </div>
                                            </div> -->
                                        @endif
                                    </div>

                                </div>
                                <div class="card-block">
                                    @if($prospecto->validado == 0)
                                        <button class="btn btn-sm btn-primary waves-effect waves-light solicitudes_estado" data-id="1" data-estado="solicitud">APROBAR SOLICITUD</button>
                                        <button class="btn btn-sm btn-danger waves-effect waves-light solicitudes_estado"  data-id="2" data-estado="solicitud">RECHAZAR SOLICITUD</button>
                                    @endif

                                    @if($prospecto->estado_voucher == 0 && $prospecto->compra_saldo == 1 && ($prospecto->validado == 1 || $prospecto->validado == 2) )
                                        <!-- <button class="btn btn-sm btn-success waves-effect waves-light solicitudes_estado" data-id="1" data-estado="voucher">APROBAR PAGO VOUCHER</button>
                                        <button class="btn btn-sm btn-danger waves-effect waves-light solicitudes_estado"  data-id="2" data-estado="voucher">RECHAZAR VOUCHER</button> -->
                                    @endif
                                    <a href="javascript:history.back()" class="btn btn-sm btn-warning waves-effect waves-light">VOLVER</a>
                                </div>

                            </div>
                        </div>


                        <!-- <div class="col-sm-4" style="display:{{ ($prospecto->compra_saldo == 1)? 'block': 'none' }}">
                            <div class="card">
                                <div class="card-block" style="">
                                    <div class="row">
                                        <img src="{{ isset($prospecto->voucher)?asset('voucher/'.$prospecto->voucher):'' }}" class="rounded" style="width:100%">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-block" style="">
                                    <div class="dt-responsive table-responsive">
                                        <table class="table table-hover m-b-0 table-sm">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Monto</th>
                                                    <th>Voucher</th>
                                                    <th>Validado</th>
                                                    <th>Acción</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php $i = 1; $valida = 0; $pendientes=0; @endphp
                                                @foreach($historial as $history)
                                                    <tr class="table-verify selector-{{ $history->id }}">
                                                        <td scope="row">{{ $i }}</td>
                                                        <td>S/. {{ $history->montos }}</td>
                                                        <td>
                                                            @if($history->voucher != 'LIBRE')
                                                                <a href="{{ asset('voucher/'.$history->voucher) }}" target="_blank">
                                                                    <img src="{{ isset($history->voucher)?asset('voucher/'.$history->voucher):'' }}" class="rounded img-60" >
                                                                </a>
                                                            @else
                                                                <label class="label label-success">LIBRE</label>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <label class="label label-{{ ($history->validado == 1)? 'success': 'danger' }}">{{ ($history->validado == 1)? 'VALIDADO': (($history->validado == 2)? 'RECHAZADO': 'PENDIENTE EN VALIDACIÓN') }}</label>
                                                        </td>
                                                        <td>
                                                            @if($history->validado == 0)
                                                                <button class="btn btn-sm btn-success waves-effect waves-light solicitudes_estado" data-id="1" data-code="{{$history->id}}" data-estado="voucher">APROBAR PAGO VOUCHER</button>
                                                                <button class="btn btn-sm btn-danger waves-effect waves-light solicitudes_estado"  data-id="2" data-code="{{$history->id}}" data-estado="voucher">RECHAZAR VOUCHER</button>
                                                            @endif
                                                        </td>
                                                    </tr>

                                                    @if($history->validado == 1)
                                                        @php $valida = $valida + $history->montos; @endphp
                                                    @else
                                                        @php $pendientes = $pendientes + $history->montos; @endphp
                                                    @endif

                                                    @php $i++; @endphp
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="btn btn-success waves-effect waves-light">
                                        <strong>VALIDADO </strong> <span class="badge">S/. {{$valida}}</span>
                                    </div>
                                    <div class="btn btn-danger waves-effect waves-light">
                                        <strong>PENDIENTE </strong> <span class="badge">S/. {{$pendientes}}</span>
                                    </div>
                                </div>
                            </div>
                        </div> -->


                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/prospecto.js') }}"></script>
@stop