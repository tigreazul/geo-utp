@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Prospectos</h5>
                        <span>Administración de Prospectos</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class=" breadcrumb breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="#"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Dashboard</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- card -->
                    <div class="row">
                        <div class="card panel-modulo col " style="margin-right: 15px;">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col">
                                        <h5>Lista de Prospectos</h5>
                                    </div>
                                    <!-- <div class="col-42">
                                        <a href="{{ route('admin.prospecto_create') }}" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus-circle"></i> NUEVO PROSPECTO</a>
                                    </div> -->
                                </div>
                            </div>
                            <div class="card-block">
                                @if (Session::has('message'))
                                    <div class="alert alert-success background-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <i class="icofont icofont-close-line-circled"></i>
                                        </button>
                                        {!! session('message') !!}
                                    </div>
                                @endif
                                <div class="dt-responsive table-responsive">
                                    <!-- lista de tablas -->
                                    <table class="table table-hover m-b-0 table-sm">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nombre Completo</th>
                                                <th>Email</th>
                                                <th>Tipo Documento</th>
                                                <th>Numero Documento</th>
                                                <th>Validado</th>
                                                <!-- <th>Recargas</th> -->
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i = 1; @endphp
                                            @foreach($prospecto as $prospect)
                                                <tr class="table-verify selector-{{ $prospect->id }}">
                                                    <td scope="row">{{ $i }}</td>
                                                    <td>{{ $prospect->apellidoPaterno.' '.$prospect->apellidoMaterno.', '.$prospect->nombre }}</td>
                                                    <td>{{ $prospect->email }}</td>
                                                    <td>{{ $prospect->Descripcion }}</td>
                                                    <td>{{ $prospect->nro_documento }}</td>
                                                    <td>
                                                        <label class="label label-{{ ($prospect->validado == 1)? 'success': 'danger' }}">{{ ($prospect->validado == 1)? 'VALIDADO': 'PENDIENTE EN VALIDACIÓN' }}</label>
                                                    </td>
                                                    <!-- <td>
                                                        <label class="badge bg-{{ ($prospect->pendiente == 0)? 'success': 'danger' }}">{{$prospect->pendiente}}</label>
                                                        <label class="label label-{{ ($prospect->pendiente == 0)? 'success': 'danger' }}">{{ ($prospect->pendiente == 0)? 'VALIDADO': 'PENDIENTE EN VALIDACIÓN' }}</label>
                                                    </td> -->
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                Acciones
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                <a class="dropdown-item" href="{{ route('admin.ver_pendientes', ['id'=>$prospect->id]) }}">
                                                                    <i class="fa fa-edit"></i>
                                                                    Ver Registro
                                                                </a>
                                                                <!-- <a class="dropdown-item alert-delete-prospect" href="#" data-id="{{ $prospect->id }}" >
                                                                    <i class="fa fa-trash-alt"></i>
                                                                    Rechazar
                                                                </a> -->
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @php $i++; @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <!-- lista de tablas -->
                                    <div class="table-responsive">
                                    </div>
                                    <div class="card-block p-b-0">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="md-overlay"></div>
                    <!-- card -->
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).on('click','.alert-delete-prospect',function(e){
            e.preventDefault();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var id = $(this).attr('data-id');
            swal({
                title: "Eliminar?",
                text: "Esta seguro que desea eliminar el registro?",
                type: "error",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Eliminar",
                cancelButtonText: "Cancelar",
            }, 
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: local.base+'/admin/prospectos/delete/'+id,
                        type: 'post',
                        dataType: 'json',
                        data: {_token: CSRF_TOKEN},
                        success: function(data){
                            if(data.status == true){
                                swal("Eliminado", "Se ha eliminado correctamente.", "success");
                                $('.selector-'+id).remove();
                            }else{
                                // swal('Ocurrio un error vuelva a intentarlo');
                                swal("Eliminado", "Ocurrio un error vuelva a intentarlo", "error");
                            }
                        }
                    });
                }
            });
        });
    </script>
@stop