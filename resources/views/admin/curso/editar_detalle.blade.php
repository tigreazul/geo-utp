@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Grupo de Temas</h5>
                        <span>Creación Grupo</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- card -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Registro de Grupo</h5>
                            <!-- <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
                        </div>
                        <div class="card-block">
                            <form id="main" method="POST" action="{{ route('admin.detalle_curso_edit_data',['id' => $detalleCurso->idDetalleCurso]) }}" >
                                @csrf
                                @method('PATCH')
                                <div class="col-lg-6 col-xl-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Grupo</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="grupo" name="grupo" required value="{{ isset($detalleCurso->titulo)?$detalleCurso->titulo:'' }}">
                                                    <input type="hidden" id="idcurso" name="idcurso" value="{{ $curso }}">
                                                    <span class="messages"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Estado</label>
                                                <div class="col-sm-10">
                                                    <select name="estado" class="form-control" id="estado">
                                                        <option value="1" {{ isset($detalleCurso->estado)?( $detalleCurso->estado == '1'?'selected':'' ):'' }} >ACTIVO</option>
                                                        <option value="2" {{ isset($detalleCurso->estado)?( $detalleCurso->estado == '2'?'selected':'' ):'' }} >DESACTIVADO</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <hr>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-sm btn-primary m-b-0">
                                            <i class="fa fa-save"></i> Editar
                                        </button>
                                        <a href="{{ route('admin.detalle_curso_list', ['id'=> $curso] ) }}" class="btn waves-effect waves-light btn-inverse btn-outline-inverse btn-sm m-b-0"><i class="fa fa-reply"></i> Volver</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

@stop