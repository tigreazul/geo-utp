@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Temas</h5>
                        <span>Creación Temas</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- card -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Registro de Temas</h5>
                            <!-- <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
                        </div>
                        <div class="card-block">
                            <form id="main" method="POST" action="{{ route('admin.tema_curso_add') }}" >
                                @csrf
                                <div class="col-lg-6 col-xl-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Titulo</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="titulo" name="titulo" required onkeypress="return soloLetras(event)">
                                                    <input type="hidden" id="idDetalleCurso" name="idDetalleCurso" value="{{ $idDetalleCurso }}">
                                                    <span class="messages"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Video</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="video" name="video" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Duración</label>
                                                <div class="col-sm-10">
                                                    <input type="time" class="form-control" id="duracion" name="duracion" required onkeypress="return soloLetras(event)">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Privado</label>
                                                <div class="col-sm-10">
                                                    <select name="privado" class="form-control" id="privado">
                                                        <option value="1">SI</option>
                                                        <option value="2">NO</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Estado</label>
                                                <div class="col-sm-10">
                                                    <select name="estado" class="form-control" id="estado">
                                                        <option value="1">ACTIVO</option>
                                                        <option value="2">DESACTIVADO</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Descripción</label>
                                                <div class="col-sm-10">
                                                    <textarea id="editor1" name="descripcion" class="form-control"></textarea>
                                                </div>
                                            </div>
                                            
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <hr>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-sm btn-primary m-b-0">
                                            <i class="fa fa-save"></i> Registrar
                                        </button>
                                        <a href="{{ route('admin.tema_curso_list', ['id'=>$idDetalleCurso] ) }}" class="btn waves-effect waves-light btn-inverse btn-outline-inverse btn-sm m-b-0"><i class="fa fa-reply"></i> Volver</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <script src="//cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'editor1' );
    </script>

@stop