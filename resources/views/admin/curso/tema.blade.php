@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5> <span style="font-weight: 900;"> {{ strtoupper($detcurso->titulo) }} </span> </h5>
                        Temas 
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class=" breadcrumb breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="#"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Lista temas</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- card -->
                    <div class="row">
                        <div class="card panel-modulo col " style="margin-right: 15px;">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col">
                                        <h5>Lista de Temas</h5>
                                    </div>
                                    <div class="col-42">
                                        <a href="{{ route('admin.detalle_curso_list', ['id'=>$detcurso->idCurso]) }}" class="btn btn-info btn-sm waves-effect waves-light"><i class="fa fa-plus-circle"></i> VOLVER</a>
                                        <a href="{{ route('admin.tema_curso_create', ['id'=>$detcurso->idDetalleCurso]) }}" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus-circle"></i> NUEVO TEMAS</a>
                                    </div>                                
                                </div>
                            </div>
                            <div class="card-block">
                                @if (Session::has('message'))
                                    <div class="alert alert-success background-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <i class="icofont icofont-close-line-circled"></i>
                                        </button>
                                        {!! session('message') !!}
                                    </div>
                                @endif
                                <div class="dt-responsive table-responsive">
                                    <div class="card-block p-b-0">
                                        <div class="table-responsive">
                                            <!-- lista de tablas -->
                                            <table class="table table-hover m-b-0">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Titulo</th>
                                                        <th>Duración</th>
                                                        <th>Privado</th>
                                                        <th>Estado</th>
                                                        <th>Acción</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                    @php $i = 1; @endphp
                                                    @foreach($tema as $temas)
                                                        <tr class="table-verify selector-{{ $temas->idTema }}">
                                                            <td>{{ $i }}</td>
                                                            <td>{{ $temas->Titulo }}</td>
                                                            <td>{{ $temas->Duracion }}</td>
                                                            <td><label class="label label-{{ ($temas->Privado == 1)? 'info': 'danger' }}">{{ ($temas->Privado == 1)? 'SI': 'NO' }}</label></td>
                                                            <td><label class="label label-{{ ($temas->Estado == 1)? 'success': 'danger' }}">{{ ($temas->Estado == 1)? 'ACTIVO': 'DESACTIVADO' }}</label></td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        Acciones
                                                                    </button>
                                                                    <div class="dropdown-menu">
                                                                        <a class="dropdown-item" href="{{ route('admin.tema_curso_edit', ['id'=>$temas->idTema, 'detalle' => $detcurso->idDetalleCurso ]) }}">
                                                                            <i class="fa fa-edit"></i>
                                                                            Editar
                                                                        </a>
                                                                        <a class="dropdown-item alert-delete" href="#" data-id="{{ $temas->idTema }}" >
                                                                            <i class="fa fa-trash-alt"></i>
                                                                            Eliminar
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @php $i++; @endphp
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <!-- lista de tablas -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="card panel-pagina col" style="display:none">
                        <!-- <div class="card panel-pagina col" style=""> -->
                            @include('admin.configuracion.pagina')
                        </div>


                    </div>
                    <div class="md-overlay"></div>
                    <!-- card -->
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).on('click','.alert-delete',function(e){
            e.preventDefault();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var id = $(this).attr('data-id');
            swal({
                title: "Eliminar?",
                text: "Esta seguro que desea eliminar el registro?",
                type: "error",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Eliminar",
                cancelButtonText: "Cancelar",
            }, 
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: local.base+'/admin/curso/tema/delete/'+id,
                        type: 'post',
                        dataType: 'json',
                        data: {_token: CSRF_TOKEN},
                        success: function(data){
                            if(data.status == true){
                                swal("Eliminado", "Se ha eliminado correctamente.", "success");
                                $('.selector-'+id).remove();
                                setTimeout(() => {
                                    // location.reload();
                                }, 3000);
                            }else{
                                // swal('Ocurrio un error vuelva a intentarlo');
                                swal("Eliminado", "Ocurrio un error vuelva a intentarlo", "error");
                            }
                        }
                    });
                }
            });
        });
    </script>
@stop