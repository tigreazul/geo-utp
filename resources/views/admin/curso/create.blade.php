@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Curso</h5>
                        <span>Creación Curso</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- card -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Registro de Curso</h5>
                        </div>
                        <div class="card-block">
                            <form id="main" method="POST" action="{{ route('admin.curso_add') }}" >
                                @csrf
                                <div class="col-lg-6 col-xl-12">
                                    
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong"> Nombre del Curso: </label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control" id="titulo" name="titulo" required value="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong"> Código Curso: </label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control" id="code_curso" name="code_curso" required value="">
                                                </div>
                                            </div>
                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Precio Curso: </label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control" id="precio" name="precio" required value="">
                                                </div>
                                            </div>
                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Precio Promoción: </label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control" id="precioPromocio" name="precioPromocio" required value="">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Docente: </label>
                                                <div class="col-sm-12">
                                                    <select name="docente" class="form-control" id="docente" required>
                                                        <option value="">[SELECCIONE]</option>
                                                        @foreach($profesor as $profe)
                                                            <option value="{{ $profe->idProfesor }}">{{ $profe->apellidoPaterno.' '.$profe->apellidoMaterno.' '.$profe->nombre }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Categoria: </label>
                                                <div class="col-sm-12">
                                                    <select name="categoria" class="form-control" id="categoria" required>
                                                        <option value="">[SELECCIONE]</option>
                                                        @foreach($categoria as $cate)
                                                            <option value="{{ $cate->idCat }}">{{ $cate->nombreCat }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div style="width: 97%;height: 220px;border: 1px solid #b3b3b3;margin: 7px;background: #efefef;">
                                                <p style="padding-top: 84px;text-align: center;color: #c5c5c5;" id="sin_imagen">SIN IMAGEN</p>
                                                <img src="{{ isset($titular->aFotografia)?asset('upload/'.$titular->aFotografia):'' }}" id="imagen_preview" style="height: 100%;display: block;margin: auto;">
                                            </div>

                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Subir Imagen: </label>
                                                <div class="col-sm-12">
                                                    <input class="form-control" type="file" name="imagen" id="aaa" onchange="mostrar()" required>
                                                    <input type="hidden" name="img_curso" id="img_curso" value="">
                                                </div>
                                            </div>

                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Estado: </label>
                                                <div class="col-sm-12">
                                                    <select name="estado" class="form-control" id="estado" required>
                                                        <option value="1">ACTIVO</option>
                                                        <option value="2">DESACTIVADO</option>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Descripción: </label>
                                                <div class="col-sm-12">
                                                    <textarea id="editor1" name="descripcion" class="form-control" required></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Descripción Adicional: </label>
                                                <div class="col-sm-12">
                                                    <textarea id="editor2" name="adicional" class="form-control" required></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Requerimiento: </label>
                                                <div class="col-sm-12">
                                                    <textarea id="editor3" name="requerimiento" class="form-control" required></textarea>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    
                                </div>

                                <div class="form-group">
                                    <hr>
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary m-b-0">
                                            <i class="fa fa-save"></i> Registrar
                                        </button>
                                        <a href="{{ route('admin.curso_list') }}" class="btn waves-effect waves-light btn-inverse btn-outline-inverse m-b-0"><i class="fa fa-reply"></i> Volver</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <script src="//cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
    <script src="{{ asset('js/curso.js') }}"></script>
    <script>
        CKEDITOR.replace( 'editor1' );
        CKEDITOR.replace( 'editor2' );
        CKEDITOR.replace( 'editor3' );
    </script>

<style>
    .bottom_cero{
        margin-bottom: 10px !important;
    }
    .bold-strong{
        font-weight: 700;
        text-transform: uppercase;
        font-size: 12px;
    }
</style>
@stop