@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Categoria</h5>
                        <span>Creación Categoria</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- card -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Editar de Categoria</h5>
                            <!-- <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
                        </div>
                        
                        <div class="card-block">
                            
                            <form id="main" method="POST" action="{{ route('admin.curso_edit_data',['id' => $curso->idCurso] ) }}" >
                                @csrf
                                @method('PATCH')
                                <div class="col-lg-12 col-xl-12">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong"> Nombre del Curso: </label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control" id="titulo" name="titulo" required value="{{ isset($curso->titulo)?$curso->titulo:'' }}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong"> Código Curso: </label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control" id="code_curso" name="code_curso" required value="{{ isset($curso->codigoCurso)?$curso->codigoCurso:'' }}">
                                                </div>
                                            </div>
                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Precio Curso: </label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control" id="precio" name="precio" required value="{{ isset($curso->precioPrincipal)?$curso->precioPrincipal:'' }}">
                                                </div>
                                            </div>
                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Precio Promoción: </label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control" id="precioPromocio" name="precioPromocio" required value="{{ isset($curso->PrecioPromocion)?$curso->PrecioPromocion:'' }}">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Docente: </label>
                                                <div class="col-sm-12">
                                                    <select name="docente" class="form-control" id="docente" required>
                                                        <option value="">[SELECCIONE]</option>
                                                        @foreach($profesor as $profe)
                                                            <option value="{{ $profe->idProfesor }}" {{ isset($curso->idProfesor)?( $curso->idProfesor == $profe->idProfesor ?'selected':'' ):'' }} >{{ $profe->apellidoPaterno.' '.$profe->apellidoMaterno.' '.$profe->nombre }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Categoria: </label>
                                                <div class="col-sm-12">
                                                    <select name="categoria" class="form-control" id="categoria" required>
                                                        <option value="">[SELECCIONE]</option>
                                                        @foreach($categoria as $cate)
                                                            <option value="{{ $cate->idCat }}" {{ isset($curso->idCategoria)?( $curso->idCategoria == $cate->idCat ?'selected':'' ):'' }} >{{ $cate->nombreCat }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div style="width: 97%;height: 220px;border: 1px solid #b3b3b3;margin: 7px;background: #efefef;">
                                                <!-- <p style="padding-top: 84px;text-align: center;color: #c5c5c5;" id="sin_imagen">SIN IMAGEN</p> -->
                                                <img src="{{ isset($curso->ImagenCurso)?asset('curso/'.$curso->ImagenCurso):'' }}" id="imagen_preview" style="height: 100%;display: block;margin: auto;">
                                            </div>

                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Subir Imagen: </label>
                                                <div class="col-sm-12">
                                                    <input class="form-control" type="file" name="imagen" id="aaa" onchange="mostrar()">
                                                    <input type="hidden" name="img_curso" id="img_curso" value="{{ isset($curso->ImagenCurso)?$curso->ImagenCurso:'' }}">
                                                </div>
                                            </div>

                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Estado: </label>
                                                <div class="col-sm-12">
                                                    <select name="estado" class="form-control" id="estado" required>
                                                        <option value="1" {{ isset($curso->estado)?( $curso->estado == '1'?'selected':'' ):'' }} >ACTIVO</option>
                                                        <option value="2" {{ isset($curso->estado)?( $curso->estado == '2'?'selected':'' ):'' }} >DESACTIVADO</option>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Descripción: </label>
                                                <div class="col-sm-12">
                                                    <textarea id="editor1" name="descripcion" class="form-control">{{ isset($curso->descripcion)?$curso->descripcion:'' }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Descripción Adicional: </label>
                                                <div class="col-sm-12">
                                                    <textarea id="editor2" name="adicional" class="form-control">{{ isset($curso->adicional)?$curso->adicional:'' }}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group bottom_cero">
                                                <label class="col-sm-8 col-form-label bold-strong">Requerimiento: </label>
                                                <div class="col-sm-12">
                                                    <textarea id="editor3" name="requerimiento" class="form-control">{{ isset($curso->requerimiento)?$curso->requerimiento:'' }}</textarea>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group ">
                                    <hr>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">
                                            <i class="fa fa-save"></i> Editar
                                        </button>
                                        <a href="{{ route('admin.curso_list') }}" class="btn waves-effect waves-light btn-inverse btn-outline-inverse m-b-0"><i class="fa fa-reply"></i> Volver</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <script src="//cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
    <script src="{{ asset('js/curso.js') }}"></script>
    <script>
        CKEDITOR.replace( 'editor1' );
        CKEDITOR.replace( 'editor2' );
        CKEDITOR.replace( 'editor3' );
    </script>

    <style>
        .bottom_cero{
            margin-bottom: 10px !important;
        }
        .bold-strong{
            font-weight: 700;
            text-transform: uppercase;
            font-size: 12px;
        }
    </style>
@stop