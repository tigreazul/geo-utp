<div class="navbar-wrapper">
    <div class="navbar-logo">
        <a href="{{ route('admin.home') }}">
            <!-- <img src="{{ asset('logo-white.png') }}" alt="logo"> -->
            <h2>TRS</h2>
        </a>
        <a class="mobile-menu" id="mobile-collapse" href="#!">
            <i class="feather icon-menu icon-toggle-right"></i>
        </a>
        <a class="mobile-options waves-effect waves-light">
            <i class="feather icon-more-horizontal"></i>
        </a>
    </div>

    <div class="navbar-container container-fluid">
        <ul class="nav-left">
            <!-- <li class="header-search">
                <div class="main-search morphsearch-search">
                    <div class="input-group">
                        <span class="input-group-prepend search-close">
                            <i class="feather icon-x input-group-text"></i>
                        </span>
                        <input type="text" class="form-control" placeholder="Enter Keyword">
                        <span class="input-group-append search-btn">
                            <i class="feather icon-search input-group-text"></i>
                        </span>
                    </div>
                </div>
            </li> -->
            <li>
                <a href="#!" onclick="if (!window.__cfRLUnblockHandlers) return false; javascript:toggleFullScreen()" class="waves-effect waves-light" data-cf-modified-8f68cca9f211ae1c772449e8-="">
                    <i class="full-screen feather icon-maximize"></i>
                </a>
            </li>
        </ul>
        <ul class="nav-right">
            <li>
                @if(Session::has('permisos'))
                    <label class="label label-inverse-info-border">Cargo: {{ session::get('permisos')->DescripcionRol }}</label>
                @endif
            </li>
            <!-- <li class="header-notification">
                <div class="dropdown-primary dropdown">
                    <div class="displayChatbox dropdown-toggle" data-toggle="dropdown">
                        <i class="feather icon-message-square"></i>
                        <span class="badge bg-c-green">3</span>
                    </div>
                </div>
            </li> -->
            <li class="user-profile header-notification">
                <div class="dropdown-primary dropdown">
                    <div class="dropdown-toggle" data-toggle="dropdown">
                        <span>{{ Auth::user()->name }}</span>
                        <i class="feather icon-chevron-down"></i>
                    </div>
                    <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                        <!--
                        <li>
                            <a href="#!">
                                <i class="feather icon-settings"></i> Settings
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="feather icon-user"></i> Profile
                            </a>
                        </li>
                        <li>
                            <a href="email-inbox.html">
                                <i class="feather icon-mail"></i> My Messages
                            </a>
                        </li>
                        <li>
                            <a href="auth-lock-screen.html">
                                <i class="feather icon-lock"></i> Lock Screen
                            </a>
                        </li>-->
                        <li>
                            <a href="{{ url('admin/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="feather icon-log-out"></i> {{ __('Salir') }}
                            </a>
                            <form id="logout-form" action="{{ url('admin/logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>