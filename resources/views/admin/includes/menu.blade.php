<div class="pcoded-inner-navbar main-menu">
    <?php 
    //print_r( \Views::menu() ); die();
    ?>
    <div class="pcoded-navigation-label">Navigation</div>
    <ul class="pcoded-item pcoded-left-item">
        @foreach(\Views::menu() as $menu)
            @if(!is_array($menu['interno']) || empty($menu['interno']) )
                <li class="" >
                    <a href="{{ $menu['link'] }}" {{ $menu['externo'] == '1' ? 'target="_blank"':''  }} class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            {!! $menu['icono'] !!}
                        </span>
                        <span class="pcoded-mtext">{{ $menu['modulo'] }}</span>
                    </a>
                </li>
            @else
                <!-- \Views::segment() == $menu['vista'] ? 'pcoded-trigger':''; dashboard  -->
                
                    <li class="pcoded-hasmenu  {{ \Views::segment() == $menu['vista'] ? 'active pcoded-trigger':'' }}" >
                        <a href="javascript:void(0)" class="waves-effect waves-dark">
                            <span class="pcoded-micon">{!! $menu['icono'] !!}</span>
                            <span class="pcoded-mtext">{{ $menu['modulo'] }}</span>
                        </a>
                        <ul class="pcoded-submenu">
                            @foreach($menu['interno'] as $interno)
                                @if(!is_array($interno['submenu']) || empty($interno['submenu']) )
                                    <li class="{{ (\Views::segment(3) == $interno['ruta'] || \Views::segment(2) == $interno['ruta']) ? 'active':'' }}">
                                        <a href="{{ url('admin/'.$interno['ruta']) }}" class="waves-effect waves-dark">
                                            <span class="pcoded-mtext">{{ $interno['cabecera'] }}</span>
                                        </a>
                                    </li>
                                @else
                                    <li class="">
                                        <a href="{{ url('admin/'.$interno['ruta']) }}" class="waves-effect waves-dark">
                                            <span class="pcoded-mtext">{{ $interno['cabecera'] }}</span>
                                            <!-- <span class="pcoded-badge label label-info ">NEW</span> -->
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                            
                        </ul>
                    </li>
                
            @endif
        @endforeach
    </ul>
    
</div>