
<!DOCTYPE html>
<html lang="es">
<head>
    @include('admin.includes.head')
    <script>
		var local = {
			base: "{{ url('/') }}"
		} 
	</script>
</head>
<body>

<div class="loader-bg">
    <div class="loader-bar"></div>
</div>
<div class="loading-fondo" style="display: none;">
    <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
</div>

<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

        <nav class="navbar header-navbar pcoded-header">
            @include('admin.includes.nav')
        </nav>

        <div id="sidebar" class="users p-chat-user showChat">
            <div class="had-container">
                <div class="p-fixed users-main">
                    <div class="user-box">
                        @include('admin.includes.users')
                    </div>
                </div>
            </div>
        </div>


        <div class="showChat_inner">
            @include('admin.includes.chat')
        </div>

        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <nav class="pcoded-navbar">
                    <div class="nav-list">
                        @include('admin.includes.menu')
                    </div>
                </nav>

                <div class="pcoded-content">
                    @yield('content')
                </div>

                <div id="styleSelector">
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .loading-fondo{
        position: fixed;background: rgba(0, 0, 0, 0.5);z-index: 9999;width: 100%;height: 100%;text-align: center;
    }
    .loading-fondo .lds-ellipsis{
        top: 40%
    }
    .lds-ellipsis {
        display: inline-block;
        position: relative;
        width: 80px;
        height: 80px;
    }
    .lds-ellipsis div {
        position: absolute;
        top: 33px;
        width: 13px;
        height: 13px;
        border-radius: 50%;
        background: #fff;
        animation-timing-function: cubic-bezier(0, 1, 1, 0);
    }
    .lds-ellipsis div:nth-child(1) {
        left: 8px;
        animation: lds-ellipsis1 0.6s infinite;
    }
    .lds-ellipsis div:nth-child(2) {
        left: 8px;
        animation: lds-ellipsis2 0.6s infinite;
    }
    .lds-ellipsis div:nth-child(3) {
        left: 32px;
        animation: lds-ellipsis2 0.6s infinite;
    }
    .lds-ellipsis div:nth-child(4) {
        left: 56px;
        animation: lds-ellipsis3 0.6s infinite;
    }
    @keyframes lds-ellipsis1 {
        0% {
        transform: scale(0);
        }
        100% {
        transform: scale(1);
        }
    }
    @keyframes lds-ellipsis3 {
        0% {
        transform: scale(1);
        }
        100% {
        transform: scale(0);
        }
    }
    @keyframes lds-ellipsis2 {
        0% {
        transform: translate(0, 0);
        }
        100% {
        transform: translate(24px, 0);
        }
    }

</style>

@include('admin.includes.footer')
</body>
</html>