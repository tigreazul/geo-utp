@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-home bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Dashboard</h5>
                        <span>Panel principal</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class=" breadcrumb breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.home') }}"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Dashboard</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-md-12 col-xl-12">
                            <div class="card sale-card" style="min-height: 700px;">
                                <h1 style="text-align: center;margin-top: 15%;">BIENVENIDO AL SISTEMA</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- @content('scripts') -->
    <script src="{{ asset('theme_admin/js/jquery.flot.js')}}" type="text/javascript"></script>
    <script src="{{ asset('theme_admin/js/jquery.flot.categories.js')}}" type="text/javascript"></script>
    <script src="{{ asset('theme_admin/js/curvedLines.js')}}" type="text/javascript"></script>
    <script src="{{ asset('theme_admin/js/jquery.flot.tooltip.min.js')}}" type="text/javascript"></script>

    <script src="{{ asset('theme_admin/js/chartist.js') }}" type="text/javascript"></script>

    <script src="{{ asset('theme_admin/js/amcharts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('theme_admin/js/serial.js') }}" type="text/javascript"></script>
    <script src="{{ asset('theme_admin/js/light.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('theme_admin/js/custom-dashboard.min.js') }} "></script>
    <!-- @endcontent -->
    
@stop