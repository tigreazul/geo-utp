@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Reporte</h5>
                        <span>Detalle de Pagos por Fechas</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class=" breadcrumb breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Dashboard</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- card -->
                    <div class="row">
                        <div class="card panel-modulo col " style="margin-right: 15px;">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col">
                                        <h5>Detalle de Pagos por Fechas</h5>
                                    </div>                          
                                </div>
                            </div>
                            <div class="card-block">

                                <form id="mains" method="POST" action="{{ route('admin.fecha_search') }}" >
                                    @csrf
                                    <div class="col-sm-8" style="margin-left: 16%;">
                                        <div class="form-group row">
                                            <div class="col">
                                                <input type="date" class="form-control" placeholder="col" name="finicio">
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="date" class="form-control" placeholder="col-sm-4" name="ffin">
                                            </div>
                                            <div class="col">
                                                <div class="input-group-append">
                                                    <button class="btn btn-primary btn-sm" type="submit">Buscar</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                                <hr>

                                <div class="dt-responsive table-responsive">
                                    <!-- lista de tablas -->
                                    @if(isset($fini))
                                        <code>Resultado del {{ $fini}} al {{ $ffin}}</code>
                                    @endif
                                    <table class="table table-hover m-b-0" style="width: 70%;margin: 0 auto;">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nro Expediente</th>
                                                <th>Nombre</th>
                                                <th>Tipo</th>
                                                <th>Nro Recibo</th>
                                                <th>Monto</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i = 1; $total = 0; @endphp
                                            @forelse($buscador as $search)
                                                <tr class="table-verify selector-{{ $search->idExpediente }}">
                                                    <td>{{ $i }}</td>
                                                    <td>{{ $search->nroExpediente }}</td>
                                                    <td>{{ $search->apellidoPaterno.' '.$search->apellidoMaterno.' '.$search->nombre }}</td>
                                                    <td>{{ $search->nomDireccion }}</td>
                                                    <td>{{ $search->nroRecibo }}</td>
                                                    <td>{{ $search->monto }}</td>
                                                </tr>
                                                @php $i++; $total = $total+ $search->monto @endphp
                                            @empty
                                                <tr>
                                                    <td colspan="7" style="text-align:center; background:#f4f6f7"> <strong>NO EXISTE RESULTADOS</strong></td>
                                                </tr>
                                            @endforelse
                                            <tr>
                                                <td colspan="5" class="text-right">
                                                    TOTAL
                                                </td>
                                                <td>{{ $total }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- lista de tablas -->
                                </div>
                            </div>
                        </div>



                    </div>
                    <div class="md-overlay"></div>
                    <!-- card -->
                </div>
            </div>
        </div>
    </div>


@stop