@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Reporte</h5>
                        <span>Titular</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class=" breadcrumb breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Dashboard</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- card -->
                    <div class="row">
                        <div class="card panel-modulo col " style="margin-right: 15px;">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col">
                                        <h5>Lista de Titular</h5>
                                    </div>                         
                                </div>
                            </div>
                            <div class="card-block">

                                <form id="mains" method="POST" action="{{ route('admin.titular_search') }}" >
                                    @csrf
                                    <div class="col-sm-8" style="margin-left: 16%;">
                                        <div class="form-group row">
                                            <div class="input-group input-group-button ">
                                                <input type="text" class="form-control" name="nombre" placeholder="Ingresar nombre">
                                                <div class="input-group-append">
                                                    <button class="btn btn-primary" type="submit">Buscar</button>
                                                </div>
                                            </div>
                                            <span class="messages" style="color: red;font-size: 12px;">
                                                @if (Session::has('message'))
                                                    {!! session('message') !!}
                                                @endif
                                            </span>
                                        </div>

                                    </div>
                                </form>
                                <hr>

                                <div class="dt-responsive table-responsive">
                                    <!-- lista de tablas -->

                                    @if( isset($persona) )
                                        <div class="col-3 offset-md-1 mb-1">
                                            <!-- <button class="btn btn-primary btn-sm" type="submit">Imprimir</button> -->
                                        </div>

                                        <div class="container">
                                            <div class="m-b-0">
                                                <div class="row mb-3">
                                                    <div class="col-12 themed-grid-col text-center">
                                                        <h3>"SECTOR RINCONADA VILLASOL"</h3>
                                                    </div>
                                                    <div class="col-12 themed-grid-col text-center">
                                                        <h6>JICAMARCA ANEXO 22 PAMPA CANTO GRANDE DISTRITO SAN ANTONIO PROVINCIA DE HUAROCHIRI - LIMA</h6>
                                                    </div>
                                                    <hr>
                                                    <div class="col-12 themed-grid-col text-left">
                                                        <h5>CUOTAS DEL TITULAR</h5>
                                                    </div>
                                                    <hr>
                                                    <div class="row col mb-3">
                                                        <div class="col-md-9 themed-grid-col text-right"></div>
                                                        <div class="col-md-2 themed-grid-col text-right" style="border: 1px solid #818588;padding: 10px;background: #818588;color: #fff;">Expediente:</div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <h4>REUNIONES CON DEUDAS</h4>
                                            <div class="dt-responsive table-responsive">
                                                <!-- lista de tablas -->
                                                <table class="table table-hover m-b-0">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <!-- <th>Tipo</th> -->
                                                            <th>Fecha</th>
                                                            <th>Mes</th>
                                                            <th>Monto</th>
                                                            <!-- <th>Acción</th> -->
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $i = 1; $total = 0; @endphp
                                                        @forelse($reuniones as $multa)
                                                            @if($multa->multa > 0)
                                                            <tr class="table-verify selector-{{ $multa->idDetalleReunion }}">
                                                                <td>{{ $i }}</td>
                                                                <!-- <td class="tipo_multa">{{ $multa->tipo }}</td> -->
                                                                <td class="fecha_tipo">{{ $multa->fecha }}</td>
                                                                <td>{{ $multa->MES }}
                                                                    <div style="display:none" class="idTipo">{{ $multa->idTipoReunion }}</div>
                                                                    <div style="display:none" class="idUsuario">{{ $multa->idUsuario }}</div>
                                                                </td>
                                                                <td>s/. <span class="multa_monto">{{ $multa->multa }}</span></td>
                                                                <!-- <td>
                                                                    <div class="btn-group">
                                                                        <a class="btn btn-primary btn-sm justificar_btn"  href="#" data-id="{{ $multa->idDetalleReunion }}" data-uri="R">
                                                                            <i class="fa fa-edit"></i>
                                                                            Justificar
                                                                        </a> 
                                                                        <a class="btn btn-success btn-sm alert-delete pagar_btn" href="#" data-id="{{ $multa->idDetalleReunion }}" style="margin-left: 5px;" data-uri="R">
                                                                            <i class="fa fa-trash-alt"></i>
                                                                            Pagar
                                                                        </a>
                                                                    </div>
                                                                </td> -->
                                                            </tr>
                                                            @php  $total = $total + $multa->multa; $i++; @endphp
                                                            @endif
                                                        @empty
                                                            <tr>
                                                                <td colspan="4" style="text-align:center; background:#f4f6f7"> <strong>NO EXISTE REGISTRO</strong></td>
                                                            </tr>
                                                        @endforelse
                                                        <tr style="background-color: rgba(64,153,255,.1)">
                                                            <td colspan="3" style="text-align:right">TOTAL:</td>
                                                            <td colspan="0" style="text-align:left">s/. {{ $total }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- lista de tablas -->
                                            </div>
                                            <hr>
                                            <h4>CUOTA LEGAL</h4>
                                            <div class="dt-responsive table-responsive">
                                                <!-- lista de tablas -->
                                                <table class="table table-hover m-b-0">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <!-- <th>Tipo</th> -->
                                                            <th>Fecha</th>
                                                            <th>Mes</th>
                                                            <th>Monto</th>
                                                            <!-- <th>Acción</th> -->
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $i = 1; $total = 0; @endphp
                                                        @forelse($legal as $leg)
                                                        @if($leg->monto > 0)
                                                            <tr class="table-verify selector-{{ $leg->idCuotaDetalle }}">
                                                                <td>{{ $i }}</td>
                                                                <!-- <td class="tipo_multa">{{ $leg->tipo }}</td> -->
                                                                <td class="fecha_tipo">{{ \Carbon\Carbon::parse($leg->fechaRegistro)->format('Y-m-d') }}</td>
                                                                <td>{{ $leg->mes }}
                                                                    <div style="display:none" class="idTipo">{{ $leg->idTipoCuota }}</div>
                                                                    <div style="display:none" class="idUsuario">{{ $leg->idUsuario }}</div>
                                                                </td>
                                                                <td> <span class="multa_monto">{{ $leg->monto }}</span></td>
                                                                <!-- <td>
                                                                    <div class="btn-group">
                                                                        <a class="btn btn-primary btn-sm justificar_btn"  href="#" data-id="{{ $leg->idCuotaDetalle }}" data-uri="C">
                                                                            <i class="fa fa-edit"></i>
                                                                            Justificar
                                                                        </a> 
                                                                        <a class="btn btn-success btn-sm alert-delete pagar_btn" href="#" data-id="{{ $leg->idCuotaDetalle }}" style="margin-left: 5px;" data-uri="C">
                                                                            <i class="fa fa-trash-alt"></i>
                                                                            Pagar
                                                                        </a>
                                                                    </div>
                                                                </td> -->
                                                            </tr>
                                                            @php  $total = $total + $leg->monto; $i++; @endphp
                                                            @endif
                                                        @empty
                                                            <tr>
                                                                <td colspan="4" style="text-align:center; background:#f4f6f7"> <strong>NO EXISTE REGISTRO</strong></td>
                                                            </tr>
                                                        @endforelse
                                                        <tr style="background-color: rgba(64,153,255,.1)">
                                                            <td colspan="3" style="text-align:right">TOTAL:</td>
                                                            <td colspan="0" style="text-align:left">{{ $total }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- lista de tablas -->
                                            </div>
                                        </div>
                                    <!-- lista de tablas -->
                                    @endif
                                </div>
                            </div>
                        </div>



                    </div>
                    <div class="md-overlay"></div>
                    <!-- card -->
                </div>
            </div>
        </div>
    </div>

    <style type="text/css">
        .tabla-exp {
            background: #818588;
            color: #fff;
            width: 20%;
        }
    </style>

@stop