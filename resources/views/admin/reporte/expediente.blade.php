@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Reporte</h5>
                        <span>Expediente</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class=" breadcrumb breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Dashboard</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- card -->
                    <div class="row">
                        <div class="card panel-modulo col " style="margin-right: 15px;">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col">
                                        <h5>Historial de expediente</h5>
                                    </div>                         
                                </div>
                            </div>
                            <div class="card-block">

                                <form id="mains" method="POST" action="{{ route('admin.expediente_search') }}" >
                                    @csrf
                                    <div class="col-sm-8" style="margin-left: 16%;">
                                        <div class="form-group row">
                                            <div class="input-group input-group-button ">
                                                <input type="text" class="form-control" name="expediente" placeholder="Ingresar Numero de expediente">
                                                <div class="input-group-append">
                                                    <button class="btn btn-primary" type="submit">Buscar</button>
                                                </div>
                                            </div>
                                            <span class="messages" style="color: red;font-size: 12px;">
                                                @if (Session::has('message'))
                                                    {!! session('message') !!}
                                                @endif
                                            </span>
                                        </div>

                                    </div>
                                </form>
                                <hr>

                                <div class="dt-responsive table-responsive">
                                    <!-- lista de tablas -->

                                    @if( isset($dato) )
                                        <div class="col-3 offset-md-1 mb-1">
                                            <!-- <button class="btn btn-primary btn-sm" type="submit">Imprimir</button> -->
                                        </div>

                                        <div class="container">
                                            <div class="m-b-0">
                                                <div class="row mb-3">
                                                    <div class="col-12 themed-grid-col text-center">
                                                        <h3>"SECTOR RINCONADA VILLASOL"</h3>
                                                    </div>
                                                    <div class="col-12 themed-grid-col text-center">
                                                        <h6>JICAMARCA ANEXO 22 PAMPA CANTO GRANDE DISTRITO SAN ANTONIO PROVINCIA DE HUAROCHIRI - LIMA</h6>
                                                    </div>
                                                    <hr>
                                                    <div class="col-12 themed-grid-col text-left">
                                                        <h5>DATOS DE TITULARES ANTERIORES</h5>
                                                    </div>
                                                    <div class="row col mb-3">
                                                        <div class="col-md-9 themed-grid-col text-right"></div>
                                                        <div class="col-md-2 themed-grid-col text-right" style="border: 1px solid #818588;padding: 10px;background: #818588;color: #fff;">Expediente:</div>
                                                        <div class="col-md-1 themed-grid-col" style="border: 1px solid #818588;padding: 10px;">{{ $dato }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <table class="m-b-0" style="width: 82%;margin: 0 auto;" cellpadding="2" cellspacing="2">
                                            <?php $tt = 0; ?>
                                            @forelse($buscador as $bs)
                                            <tr>
                                                <td>
                                                    <table class="table m-b-0">
                                                        <tr>
                                                            <td class="tabla-exp">NOMBRE</td>
                                                            <td>{{ $bs->nombre }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tabla-exp">APELLIDO</td>
                                                            <td>{{ $bs->apellidoPaterno.' '.$bs->apellidoMaterno }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tabla-exp">DIRECCIÓN</td>
                                                            <td>{{ $bs->nomDireccion }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tabla-exp">DNI</td>
                                                            <td>{{ $bs->dni }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tabla-exp">MONTO DE ADEUDA</td>
                                                            <td>{{ $bs->monto }} SOLES</td>
                                                            <?php $tt = $tt + $bs->monto ?>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td>
                                                    <table class="table m-b-0">
                                                        <tr>
                                                            <td colspan="" class="tabla-exp">TOTAL</td>
                                                            <td>{{ $tt }}</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                                
                                            @endforelse
                                        </table>
                                    <!-- lista de tablas -->
                                    @endif
                                </div>
                            </div>
                        </div>



                    </div>
                    <div class="md-overlay"></div>
                    <!-- card -->
                </div>
            </div>
        </div>
    </div>

    <style type="text/css">
        .tabla-exp {
            background: #818588;
            color: #fff;
            width: 20%;
        }
    </style>

@stop