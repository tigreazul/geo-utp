@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Asignación</h5>
                        <span>Administración de Asignación</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class=" breadcrumb breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="#"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Dashboard</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- card -->
                    <div class="row">
                        <div class="card panel-modulo col " style="margin-right: 15px;">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col">
                                        <h5>Lista de Asignación</h5>
                                    </div>
                                    <div class="col-42">
                                        <!-- <a href="{{ route('admin.actividad_create') }}" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus-circle"></i> ASIGNAR ACTIVIDADES</a> -->
                                    </div>
                                </div>
                            </div>
                            <div class="card-block">
                                @if (Session::has('message'))
                                    <div class="alert alert-success background-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <i class="icofont icofont-close-line-circled"></i>
                                        </button>
                                        {!! session('message') !!}
                                    </div>
                                @endif
                                <table class="table table-hover m-b-0 table-sm">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Personal</th>
                                            <th>Email</th>
                                            <th>Username</th>
                                            <th>Estado</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i = 1; @endphp
                                        @foreach($actividades as $activid)
                                            <tr class="table-verify selector-{{ $activid->id }}">
                                                <td scope="row">{{ $i }}</td>
                                                <td>{{ $activid->apellidoPaterno.' '.$activid->apellidoMaterno.' '.$activid->nombre }}</td>
                                                <td>{{ $activid->email }}</td>
                                                <td>{{ $activid->username }}</td>
                                                <td><label class="label label-{{ ($activid->estado == 1)? 'success': 'danger' }}">{{ ($activid->estado == 1)? 'ACTIVO': 'DESACTIVADO' }}</label></td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Acciones
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            <!-- <a class="dropdown-item select-page" data-sel="{{ $activid->id }}" > -->
                                                            <!-- <a class="dropdown-item" href="{{ route('admin.actividad_edit', ['id'=>$activid->id]) }}">
                                                                <i class="fa fa-edit"></i>
                                                                Editar
                                                            </a> -->
                                                            <a class="dropdown-item select-page" data-sel="{{ $activid->idUsuario }}" >
                                                                <i class="fa fa-edit"></i>
                                                                Actividades
                                                            </a>
                                                            <!-- <a class="dropdown-item alert-delete-activid" href="#" data-id="{{ $activid->id }}" >
                                                                <i class="fa fa-trash-alt"></i>
                                                                Eliminar
                                                            </a> -->
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            @php $i++; @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="dt-responsive table-responsive">
                                    <!-- lista de tablas -->
                                </div>
                            </div>
                        </div>

                        <div class="card panel-pagina col" style="display:none">
                            @include('admin.asignacion.activity')
                        </div>

                    </div>
                    <div class="md-overlay"></div>
                    <!-- card -->
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{ asset('theme_admin/custom/js/asociacion.js')}}"></script>
    

@stop