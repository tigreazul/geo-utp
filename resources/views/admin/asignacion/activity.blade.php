<div class="card-header">
    <div class="row">
        <div class="col">
            <h5>Lista de Actividades de: <code class="code_page"></code> </h5>
            <div class="card-header-right loader-cards"></div>
            <!-- <div class="json"></div> -->
            <input type="hidden" class="json" name="json">
            <input type="hidden" class="id_personal" name="id_personal">
        </div>
        <div class="col-4">
            <button type="button" class="btn btn-primary btn-sm data_asignar" data-modal="modal-12"><i class="fa fa-plus-circle"></i> ASIGNAR ACTIVIDADES</button>
            <!-- <button type="button" class="btn btn-primary btn-sm waves-effect md-trigger data_asignar" data-modal="modal-12"><i class="fa fa-plus-circle"></i> ASIGNAR ACTIVIDAD</button> -->
            <!-- <a href="{{ route('admin.modulo_add') }}" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus-circle"></i> NUEVA PAGINA</a> -->
        </div>

        <div class="col-12">
            <form action="#" method="post" id="actividades_form">
                <select class="listActivity" name="industryExpect[]" multiple="multiple" style="width: 100%;display:none"></select>
                <button type="submit" class="btn btn-primary btn-sm agregar_actividades" data-modal="modal-12" style="display:none"><i class="fa fa-plus-circle"></i> AGREGAR</button>
            </form>
        </div>
    </div>

</div>
<div class="card-block">

    <table class="table table-hover m-b-0">
        <thead>
            <tr>
                <th>#</th>
                <th>Descripción</th>
                <th>Estado</th>
                <th></th>
            </tr>
        </thead>
        <tbody  id="page-body-table">
        </tbody>
    </table>
</div>



<div class="md-modal md-effect-12" id="modal-12">
    <div class="md-content">
        <h3><span class="text-muted text-center">Asignar Actividades</span></h3>
    <div>
    <div class="card-block loader-cards">
        <div class="row">
            <div class="col-sm-12 mobile-inputs">
                <div id="transfer1" class="transfer-demo"></div>
                <br>
                <button type="button" class="btn btn-sm btn-default waves-effect md-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<!-- <script src="{{ asset('theme_admin/js/modal.js') }}" type="text/javascript"></script> -->
<script src="{{ asset('theme_admin/js/modalEffects.js') }}" type="text/javascript"></script>
<script src="{{ asset('theme_admin/js/classie.js') }}" type="text/javascript"></script>

<!-- <link rel="stylesheet" type="text/css" href="{{ asset('theme_admin/listbox/icon_font/css/icon_font.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('theme_admin/listbox/css/jquery.transfer.css?v=0.0.3') }}" />
<script type="text/javascript" src="{{ asset('theme_admin/listbox/js/jquery.transfer.js?v=0.0.6') }}"></script> -->

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script type="text/javascript">

    $('.data_asignar').click(function(e){
        e.preventDefault();
        $('.listActivity').show();
        $('.agregar_actividades').show();
        var datas = JSON.parse($('.json').val());
        // console.log( datas )
        let data = [];
        $.each(datas,function(k,v){
            let arr = {
                'id' : v.value ,
                'text' : v.city 
            };
            data.push(arr);
        });
        // console.log(data);
        $('.listActivity').select2({
            placeholder: 'Seleccione nueva actividad',
            data: data
        });
    });

    $(document).on('submit','#actividades_form',function(e){
        e.preventDefault();
        // let frm = $(this).serialize();
        // console.log(frm);
        var CSRF_TOKEN  = $('meta[name="csrf-token"]').attr('content');
        let datos = $('.listActivity').val();
        let id_personal = $('.id_personal').val();
        let datax = {
            _token  : CSRF_TOKEN,
            data    : datos,
            id      : id_personal
        }
        
        // return false;
        $.ajax({
            url: local.base+'/admin/actividades/post-actividades-add/',
            type: 'POST',
            dataType: 'json',
            data: datax,
            beforeSend: function(){
                var $this = $(this);
                $('.loader-cards').parents('.card').addClass("card-load");
                $('.loader-cards').parents('.card').append('<div class="card-loader"><i class="feather icon-radio rotate-refresh"></div>');
            },
            success: function(data){
                $('.listActivity').val(null).trigger('change');
                let dataos = [];
                $.each(data.dataJson,function(k,v){
                    let arr = {
                        'id' : v.value ,
                        'text' : v.city 
                    };
                    dataos.push(arr);
                });
                // console.log(dataos);
                $('.listActivity').select2({
                    placeholder: 'Seleccione nueva actividad',
                    data: dataos
                });

                let vhtml = "";
                // console.log(data.activity.length);
                if(data.activity.length != 0){
                    $.each(data.activity,function(k,v){
                        vhtml += '<tr>';
                        vhtml += '<td>'+(k+1)+'</td>';
                        vhtml += '<td>'+v.descripcion+'</td>';
                        if( v.concluido == 1 )
                        {
                            vhtml += '<td><label class="label label-success">Terminado</label></td>';
                            vhtml += '<td></td>';
                        }else{
                            vhtml += '<td><label class="label label-danger">Pendiente</label></td>';
                            vhtml += `<td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Acciones
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item alert-delete-page" href="#" data-id="`+v.ID+`" >
                                                    <i class="fa fa-trash-alt"></i>
                                                    Quitar
                                                </a>
                                            </div>
                                        </div>
                                    </td>`;
                        }
                        vhtml += '</tr>';
                    });

                }else{
                    vhtml = `
                    <tr>
                        <td colspan="4" class="not-row"><strong> No existen actividades asignadas</strong></td>
                    </tr>
                    `
                }
                $('#page-body-table').html(vhtml);

                
            },
            complete: function(){
                $('.loader-cards').parents('.card').children(".card-loader").remove();
                $('.loader-cards').parents('.card').removeClass("card-load");
            }
        });
    });
</script>
