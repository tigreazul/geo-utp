@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Usuario</h5>
                        <span>Editar Usuario</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- card -->
                    <div class="card">
                        <!-- <div class="card-header"> -->
                            <!-- <h5>Editar de Usuario</h5> -->
                            <!-- <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
                        <!-- </div> -->
                        
                        <div class="card-block">
                            
                            <form id="main" method="POST" action="{{ route('admin.user_edit_data',['id' => $usuarios->id] ) }}" >
                                @csrf
                                @method('PATCH')
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Rol</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="rol" name="rol" required>
                                                    <option value="">[SELECCIONE]</option>
                                                    @foreach($roles as $rol)
                                                        <option value="{{ $rol->id }}" {{ ($rol->id == $usuarios->rol)?'selected':'' }} >{{ $rol->description }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Username</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="username" name="username" required value="{{ isset($usuarios->username)?$usuarios->username:''}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Nombre</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="nombre" name="nombre" required onkeypress="return soloLetras(event)" value="{{ isset($usuarios->nombre)?$usuarios->nombre:''}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Apellido Paterno</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="apaterno" name="apaterno" required onkeypress="return soloLetras(event)" value="{{ isset($usuarios->apellidoPaterno)?$usuarios->apellidoPaterno:''}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Apellido Materno</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="amaterno" name="amaterno" required onkeypress="return soloLetras(event)" value="{{ isset($usuarios->apellidoMaterno)?$usuarios->apellidoMaterno:''}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Fecha de Nacimiento</label>
                                            <div class="col-sm-10">
                                                <input type="date" class="form-control" id="fnacimiento" name="fnacimiento" required value="{{ isset($usuarios->fechaNacimiento)?date('Y-m-d', strtotime($usuarios->fechaNacimiento)):''}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-10">
                                                <input type="email" class="form-control" id="email" name="email" required value="{{ isset($usuarios->email)?$usuarios->email:'' }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Pais</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="form-stacked-select" name="pais" required>
                                                    <option value="">[SELECCIONE]</option>
                                                    @foreach($pais as $vpais)
                                                        <option value="{{ $vpais->id }}" {{ ($vpais->id == $usuarios->idpais)?'selected':'' }} >{{ $vpais->pais}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <!-- <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Sobre mi</label>
                                            <div class="col-sm-10">
                                                <textarea rows="5" cols="5" class="form-control" name="sobremi" placeholder="Sobre mi">{{ isset($usuarios->sobremi)?$usuarios->sobremi:'' }}</textarea>
                                            </div>
                                        </div> -->
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Contraseña</label>
                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="password" name="password">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <hr>
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-sm btn-primary m-b-0">
                                                <i class="fa fa-save"></i> Editar
                                        </button>
                                        <a href="{{ route('admin.user_list') }}" class="btn btn-info waves-effect waves-light btn-sm m-b-0"><i class="fa fa-reply"></i> Volver</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <script>
        // $('body').addClass('wysihtml5-supported');
    </script>
    <script src="{{ asset('js/titular.js') }}"></script>
@stop