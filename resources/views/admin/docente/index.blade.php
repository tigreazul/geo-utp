@extends('admin.layouts.admin')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-server bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Docente</h5>
                        <span>Administración de Docente</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class=" breadcrumb breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="#"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Dashboard</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- card -->
                    <div class="row">
                        <div class="card panel-modulo col " style="margin-right: 15px;">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col">
                                        <h5>Lista de Docente</h5>
                                    </div>
                                    <div class="col-42">
                                        <a href="{{ route('admin.docente_create') }}" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus-circle"></i> NUEVO DOCENTE</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-block">
                                @if (Session::has('message'))
                                    <div class="alert alert-success background-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <i class="icofont icofont-close-line-circled"></i>
                                        </button>
                                        {!! session('message') !!}
                                    </div>
                                @endif
                                <div class="dt-responsive table-responsive">
                                    <!-- lista de tablas -->
                                    <table class="table table-hover m-b-0 table-sm">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Username</th>
                                                <th>Nombre Completo</th>
                                                <th>Email</th>
                                                <th>Estado</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i = 1; @endphp
                                            @foreach($usuarios as $user)
                                                <tr class="table-verify selector-{{ $user->id }}">
                                                    <td scope="row">{{ $i }}</td>
                                                    <td>{{ $user->username }}</td>
                                                    <td>{{ $user->apellidoPaterno.' '.$user->apellidoMaterno.', '.$user->nombre }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td><label class="label label-{{ ($user->estado == 1)? 'success': 'danger' }}">{{ ($user->estado == 1)? 'ACTIVO': 'DESACTIVADO' }}</label></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                Acciones
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                <a class="dropdown-item" href="{{ route('admin.docente_edit', ['id'=>$user->id]) }}">
                                                                    <i class="fa fa-edit"></i>
                                                                    Editar
                                                                </a>
                                                                <a class="dropdown-item alert-delete-user" href="#" data-id="{{ $user->id }}" >
                                                                    <i class="fa fa-trash-alt"></i>
                                                                    Eliminar
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @php $i++; @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <!-- lista de tablas -->
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="md-overlay"></div>
                    <!-- card -->
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).on('click','.alert-delete-user',function(e){
            e.preventDefault();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var id = $(this).attr('data-id');
            swal({
                title: "Eliminar?",
                text: "Esta seguro que desea eliminar el registro?",
                type: "error",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Eliminar",
                cancelButtonText: "Cancelar",
            }, 
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: local.base+'/admin/docente/delete/'+id,
                        type: 'post',
                        dataType: 'json',
                        data: {_token: CSRF_TOKEN},
                        success: function(data){
                            if(data.status == true){
                                swal("Eliminado", "Se ha eliminado correctamente.", "success");
                                $('.selector-'+id).remove();
                            }else{
                                // swal('Ocurrio un error vuelva a intentarlo');
                                swal("Eliminado", "Ocurrio un error vuelva a intentarlo", "error");
                            }
                        }
                    });
                }
            });
        });
    </script>
@stop