
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ isset($title) ? 'STRATUM | '.$title: '.:: STRATUM ::.' }}</title>
    <link rel="shortcut icon" type="image/png" href="img/favicon.png" >
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('vendor/css/main.css') }}" />
    <script src="{{ asset('vendor/js/uikit.js') }}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        var local = {
            base: "{{ url('/') }}"
        } 
    </script>
</head>
<body>
    <div class="uk-grid-collapse" data-uk-grid>
        <div class="uk-width-1-2@m uk-padding-large uk-flex uk-flex-middle uk-flex-center" data-uk-height-viewport>
            <div class="uk-width-3-4@s">
                <div class="uk-text-center uk-margin-bottom">
                    <a class="uk-logo uk-text-success uk-text-bold" href="#">Curso</a>
                </div>
                <div class="uk-text-center uk-margin-medium-bottom">
                    <h1 class="uk-letter-spacing-small">Registrar</h1>
                </div>
                <!-- <div data-uk-grid class="uk-child-width-auto uk-grid-small uk-flex uk-flex-center uk-margin">
                    <div>
                        <a href="#" data-uk-icon="icon: facebook" class="uk-icon-button uk-icon-button-large facebook"></a>
                    </div>
                    <div>
                        <a href="#" data-uk-icon="icon: google-plus" class="uk-icon-button uk-icon-button-large google-plus"></a>
                    </div>
                    <div>
                        <a href="#" data-uk-icon="icon: linkedin" class="uk-icon-button uk-icon-button-large linkedin"></a>
                    </div>
                </div>    
                <div class="uk-text-center uk-margin">
                    <p class="uk-margin-remove">Or use your email for registration:</p>
                </div> -->
                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <div class="uk-width-1-1 uk-margin">
                        <label class="uk-form-label" for="name">Nombre Completo</label>
                        <input id="name" class="uk-input uk-form-large {{ $errors->has('name') ? ' is-invalid' : '' }}" type="text" name="name" placeholder="Juan Perez" value="{{ old('name') }}" required>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="uk-width-1-1 uk-margin">
                        <label class="uk-form-label" for="name">Username</label>
                        <input id="username" class="uk-input uk-form-large {{ $errors->has('username') ? ' is-invalid' : '' }}" type="text" name="username" placeholder="jperez" value="{{ old('username') }}" required>
                        @if ($errors->has('username'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="uk-width-1-1 uk-margin">
                        <label class="uk-form-label" for="email">Email</label>
                        <input id="email" class="uk-input uk-form-large {{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" name="email" placeholder="jperez@company.com" value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="uk-width-1-1 uk-margin">
                        <label class="uk-form-label" for="password">Password</label>
                        <input id="password" class="uk-input uk-form-large {{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" placeholder="Min 8 caracteres" minlength="8" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="uk-width-1-1 uk-margin">
                        <label class="uk-form-label" for="password">Confirmar Password</label>
                        <input id="password_confirmation" class="uk-input uk-form-large" type="password" name="password_confirmation" placeholder="Min 8 caracteres" minlength="8" required>
                    </div>
                    <div class="uk-width-1-1 uk-text-center">
                        <button type="submit" class="uk-button uk-button-primary uk-button-large">Registrar</button>
                    </div>

                    <div class="uk-width-1-1 uk-margin uk-text-center">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                @if (Session::has('message'))
                                <h5 style="text-transform:uppercase;text-align:center">{!! session('message') !!}</h5>
                                @endif
                            </div>
                        @endif
                        <p class="uk-text-small uk-margin-remove">Al registrarse, acepta nuestros <a class="uk-link-border" href="#">términos </a> de servicio.</p>
                    </div>
                </form>
            </div>
        </div>
        <div class="uk-width-1-2@m uk-padding-large uk-flex uk-flex-middle uk-flex-center uk-light uk-background-cover uk-background-norepeat uk-background-blend-overlay uk-background-primary" style="background-image: url(https://source.unsplash.com/nF8xhLMmg0c/680x1000);" data-uk-height-viewport>
            <div>
                <div class="uk-text-center">
                    <h2 class="uk-h1 uk-letter-spacing-small">Ingresar</h2>
                </div>
                <div class="uk-margin-top uk-margin-medium-bottom uk-text-center">
                    <p>Ya registrado, ingrese sus datos y comience a aprender hoy.</p>
                </div>
                <div class="uk-width-1-1 uk-text-center">
                    <a href="{{ url('login')}}" class="uk-button uk-button-success-outline uk-button-large">Entrar</a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
