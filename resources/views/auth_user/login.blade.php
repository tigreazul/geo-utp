@extends('admin.layouts.app')

@section('content')


<div class="row">
    <div class="col-sm-12">
        
        <form method="POST" action="{{ route('login') }}" class="md-float-material form-material">
            @csrf
            <div class="text-center">
                <img src="{{ asset('logo-white.png') }}" alt="logo">
            </div>
            <div class="auth-box card">
                <div class="card-block">
                    <div class="row m-b-20">
                        <div class="col-md-12">
                            <h3 class="text-center txt-primary">Inciar Sesión</h3>
                        </div>
                    </div>
                    <div class="form-group form-primary">
                        <label for="usuario">Usuario: </label>
                        <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-primary">
                        <label for="usuario">Password: </label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <span class="form-bar"></span>
                    </div>
                    <div class="row m-t-25 text-left">
                        <div class="col-12">
                        </div>
                    </div>
                    <div class="row m-t-30">
                        <div class="col-md-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    @if (Session::has('message'))
                                    <h5 style="text-transform:uppercase;text-align:center">{!! session('message') !!}</h5>
                                    @endif
                                </div>
                            @endif
                            
                            <button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">
                                Entrar
                            </button>
                        </div>
                    </div>
                    <!-- <p class="text-inverse text-left">Don't have an account?<a href="{{ route('register') }}"> <b>Register here </b></a>for free!</p> -->
                </div>
            </div>
        </form>
    </div>
</div>


@endsection
