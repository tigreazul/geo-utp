@extends('portal.layout.default')
@section('cabecera')
        @include('portal.layout.menu',['data'=>'3'])
@stop


@section('content')

<div class="uk-section" style="padding-top: 30px !important">
    <div class="uk-container">
        <ul class="uk-breadcrumb">
            <li class="uk-disabled"><a>Información</a></li>
            <li><span>Recarga de saldo</span></li>
        </ul>

        <div class="uk-grid">
            <div class="uk-width-1-4">
                @include('portal.layout.menu_perfil')
            </div>
            <div class="uk-width-3-4">
                <div uk-alert>
                    <h4>RECARGA DE SALDO</h4>
                </div>
                <div class="uk-card uk-card-default uk-card-body">
                    @if (Session::has('message'))
                        <div class="uk-alert-success" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <p>{!! session('message') !!}</p>
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="uk-alert-danger" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                    <form class="uk-form-stacked" method="post" action="{{ route('recarga') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-select">Monto</label>
                            <div class="uk-form-controls">
                                <select class="uk-select" id="form-stacked-select" name="saldos" required>
                                    <option value="">[SELECCIONE]</option>
                                    @foreach($montos as $monto)
                                        <option value="{{ $monto->id }}" >{{ $monto->montos}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-email">Voucher:</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-stacked-email" type="file" name="voucher" value="" required>
                            </div>
                        </div>
                        

                        <hr>
                        <button type="submit" class="uk-button uk-button-primary">Enviar</button>
                        <a href="{{ route('historia_recarga') }}" class="uk-button uk-button-default">Volver</a>

                    </form>
                </div>
                <!-- <div class="uk-panel uk-panel-box">3-4</div> -->
            </div>
        </div>

    </div>


</div>


@stop