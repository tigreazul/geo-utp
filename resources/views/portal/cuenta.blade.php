@extends('portal.layout.default')
@section('cabecera')
        @include('portal.layout.menu',['data'=>'3'])
@stop


@section('content')

<div class="uk-section" style="padding-top: 30px !important">
    <div class="uk-container">
        <ul class="uk-breadcrumb">
            <li class="uk-disabled"><a>Información</a></li>
            <li><span>Cuenta</span></li>
        </ul>

        <div class="uk-grid">
            <div class="uk-width-1-4">
                @include('portal.layout.menu_perfil')
            </div>
            <div class="uk-width-3-4">
                <div uk-alert>
                    <h4>Cuenta</h4>
                </div>
                <div class="uk-card uk-card-default uk-card-body">
                    @if (Session::has('message'))
                        <div class="uk-alert-success" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <p>{!! session('message') !!}</p>
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="uk-alert-danger" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class="uk-form-stacked" method="post" action="{{ route('edit_cuenta',['id' => $perfil->id] ) }}" >
                        @csrf
                        @method('PATCH')
                        <!-- <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-email">Email:</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-stacked-email" type="text" placeholder="jperez@correo.com">
                            </div>
                        </div> -->
                        
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-pass">Contraseña</label>
                            <div class="uk-inline" style="width:100%">
                                <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                                <input class="uk-input" type="password" name="password" id="form-stacked-pass" placeholder="Escribe la contraseña actual">
                            </div>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-inline" style="width:100%">
                                <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                                <input class="uk-input" type="password" id="form-stacked-pass" name="newpassword" placeholder="Escribe la contraseña nueva">
                            </div>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-inline" style="width:100%">
                                <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                                <input class="uk-input" type="password" id="form-stacked-pass" name="renewpassword" placeholder="Escribe la contraseña otra vez">
                            </div>
                        </div>
                        <hr>
                        <button class="uk-button uk-button-primary">Cambiar la contraseña</button>

                    </form>
                </div>
                <!-- <div class="uk-panel uk-panel-box">3-4</div> -->
            </div>
        </div>

    </div>


</div>


@stop