@extends('portal.layout.default')
@section('cabecera')
<header id="header" class="uk-background-cover uk-background-norepeat uk-background-center-center uk-background-blend-soft-light uk-background-primary" style="background-image: url({{ asset('vendor/img/working.jpg') }});">
    <video src="{{ asset('vendor/img/working.mp4') }}" data-uk-cover></video>
    <div class="uk-overlay uk-position-cover uk-overlay-video"></div>
    @include('portal.layout.menu')
    
	<div class="uk-container uk-container-large uk-light" data-uk-height-viewport="offset-top: true">
        <div data-uk-grid data-uk-height-viewport="offset-top: true">
            <div class="uk-header-left uk-section uk-visible@m uk-flex uk-flex-bottom">
                <div class="uk-text-xsmall uk-text-bold">
                    <a class="hvr-back" href="#about" data-uk-scroll="offset: 80"><span class="uk-margin-small-right" 
                        data-uk-icon="arrow-left"></span>Scroll down</a>
                </div>
            </div>
            <div class="uk-width-expand@m uk-section uk-flex uk-flex-column">
                <div class="uk-margin-auto-top uk-margin-auto-bottom">
                    <h1 class="uk-heading-medium uk-margin-remove-top uk-letter-spacing-xl" 
                        data-uk-scrollspy="cls: uk-animation-slide-bottom-medium; repeat: true"><mark>Aprenda</mark> habilidades con proyectos de <mark>práctica</mark>   y obtenga comentarios del <mark>tutor</mark>
                    </h1>
                    <form action="{{ route('buscador-curso') }}" method="post">
                        {{csrf_field()}}
                        <div class="uk-grid-collapse uk-width-3-4@m uk-margin-medium-top" data-uk-grid 
                            data-uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true">
                            <div class="uk-width-expand">
                                <input class="uk-input uk-form-large uk-border-remove-right" type="text" name="buscar" placeholder="Busca tu curso" required>
                            </div>
                            <div class="uk-width-auto">
                                <button type="submit" class="uk-button uk-button-large uk-button-success-outline">Buscar cursos</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="uk-margin-auto-top" 
                    data-uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 400; repeat: true">
                    <div class="uk-child-width-1-2@s uk-grid-large uk-margin-medium-top" data-uk-grid>
                        <div>
                            <h4 class="uk-margin-remove">Elija entre más de 15000 cursos.</h4>
                            <p class="uk-margin-xsmall-top uk-text-small uk-text-muted uk-text-bold">Recaptualice de manera distintiva las potencialidades escalables a través de servicios web escalables.</p>
                        </div>
                        <div>
                            <h4 class="uk-margin-remove">Estudia en línea a tu propio ritmo</h4>
                            <p class="uk-margin-xsmall-top uk-text-small uk-text-muted uk-text-bold">Actualice de manera asertiva modelos orientados a objetivos mientras que los servicios electrónicos de clase mundial.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-header-right uk-section uk-visible@m uk-flex uk-flex-right uk-flex-bottom">
                <div>
                    <ul class="uk-subnav uk-text-xsmall uk-text-bold">
                        <li><a class="uk-link-border" href="#" target="_blank">facebook</a></li>
                        <li><a class="uk-link-border" href="#" target="_blank">Youtube</a></li>
                        <li><a class="uk-link-border" href="#" target="_blank">instagram</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
@stop


@section('content')
<!-- Cursos -->
<div id="about" class="uk-section uk-section-muted uk-section-large">
    <div class="uk-container">
        <div class="uk-width-4-5@m">
            <h2 class="uk-heading-small">Acceso <mark>Ilimitado</mark> a miles de <mark>Cursos</mark></h2>
        </div>
        <div class="uk-child-width-1-2@s uk-child-width-1-3@m uk-margin-large-top uk-grid-match" data-uk-grid>
            @foreach($curso as $cur)
                <div>
                    <div class="uk-card uk-card-small uk-card-border">
                        <div class="uk-card-media-top uk-position-relative uk-light">
                            <img src="{{ asset('curso/'.$cur->ImagenCurso) }}" alt="{{ $cur->titulo }}">
                            <div class="uk-position-cover uk-overlay-xlight"></div>
                            <div class="uk-position-top-left">
                            <span class="uk-text-bold uk-text-price uk-text-small">S/. {{ $cur->precioPrincipal }}</span>
                            </div>
                            <div class="uk-position-top-right">
                                <a href="{{ route('detalle', ['curso'=>$cur->slug]) }}" class="uk-icon-button uk-like uk-position-z-index uk-position-relative" data-uk-icon="heart"></a>
                            </div>            
                        </div>
                        <div class="uk-card-body">
                            <h3 class="uk-card-title uk-margin-small-bottom">{{ $cur->titulo }}</h3>
                            <div class="uk-text-muted uk-text-small">{{ $cur->apellidoPaterno.' '.$cur->apellidoMaterno.' '.$cur->nombre }}</div>
                            <!-- <div class="uk-text-muted uk-text-small uk-rating uk-margin-small-top">
                                <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                                <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                                <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                                <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                                <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                                <span class="uk-margin-small-left uk-text-bold">5.0</span>
                                <span>(324)</span>
                            </div> -->
                        </div>
                        <a href="{{ route('detalle', ['curso'=>$cur->slug]) }}" class="uk-position-cover"></a>
                    </div>
                </div>
            @endforeach

        </div>
        <a href="{{ route('lista-cursos') }}" class="uk-button uk-button-success-outline uk-button-large uk-margin-medium-top">Ver cursos</a>
    </div>
</div>
<!-- End Cursos -->

<div class="uk-section uk-section-large uk-padding-remove-bottom" style="padding-top: 60px;">
    <div class="uk-container uk-container-small uk-text-center">
        <h2 class="uk-heading-small"><mark>Únete</mark> a los miles y aprende algo nuevo <mark>hoy</mark></h2>
        <a href="{{ url('register') }}" class="uk-button uk-button-primary uk-button-large uk-margin-medium-top">Registrar</a>
    </div>
    <div class="uk-child-width-1-5 uk-child-width-expand@m uk-margin-xlarge-top uk-grid-collapse" data-uk-grid> 
    </div>
</div>

<div class="uk-section uk-section-large uk-background-cover uk-background-norepeat uk-background-center-center uk-background-blend-soft-light uk-light uk-background-primary" style="background-image: url(https://source.unsplash.com/QckxruozjRg);">
  <div class="uk-container uk-container-large">
    <div class="uk-width-1-2@m">
      <h2 class="uk-heading-small uk-margin-remove">Qué quieres aprender <mark>hoy</mark></h2>
    </div>
  </div>
  <div class="uk-container uk-container-large uk-margin-large-top">
    <div class="uk-grid-large" data-uk-grid>
      <div class="uk-width-1-3@m">
        <p class="uk-text-large">Desarrolle profesionalmente ideas adhesivas con respecto a la convergencia frontal.
           Habilita intrínsecamente el enlace total 
          multifuncional y también paradigmas revolucionarios.</p>
        <a href="{{ route('lista-cursos') }}" class="uk-button uk-button-success-outline uk-button-large uk-margin-medium-top">Buscar tu curso</a>
      </div>
      <div class="uk-width-expand@m">

        <div data-uk-slider="sets: true">
          <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
            <div class="uk-slider-container">
              <div class="uk-position-absolute uk-slidenav-above">
                <a class="uk-slidenav-large uk-visible@m uk-text-success uk-margin-right" href="#" data-uk-slidenav-previous data-uk-slider-item="previous"></a>
                <a class="uk-slidenav-large uk-visible@m uk-text-success" href="#" data-uk-slidenav-next data-uk-slider-item="next"></a>
              </div>
              <ul class="uk-slider-items uk-child-width-1-3@s uk-grid-large">
                @foreach($categoria as $cat)
                <li>
                  <div class="uk-card uk-border-light-hover uk-card-small uk-height-small uk-inline uk-border-light-xlarge uk-flex uk-flex-column">
                    <div class="uk-card-body uk-margin-auto-top">
                      <h3 class="uk-card-title uk-margin-remove">{{ $cat->nombreCat }}</h3>
                      <!-- <p class="uk-text-small uk-text-demi-bold uk-margin-xsmall-top">185 courses</p> -->
                    </div>
                    <a href="#" class="uk-position-cover"></a>
                  </div>
                </li>
                @endforeach
              </ul>
            </div>
            <a class="uk-position-center-left uk-position-small uk-hidden@m" href="#" data-uk-slidenav-previous data-uk-slider-item="previous"></a>
            <a class="uk-position-center-right uk-position-small uk-hidden@m" href="#" data-uk-slidenav-next data-uk-slider-item="next"></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="uk-section uk-section-secondary">
	<div class="uk-container">
		<div class="uk-child-width-1-2@m uk-grid-large uk-flex uk-flex-middle" data-uk-grid>
			<div>
				<h3>Suscríbete a nuestro boletín para recibir actualizaciones y ofertas de productos.</h3>
			</div>
			<div>
				<div class="uk-grid-collapse" data-uk-grid>
					<div class="uk-width-expand">
						<input class="uk-input uk-form-large uk-border-remove-right" type="email" placeholder="Tu email">
					</div>
					<div class="uk-width-auto">
						<button class="uk-button uk-button-large uk-button-success-outline">Suscribete</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop