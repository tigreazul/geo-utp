@extends('portal.layout.default')
@section('cabecera')
<header id="header" class="uk-background-cover uk-background-norepeat uk-background-center-center uk-background-blend-soft-light uk-background-primary" style="background-image: url({{ asset('vendor/img/slider.jpg') }});">
	@include('portal.layout.menu')
	<div class="uk-container uk-container-large uk-light">
		<div data-uk-grid>
			<div class="uk-header-left uk-section uk-visible@m uk-flex uk-flex-bottom">
				<div class="uk-text-xsmall uk-text-bold">
					<a class="hvr-back" href="#courses" data-uk-scroll="offset: 80"><span class="uk-margin-small-right" 
						data-uk-icon="arrow-left"></span>Scroll down</a>
				</div>
			</div>
			<div class="uk-width-expand@m uk-section">
				<div class="uk-margin-top">          
          <div class="uk-width-4-5@m" data-uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true">
            <h1 class="uk-heading-medium uk-margin-remove-top uk-letter-spacing-xl">
            Miles de clases para <mark>explorar</mark> tu <mark>creatividad</mark> y hacer <mark>crecer</mark> tu carrera</h1>
            <a href="{{ url('register') }}" class="uk-button uk-button-large uk-button-success-outline uk-margin-medium-top">
            Comienza gratis</a>
					</div>
				</div>
			</div>
			<div class="uk-header-right uk-section uk-visible@m uk-flex uk-flex-right uk-flex-bottom">
				<div>
					<ul class="uk-subnav uk-text-xsmall uk-text-bold">
						<li><a class="uk-link-border" href="#" target="_blank">facebook</a></li>
						<li><a class="uk-link-border" href="#" target="_blank">twitter</a></li>
						<li><a class="uk-link-border" href="#" target="_blank">instagram</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>
@stop

@section('content')
<div id="courses" class="uk-section">
  <div class="uk-container">
        <h2 class="uk-text-center">Acceso ilimitado a muchos cursos.</h2>
        <ul class="uk-flex-center uk-margin-medium" data-uk-tab>
            <li><a href="#">Todos </a></li>
            <!-- <li><a href="#">Design</a></li>-->
        </ul>
        <ul class="uk-switcher">
            <li>
                <div class="uk-child-width-1-2@s uk-child-width-1-3@m uk-margin-large-top uk-grid-match" data-uk-grid>
                    @foreach($curso as $cur)
                        <div>
                            <div class="uk-card uk-card-small uk-card-border">
                                <div class="uk-card-media-top uk-position-relative uk-light">
                                    <img src="{{ asset('curso/'.$cur->ImagenCurso) }}" alt="{{ $cur->titulo }}">
                                    <div class="uk-position-cover uk-overlay-xlight"></div>
                                    <div class="uk-position-top-left">
                                    <span class="uk-text-bold uk-text-price uk-text-small">S/. {{ $cur->precioPrincipal }}</span>
                                    </div>
                                    <div class="uk-position-top-right">
                                        <!-- <a href="{{ route('detalle', ['curso'=>$cur->slug]) }}" class="uk-icon-button uk-like uk-position-z-index uk-position-relative" data-uk-icon="heart"></a> -->
                                    </div>            
                                </div>
                                <div class="uk-card-body">
                                    <h3 class="uk-card-title uk-margin-small-bottom">{{ $cur->titulo }}</h3>
                                    <div class="uk-text-muted uk-text-small">{{ $cur->apellidoPaterno.' '.$cur->apellidoMaterno.' '.$cur->nombre }}</div>
                                    <!-- <div class="uk-text-muted uk-text-small uk-rating uk-margin-small-top">
                                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                                        <span class="uk-margin-small-left uk-text-bold">5.0</span>
                                        <span>(214)</span>
                                    </div> -->
                                </div>
                                <a href="{{ route('detalle', ['curso'=>$cur->slug]) }}" class="uk-position-cover"></a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </li>
        </ul>
        <!-- <a href="#" class="uk-button uk-button-success-outline uk-margin-large-top">Show more classes</a> -->
  </div>
</div>


<div class="uk-section uk-section-secondary">
	<div class="uk-container">
		<div class="uk-child-width-1-2@m uk-grid-large uk-flex uk-flex-middle" data-uk-grid>
			<div>
				<h3>Suscríbase a nuestro boletín para recibir actualizaciones y ofertas de productos.</h3>
			</div>
			<div>
				<div class="uk-grid-collapse" data-uk-grid>
					<div class="uk-width-expand">
						<input class="uk-input uk-form-large uk-border-remove-right" type="email" placeholder="Tu correo electronico">
					</div>
					<div class="uk-width-auto">
						<button class="uk-button uk-button-large uk-button-success-outline">Suscribete</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop