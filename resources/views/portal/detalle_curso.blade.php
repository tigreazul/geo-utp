@extends('portal.layout.default')
@section('cabecera')
<header id="header" class="uk-background-cover uk-background-norepeat uk-background-center-center uk-background-blend-soft-light uk-background-primary" style="background-image: url({{ asset('vendor/img/slider.jpg') }});">
    @include('portal.layout.menu')
	<div class="uk-container uk-container-large uk-light" data-uk-height-viewport="offset-top: true">
		<div data-uk-grid data-uk-height-viewport="offset-top: true">
			<div class="uk-header-left uk-section uk-visible@m uk-flex uk-flex-bottom">
				<div class="uk-text-xsmall uk-text-bold">
					<a class="hvr-back" href="#course" data-uk-scroll="offset: 80"><span class="uk-margin-small-right" data-uk-icon="arrow-left"></span>Scroll down</a>
				</div>
            </div>
            <div class="uk-width-expand@m uk-section">
                <div class="uk-margin-top">          
                    <div class="uk-grid-large" data-uk-grid data-uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 200; repeat: true">
                        <div class="uk-width-1-2@m">
                            <h1 class="uk-heading-medium uk-margin-remove-top uk-letter-spacing-xl">{{ $curso->titulo }}</h1>
                            @if(!$cursosInscrito['status'])
                                <a class="uk-button uk-button-large uk-button-success-outline curso_detalle" data-slug="{{$curso->slug}}" uk-toggle="target: #modal-close-default">Unete Ahora</a>
                            @endif
                        </div>
                        <div class="uk-width-1-2@m uk-text-large uk-flex uk-flex-middle"><div>
                        <p>Completamente malla interactiva de preparación web a través de estrategias de crecimiento de misión crítica. Mantenga sin problemas comunidades granulares a través de nichos multiplataforma.</p>
                        <p>Libere holísticamente a los usuarios de extremo a extremo después de los canales de alto impacto a largo plazo. Sintetice globalmente el ancho de banda proactivo con contenido interactivo.</p>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="modal-close-default" uk-modal>
            <div class="uk-modal-dialog uk-modal-body">
                <button class="uk-modal-close-default" type="button" uk-close></button>

                <div style="display:none" class="loader">
                    <div class="uk-child-width-1-3@s" uk-grid>
                        <div>
                            <div class="uk-height-medium uk-flex uk-flex-center uk-flex-middle">
                                <span class="uk-position-center" uk-spinner="ratio: 3"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="contenido_curso">
                    @if( !is_null(session()->get('permisos')) )
                        @include('portal.gadgets.inscripcion_curso')
                    @else
                        <div class="uk-child-width-1-2@m uk-grid-small uk-grid-match" uk-grid>
                            <div>
                                <div class="uk-card uk-card-default uk-card-body">
                                    <h3 class="uk-card-title">Ingresar</h3>
                                    <p>Ya registrado, ingrese sus datos y comience a aprender hoy.</p>
                                    <a href="{{ url('login') }}" class="uk-button uk-button-primary uk-button-large">Entrar</a>
                                </div>
                            </div>
                            <div>
                                <div class="uk-card uk-card-primary uk-card-body">
                                    <h3 class="uk-card-title">Hola, únete a nosotros</h3>
                                    <p>Ingrese sus datos personales y únase a la comunidad de aprendizaje.</p>
                                    <a href="{{ url('register') }}" class="uk-button uk-button-success-outline uk-button-large">Regístrate</a>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                
                
            </div>
        </div>

        
	</div>
    <div class="uk-header-right uk-section uk-visible@m uk-flex uk-flex-right uk-flex-bottom">
        <div>
            <ul class="uk-subnav uk-text-xsmall uk-text-bold">
                <li><a class="uk-link-border" href="#" target="_blank">facebook</a></li>
                <li><a class="uk-link-border" href="#" target="_blank">twitter</a></li>
                <li><a class="uk-link-border" href="#" target="_blank">instagram</a></li>
            </ul>
        </div>
    </div>
</header>
@stop

@section('content')
<div id="course" class="uk-section" style="padding-top: 0;">
    <div class="uk-container uk-margin-pricing-offset">
        <div class="uk-grid-large" data-uk-grid>
            <div class="uk-width-expand@m">
                <div class="uk-article">
                    <h2>Descripción</h2>
                    {!! $curso->descripcion !!}
                    <h2 class="uk-margin-large-top">Content</h2>
                    <ul class="uk-margin-top" data-uk-accordion="multiple: true">
                        @php $i = 0; @endphp
                        @foreach($detcurso as $det)
                            <li class="{{ $i == 0 ? 'uk-open': ''}}">
                                <a class="uk-accordion-title" href="#">{{ $det['contenido'] }}<span class="uk-align-right uk-margin-remove-bottom"></span></a>
                                <div class="uk-accordion-content">
                                    <table class="uk-table uk-table-justify uk-table-middle uk-table-divider">
                                        <tbody>
                                            @foreach($det['temas'] as $tema)
                                                <tr class="{{ ($cursosInscrito['status']) ? 'uk-text-primary' : ($tema->Privado == 1 ? 'uk-text-muted': 'uk-text-primary') }}">
                                                    <td class="uk-table-expand">
                                                        <span class="uk-margin-small-right" data-uk-icon="play-circle"></span>
                                                        @if($cursosInscrito['status'])
                                                            <a href="{{ route('videos_curso',['tema'=>$tema->idTema,'curso'=>$curso->idCurso]) }}" >{{ $tema->Titulo }}</a>
                                                        @else
                                                            @if( $tema->Privado == 1 )
                                                                {{ $tema->Titulo }}
                                                            @else
                                                                <a href="#" class="modal-course" data-code="{{ $tema->idTema }}">{{ $tema->Titulo }}</a>
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td><span data-uk-icon="{{ ($cursosInscrito['status']) ? 'unlock' : ($tema->Privado == 1 ? 'lock': 'unlock') }}"></span></td>
                                                    <td class="uk-table-shrink">{{ $tema->Duracion }}</td>
                                                </tr>
                                            @endforeach                  
                                        </tbody>
                                    </table>
                                </div>
                            </li>
                            @php $i++; @endphp
                        @endforeach
                    </ul>
                    <h2 class="uk-margin-large-top">Requerimiento</h2>
                    {!! $curso->requerimiento !!}
                </div>
            </div>
            <div class="uk-width-1-3@m">
                <div>
                    <div>
                        <h3>Este curso incluye</h3>
                        <ul class="uk-list uk-margin-small-top">
                            <li><span class="uk-margin-small-right" data-uk-icon="clock"></span>10 horas de video a pedido</li>
                            <li><span class="uk-margin-small-right" data-uk-icon="unlock"></span>Acceso total de por vida</li>
                            <li><span class="uk-margin-small-right" data-uk-icon="tablet"></span>Acceso en móvil</li>
                            <!-- <li><span class="uk-margin-small-right" data-uk-icon="file-text"></span>Certificado de finalización</li> -->
                            <li><span class="uk-margin-small-right" data-uk-icon="file-pdf"></span>Descargas de hojas de trabajo</li>
                            <li><span class="uk-margin-small-right" data-uk-icon="question"></span>Preguntas respondidas</li>
                            <li><span class="uk-margin-small-right" data-uk-icon="future"></span>Actualizaciones del curso</li>
                        </ul>
                    </div>			
                    <h3 class="uk-margin-large-top">Tags</h3>
                    <div data-uk-margin>
                        @foreach($categoria as $cat)
                            <a class="uk-display-inline-block" href="#"><span class="uk-label uk-label-light">{{ $cat->nombreCat }}</span></a>
                        @endforeach        
                    </div>
                    <h3 class="uk-margin-large-top">Compartir Curso</h3>
                    <div class="uk-margin">
                        <div data-uk-grid class="uk-child-width-auto uk-grid-small">
                            <div>
                                <a href="#" data-uk-icon="icon: facebook" class="uk-icon-button facebook" target="_blank"></a>
                            </div>
                            <div>
                                <a href="#" data-uk-icon="icon: linkedin" class="uk-icon-button linkedin" target="_blank"></a>
                            </div>
                            <div>
                                <a href="#" data-uk-icon="icon: twitter" class="uk-icon-button twitter" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="lesson" class="uk-flex-top uk-modal-container" data-uk-modal>
    <link rel="stylesheet" href="https://cdn.plyr.io/3.5.10/plyr.css" />
	<div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical uk-background-muted">
		<button class="uk-modal-close-outside" type="button" data-uk-close></button>
        <h2 class="uk-modal-title"></h2>
        <div>
            <div class="container" id="player-conteiner">
                <video id="player" controls></video>
            </div>
        </div><div class="uk-margin-top uk-article"></div>    
    </div>
</div>

<div class="uk-section uk-section-muted" style="padding-bottom:0px">
    <div class="uk-container" style="padding-bottom: 50px;">
        <h2>Otros cursos que te pueden gustar</h2>
        <div class="uk-child-width-1-2@s uk-child-width-1-3@m uk-margin-large-top" data-uk-grid>
        <!-- <div>
            <div class="uk-card uk-card-small uk-card-border">
                <div class="uk-card-media-top uk-position-relative uk-light">
                    <img src="https://source.unsplash.com/gMsnXqILjp4/640x400" alt="Course Title">
                    <div class="uk-position-cover uk-overlay-xlight"></div>
                    <div class="uk-position-top-left">
                        <span class="uk-text-bold uk-text-price uk-text-small">$27.00</span>
                    </div>
                    <div class="uk-position-top-right">
                        <a href="#" class="uk-icon-button uk-like uk-position-z-index uk-position-relative" data-uk-icon="heart"></a>
                    </div>            
                </div>

                <div class="uk-card-body">
                    <h3 class="uk-card-title uk-margin-small-bottom">Business Presentation Course</h3>
                    <div class="uk-text-muted uk-text-small">Thomas Haller</div>
                    <div class="uk-text-muted uk-text-small uk-rating uk-margin-small-top">
                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                        <span class="uk-margin-small-left uk-text-bold">5.0</span>
                        <span>(324)</span>
                    </div>
                </div>
                <a href="course.html" class="uk-position-cover"></a>
            </div>
        </div> -->

        @foreach($listCurso as $cur)
            <div>
                <div class="uk-card uk-card-small uk-card-border">
                    <div class="uk-card-media-top uk-position-relative uk-light">
                        <img src="{{ asset('curso/'.$cur->ImagenCurso) }}" alt="{{ $cur->titulo }}">
                        <div class="uk-position-cover uk-overlay-xlight"></div>
                        <div class="uk-position-top-left">
                        <span class="uk-text-bold uk-text-price uk-text-small">S/. {{ $cur->precioPrincipal }}</span>
                        </div>
                        <div class="uk-position-top-right">
                            <a href="{{ route('detalle', ['curso'=>$cur->slug]) }}" class="uk-icon-button uk-like uk-position-z-index uk-position-relative" data-uk-icon="heart"></a>
                        </div>            
                    </div>
                    <div class="uk-card-body">
                        <h3 class="uk-card-title uk-margin-small-bottom">{{ $cur->titulo }}</h3>
                        <div class="uk-text-muted uk-text-small">{{ $cur->apellidoPaterno.' '.$cur->apellidoMaterno.' '.$cur->nombre }}</div>
                    </div>
                    <a href="{{ route('detalle', ['curso'=>$cur->slug]) }}" class="uk-position-cover"></a>
                </div>
            </div>
        @endforeach
    </div>
</div>

<script src="https://cdn.plyr.io/3.5.10/plyr.polyfilled.js"></script>
<script src="{{ asset('vendor/js/curso.js?v=1020') }}"></script>

@stop