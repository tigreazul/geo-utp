@extends('portal.layout.default')
@section('cabecera')
        @include('portal.layout.menu',['data'=>'3'])
@stop


@section('content')

<div class="uk-section" style="padding-top: 30px !important">
    <div class="uk-container">
        <ul class="uk-breadcrumb">
            <li class="uk-disabled"><a>Información</a></li>
            <li><span>Mis Cursos</span></li>
        </ul>

        <div class="uk-grid">
            <div class="uk-width-1-4">
                @include('portal.layout.menu_perfil')
            </div>
            <div class="uk-width-3-4">
                <div uk-alert>
                    <h4>Mis Cursos</h4>
                </div>
                <div class="uk-card uk-card-default uk-card-body">
                    
                    <div class="uk-overflow-auto">
                        <table class="uk-table uk-table-hover uk-table-middle uk-table-divider">
                            <thead>
                                <tr>
                                    <th class="uk-table-shrink" colspan="2">Cursos</th>
                                    <th class="uk-width-small">Fecha Registro</th>
                                    <th class="uk-table-shrink uk-text-nowrap"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($cursos as $cur)
                                    <tr>
                                        <td><img class="uk-preserve-width" src="{{ asset('curso/'.$cur->ImagenCurso) }}" width="40" alt=""></td>
                                        <td class="uk-table-link">
                                            <a class="uk-link-reset" href="{{ route('detalle', ['curso'=>$cur->slug]) }}">{{$cur->titulo}}</a>
                                        </td>
                                        <td class="uk-text-truncate">{{ \Carbon\Carbon::parse($cur->fechaInscripcion)->format('d-m-Y') }}</td>
                                        <td><a class="uk-button uk-button-default" href="{{ route('detalle', ['curso'=>$cur->slug]) }}">Suscrito</a></td>
                                    </tr>
                                @empty
                                    <div class="uk-alert-primary" uk-alert>
                                        <p>No tienes cursos registrados</p>
                                    </div>
                                @endforelse
                            </tbody>
                        </table>
                    </div>

                </div>
                <!-- <div class="uk-panel uk-panel-box">3-4</div> -->
            </div>
        </div>

    </div>


</div>


@stop