@extends('portal.layout.default')
@section('cabecera')
        @include('portal.layout.menu',['data'=>'3'])
@stop


@section('content')

<div class="uk-section" style="padding-top: 30px !important">
    <div class="uk-container">
        <ul class="uk-breadcrumb">
            <li class="uk-disabled"><a>Solicitudes</a></li>
            <li><span>Solicitud de reporte</span></li>
        </ul>

        <div class="uk-grid">
            <div class="uk-width-1-4">
                @include('portal.layout.menu_perfil')
            </div>
            <div class="uk-width-3-4">
                <div uk-alert>
                    <h4>SOLICITUD DE REPORTES</h4>
                </div>
                <div class="uk-card uk-card-default uk-card-body">
                    @if (Session::has('message'))
                        <div class="uk-alert-danger" uk-alert>
                            <!-- <a class="uk-alert-close" uk-close></a> -->
                            <p>{!! session('message') !!}</p>
                        </div>
                    @endif
                    @if (Session::has('messageOk'))
                        <div class="uk-alert-success" uk-alert>
                            <!-- <a class="uk-alert-close" uk-close></a> -->
                            <p>{!! session('messageOk') !!}</p>
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="uk-alert-danger" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="uk-child-width-1-2@m uk-grid-small uk-grid-match" uk-grid>
                        <div>
                            <div class="uk-card uk-card-primary uk-card-body">
                                <a href="{{ route('solicitar_mi_reporte') }}">
                                    <h3 class="uk-card-title">Mi Reporte</h3>
                                    <p>Se solicitará el reporte propio.</p>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="uk-card uk-card-secondary uk-card-body">
                                <a href="{{ route('solicitar_reporte_terceros') }}">
                                    <h3 class="uk-card-title">Reporte de terceros</h3>
                                    <p>Se solicita el reporte de personas terceras.</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


</div>


@stop