@extends('portal.layout.default')
@section('cabecera')
        @include('portal.layout.menu',['data'=>'3'])
@stop


@section('content')

<div class="uk-section" style="padding-top: 30px !important">
    <div class="uk-container">
        <ul class="uk-breadcrumb">
            <li class="uk-disabled"><a>Solicitudes</a></li>
            <li class="uk-disabled"><span>Solicitud de reporte</span></li>
            <li><span>Mi Reporte</span></li>
        </ul>

        <div class="uk-grid">
            <div class="uk-width-1-4">
                @include('portal.layout.menu_perfil')
            </div>
            <div class="uk-width-3-4">
                <div uk-alert>
                    <h4>MI REPORTE</h4>
                </div>
                <div class="uk-card uk-card-default uk-card-body">
                    @if (Session::has('message'))
                        <div class="uk-alert-success" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <p>{!! session('message') !!}</p>
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="uk-alert-danger" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class="uk-form-stacked" method="post" action="{{ route('consulta_reporte' ) }}" >
                        @csrf
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-text">Comprobante:</label>
                            <div class="uk-form-controls">
                                <select class="uk-select" id="form-stacked-select" name="tipoComprobante" required>
                                    <option value="">[SELECCIONE]</option>
                                    <option value="BOL" >BOLETA</option>
                                    <option value="FAC" >FACTURA</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-text">Numero de documento:</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-stacked-text" name="nroDocumentoComprobante" type="text" value="" required>
                            </div>
                        </div>
                        <hr>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-text">Tipo Consulta:</label>
                            <div class="uk-form-controls">
                                <select class="uk-select" id="form-stacked-select" name="tipoConsulta" required>
                                    <option value="">[SELECCIONE]</option>
                                    <option value="R" >Resumido</option>
                                    <option value="D" >Detallado</option>
                                    <option value="B" >Basico</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-text">Tipo Documento:</label>
                            <div class="uk-form-controls">
                                <select class="uk-select" id="form-stacked-select" name="tdocumento" >
                                    <option value="">[SELECCIONE]</option>
                                    @foreach($documento as $tipo)
                                        <option value="{{ $tipo->Valor }}"  {{ ($tipo->Valor == $perfil->tipo_documento)?'selected':'' }} >{{ $tipo->Descripcion}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-text">Numero de documento:</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-stacked-text" name="nroDocumento" type="text" value="{{ $perfil->nro_documento }}" >
                                <input name="tipoDocumentoPara" type="hidden" value="{{ $perfil->tipo_documento }}">
                                <input name="nroDocumentoPara" type="hidden" value="{{ $perfil->nro_documento }}">
                                
                            </div>
                        </div>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-text">Telefono:</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-stacked-text" name="telefono" type="text" value="{{ $perfil->telefono }}" >
                            </div>
                        </div>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-email">Email:</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-stacked-email" type="text" name="email" placeholder="jperez@correo.com" value="{{ $perfil->email }}" >
                            </div>
                        </div>
                        <hr>
                        <button type="submit" class="uk-button uk-button-primary">Consultar</button>
                    </form>
                    
                </div>
            </div>
        </div>

    </div>


</div>


@stop
