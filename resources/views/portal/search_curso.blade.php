@extends('portal.layout.default')
@section('cabecera')
@include('portal.layout.menu',['data'=>'1'])
@stop


@section('content')

<div class="uk-section">
  <div class="uk-container">
    <div class="uk-child-width-1-2@m" data-uk-grid>
      <div>
        <form class="uk-search uk-search-default uk-width-1-1" action="{{ route('buscador-curso') }}" method="post">
            {{csrf_field()}}
          <div class="uk-grid-collapse" data-uk-grid>
            <div class="uk-width-expand">
              <input class="uk-input uk-form-large uk-border-remove-right uk-border-success" name="buscar" type="search" placeholder="Buscar..." required>
            </div>
            <div class="uk-width-auto">
              <button type="submit" class="uk-button uk-button-large uk-button-success-outline">Buscar</button>
            </div>
          </div>
        </form>
      </div>
      <!-- <div>
        <div class="uk-grid-small uk-child-width-auto@m uk-flex uk-flex-right" data-uk-grid>
            <div>
                <select class="uk-select uk-select-light">
                    <option>Any Type</option>
                    <option>Online</option>
                    <option>School</option>
                </select>
            </div>
            <div>
                <select class="uk-select uk-select-light">
                <option>Any Category</option>
                <option>Web Design</option>
                <option>Marketing</option>
                <option>Accounting</option>
                <option>Business</option>
                <option>Design</option>
                </select>
            </div>
        </div>
      </div> -->
    </div>

    <div class="uk-child-width-1-2@s uk-child-width-1-3@m uk-grid-match uk-margin-medium-top" data-uk-grid>
        @forelse($curso as $cur)
            <div>
                <div class="uk-card uk-card-small uk-card-border">
                <div class="uk-card-media-top uk-position-relative uk-light">
                    <img src="{{ asset('curso/'.$cur->ImagenCurso) }}" alt="{{ $cur->titulo }}">
                    <div class="uk-position-cover uk-overlay-xlight"></div>
                    <div class="uk-position-top-left">
                    <span class="uk-text-bold uk-text-price uk-text-small">S/. {{ $cur->precioPrincipal }}</span>
                    </div>
                    <div class="uk-position-top-right">
                        <!-- <a href="{{ route('detalle', ['curso'=>$cur->slug]) }}" class="uk-icon-button uk-like uk-position-z-index uk-position-relative" data-uk-icon="heart"></a> -->
                    </div>            
                </div>
                <div class="uk-card-body">
                    <h3 class="uk-card-title uk-margin-small-bottom">{{ $cur->titulo }}</h3>
                    <div class="uk-text-muted uk-text-small">{{ $cur->apellidoPaterno.' '.$cur->apellidoMaterno.' '.$cur->nombre }}</div>
                    <!-- <div class="uk-text-muted uk-text-small uk-rating uk-margin-small-top">
                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.75"></span>
                        <span class="uk-margin-small-left uk-text-bold">5.0</span>
                        <span>(214)</span>
                    </div> -->
                </div>
                <a href="{{ route('detalle', ['curso'=>$cur->slug]) }}" class="uk-position-cover"></a>
                </div>
            </div>
        @empty
            <p>NO EXISTEN CURSOS</p>
        @endforelse
    </div>
    <!-- <div class="uk-text-center uk-margin-large-top">
      <a href="#" class="uk-button uk-button-success-outline"><div class="uk-margin-small-right" data-uk-spinner="ratio: 0.6"></div>Load more</a>
    </div>     -->
  </div>
</div>
@stop