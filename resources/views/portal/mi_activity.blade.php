@extends('portal.layout.default')
@section('cabecera')
        @include('portal.layout.menu',['data'=>'3'])
@stop

@section('content')

<div class="uk-section" style="padding-top: 30px !important">
    <div class="uk-container">
        <ul class="uk-breadcrumb">
            <li class="uk-disabled"><a>Actividades</a></li>
            <li><span>Mis Actividades</span></li>
        </ul>

        <div class="uk-grid">
            <div class="uk-width-1-4">
                @include('portal.layout.menu_perfil')
            </div>
            <div class="uk-width-3-4">
                <div uk-alert>
                    <h4>Mis Actividades</h4>
                </div>
                <div class="uk-card uk-card-default uk-card-body">
                    <div class="uk-overflow-auto">
                        <table class="uk-table uk-table-hover uk-table-middle uk-table-divider uk-table-small">
                            <thead>
                                <tr>
                                    <th class="uk-table-shrink" >#</th>
                                    <th class="uk-table-shrink" >Actividad</th>
                                    <th class="uk-table-shrink uk-text-nowrap">Estado</th>
                                    <th class="uk-width-small">Fecha Conclusión</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!empty($activity))
                                @php $i = 1; @endphp
                                @foreach($activity as $activities)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ $activities->descripcion }}</td>
                                        <td>
                                            <div class="uk-alert-{{ ($activities->concluido == 1)? 'success': 'danger' }}" uk-alert>
                                                <p>{{ ($activities->concluido == 1)? 'CONCLUIDO': 'PENDIENTE' }}</p>
                                            </div>
                                        </td>
                                        <td style="text-align:center">
                                            @if($activities->concluido == 0)
                                                <button class="uk-button uk-button-primary uk-button-small" id="terminar">Concluir</button>
                                            @else 
                                                {{ \Carbon\Carbon::parse($activities->fechaConclusion)->format('d-m-Y H:i:s') }}
                                            @endif
                                        </td>
                                    </tr>
                                    @php $i++; @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">
                                        <div class="uk-alert-primary" uk-alert>
                                            <p>No tienes actividades asignadas</p>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            <input type="hidden" name="latitud" id="lat">
                                    <input type="hidden" name="longitud" id="lon">
                            </tbody>
                        </table>

                    </div>

                </div>
                <!-- <div class="uk-panel uk-panel-box">3-4</div> -->
            </div>
        </div>

    </div>


</div>






<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" />
<script>
    function success(position) {

        var lati  = position.coords.latitude.toFixed(8);
        var long = position.coords.longitude.toFixed(8);

        document.getElementById('lat').value = lati;
        document.getElementById('lon').value = long;
    };

    function error() {
        // swal("Unable to retrieve your location");
        mapa('','');
    };
    navigator.geolocation.getCurrentPosition(success, error);



    $(document).on('click','#terminar',function(e){
        e.preventDefault();
        $.ajax({
            url : 'http://localhost:8080/actividades',
            data : { id : 123 },
            type : 'GET',
            dataType : 'json',
            success : function(json) {
                $('<h1/>').text(json.title).appendTo('body');
                $('<div class="content"/>')
                    .html(json.html).appendTo('body');
            },
            error : function(xhr, status) {
                alert('Disculpe, existió un problema');
            },
            complete : function(xhr, status) {
                // alert('Petición realizada');
            }
        });

    });
</script>

@stop