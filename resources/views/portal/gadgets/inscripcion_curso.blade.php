<h2 class="uk-modal-title title-panel"></h2>
<div style="display:none" class="slug-panel"></div>
<p class="uk-text-center">
    <button class="uk-button uk-button-danger" id="inscribir_curso" type="button">
        <span class="uk-margin-small-right" uk-icon="icon: database; ratio: 1"></span>
        Incribirse al curso
    </button>
    <div class="contenido-panel"></div>
</p>