@extends('portal.layout.default')
@section('cabecera')
        @include('portal.layout.menu',['data'=>'3'])
@stop


@section('content')

<div class="uk-section" style="padding-top: 30px !important">
    <div class="uk-container">
        <ul class="uk-breadcrumb">
            <li class="uk-disabled"><a>Información</a></li>
            <li><span>Mis datos</span></li>
        </ul>

        <div class="uk-grid">
            <div class="uk-width-1-4">
                @include('portal.layout.menu_perfil')
            </div>
            <div class="uk-width-3-4">
                <div uk-alert>
                    <h4>MIS DATOS</h4>
                </div>
                <div class="uk-card uk-card-default uk-card-body">
                    @if (Session::has('message'))
                        <div class="uk-alert-success" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <p>{!! session('message') !!}</p>
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="uk-alert-danger" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                    <form class="uk-form-stacked" method="post" action="{{ route('edit_profile',['id' => $perfil->id] ) }}" >
                        @csrf
                        @method('PATCH')
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-text">Usuario:</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-stacked-text" type="text" value="{{ $perfil->username }}" placeholder="@usuario" disabled required>
                            </div>
                        </div>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-text">Nombre Completo:</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-stacked-text" name="nombre" type="text" placeholder="Juan Perez" value="{{ $perfil->nombre }}" required>
                            </div>
                        </div>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-text">Apellido Paterno:</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-stacked-text" type="text" name="apaterno" placeholder="Perez" value="{{ $perfil->apellidoPaterno }}" required>
                            </div>
                        </div>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-text">Apellido Materno:</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-stacked-text" type="text" name="amaterno" placeholder="Perez" value="{{ $perfil->apellidoMaterno }}" required>
                            </div>
                        </div>
                        
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-email">Email:</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-stacked-email" type="text" name="email" placeholder="jperez@correo.com" value="{{ $perfil->email }}" required>
                            </div>
                        </div>
                        

                        <hr>
                        <button type="submit" class="uk-button uk-button-primary">Guardar</button>

                    </form>
                </div>
                <!-- <div class="uk-panel uk-panel-box">3-4</div> -->
            </div>
        </div>

    </div>


</div>


@stop