@if(!isset($footer))
<footer class="uk-border-dark-top">
	<!-- <div class="uk-section uk-section-secondary">
		<div class="uk-container uk-h6 uk-margin-top">
			<div class="uk-child-width-1-2@s uk-child-width-1-4@m uk-grid-large" data-uk-grid>
				<div>
					<a href="#" class="uk-logo">curso</a>
				</div>
				<div>
					<ul class="uk-list uk-list-large">
						<li><a class="uk-link-border" href="#">Courses</a></li>
						<li><a class="uk-link-border" href="#">Developers</a></li>
						<li><a class="uk-link-border" href="#">Workshops</a></li>
						<li><a class="uk-link-border" href="#">Contact</a></li>
					</ul>
				</div>
				<div>
					<ul class="uk-list uk-list-large">
						<li><a class="uk-link-border" href="#">Our Initiatives</a></li>
						<li><a class="uk-link-border" href="#">Giving Back</a></li>
						<li><a class="uk-link-border" href="#">Communities</a></li>
						<li><a class="uk-link-border" href="#">Youth Program</a></li>
					</ul>
				</div>
				<div>
					<ul class="uk-list uk-list-large">
						<li><a class="uk-link-border" href="#">Terms of use</a></li>
						<li><a class="uk-link-border" href="#">Privacy policy</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div> -->
	<div class="uk-section uk-section-secondary" style="padding: 20px;">
		<div class="uk-container uk-h6">	
			<div class="uk-child-width-1-2@m uk-grid-large" data-uk-grid>
				<div class="uk-flex-first@m">
					<p>Derechos reservados {{ date('Y') }}</p>
				</div>
			</div>
		</div>
	</div>
</footer>
@endif