<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ isset($title) ? 'STRATUM | '.$title: '.:: STRATUM ::.' }}</title>
    <link rel="shortcut icon" type="image/png" href="img/favicon.png" >
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('vendor/css/uikit.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/css/main.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/css/default.css') }}" />

    <!-- <script src="{{ asset('vendor/js/uikit.js') }}"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.3.7/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.3.7/dist/js/uikit-icons.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        var local = {
            base: "{{ url('/') }}"
        } 
    </script>
</head>

<body>

<!-- header -->
@yield('cabecera')
<!-- end header -->

@yield('content')
<!-- Footer -->
@include('portal.layout.footer')
<!-- end footer -->

<div id="offcanvas" data-uk-offcanvas="flip: true; overlay: true">
    <div class="uk-offcanvas-bar">
        <a class="uk-logo" href="index.html">curso</a>
        <button class="uk-offcanvas-close" type="button" data-uk-close="ratio: 1.2"></button>
        <ul class="uk-nav uk-nav-primary uk-nav-offcanvas uk-margin-medium-top uk-text-center">
            <li class="uk-active"><a href="{{ route('inicio') }}">Inicio</a></li>
        </ul>
        <div class="uk-margin-medium-top">
            @if( isset(Auth::user()->name) )
                <ul class="uk-nav uk-nav-primary uk-nav-offcanvas uk-margin-medium-top uk-text-center">
                    <li class=""><a href="{{ url('profile') }}">Mi Perfil</a></li>
                    
                    <li class="uk-nav-divider"></li>
                    <li><a href="{{ route('salir') }}" class="text-danger">Salir</a></li>
                </ul>
            @else
                <a class="uk-button uk-width-1-1 uk-button-default" href="{{ url('/login') }}">Entrar</a>
            @endif
        </div>
        <div class="uk-margin-medium-top uk-text-center">
            <div data-uk-grid class="uk-child-width-auto uk-grid-small uk-flex-center">
                <div>
                    <a href="https://www.facebook.com/" data-uk-icon="icon: facebook" class="uk-icon-link" target="_blank"></a>
                </div>
                <div>
                    <a href="https://www.instagram.com/" data-uk-icon="icon: instagram" class="uk-icon-link" target="_blank"></a>
                </div>
                <div>
                    <a href="https://vimeo.com/" data-uk-icon="icon: vimeo" class="uk-icon-link" target="_blank"></a>
                </div>
            </div>
        </div>
    </div>
</div>

</body>

</html>