<div class="uk-card uk-card-default uk-card-body">
    <h4>Actividades</h4>
    <ul class="uk-list uk-list-large uk-list-divider">
        <li><a href="{{ route('asignacion_actividades') }}">Mis Actividades</a></li>
        <!-- <li><a href="{{ route('mis_reportes') }}">Mis Reportes</a></li>
        <li><a href="{{ route('historia_recarga') }}">Recarga Saldo</a></li> -->
    </ul>
</div>
<div class="uk-card uk-card-default uk-card-body" style="margin-top: 20px;">
    <h4>Información</h4>
    <ul class="uk-list uk-list-large uk-list-divider">
        <li><a href="{{ route('profile') }}">Mis datos</a></li>
        <li><a href="{{ route('cuenta') }}">Cambio de clave</a></li>
    </ul>
</div>