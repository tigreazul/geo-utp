@php
if(isset($data) && $data == 1)
{
	$var = '';
}else{
	$var = 'uk-light';
}

if(isset($data) && $data == 3)
{
	$style = 'background: #130060;';
}else{
	$style = '';
}
    
@endphp
<div data-uk-sticky="animation: uk-animation-slide-top; sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; cls-inactive: uk-navbar-transparent {{$var}}; top: 500" style="{{$style}}">
	<nav class="uk-navbar-container uk-letter-spacing-small uk-text-bold">
		<div class="uk-container uk-container-large">
			<div class="uk-position-z-index" data-uk-navbar>
				<div class="uk-navbar-left">
					<a class="uk-navbar-item uk-logo" href="{{ route('profile') }}">
						<!-- <img src="{{ asset('logo-white.png') }}" alt="logo" style="width: 200px;"> -->
						TRS
					</a>
				</div>
				<div class="uk-navbar-center">
					<ul class="uk-navbar-nav uk-visible@m">
					</ul>
				</div>
				<div class="uk-navbar-right">
					<!-- <div style="padding-right: 20px;">
						Saldo: S/. {{ \Views::montototal() }}
					</div>
					<div>
						<a class="uk-button uk-button-danger" href="{{ route('solicitar_reporte') }}">Solicitar Reporte</a>
					</div> -->
					<div class="uk-navbar-item">
						@if( isset(Auth::user()->name) )
							<div class="uk-inline">
								<button class="uk-button uk-button-default" type="button">{{ Auth::user()->name }}</button>
								<div uk-dropdown="pos: top-right">
									<ul class="uk-nav uk-dropdown-nav">
										<li class=""><a href="{{ url('profile') }}">Mi Perfil</a></li>
										<li class="uk-nav-divider"></li>
										<li><a href="{{ route('salir') }}" class="text-danger">Salir</a></li>
									</ul>
								</div>
							</div>
						@else
							<div><a class="uk-button uk-button-success-outline" href="{{ url('/login') }}">Entrar</a></div>
						@endif
					</div>          
					<a class="uk-navbar-toggle uk-hidden@m" href="#offcanvas" data-uk-toggle><span data-uk-navbar-toggle-icon></span></a>
				</div>
			</div>
		</div>
	</nav>
</div>