@extends('portal.layout.default')
@section('cabecera')
        @include('portal.layout.menu',['data'=>'3'])
@stop


@section('content')

<div class="uk-section" style="padding-top: 30px !important">
    <div class="uk-container">
        <ul class="uk-breadcrumb">
            <li class="uk-disabled"><a>Información</a></li>
            <li><span>Mis Recargas</span></li>
        </ul>

        <div class="uk-grid">
            <div class="uk-width-1-4">
                @include('portal.layout.menu_perfil')
            </div>
            <div class="uk-width-3-4">
                <div uk-alert>
                    <h4>Mis Recargas</h4>
                </div>
                <a href="{{ route('recarga_saldo') }}" class="uk-button uk-button-primary uk-button-small" style="margin-bottom: 10px;">Solicitar Recarga</a>
                <div class="uk-card uk-card-default uk-card-body">
                    <div class="uk-overflow-auto">
                        <table class="uk-table uk-table-hover uk-table-middle uk-table-divider uk-table-small">
                            <thead>
                                <tr>
                                    <th class="uk-table-shrink" >#</th>
                                    <th class="uk-table-shrink" >Monto</th>
                                    <th class="uk-width-small">Fecha Recarga</th>
                                    <th class="uk-table-shrink uk-text-nowrap">Estado</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!empty($historial))
                                @php $i = 1; @endphp
                                @foreach($historial as $history)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>S/. {{ $history->montos }}</td>
                                        <td>{{ \Carbon\Carbon::parse($history->fechaRegistro)->format('d-m-Y H:i:s') }}</td>
                                        <td>
                                            <div class="uk-alert-{{ ($history->validado == 1)? 'success': 'danger' }}" uk-alert>
                                                <p>{{ ($history->validado == 1)? 'VALIDADO': 'PENDIENTE EN VALIDACIÓN' }}</p>
                                            </div>
                                        </td>
                                    </tr>
                                    @php $i++; @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">
                                        <div class="uk-alert-primary" uk-alert>
                                            <p>No tienes recargas registradas</p>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                    </div>

                </div>
                <!-- <div class="uk-panel uk-panel-box">3-4</div> -->
            </div>
        </div>

    </div>


</div>


@stop