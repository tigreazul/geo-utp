@extends('portal.layout.default')
@section('content')
<link rel="stylesheet" href="https://cdn.plyr.io/3.5.10/plyr.css" />
<div id="course" class="uk-section" style="padding-top: 0;padding-bottom: 0;">
    <div class="uk-container uk-container-expand" style="padding: 0;">
        <div class="uk-grid-large" data-uk-grid>
        <div id="sidebar">
            <!-- Sidebar contents -->
            <div id="sidebar-content" style="height: 700px">
                <div class="uk-article">
                    <div class="titulo_video">
                        <p><a href="{{ route('detalle',$slug) }}"><span uk-icon="arrow-left"></span> Volver a la lista</a></p>
                    </div>
                    <div class="video-responsive">
                        <div class="container" id="player-conteiner">
                            <video id="player" controls></video>
                        </div>
                        <!-- <iframe src="https://player.vimeo.com/video/243244233" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->
                    </div>
                </div>
            </div>
        </div>
        <div id="content" style="padding: 10px;">
            <!-- Main contents -->
            <div id="main-content" style="height: 700px;">
                <div>
                    <ul class="uk-child-width-expand" data-uk-tab="{connect:'#my-id'}">
                        <li><a href="">Modulo</a></li>
                        <li><a href="">Recursos</a></li>
                        <!-- <li><a href="">Tab 3</a></li> -->
                    </ul>
                    <ul id="my-id" class="uk-switcher uk-margin">
                        <li class="">
                            <ul class="uk-margin-top uk-accordion" data-uk-accordion>
                            @php $i = 0; @endphp
                            @foreach($detcurso as $det)
                                <li class="{{ $tema_detalle->idDetalleCurso == $det['idDetalleCurso'] ? 'uk-open': ''}} lista-curso">
                                    <a class="uk-accordion-title" href="#">{{ $det['contenido'] }}</a>
                                    <div class="uk-accordion-content" aria-hidden="false">
                                        <table class="uk-table uk-table-justify uk-table-middle uk-table-divider">
                                            <tbody>
                                                @foreach($det['temas'] as $tema)
                                                <tr style="border: none;" class="{{ $tema_detalle->idTema == $tema->idTema ? 'active_curso': ''}}">
                                                    <td></td>
                                                    <td class="uk-table-expand">
                                                        <a href="{{ route('videos_curso',['tema'=>$tema->idTema,'curso'=>$cursoid]) }}" class="contenido">{{ $tema->Titulo }}</a>
                                                    </td>
                                                    <td class="uk-table-shrink" style="font-size: .75rem;">{{ $tema->Duracion }}</td>
                                                    <td></td>
                                                </tr>  
                                                @endforeach                
                                            </tbody>
                                        </table>
                                    </div>
                                </li>
                            @php $i++; @endphp
                            @endforeach
                            </ul>
                        </li>
                        <li>Proximamente</li>
                        <!-- <li>Content 3</li> -->
                    </ul>
                </div>
            </div>
        </div>

            <!-- <div class="uk-width-expand@m" style="background:red">
                <div class="uk-article">
                    <h2>Descripción</h2>
                </div>
            </div> -->
            <!-- <div class="uk-width-1-3@m" style="padding-right: 70px;padding-top: 30px;">
            
                <div style="height:100%">
                    <ul class="uk-child-width-expand" data-uk-tab="{connect:'#my-id'}">
                        <li><a href="">Tab 1</a></li>
                        <li><a href="">Tab 2</a></li>
                        <li><a href="">Tab 3</a></li>
                    </ul>
                    <ul id="my-id" class="uk-switcher uk-margin">
                        <li class="">
                            <div id="content">
                                <ul class="uk-margin-top uk-accordion" id="example" data-uk-accordion="multiple: true">
                                    <li class="uk-open">
                                        <a class="uk-accordion-title" href="#">What is User Experience Design?<span class="uk-align-right uk-margin-remove-bottom">28:56</span></a>
                                        <div class="uk-accordion-content" aria-hidden="false">
                                            <table class="uk-table uk-table-justify uk-table-middle uk-table-divider">
                                                <tbody>
                                                    <tr class="uk-text-primary">
                                                    <td class="uk-table-expand"><span class="uk-margin-small-right uk-icon" data-uk-icon="play-circle"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="play-circle"><polygon fill="none" stroke="#000" stroke-width="1.1" points="8.5 7 13.5 10 8.5 13"></polygon><circle fill="none" stroke="#000" stroke-width="1.1" cx="10" cy="10" r="9"></circle></svg></span><a href="#lesson" data-uk-toggle="">Introduction: a UXD Parable</a></td>
                                                    <td><span data-uk-icon="unlock" class="uk-icon"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="unlock"><rect fill="none" stroke="#000" x="3.5" y="8.5" width="13" height="10"></rect><path fill="none" stroke="#000" d="M6.5,8.5 L6.5,4.9 C6.5,3 8.1,1.5 10,1.5 C11.9,1.5 13.5,3 13.5,4.9"></path></svg></span></td>
                                                    <td class="uk-table-shrink">04:24</td>
                                                    </tr>
                                                    <tr class="uk-text-primary">
                                                    <td><span class="uk-margin-small-right uk-icon" data-uk-icon="play-circle"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="play-circle"><polygon fill="none" stroke="#000" stroke-width="1.1" points="8.5 7 13.5 10 8.5 13"></polygon><circle fill="none" stroke="#000" stroke-width="1.1" cx="10" cy="10" r="9"></circle></svg></span><a href="#lesson" data-uk-toggle="">What UXD Isn't</a></td>
                                                    <td><span data-uk-icon="unlock" class="uk-icon"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="unlock"><rect fill="none" stroke="#000" x="3.5" y="8.5" width="13" height="10"></rect><path fill="none" stroke="#000" d="M6.5,8.5 L6.5,4.9 C6.5,3 8.1,1.5 10,1.5 C11.9,1.5 13.5,3 13.5,4.9"></path></svg></span></td>
                                                    <td>04:24</td>
                                                    </tr>
                                                    <tr class="uk-text-muted">
                                                    <td><span class="uk-margin-small-right uk-icon" data-uk-icon="play-circle"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="play-circle"><polygon fill="none" stroke="#000" stroke-width="1.1" points="8.5 7 13.5 10 8.5 13"></polygon><circle fill="none" stroke="#000" stroke-width="1.1" cx="10" cy="10" r="9"></circle></svg></span>Why Should We Care About UXD?</td>
                                                    <td><span data-uk-icon="lock" class="uk-icon"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="lock"><rect fill="none" stroke="#000" height="10" width="13" y="8.5" x="3.5"></rect><path fill="none" stroke="#000" d="M6.5,8 L6.5,4.88 C6.5,3.01 8.07,1.5 10,1.5 C11.93,1.5 13.5,3.01 13.5,4.88 L13.5,8"></path></svg></span></td>
                                                    <td>04:24</td>
                                                    </tr>
                                                    <tr class="uk-text-muted">
                                                    <td><span class="uk-margin-small-right uk-icon" data-uk-icon="play-circle"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="play-circle"><polygon fill="none" stroke="#000" stroke-width="1.1" points="8.5 7 13.5 10 8.5 13"></polygon><circle fill="none" stroke="#000" stroke-width="1.1" cx="10" cy="10" r="9"></circle></svg></span>The Elements of User Experience</td>
                                                    <td><span data-uk-icon="lock" class="uk-icon"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="lock"><rect fill="none" stroke="#000" height="10" width="13" y="8.5" x="3.5"></rect><path fill="none" stroke="#000" d="M6.5,8 L6.5,4.88 C6.5,3.01 8.07,1.5 10,1.5 C11.93,1.5 13.5,3.01 13.5,4.88 L13.5,8"></path></svg></span></td>
                                                    <td>04:24</td>
                                                    </tr>                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="uk-accordion-title" href="#">Understanding the Elements of User Experience<span class="uk-align-right uk-margin-remove-bottom">19:26</span></a>
                                        <div class="uk-accordion-content" hidden="" aria-hidden="true">
                                            <table class="uk-table uk-table-justify uk-table-middle uk-table-divider">
                                                <tbody>
                                                    <tr class="uk-text-muted">
                                                    <td class="uk-table-expand"><span class="uk-margin-small-right uk-icon" data-uk-icon="play-circle"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="play-circle"><polygon fill="none" stroke="#000" stroke-width="1.1" points="8.5 7 13.5 10 8.5 13"></polygon><circle fill="none" stroke="#000" stroke-width="1.1" cx="10" cy="10" r="9"></circle></svg></span>The Elements of User Experience</td>
                                                    <td><span data-uk-icon="lock" class="uk-icon"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="lock"><rect fill="none" stroke="#000" height="10" width="13" y="8.5" x="3.5"></rect><path fill="none" stroke="#000" d="M6.5,8 L6.5,4.88 C6.5,3.01 8.07,1.5 10,1.5 C11.93,1.5 13.5,3.01 13.5,4.88 L13.5,8"></path></svg></span></td>
                                                    <td class="uk-table-shrink">04:24</td>
                                                    </tr>
                                                    <tr class="uk-text-muted">
                                                    <td><span class="uk-margin-small-right uk-icon" data-uk-icon="play-circle"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="play-circle"><polygon fill="none" stroke="#000" stroke-width="1.1" points="8.5 7 13.5 10 8.5 13"></polygon><circle fill="none" stroke="#000" stroke-width="1.1" cx="10" cy="10" r="9"></circle></svg></span>Exploring the Elements of User Experience</td>
                                                    <td><span data-uk-icon="lock" class="uk-icon"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="lock"><rect fill="none" stroke="#000" height="10" width="13" y="8.5" x="3.5"></rect><path fill="none" stroke="#000" d="M6.5,8 L6.5,4.88 C6.5,3.01 8.07,1.5 10,1.5 C11.93,1.5 13.5,3.01 13.5,4.88 L13.5,8"></path></svg></span></td>
                                                    <td>04:24</td>
                                                    </tr>
                                                    <tr class="uk-text-muted">
                                                    <td><span class="uk-margin-small-right uk-icon" data-uk-icon="play-circle"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="play-circle"><polygon fill="none" stroke="#000" stroke-width="1.1" points="8.5 7 13.5 10 8.5 13"></polygon><circle fill="none" stroke="#000" stroke-width="1.1" cx="10" cy="10" r="9"></circle></svg></span>How the Elements Work Together</td>
                                                    <td><span data-uk-icon="lock" class="uk-icon"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="lock"><rect fill="none" stroke="#000" height="10" width="13" y="8.5" x="3.5"></rect><path fill="none" stroke="#000" d="M6.5,8 L6.5,4.88 C6.5,3.01 8.07,1.5 10,1.5 C11.93,1.5 13.5,3.01 13.5,4.88 L13.5,8"></path></svg></span></td>
                                                    <td>04:24</td>
                                                    </tr>
                                                    <tr class="uk-text-muted">
                                                    <td><span class="uk-margin-small-right uk-icon" data-uk-icon="play-circle"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="play-circle"><polygon fill="none" stroke="#000" stroke-width="1.1" points="8.5 7 13.5 10 8.5 13"></polygon><circle fill="none" stroke="#000" stroke-width="1.1" cx="10" cy="10" r="9"></circle></svg></span>Identifying Business Goals</td>
                                                    <td><span data-uk-icon="lock" class="uk-icon"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="lock"><rect fill="none" stroke="#000" height="10" width="13" y="8.5" x="3.5"></rect><path fill="none" stroke="#000" d="M6.5,8 L6.5,4.88 C6.5,3.01 8.07,1.5 10,1.5 C11.93,1.5 13.5,3.01 13.5,4.88 L13.5,8"></path></svg></span></td>
                                                    <td>04:24</td>
                                                    </tr>                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="uk-accordion-title" href="#">Using the Design Elements Structure<span class="uk-align-right uk-margin-remove-bottom">35:30</span></a>
                                        <div class="uk-accordion-content" hidden="" aria-hidden="true">
                                            <table class="uk-table uk-table-justify uk-table-middle uk-table-divider">
                                                <tbody>
                                                    <tr class="uk-text-muted">
                                                    <td class="uk-table-expand"><span class="uk-margin-small-right uk-icon" data-uk-icon="play-circle"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="play-circle"><polygon fill="none" stroke="#000" stroke-width="1.1" points="8.5 7 13.5 10 8.5 13"></polygon><circle fill="none" stroke="#000" stroke-width="1.1" cx="10" cy="10" r="9"></circle></svg></span>Generating Effective Requirements</td>
                                                    <td><span data-uk-icon="lock" class="uk-icon"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="lock"><rect fill="none" stroke="#000" height="10" width="13" y="8.5" x="3.5"></rect><path fill="none" stroke="#000" d="M6.5,8 L6.5,4.88 C6.5,3.01 8.07,1.5 10,1.5 C11.93,1.5 13.5,3.01 13.5,4.88 L13.5,8"></path></svg></span></td>
                                                    <td class="uk-table-shrink">04:24</td>
                                                    </tr>
                                                    <tr class="uk-text-muted">
                                                    <td><span class="uk-margin-small-right uk-icon" data-uk-icon="play-circle"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="play-circle"><polygon fill="none" stroke="#000" stroke-width="1.1" points="8.5 7 13.5 10 8.5 13"></polygon><circle fill="none" stroke="#000" stroke-width="1.1" cx="10" cy="10" r="9"></circle></svg></span>Exploring Information Architecture</td>
                                                    <td><span data-uk-icon="lock" class="uk-icon"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="lock"><rect fill="none" stroke="#000" height="10" width="13" y="8.5" x="3.5"></rect><path fill="none" stroke="#000" d="M6.5,8 L6.5,4.88 C6.5,3.01 8.07,1.5 10,1.5 C11.93,1.5 13.5,3.01 13.5,4.88 L13.5,8"></path></svg></span></td>
                                                    <td>04:24</td>
                                                    </tr>
                                                    <tr class="uk-text-muted">
                                                    <td><span class="uk-margin-small-right uk-icon" data-uk-icon="play-circle"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="play-circle"><polygon fill="none" stroke="#000" stroke-width="1.1" points="8.5 7 13.5 10 8.5 13"></polygon><circle fill="none" stroke="#000" stroke-width="1.1" cx="10" cy="10" r="9"></circle></svg></span>Convention and Metaphor</td>
                                                    <td><span data-uk-icon="lock" class="uk-icon"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="lock"><rect fill="none" stroke="#000" height="10" width="13" y="8.5" x="3.5"></rect><path fill="none" stroke="#000" d="M6.5,8 L6.5,4.88 C6.5,3.01 8.07,1.5 10,1.5 C11.93,1.5 13.5,3.01 13.5,4.88 L13.5,8"></path></svg></span></td>
                                                    <td>04:24</td>
                                                    </tr>
                                                    <tr class="uk-text-muted">
                                                    <td><span class="uk-margin-small-right uk-icon" data-uk-icon="play-circle"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="play-circle"><polygon fill="none" stroke="#000" stroke-width="1.1" points="8.5 7 13.5 10 8.5 13"></polygon><circle fill="none" stroke="#000" stroke-width="1.1" cx="10" cy="10" r="9"></circle></svg></span>Contrast and Uniformity</td>
                                                    <td><span data-uk-icon="lock" class="uk-icon"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="lock"><rect fill="none" stroke="#000" height="10" width="13" y="8.5" x="3.5"></rect><path fill="none" stroke="#000" d="M6.5,8 L6.5,4.88 C6.5,3.01 8.07,1.5 10,1.5 C11.93,1.5 13.5,3.01 13.5,4.88 L13.5,8"></path></svg></span></td>
                                                    <td>04:24</td>
                                                    </tr>                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>Content 2</li>
                        <li>Content 3</li>
                    </ul>

                </div>
            </div> -->
        </div>
    </div>
</div>

<style>
    html {
        height: 100%;
    }

    body {
        margin: 0;
        height: 100%;
        overflow: hidden;
        background-color: #171b32; 
        color: #ffffff;
    }
    #container {
        height: 100%;
    }

    #sidebar {
        display: inline-block;
        background:#000;
        vertical-align: top;
        height: 100%;
        width: 70%;
        overflow: auto;
    }

    #content {
        display: inline-block;
        vertical-align: top;
        height: 100%;
        width: 23%;
        overflow: auto;
    }
    
	a { color: #616373 }
    
    .uk-tab > .uk-active > a {
        color: #45dfe6;
        border-color: #45dfe6;
    }
    .video-responsive {
        /* position: relative; */
        /* padding-bottom: 56.25%; 16/9 ratio */
        /* padding-top: 30px; IE6 workaround */
        /* height: 0; */
        overflow: hidden;
        padding-top: 5%;
    }

    .video-responsive iframe,
    .video-responsive object,
    .video-responsive embed {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    .lista-curso{
        padding:10px;background: #333751;margin-top: 5px !important;
    }
    .uk-open .uk-accordion-title{
        color:#45dfe6 !important;
    }
    .uk-accordion-title{
        font-size: .9rem;
        padding: 10px 0;
        color:#fff;
    }
    .contenido{
        color:#fff !important;
        font-size: .75rem;
    }

    .uk-accordion-title:hover, .uk-accordion-title:focus {
        color: #45dfe6 !important;
    }
    .titulo_video{
        background: rgb(23, 27, 50);
        /* background: #1b1b1b; */
        padding: 14px;
        position: relative;
        width: 100%;
        border-bottom: 1px solid #6fdfe7;
    }
    .active_curso{
        background: rgba(160,167,203,0.2);
    }

    .titulo_video p{
        color: #fff;
        margin: 0;
    }
    a:hover, .uk-link:hover, .uk-link-toggle:hover .uk-link, .uk-link-toggle:focus .uk-link {
        color: #6fdfe7;
    }
    a{
        color:#fff;
    }
</style>
<script src="https://cdn.plyr.io/3.5.10/plyr.polyfilled.js"></script>
<script>
    const player = new Plyr('#player');
    player.source = {
        type: 'video',
        sources: [{
        src: "{{ $video['codigo'] }}", // From the Vimeo video link
        provider: "{{ $video['tipo'] }}",
        }, ],
    };
</script>
<!-- <script src="{{ asset('vendor/js/curso.js') }}"></script> -->
@php $footer = false @endphp
@stop