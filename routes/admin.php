<?php

Route::get('/', function () {
    return redirect()->route('admin.home');
});
#---------------------------------------------------------
//Login Routes
    Route::get('login','Admin\Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('login','Admin\Auth\LoginController@login')->name('admin.login.submit');
    Route::post('logout','Admin\Auth\LoginController@logout')->name('admin.logout_admin');
    //Register Routes
    // Route::get('/register','RegisterController@showRegistrationForm')->name('register');
    // Route::post('/register','RegisterController@register');
    //Forgot Password Routes
    Route::get('password/reset','Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('password/email','Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    //Reset Password Routes
    Route::get('password/reset/{token}','Admin\Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::post('password/reset','Admin\Auth\ResetPasswordController@reset')->name('admin.password.update');
    // Email Verification Route(s)
    Route::get('email/verify','Admin\VerificationController@show')->name('admin.verification.notice');
    Route::get('email/verify/{id}','Admin\VerificationController@verify')->name('admin.verification.verify');
    Route::get('email/resend','Admin\VerificationController@resend')->name('admin.verification.resend');
#---------------------------------------------------------


Route::get('dashboard', 'AdminController@index')->name('admin.home');

Route::group(['prefix' => 'configuracion'], function() {
    Route::get('menu', 'Admin\MenuController@index')->name('admin.menu');
    Route::get('menu/modulo-add', 'Admin\MenuController@modulo_add')->name('admin.modulo_add');
    Route::get('menu/get-page/{id}', 'Admin\MenuController@get_page')->name('admin.get_page');
    Route::get('menu/get-page-id/{id}', 'Admin\MenuController@get_page_id')->name('admin.get_page_id');
    Route::post('menu/modulo-add', 'Admin\MenuController@store_modulo')->name('admin.modulo_add_store');
    Route::get('menu/{id}/editar', 'Admin\MenuController@edit_modulo')->name('admin.modulo_edit');
    Route::post('menu/{id}/update', 'Admin\MenuController@update_modulo')->name('admin.modulo_update');
    Route::post('delete/module/{id}', 'Admin\MenuController@delete_modulo')->name('admin.delete_modulo');
    
    Route::post('delete/page/{id}', 'Admin\MenuController@delete_page')->name('admin.delete_page');
    Route::post('menu/page-add', 'Admin\MenuController@store_pagina')->name('admin.page_add');
    Route::post('menu/page-update', 'Admin\MenuController@update_pagina')->name('admin.page_update');
});

Route::group(['prefix' => 'personal'], function() {
    Route::get('lista', 'Admin\AlumnosController@index')->name('admin.alumno_list');
    Route::get('user/create', 'Admin\AlumnosController@create')->name('admin.alumno_create');
    Route::post('user/create-add', 'Admin\AlumnosController@store')->name('admin.alumno_add');
    // Route::get('user/{id}', 'Admin\AlumnosController@show')->name('admin.alumno_list_id');
    Route::get('user/{id}/editar', 'Admin\AlumnosController@edit')->name('admin.alumno_edit');
    Route::post('user/delete/{id}', 'Admin\AlumnosController@delete')->name('admin.delete_edit');
    Route::patch('user/{id}', 'Admin\AlumnosController@update')->name('admin.alumno_edit_data');
});

Route::group(['prefix' => 'usuario'], function() {
    Route::get('lista', 'Admin\UsuarioController@index')->name('admin.user_list');
    Route::get('user/create', 'Admin\UsuarioController@create')->name('admin.user_create');
    Route::post('user/create-add', 'Admin\UsuarioController@store')->name('admin.user_add');
    // Route::get('user/{id}', 'Admin\UsuarioController@show')->name('admin.user_list_id');
    Route::get('user/{id}/editar', 'Admin\UsuarioController@edit')->name('admin.user_edit');
    Route::post('user/delete/{id}', 'Admin\UsuarioController@delete')->name('admin.user_delete_edit');
    Route::patch('user/{id}', 'Admin\UsuarioController@update')->name('admin.user_edit_data');
});

Route::group(['prefix' => 'prospectos'], function() {
    Route::get('lista', 'Admin\ProspectoController@index')->name('admin.prospecto_list');
    Route::get('user/create', 'Admin\ProspectoController@create')->name('admin.prospecto_create');
    Route::post('user/create-add', 'Admin\ProspectoController@store')->name('admin.prospecto_add');
    Route::get('user/{id}/editar', 'Admin\ProspectoController@edit')->name('admin.prospecto_edit');
    Route::post('user/delete/{id}', 'Admin\ProspectoController@delete')->name('admin.delete_edit');
    Route::patch('user/{id}', 'Admin\ProspectoController@update')->name('admin.prospecto_edit_data');
    Route::get('recarga/{id}', 'Admin\ProspectoController@recarga')->name('admin.prospecto_recarga');
    Route::post('datos-personal', 'Admin\ProspectoController@datosPersonal')->name('admin.datos_personal');
    Route::post('enviar-recarga', 'Admin\ProspectoController@enviar_recarga')->name('admin.enviar_recarga');
});


Route::group(['prefix' => 'solicitudes'], function() {
    Route::get('pendientes', 'Admin\ProspectoController@pendientes')->name('admin.prospecto_pendientes');
    Route::get('prospecto/{id}/ver', 'Admin\ProspectoController@ver_prospectos')->name('admin.ver_pendientes');
    Route::post('prospecto/{id}/ver', 'Admin\ProspectoController@estado_pendientes')->name('admin.estado_pendientes');
    Route::get('terminado', 'Admin\ProspectoController@terminado')->name('admin.prospecto_terminado');
    
});

Route::group(['prefix' => 'curso'], function() {
    //CURSO
    Route::get('listado', 'Admin\CursoController@index')->name('admin.curso_list');
    Route::get('create', 'Admin\CursoController@create')->name('admin.curso_create');
    Route::post('create-add', 'Admin\CursoController@store')->name('admin.curso_add');
    Route::get('editar/{id}', 'Admin\CursoController@edit')->name('admin.curso_edit');
    Route::patch('editar/{id}', 'Admin\CursoController@update')->name('admin.curso_edit_data');
    Route::post('delete/{id}', 'Admin\CursoController@delete')->name('admin.curso_delete_edit');

    // GRUPO
    Route::get('detalle/{id}', 'Admin\CursoController@indexDetalle')->name('admin.detalle_curso_list');
    Route::get('detalle/create/{id}', 'Admin\CursoController@createDetalle')->name('admin.detalle_curso_create');
    Route::post('detalle/create-add', 'Admin\CursoController@storeDetalle')->name('admin.detalle_curso_add');
    Route::get('detalle/editar/{id}/{curso}', 'Admin\CursoController@editDetalle')->name('admin.detalle_curso_edit');
    Route::patch('detalle/editar/{id}', 'Admin\CursoController@updateDetalle')->name('admin.detalle_curso_edit_data');
    Route::post('detalle/delete/{id}', 'Admin\CursoController@deleteDetalle')->name('admin.detalle_curso_delete_edit');
    
    //TEMAS
    Route::get('tema/{id}', 'Admin\CursoController@indexTema')->name('admin.tema_curso_list');
    Route::get('tema/create/{id}', 'Admin\CursoController@createTema')->name('admin.tema_curso_create');
    Route::post('tema/create-add', 'Admin\CursoController@storeTema')->name('admin.tema_curso_add');
    Route::get('tema/editar/{id}/{detalle}', 'Admin\CursoController@editTema')->name('admin.tema_curso_edit');
    Route::patch('tema/editar/{id}', 'Admin\CursoController@updateTema')->name('admin.tema_curso_edit_data');
    Route::post('tema/delete/{id}', 'Admin\CursoController@deleteTema')->name('admin.tema_curso_delete_edit');

    // CATEGORIA
    // Route::get('categoria', function(){
    //     dd('ss');
    // });
    Route::get('categoria', 'Admin\CursoController@indexCategoria')->name('admin.categoria_list');
    Route::get('categoria/create', 'Admin\CursoController@createCategoria')->name('admin.categoria_create');
    Route::post('categoria/create-add', 'Admin\CursoController@storeCategoria')->name('admin.categoria_add');
    // Route::get('categoria/{id}', 'Admin\CursoController@show')->name('admin.categoria_list_id');
    Route::get('categoria/{id}/editar', 'Admin\CursoController@editCategoria')->name('admin.categoria_edit');
    Route::patch('categoria/{id}', 'Admin\CursoController@updateCategoria')->name('admin.categoria_edit_data');
    Route::post('categoria/delete/{id}', 'Admin\CursoController@deleteCategoria')->name('admin.categoriadelete_edit');
});

Route::group(['prefix' => 'docente'], function() {
    Route::get('lista', 'Admin\DocenteController@index')->name('admin.docente_list');
    Route::get('create', 'Admin\DocenteController@create')->name('admin.docente_create');
    Route::post('create-add', 'Admin\DocenteController@store')->name('admin.docente_add');
    // Route::get('{id}', 'Admin\DocenteController@show')->name('admin.docente_list_id');
    Route::get('{id}/editar', 'Admin\DocenteController@edit')->name('admin.docente_edit');
    Route::post('delete/{id}', 'Admin\DocenteController@delete')->name('admin.docente_delete_edit');
    Route::patch('{id}', 'Admin\DocenteController@update')->name('admin.docente_edit_data');
});



Route::group(['prefix' => 'titular'], function() {
    // Titular
    Route::get('lista', 'Admin\TitularController@index')->name('admin.titular_list');                 // Listado
    Route::get('formato/create', 'Admin\TitularController@create')->name('admin.titular_create');       // Form View Create
    Route::post('formato/create-add', 'Admin\TitularController@store')->name('admin.titular_add');      // Crear dato
    
    Route::post('formato/sub-create-add', 'Admin\TitularController@subdireccion')->name('admin.subtitular_add');      // Crear dato
    Route::post('formato/sub-create-view', 'Admin\TitularController@jsonSubTitular')->name('admin.subtitular_view');      // Crear dato
    Route::patch('formato/sub-create-edit', 'Admin\TitularController@updateSubTitular')->name('admin.subtitular_update');      // Crear dato

    Route::post('formato/habitan-create-add', 'Admin\TitularController@habitan')->name('admin.habitan_add');      // Crear dato
    Route::post('formato/habitan-create-view', 'Admin\TitularController@jsonSubHabitan')->name('admin.habitan_view');      // Crear dato
    Route::patch('formato/habitan-edit', 'Admin\TitularController@updateSubHabitan')->name('admin.habitan_update');      // Crear dato

    Route::get('formato/{id}', 'Admin\TitularController@show')->name('admin.titular_list_id');          // Mostrar por id
    Route::get('formato/{user}/editar', 'Admin\TitularController@edit')->name('admin.titular_edit');    // Form editar
    Route::patch('formato/{id}', 'Admin\TitularController@update')->name('admin.titular_edit_data');    // Update datos

    Route::post('formato/buscar', 'Admin\TitularController@validaBusquedaTitular')->name('admin.titular_search');
    Route::get('formato/buscar/{codigo}', 'Admin\TitularController@busquedaTitular')->name('admin.titular_create_search');

    Route::get('formato/ficha/{codigo}', 'Admin\TitularController@fichaPadron')->name('admin.titular_ficha_padron');

    Route::get('example','Admin\TitularController@example')->name('admin.example');

}); 

Route::group(['prefix' => 'pagos'], function() {
    // Pagina
    Route::get('lista', 'Admin\PagosController@index')->name('admin.pagos_list');                 // Listado
    Route::get('pago/buscar', 'Admin\PagosController@create')->name('admin.pagos_create');       // Form View Create
    Route::post('pago/buscar', 'Admin\PagosController@validaBusqueda')->name('admin.pagos_create_post');       // Form View Create
    Route::get('pago/buscar/{codigo}/{filtro}', 'Admin\PagosController@busqueda')->name('admin.pagos_create_search');       // Form View Create
    Route::post('pago/justificar', 'Admin\PagosController@justificar')->name('admin.pagos_justificar');
    Route::post('pago/pagar', 'Admin\PagosController@pagar')->name('admin.pagos_pagar');

    Route::get('pago/imprimir/{codigo}/{filtro}', 'Admin\PagosController@imprimirpago')->name('admin.pagos_imprimir');

    // Route::post('pago/create-add', 'Admin\PagosController@store')->name('admin.pagos_add');      // Crear dato
    // Route::get('pago/{id}', 'Admin\PagosController@show')->name('admin.pagos_list_id');          // Mostrar por id
    // Route::get('pago/{user}/editar', 'Admin\PagosController@edit')->name('admin.pagos_edit');    // Form editar
    // Route::patch('pago/{id}', 'Admin\PagosController@update')->name('admin.pagos_edit_data');    // Update datos
}); 

Route::group(['prefix' => 'reporte'], function() {
    // Reporte
    Route::get('lista', 'Admin\ReporteController@index')->name('admin.report_list');                 // Listado
    Route::post('socio/buscar', 'Admin\ReporteController@validaBusquedaSocio')->name('admin.socio_search');
    Route::get('socio/buscar/{codigo}/{manzana}', 'Admin\ReporteController@busquedaSocio')->name('admin.socio_create_search');
    

    Route::post('expediente/buscar', 'Admin\ReporteController@validaBusquedaExpediente')->name('admin.expediente_search');
    Route::get('expediente/buscar/{codigo}', 'Admin\ReporteController@busquedaExpediente')->name('admin.exp_create_search');
    Route::get('expediente', 'Admin\ReporteController@expediente')->name('admin.report_exp');                 // Listado

    Route::post('report/buscar', 'Admin\ReporteController@validaBusqueda')->name('admin.report_create_post');

    Route::get('socio/{user}/{manzana}', 'Admin\ReporteController@reporteSocio')->name('admin.socio_report');
    Route::get('exp/{user}/report', 'Admin\ReporteController@reporteSocio')->name('admin.exp_report');


    Route::get('page/create', 'Admin\ReporteController@create')->name('admin.front_create');       // Form View Create
    Route::post('page/create-add', 'Admin\ReporteController@store')->name('admin.front_add');      // Crear dato
    Route::get('page/{id}', 'Admin\ReporteController@show')->name('admin.front_list_id');          // Mostrar por id
    Route::get('page/{user}/editar', 'Admin\ReporteController@edit')->name('admin.front_edit');    // Form editar
    Route::patch('page/{id}', 'Admin\ReporteController@update')->name('admin.front_edit_data');    // Update datos

    Route::get('fecha', 'Admin\ReporteController@viewFecha')->name('admin.indx_fecha');
    Route::post('fecha/buscar', 'Admin\ReporteController@validarBuscarFecha')->name('admin.fecha_search');
    Route::get('fecha/buscar/{finicio}/{ffin}', 'Admin\ReporteController@buscarFecha')->name('admin.fecha_csearch');


    Route::get('titular', 'Admin\ReporteController@viewTitular')->name('admin.indx_titular');
    Route::post('fecha/titular', 'Admin\ReporteController@validarBuscarTitular')->name('admin.report_search');
    Route::get('fecha/titular/{finicio}', 'Admin\ReporteController@buscarTitular')->name('admin.report_csearch');


}); 

Route::group(['prefix' => 'reuniones'], function() {
    Route::get('lista', 'Admin\ReunionController@index')->name('admin.reunion_list');                 // Listado
    Route::get('reunion/create', 'Admin\ReunionController@create')->name('admin.reunion_create');       // Form View Create
    Route::post('reunion/create-add', 'Admin\ReunionController@store')->name('admin.reunion_add');      // Crear dato
    Route::get('reunion/{id}', 'Admin\ReunionController@show')->name('admin.reunion_list_id');          // Mostrar por id
    Route::get('reunion/{user}/editar', 'Admin\ReunionController@edit')->name('admin.reunion_edit');    // Form editar
    Route::patch('reunion/{id}', 'Admin\ReunionController@update')->name('admin.reunion_edit_data');    // Update datos

    Route::get('asistencia/{id}', 'Admin\ReunionController@tarjetero')->name('admin.tarjetero_asistencia');    // Tarjetero
    Route::post('asistencia/targetero', 'Admin\ReunionController@uploadTarjetero')->name('admin.upload_tarjetero_asistencia');    // Tarjetero
}); 

Route::group(['prefix' => 'backup'], function() {
    Route::get('lista', 'Admin\BackupController@index')->name('admin.bk_lista');                 // Listado
    Route::post('generar', 'Admin\BackupController@generar')->name('admin.bk_genera');       // Form View Create
    Route::post('restaurar', 'Admin\BackupController@restaurar')->name('admin.bk_restaurar');       // Form View Create
    
}); 


Route::group(['prefix' => 'actividades'], function() {
    Route::get('lista', 'Admin\ActividadesController@index')->name('admin.actividad_list');
    Route::get('create', 'Admin\ActividadesController@create')->name('admin.actividad_create');
    Route::post('create-add', 'Admin\ActividadesController@store')->name('admin.actividad_add');
    Route::get('/{id}/editar', 'Admin\ActividadesController@edit')->name('admin.actividad_edit');
    Route::post('delete/{id}', 'Admin\ActividadesController@delete')->name('admin.actividad_delete_edit');
    Route::patch('/{id}', 'Admin\ActividadesController@update')->name('admin.actividad_edit_data');

    Route::get('get-actividades/{id}', 'Admin\ActividadesController@show_activity_person')->name('admin.actividadd_edit');
    Route::get('get-activity-actived/{id}', 'Admin\ActividadesController@show_activity_person_actived')->name('admin.actividads_edit');
    
    Route::post('post-actividades-add', 'Admin\ActividadesController@activy_person_add')->name('admin.actividads_add');
    

    Route::get('asignacion/lista', 'Admin\ActividadesController@indexAsignacion')->name('admin.asignacion_list');
    Route::get('asignacion/create', 'Admin\ActividadesController@createAsignacion')->name('admin.asignacion_create');

    Route::post('asignacion/create-add', 'Admin\ActividadesController@storeAsignacion')->name('admin.asignacion_add');
    Route::get('asignacion/{id}/editar', 'Admin\ActividadesController@editAsignacion')->name('admin.asignacion_edit');
    Route::post('asignacion/delete/{id}', 'Admin\ActividadesController@deleteAsignacion')->name('admin.asignacion_delete_edit');
    Route::patch('asignacion/{id}', 'Admin\ActividadesController@updateAsignacion')->name('admin.asignacion_edit_data');

});





Route::group(['prefix' => 'monitoreo'], function() {
    Route::get('gps', 'Admin\MonitoreoController@index')->name('admin.gps_list');
});


Route::get('volver',function(){
    return redirect()->back();
})->name('admin.back');


