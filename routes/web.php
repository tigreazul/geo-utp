<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// PORTAL
// Route::get('/', 'Portal\PortalController@index')->name('inicio');
Route::get('/', function(){
    // /login
    return Redirect::to('/profile');
})->name('inicio');

// Route::get('curso/{curso}','Portal\PortalController@showCurso')->name('detalle');
// Route::get('cursos','Portal\PortalController@listCursos')->name('lista-cursos');
// Route::get('search/{texto}','Portal\PortalController@searchCursos')->name('search-cursos');
// Route::post('curso/buscar', 'Portal\PortalController@busqueda')->name('buscador-curso');

Route::get('profile', 'Portal\PerfilController@index')->name('profile');
Route::patch('profile/{id}', 'Portal\PerfilController@profileUpdate')->name('edit_profile');

Route::get('cuenta', 'Portal\PerfilController@cuenta')->name('cuenta');
Route::patch('cuenta/{id}', 'Portal\PerfilController@cuentaUpdate')->name('edit_cuenta');

// Route::get('mis-cursos', 'Portal\PerfilController@mis_cursos')->name('miscursos');
// Route::post('inscripcion-curso', 'Portal\PortalController@datosCurso')->name('validaregistro');
// Route::post('curso-id', 'Portal\PortalController@cursoid')->name('curso_id');
// Route::post('registro-curso', 'Portal\PortalController@registroCursos')->name('registro_cursos');


Route::get('recarga-saldo', 'Portal\PerfilController@recarga_saldo')->name('recarga_saldo');
Route::get('historial-recarga', 'Portal\PerfilController@historial_recarga')->name('historia_recarga');
Route::post('recarga-saldo', 'Portal\PerfilController@recargar')->name('recarga');
Route::get('mis-reportes', 'Portal\PerfilController@mis_reportes')->name('mis_reportes');


Route::get('solicitar-reporte', 'Portal\PerfilController@reporte_solicitud')->name('solicitar_reporte');
Route::get('solicitar-reporte-terceros', 'Portal\PerfilController@reporte_solicitud_terceros')->name('solicitar_reporte_terceros');
Route::get('solicitar-mi-reporte', 'Portal\PerfilController@reporte_mi_solicitud')->name('solicitar_mi_reporte');
Route::post('consulta-reporte', 'SentinelController@consulta_reporte')->name('consulta_reporte');


// ADMIN
// Route::get('clases/{tema}/player/{curso}', 'Portal\PortalController@videos_curso')->name('videos_curso');
// Route::get('clases/{tema}/player/{curso}', function ($tema,$curso) {});

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('salir');
Route::post('image', 'RecursoController@index')->name('image');

Auth::routes();


Route::get('rest', 'SentinelController@ver')->name('rest');
Route::post('rest', 'SentinelController@encryp');


Route::get('actividades-asignadas', 'Portal\PerfilController@actividades_asignadas')->name('asignacion_actividades');


// Route::prefix('/admin')->name('admin.')->namespace('Admin')->group(function(){
    
    /**
     * Admin Auth Route(s)
     */
    // Route::namespace('Auth')->group(function(){
        
        // //Login Routes
        // Route::get('login','LoginController@showLoginForm')->name('login');
        // Route::post('login','LoginController@login')->name('login.submit');
        // Route::post('logout','LoginController@logout')->name('logout_admin');

        // //Register Routes
        // // Route::get('/register','RegisterController@showRegistrationForm')->name('register');
        // // Route::post('/register','RegisterController@register');

        // //Forgot Password Routes
        // Route::get('password/reset','ForgotPasswordController@showLinkRequestForm')->name('password.request');
        // Route::post('password/email','ForgotPasswordController@sendResetLinkEmail')->name('password.email');

        // //Reset Password Routes
        // Route::get('password/reset/{token}','ResetPasswordController@showResetForm')->name('password.reset');
        // Route::post('password/reset','ResetPasswordController@reset')->name('password.update');

        // // Email Verification Route(s)
        // Route::get('email/verify','Admin\VerificationController@show')->name('verification.notice');
        // Route::get('email/verify/{id}','Admin\VerificationController@verify')->name('verification.verify');
        // Route::get('email/resend','Admin\VerificationController@resend')->name('verification.resend');

    // });

    // Route::get('/dashboard','HomeController@index')->name('home')->middleware('guard.verified:admin,admin.verification.notice');

    //Put all of your admin routes here...
    

// });