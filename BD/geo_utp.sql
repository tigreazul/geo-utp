/*
 Navicat Premium Data Transfer

 Source Server         : LOCALHOST
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : geo_utp

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 23/07/2021 00:03:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for actividades
-- ----------------------------
DROP TABLE IF EXISTS `actividades`;
CREATE TABLE `actividades`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `estado` int(11) NULL DEFAULT NULL,
  `fecha_registro` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of actividades
-- ----------------------------
INSERT INTO `actividades` VALUES (1, 'nueva actividad 2', 1, NULL);
INSERT INTO `actividades` VALUES (2, 'anteriores', 1, NULL);
INSERT INTO `actividades` VALUES (3, 'antigua actividad', 1, NULL);
INSERT INTO `actividades` VALUES (4, 'actividad 2', 1, NULL);
INSERT INTO `actividades` VALUES (5, 'actividad 3', 1, NULL);
INSERT INTO `actividades` VALUES (6, 'actividad 4', 1, NULL);
INSERT INTO `actividades` VALUES (7, 'actividad 5', 1, NULL);
INSERT INTO `actividades` VALUES (8, 'actividad 6', 1, NULL);
INSERT INTO `actividades` VALUES (9, 'actividad 7', 1, NULL);
INSERT INTO `actividades` VALUES (10, 'actividad 8', 1, NULL);

-- ----------------------------
-- Table structure for actividadpersona
-- ----------------------------
DROP TABLE IF EXISTS `actividadpersona`;
CREATE TABLE `actividadpersona`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPersona` int(11) NULL DEFAULT NULL,
  `idActividad` int(11) NULL DEFAULT NULL,
  `fechaRegistro` datetime(0) NULL DEFAULT NULL,
  `estado` int(11) NULL DEFAULT NULL,
  `concluido` int(11) NULL DEFAULT NULL,
  `fechaConclusion` datetime(0) NULL DEFAULT NULL,
  `latitud` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `longitud` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of actividadpersona
-- ----------------------------
INSERT INTO `actividadpersona` VALUES (23, 2, 1, '2021-07-17 01:22:12', 0, 0, NULL, NULL, NULL);
INSERT INTO `actividadpersona` VALUES (24, 2, 2, '2021-07-17 01:22:12', 0, 0, NULL, NULL, NULL);
INSERT INTO `actividadpersona` VALUES (25, 7, 6, '2021-07-17 01:22:50', 0, 0, NULL, NULL, NULL);
INSERT INTO `actividadpersona` VALUES (26, 7, 9, '2021-07-17 01:22:50', 0, 0, NULL, NULL, NULL);
INSERT INTO `actividadpersona` VALUES (27, 6, 9, '2021-07-17 01:29:38', 1, 0, NULL, NULL, NULL);
INSERT INTO `actividadpersona` VALUES (28, 6, 10, '2021-07-17 01:29:38', 1, 0, NULL, NULL, NULL);
INSERT INTO `actividadpersona` VALUES (29, 7, 2, '2021-07-17 01:39:38', 0, 0, NULL, NULL, NULL);
INSERT INTO `actividadpersona` VALUES (30, 7, 4, '2021-07-17 01:43:59', 0, 0, NULL, NULL, NULL);
INSERT INTO `actividadpersona` VALUES (31, 7, 3, '2021-07-17 01:45:52', 1, 0, NULL, NULL, NULL);
INSERT INTO `actividadpersona` VALUES (32, 7, 5, '2021-07-17 01:45:52', 1, 0, NULL, NULL, NULL);
INSERT INTO `actividadpersona` VALUES (33, 2, 4, '2021-07-17 02:19:37', 1, 0, NULL, NULL, NULL);
INSERT INTO `actividadpersona` VALUES (34, 2, 6, '2021-07-17 02:19:38', 1, 1, NULL, NULL, NULL);
INSERT INTO `actividadpersona` VALUES (35, 2, 7, '2021-07-17 02:19:38', 1, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for admin_role
-- ----------------------------
DROP TABLE IF EXISTS `admin_role`;
CREATE TABLE `admin_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of admin_role
-- ----------------------------
INSERT INTO `admin_role` VALUES (1, 1, 1, NULL, NULL);
INSERT INTO `admin_role` VALUES (2, 3, 4, '2021-03-03 18:41:52', '2021-03-03 18:41:52');
INSERT INTO `admin_role` VALUES (3, 4, 5, '2021-03-06 01:52:13', '2021-03-06 01:52:13');
INSERT INTO `admin_role` VALUES (4, 5, 6, NULL, NULL);

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `idPersona` int(11) NULL DEFAULT NULL,
  `estado` int(11) NULL DEFAULT NULL,
  `username` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `autorizacion` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of admins
-- ----------------------------
INSERT INTO `admins` VALUES (1, 'sss', 'admin', '$2y$10$tGXNkEofrIBARimWOo/s5.UBYnyPv8n5wjlMZRdHKgLcN3jbk4k8a', 1, 1, 'admin', 1);
INSERT INTO `admins` VALUES (3, 'perez perez, juan', 'perez@ffm.com', '$2y$10$tGXNkEofrIBARimWOo/s5.UBYnyPv8n5wjlMZRdHKgLcN3jbk4k8a', 18, 1, 'jperez', 1);
INSERT INTO `admins` VALUES (4, 'Sanchez Marino, Miguel', 'msanchez@aaaa.com', '$2y$10$isb6aK0aWwQWpyQ.OE/taea2JToScXdQ0kL2c9hox9ePW8WXvtQmi', 19, 1, 'msanchez', 1);
INSERT INTO `admins` VALUES (5, 'Smit Martinez Marco', 'msmit@aaa.com', '$2y$10$eygdbqAxeyxxIUyKgX2Hvux5nfq0Y2yyFPkh7pwSIjHud5O1szU6W', 20, 1, 'msmit', 1);

-- ----------------------------
-- Table structure for categoria
-- ----------------------------
DROP TABLE IF EXISTS `categoria`;
CREATE TABLE `categoria`  (
  `idCat` int(11) NOT NULL AUTO_INCREMENT,
  `nombreCat` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `estadoCat` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `fechaCat` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`idCat`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of categoria
-- ----------------------------
INSERT INTO `categoria` VALUES (1, 'Bolsas', '1', NULL);
INSERT INTO `categoria` VALUES (2, 'Traiding', '1', NULL);

-- ----------------------------
-- Table structure for categoriacurso
-- ----------------------------
DROP TABLE IF EXISTS `categoriacurso`;
CREATE TABLE `categoriacurso`  (
  `idCategoria` int(11) NOT NULL,
  `idCurso` int(11) NOT NULL,
  `fechaRegistro` datetime(0) NULL DEFAULT NULL,
  `estado` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idCategoria`, `idCurso`) USING BTREE,
  INDEX `fk_curso1`(`idCurso`) USING BTREE,
  CONSTRAINT `fk_categoria1` FOREIGN KEY (`idCategoria`) REFERENCES `categoria` (`idCat`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_curso1` FOREIGN KEY (`idCurso`) REFERENCES `curso` (`idCurso`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of categoriacurso
-- ----------------------------
INSERT INTO `categoriacurso` VALUES (1, 1, '2020-01-30 01:16:01', 1);
INSERT INTO `categoriacurso` VALUES (1, 2, '2020-03-31 01:38:45', 1);
INSERT INTO `categoriacurso` VALUES (1, 3, '2020-03-31 03:00:00', 1);
INSERT INTO `categoriacurso` VALUES (2, 4, '2020-03-31 03:03:26', 1);

-- ----------------------------
-- Table structure for comentario
-- ----------------------------
DROP TABLE IF EXISTS `comentario`;
CREATE TABLE `comentario`  (
  `idComentario` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `valoracion` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `idUsuario` int(11) NULL DEFAULT NULL,
  `estado` int(11) NULL DEFAULT NULL,
  `idCurso` int(11) NULL DEFAULT NULL,
  `fechaComentario` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`idComentario`) USING BTREE,
  INDEX `fk_cursocomentario`(`idCurso`) USING BTREE,
  CONSTRAINT `fk_cursocomentario` FOREIGN KEY (`idCurso`) REFERENCES `curso` (`idCurso`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of comentario
-- ----------------------------

-- ----------------------------
-- Table structure for curso
-- ----------------------------
DROP TABLE IF EXISTS `curso`;
CREATE TABLE `curso`  (
  `idCurso` int(11) NOT NULL AUTO_INCREMENT,
  `codigoCurso` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `titulo` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `idAutor` int(11) NULL DEFAULT NULL,
  `precioPrincipal` decimal(10, 2) NULL DEFAULT NULL,
  `PrecioPromocion` decimal(10, 2) NULL DEFAULT NULL,
  `ImagenCurso` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `like` int(11) NULL DEFAULT NULL,
  `descripcion` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `adicional` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `fecha_registro` datetime(0) NULL DEFAULT NULL,
  `requerimiento` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `idProfesor` int(11) NULL DEFAULT NULL,
  `estado` int(11) NULL DEFAULT NULL,
  `slug` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idCurso`) USING BTREE,
  INDEX `fk_personaCurso`(`idProfesor`) USING BTREE,
  CONSTRAINT `fk_personaCurso` FOREIGN KEY (`idProfesor`) REFERENCES `profesor` (`idProfesor`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of curso
-- ----------------------------
INSERT INTO `curso` VALUES (1, 'SSKDJKCXD', 'kubernetes y docker en aws desde cero', NULL, 343.00, 43543.00, '5e322def77617e7724cee1e068ca28df943d6b873fee8.jpeg', NULL, '<p>Labour, of evaluated would he the a the our what is in the arduous sides behavioural is which the have didn&#39;t kicked records the it framework by the for traveler sure the can most well her. Entered have break himself cheek, and with activity, for bit of text.</p>\r\n\r\n<p>Not off be and of where and the together absolutely in a step I which where the original are feel as he of rung. On be walls. Create over to king intended musical relief. Project the of if both sort have a to clues okay. Produce the coast searched clarinet of it and ocean.</p>\r\n\r\n<p>Not off be and of where and the together absolutely in a step I which where the original are feel as he of rung. On be walls. Create over to king intended musical relief. Project the of if both sort have a to clues okay. Produce the coast searched clarinet of it and ocean.</p>\r\n\r\n<p>Not off be and of where and the together absolutely in a step I which where the original are feel as he of rung. On be walls. Create over to king intended musical relief. Project the of if both sort have a to clues okay. Produce the coast searched clarinet of it and ocean.</p>', '<p>Not off be and of where and the together absolutely in a step I which where the original are feel as he of rung. On be walls. Create over to king intended musical relief. Project the of if both sort have a to clues okay. Produce the coast searched clarinet of it and ocean.</p>\r\n\r\n<div id=\"gtx-trans\" style=\"position: absolute; left: -183px; top: -6px;\">\r\n<div class=\"gtx-trans-icon\">&nbsp;</div>\r\n</div>', NULL, '<p>Not off be and of where and the together absolutely in a step I which where the original are feel as he of rung. On be walls. Create over to king intended musical relief. Project the of if both sort have a to clues okay. Produce the coast searched clarinet of it and ocean.Not off be and of where and the together absolutely in a step I which where the original are feel as he of rung. On be walls. Create over to king intended musical relief. Project the of if both sort have a to clues okay. Produce the coast searched clarinet of it and ocean.</p>\r\n\r\n<div id=\"gtx-trans\" style=\"position: absolute; left: -271px; top: -6px;\">\r\n<div class=\"gtx-trans-icon\">&nbsp;</div>\r\n</div>', 1, 1, 'kubernetes-y-docker-en-aws-desde-cero');
INSERT INTO `curso` VALUES (2, 'RSKEXASF', 'Curso Forex Básico - Ejemplos EN VIVO de Trading Real', NULL, 500.00, 0.00, '5e829f18cdb651c94962dd6338e5be32a81c62fb99d7a.jpg', NULL, '<p>cursoooosdcdsvd</p>', '<p>adsvasdvsdvdsv</p>', '2020-03-31 01:38:45', '<p>nnkjbkjb</p>', 1, 1, 'curso-forex-basico-ejemplos-en-vivo-de-trading-real');
INSERT INTO `curso` VALUES (3, 'EWHDCSD', 'Aprende mi estrategia de Forex', NULL, 201.00, 0.00, '5e82b22b47fbb5f38a5e55873a8cb0342dd2522579293.jpg', NULL, '<p>assffsdfjsndflamsionweofinwksncl</p>', '<p>dsclsdnvweiovnerofnwoenwenfewoinf</p>', '2020-03-31 03:00:00', '<p>efwneiofnrovnsdlkcaslcmqpenfwefnwepifn</p>', 1, 1, 'aprende-mi-estrategia-de-forex');
INSERT INTO `curso` VALUES (4, 'IECYASC', 'Curso Forex Avanzado - Forex Crew Academy - Trading Simple', NULL, 231.00, 120.00, '5e82b2ee6d311de2d070c1b525b63c533356743545a3a.jpg', NULL, '<p>yguyuguyuuyvuv</p>', '<p>vyoyvoytcyocyc</p>', '2020-03-31 03:03:26', '<p>tyciytciytciytci</p>', 1, 1, 'curso-forex-avanzado-forex-crew-academy-trading-simple');

-- ----------------------------
-- Table structure for detallecurso
-- ----------------------------
DROP TABLE IF EXISTS `detallecurso`;
CREATE TABLE `detallecurso`  (
  `idDetalleCurso` int(11) NOT NULL AUTO_INCREMENT,
  `idCurso` int(11) NULL DEFAULT NULL,
  `titulo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '',
  `estado` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '',
  `fechaRegistro` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`idDetalleCurso`) USING BTREE,
  INDEX `fk_detalleCurso`(`idCurso`) USING BTREE,
  CONSTRAINT `fk_detalleCurso` FOREIGN KEY (`idCurso`) REFERENCES `curso` (`idCurso`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of detallecurso
-- ----------------------------
INSERT INTO `detallecurso` VALUES (1, 1, 'Understanding the Elements of User Experiences', '1', NULL);
INSERT INTO `detallecurso` VALUES (2, 1, 'Understanding the Elements of User Experience', '1', NULL);
INSERT INTO `detallecurso` VALUES (3, 1, 'Using the Design Elements Structure', '1', NULL);
INSERT INTO `detallecurso` VALUES (4, 1, 'Understanding the Elements of User Experience', '0', NULL);
INSERT INTO `detallecurso` VALUES (5, 1, 'Understanding the Elements of User Experience2222', '1', NULL);
INSERT INTO `detallecurso` VALUES (6, 2, 'Antes de Comenzar', '1', '2020-03-31 02:52:11');
INSERT INTO `detallecurso` VALUES (7, 2, 'Conceptos y Terminología Básica del FOREX 21 clases', '1', '2020-03-31 02:53:54');
INSERT INTO `detallecurso` VALUES (8, 2, 'Software de Trading: MetaTrader', '1', '2020-03-31 02:55:01');
INSERT INTO `detallecurso` VALUES (9, 3, 'Introducción', '1', '2020-03-31 03:05:25');
INSERT INTO `detallecurso` VALUES (10, 3, 'Empezar a operar', '1', '2020-03-31 03:05:55');
INSERT INTO `detallecurso` VALUES (11, 3, 'Mejorar las operaciones', '1', '2020-03-31 03:06:15');
INSERT INTO `detallecurso` VALUES (12, 3, 'Ejemplo de operaciones en directo', '1', '2020-03-31 03:06:33');
INSERT INTO `detallecurso` VALUES (13, 4, 'Introducción', '1', '2020-03-31 03:13:48');
INSERT INTO `detallecurso` VALUES (14, 4, 'Sección Avanzada', '1', '2020-03-31 03:14:05');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for frontend
-- ----------------------------
DROP TABLE IF EXISTS `frontend`;
CREATE TABLE `frontend`  (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Categoria` int(11) NOT NULL,
  `Tag` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Imagen_principal` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Contenido` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Estado` int(11) NOT NULL,
  `FechaIngreso` datetime(0) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of frontend
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_06_20_030304_create_roles_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_06_20_030857_create_role_user_table', 1);
INSERT INTO `migrations` VALUES (5, '2019_06_27_052846_create_modulos_table', 1);
INSERT INTO `migrations` VALUES (6, '2019_06_27_055429_create_paginas_table', 1);
INSERT INTO `migrations` VALUES (7, '2019_07_05_010724_create_frontend_table', 1);
INSERT INTO `migrations` VALUES (8, '2019_07_11_053057_create_fronts_table', 1);
INSERT INTO `migrations` VALUES (9, '2019_08_19_000000_create_failed_jobs_table', 1);

-- ----------------------------
-- Table structure for modulo
-- ----------------------------
DROP TABLE IF EXISTS `modulo`;
CREATE TABLE `modulo`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Descripcion` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Estado` int(11) NULL DEFAULT NULL,
  `Orden` int(11) NULL DEFAULT NULL,
  `Icono` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Link` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `LinkExterno` int(11) NULL DEFAULT NULL,
  `Route` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of modulo
-- ----------------------------
INSERT INTO `modulo` VALUES (1, 'Dasboard', 'Dasboard', 1, 1, '<i class=\"feather icon-home\"></i>', NULL, 0, 'admin/dashboard', NULL, NULL);
INSERT INTO `modulo` VALUES (2, 'Titular', 'Titular', 0, 5, '<i class=\"feather icon-box\"></i>', NULL, 0, 'admin/titular', '2019-07-16 23:41:37', NULL);
INSERT INTO `modulo` VALUES (3, 'Usuario', 'Usuario', 1, 3, '<i class=\"feather icon-settings\"></i>', 'http://google.com', 0, 'admin/usuario', '2019-07-16 23:41:50', NULL);
INSERT INTO `modulo` VALUES (4, 'Configuraciòn', 'Modulo de configuración', 1, 2, '<i class=\"feather icon-settings\"></i>', NULL, 0, 'admin/configuracion', NULL, NULL);
INSERT INTO `modulo` VALUES (7, 'Cuotas', 'Cuotas', 0, 7, '<i class=\"feather icon-box\"></i>', NULL, 0, 'admin/cuota', '2019-06-27 17:02:24', '2019-06-27 16:45:47');
INSERT INTO `modulo` VALUES (8, 'Expediente', 'Expediente', 0, 4, '<i class=\"feather icon-sidebar\"></i>', '#', 0, 'admin/expediente', '2019-07-16 23:58:44', '2019-07-03 23:25:48');
INSERT INTO `modulo` VALUES (9, 'Pagos', 'Pagos', 0, 8, '<i class=\"feather icon-sidebar\"></i>', NULL, 0, 'admin/pagos', '2019-07-03 23:25:48', '2019-07-03 23:25:48');
INSERT INTO `modulo` VALUES (10, 'Reporte', 'Reporte', 0, 9, '<i class=\"feather icon-sidebar\"></i>', NULL, 0, 'admin/reporte', '2019-07-03 23:25:48', '2019-07-03 23:25:48');
INSERT INTO `modulo` VALUES (11, 'Reuniones', 'Reuniones', 0, 6, '<i class=\"feather icon-home\"></i>', NULL, 0, 'admin/reuniones', '2019-08-08 18:31:31', '2019-08-08 18:31:31');
INSERT INTO `modulo` VALUES (12, 'Backup', 'Copia de seguridad', 0, 10, '<i class=\"feather icon-settings\"></i>', NULL, 0, 'admin/backup', NULL, NULL);
INSERT INTO `modulo` VALUES (13, 'Prospecto', 'Prospectos', 0, 11, '<i class=\"feather icon-box\"></i>', '#', 0, 'admin/prospectos', '2021-07-09 23:21:35', '2021-03-02 04:29:13');
INSERT INTO `modulo` VALUES (14, 'Solicitudes', 'Solicitudes', 0, 12, '<i class=\"feather icon-book\"></i>', '#', 0, 'admin/solicitudes', '2021-07-09 23:21:49', '2021-03-02 23:58:45');
INSERT INTO `modulo` VALUES (15, 'Personal', 'Personal', 1, 13, '<i class=\"feather icon-box\"></i>', NULL, NULL, 'admin/personal', '2021-07-08 03:20:49', '2021-07-08 03:20:49');
INSERT INTO `modulo` VALUES (16, 'Actividades', 'Actividades', 1, 14, '<i class=\"feather icon-book\"></i>', '#', 0, 'admin/actividades', '2021-07-14 22:30:31', '2021-07-14 22:30:31');

-- ----------------------------
-- Table structure for pagina
-- ----------------------------
DROP TABLE IF EXISTS `pagina`;
CREATE TABLE `pagina`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Padre` char(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ModuloID` int(11) NULL DEFAULT NULL,
  `Descripcion` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Ruta` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Slug` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Estado` int(11) NULL DEFAULT NULL,
  `Visible` int(11) NULL DEFAULT NULL,
  `Orden` int(11) NULL DEFAULT NULL,
  `FechaIngreso` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `FK_Modulo`(`ModuloID`) USING BTREE,
  CONSTRAINT `FK_Modulo` FOREIGN KEY (`ModuloID`) REFERENCES `modulo` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of pagina
-- ----------------------------
INSERT INTO `pagina` VALUES (1, '1', 1, 'Principal', 'dashboard', NULL, 1, 1, 1, '2019-06-24 23:05:15', NULL, NULL);
INSERT INTO `pagina` VALUES (2, '1', 2, 'Listado', '-', '-', 0, 1, 2, '2019-06-24 23:05:12', NULL, NULL);
INSERT INTO `pagina` VALUES (3, '1', 2, 'listado', 'titular/lista', '-', 1, 1, 3, NULL, '2019-07-17 01:09:11', NULL);
INSERT INTO `pagina` VALUES (4, '0', 4, 'Menú', 'configuracion/menu', '-', 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `pagina` VALUES (6, NULL, 3, 'listado', 'usuario/lista', NULL, 1, NULL, 5, NULL, '2019-07-17 01:09:02', '2019-07-01 19:05:30');
INSERT INTO `pagina` VALUES (7, NULL, 8, 'Listado', 'expediente/lista', NULL, 1, NULL, 6, NULL, '2019-07-17 01:09:26', '2019-07-03 23:26:17');
INSERT INTO `pagina` VALUES (8, NULL, 7, 'Listado', 'cuota/lista', NULL, 1, NULL, 7, NULL, '2019-07-17 01:09:19', '2019-07-17 00:59:03');
INSERT INTO `pagina` VALUES (9, NULL, 9, 'Buscar', 'pagos/pago/buscar', NULL, 1, NULL, 8, NULL, '2019-08-08 23:39:31', '2019-07-17 01:05:11');
INSERT INTO `pagina` VALUES (10, NULL, 10, 'Lista de socios', 'reporte/lista', NULL, 1, NULL, 9, NULL, '2019-08-10 00:49:36', '2019-07-17 01:09:52');
INSERT INTO `pagina` VALUES (11, NULL, 11, 'Listado', 'reuniones/lista', NULL, 1, NULL, 10, NULL, '2019-08-08 18:32:35', '2019-08-08 18:32:35');
INSERT INTO `pagina` VALUES (12, NULL, 10, 'Historial de expediente', 'reporte/expediente', NULL, 1, NULL, 11, NULL, '2019-08-10 02:42:14', '2019-08-10 00:51:48');
INSERT INTO `pagina` VALUES (13, NULL, 12, 'Backup', 'backup/lista', NULL, 1, NULL, 12, NULL, NULL, NULL);
INSERT INTO `pagina` VALUES (14, NULL, 13, 'Lista', 'prospectos/lista', NULL, 1, NULL, 13, NULL, '2021-03-02 04:30:18', '2021-03-02 04:30:18');
INSERT INTO `pagina` VALUES (15, NULL, 14, 'Pendientes', 'solicitudes/pendientes', NULL, 1, NULL, 14, NULL, '2021-03-03 00:00:00', '2021-03-03 00:00:00');
INSERT INTO `pagina` VALUES (16, NULL, 14, 'Terminado', 'solicitudes/terminado', NULL, 1, NULL, 15, NULL, '2021-03-03 05:04:48', '2021-03-03 05:04:48');
INSERT INTO `pagina` VALUES (17, NULL, 15, 'Lista', 'personal/lista', NULL, 1, NULL, 16, NULL, '2021-07-08 04:05:59', '2021-07-08 03:26:03');
INSERT INTO `pagina` VALUES (18, NULL, 16, 'Listado', 'actividades/lista', NULL, 1, NULL, 17, NULL, '2021-07-14 22:32:07', '2021-07-14 22:32:07');
INSERT INTO `pagina` VALUES (19, NULL, 16, 'Asignacion', 'actividades/asignacion/lista', NULL, 1, NULL, 18, NULL, '2021-07-15 00:52:13', '2021-07-14 22:33:19');

-- ----------------------------
-- Table structure for pais
-- ----------------------------
DROP TABLE IF EXISTS `pais`;
CREATE TABLE `pais`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pais` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '',
  `sigla` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `estado` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 255 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of pais
-- ----------------------------
INSERT INTO `pais` VALUES (1, 'Afghanistan', 'AF', 1);
INSERT INTO `pais` VALUES (2, 'Aland Islands', 'AX', 1);
INSERT INTO `pais` VALUES (3, 'Albania', 'AL', 1);
INSERT INTO `pais` VALUES (4, 'Algeria', 'DZ', 1);
INSERT INTO `pais` VALUES (5, 'American Samoa', 'AS', 1);
INSERT INTO `pais` VALUES (6, 'Andorra', 'AD', 1);
INSERT INTO `pais` VALUES (7, 'Angola', 'AO', 1);
INSERT INTO `pais` VALUES (8, 'Anguilla', 'AI', 1);
INSERT INTO `pais` VALUES (9, 'Antarctica', 'AQ', 1);
INSERT INTO `pais` VALUES (10, 'Antigua and Barbuda', 'AG', 1);
INSERT INTO `pais` VALUES (11, 'Argentina', 'AR', 1);
INSERT INTO `pais` VALUES (12, 'Armenia', 'AM', 1);
INSERT INTO `pais` VALUES (13, 'Aruba', 'AW', 1);
INSERT INTO `pais` VALUES (14, 'Australia', 'AU', 1);
INSERT INTO `pais` VALUES (15, 'Austria', 'AT', 1);
INSERT INTO `pais` VALUES (16, 'Azerbaijan', 'AZ', 1);
INSERT INTO `pais` VALUES (17, 'Bahamas', 'BS', 1);
INSERT INTO `pais` VALUES (18, 'Bahrain', 'BH', 1);
INSERT INTO `pais` VALUES (19, 'Bangladesh', 'BD', 1);
INSERT INTO `pais` VALUES (20, 'Barbados', 'BB', 1);
INSERT INTO `pais` VALUES (21, 'Belarus', 'BY', 1);
INSERT INTO `pais` VALUES (22, 'Belgium', 'BE', 1);
INSERT INTO `pais` VALUES (23, 'Belize', 'BZ', 1);
INSERT INTO `pais` VALUES (24, 'Benin', 'BJ', 1);
INSERT INTO `pais` VALUES (25, 'Bermuda', 'BM', 1);
INSERT INTO `pais` VALUES (26, 'Bhutan', 'BT', 1);
INSERT INTO `pais` VALUES (27, 'Bolivia', 'BO', 1);
INSERT INTO `pais` VALUES (28, 'Bonaire, Saint Eustatius and Saba ', 'BQ', 1);
INSERT INTO `pais` VALUES (29, 'Bosnia and Herzegovina', 'BA', 1);
INSERT INTO `pais` VALUES (30, 'Botswana', 'BW', 1);
INSERT INTO `pais` VALUES (31, 'Bouvet Island', 'BV', 1);
INSERT INTO `pais` VALUES (32, 'Brazil', 'BR', 1);
INSERT INTO `pais` VALUES (33, 'British Indian Ocean Territory', 'IO', 1);
INSERT INTO `pais` VALUES (34, 'British Virgin Islands', 'VG', 1);
INSERT INTO `pais` VALUES (35, 'Brunei', 'BN', 1);
INSERT INTO `pais` VALUES (36, 'Bulgaria', 'BG', 1);
INSERT INTO `pais` VALUES (37, 'Burkina Faso', 'BF', 1);
INSERT INTO `pais` VALUES (38, 'Burundi', 'BI', 1);
INSERT INTO `pais` VALUES (39, 'Cambodia', 'KH', 1);
INSERT INTO `pais` VALUES (40, 'Cameroon', 'CM', 1);
INSERT INTO `pais` VALUES (41, 'Canada', 'CA', 1);
INSERT INTO `pais` VALUES (42, 'Cape Verde', 'CV', 1);
INSERT INTO `pais` VALUES (43, 'Cayman Islands', 'KY', 1);
INSERT INTO `pais` VALUES (44, 'Central African Republic', 'CF', 1);
INSERT INTO `pais` VALUES (45, 'Chad', 'TD', 1);
INSERT INTO `pais` VALUES (46, 'Chile', 'CL', 1);
INSERT INTO `pais` VALUES (47, 'China', 'CN', 1);
INSERT INTO `pais` VALUES (48, 'Christmas Island', 'CX', 1);
INSERT INTO `pais` VALUES (49, 'Cocos Islands', 'CC', 1);
INSERT INTO `pais` VALUES (50, 'Colombia', 'CO', 1);
INSERT INTO `pais` VALUES (51, 'Comoros', 'KM', 1);
INSERT INTO `pais` VALUES (52, 'Cook Islands', 'CK', 1);
INSERT INTO `pais` VALUES (53, 'Costa Rica', 'CR', 1);
INSERT INTO `pais` VALUES (54, 'Croatia', 'HR', 1);
INSERT INTO `pais` VALUES (55, 'Cuba', 'CU', 1);
INSERT INTO `pais` VALUES (56, 'Curacao', 'CW', 1);
INSERT INTO `pais` VALUES (57, 'Cyprus', 'CY', 1);
INSERT INTO `pais` VALUES (58, 'Czech Republic', 'CZ', 1);
INSERT INTO `pais` VALUES (59, 'Czechia', 'CZ', 1);
INSERT INTO `pais` VALUES (60, 'Democratic Republic of the Congo', 'CD', 1);
INSERT INTO `pais` VALUES (61, 'Denmark', 'DK', 1);
INSERT INTO `pais` VALUES (62, 'Djibouti', 'DJ', 1);
INSERT INTO `pais` VALUES (63, 'Dominica', 'DM', 1);
INSERT INTO `pais` VALUES (64, 'Dominican Republic', 'DO', 1);
INSERT INTO `pais` VALUES (65, 'East Timor', 'TL', 1);
INSERT INTO `pais` VALUES (66, 'Ecuador', 'EC', 1);
INSERT INTO `pais` VALUES (67, 'Egypt', 'EG', 1);
INSERT INTO `pais` VALUES (68, 'El Salvador', 'SV', 1);
INSERT INTO `pais` VALUES (69, 'Equatorial Guinea', 'GQ', 1);
INSERT INTO `pais` VALUES (70, 'Eritrea', 'ER', 1);
INSERT INTO `pais` VALUES (71, 'Estonia', 'EE', 1);
INSERT INTO `pais` VALUES (72, 'Ethiopia', 'ET', 1);
INSERT INTO `pais` VALUES (73, 'Falkland Islands', 'FK', 1);
INSERT INTO `pais` VALUES (74, 'Faroe Islands', 'FO', 1);
INSERT INTO `pais` VALUES (75, 'Fiji', 'FJ', 1);
INSERT INTO `pais` VALUES (76, 'Finland', 'FI', 1);
INSERT INTO `pais` VALUES (77, 'France', 'FR', 1);
INSERT INTO `pais` VALUES (78, 'French Guiana', 'GF', 1);
INSERT INTO `pais` VALUES (79, 'French Polynesia', 'PF', 1);
INSERT INTO `pais` VALUES (80, 'French Southern Territories', 'TF', 1);
INSERT INTO `pais` VALUES (81, 'Gabon', 'GA', 1);
INSERT INTO `pais` VALUES (82, 'Gambia', 'GM', 1);
INSERT INTO `pais` VALUES (83, 'Georgia', 'GE', 1);
INSERT INTO `pais` VALUES (84, 'Germany', 'DE', 1);
INSERT INTO `pais` VALUES (85, 'Ghana', 'GH', 1);
INSERT INTO `pais` VALUES (86, 'Gibraltar', 'GI', 1);
INSERT INTO `pais` VALUES (87, 'Greece', 'GR', 1);
INSERT INTO `pais` VALUES (88, 'Greenland', 'GL', 1);
INSERT INTO `pais` VALUES (89, 'Grenada', 'GD', 1);
INSERT INTO `pais` VALUES (90, 'Guadeloupe', 'GP', 1);
INSERT INTO `pais` VALUES (91, 'Guam', 'GU', 1);
INSERT INTO `pais` VALUES (92, 'Guatemala', 'GT', 1);
INSERT INTO `pais` VALUES (93, 'Guernsey', 'GG', 1);
INSERT INTO `pais` VALUES (94, 'Guinea-Bissau', 'GW', 1);
INSERT INTO `pais` VALUES (95, 'Guinea', 'GN', 1);
INSERT INTO `pais` VALUES (96, 'Guyana', 'GY', 1);
INSERT INTO `pais` VALUES (97, 'Haiti', 'HT', 1);
INSERT INTO `pais` VALUES (98, 'Heard Island and McDonald Islands', 'HM', 1);
INSERT INTO `pais` VALUES (99, 'Honduras', 'HN', 1);
INSERT INTO `pais` VALUES (100, 'Hong Kong', 'HK', 1);
INSERT INTO `pais` VALUES (101, 'Hungary', 'HU', 1);
INSERT INTO `pais` VALUES (102, 'Iceland', 'IS', 1);
INSERT INTO `pais` VALUES (103, 'India', 'IN', 1);
INSERT INTO `pais` VALUES (104, 'Indonesia', 'ID', 1);
INSERT INTO `pais` VALUES (105, 'Iran', 'IR', 1);
INSERT INTO `pais` VALUES (106, 'Iraq', 'IQ', 1);
INSERT INTO `pais` VALUES (107, 'Ireland', 'IE', 1);
INSERT INTO `pais` VALUES (108, 'Isle of Man', 'IM', 1);
INSERT INTO `pais` VALUES (109, 'Israel', 'IL', 1);
INSERT INTO `pais` VALUES (110, 'Italy', 'IT', 1);
INSERT INTO `pais` VALUES (111, 'Ivory Coast', 'CI', 1);
INSERT INTO `pais` VALUES (112, 'Jamaica', 'JM', 1);
INSERT INTO `pais` VALUES (113, 'Japan', 'JP', 1);
INSERT INTO `pais` VALUES (114, 'Jersey', 'JE', 1);
INSERT INTO `pais` VALUES (115, 'Jordan', 'JO', 1);
INSERT INTO `pais` VALUES (116, 'Kazakhstan', 'KZ', 1);
INSERT INTO `pais` VALUES (117, 'Kenya', 'KE', 1);
INSERT INTO `pais` VALUES (118, 'Kiribati', 'KI', 1);
INSERT INTO `pais` VALUES (119, 'Korea, South', 'KR', 1);
INSERT INTO `pais` VALUES (120, 'Kosovo', 'XK', 1);
INSERT INTO `pais` VALUES (121, 'Kuwait', 'KW', 1);
INSERT INTO `pais` VALUES (122, 'Kyrgyzstan', 'KG', 1);
INSERT INTO `pais` VALUES (123, 'Laos', 'LA', 1);
INSERT INTO `pais` VALUES (124, 'Latvia', 'LV', 1);
INSERT INTO `pais` VALUES (125, 'Lebanon', 'LB', 1);
INSERT INTO `pais` VALUES (126, 'Lesotho', 'LS', 1);
INSERT INTO `pais` VALUES (127, 'Liberia', 'LR', 1);
INSERT INTO `pais` VALUES (128, 'Libya', 'LY', 1);
INSERT INTO `pais` VALUES (129, 'Liechtenstein', 'LI', 1);
INSERT INTO `pais` VALUES (130, 'Lithuania', 'LT', 1);
INSERT INTO `pais` VALUES (131, 'Luxembourg', 'LU', 1);
INSERT INTO `pais` VALUES (132, 'Macao', 'MO', 1);
INSERT INTO `pais` VALUES (133, 'Macedonia', 'MK', 1);
INSERT INTO `pais` VALUES (134, 'Madagascar', 'MG', 1);
INSERT INTO `pais` VALUES (135, 'Mainland China', 'CN', 1);
INSERT INTO `pais` VALUES (136, 'Malawi', 'MW', 1);
INSERT INTO `pais` VALUES (137, 'Malaysia', 'MY', 1);
INSERT INTO `pais` VALUES (138, 'Maldives', 'MV', 1);
INSERT INTO `pais` VALUES (139, 'Mali', 'ML', 1);
INSERT INTO `pais` VALUES (140, 'Malta', 'MT', 1);
INSERT INTO `pais` VALUES (141, 'Marshall Islands', 'MH', 1);
INSERT INTO `pais` VALUES (142, 'Martinique', 'MQ', 1);
INSERT INTO `pais` VALUES (143, 'Mauritania', 'MR', 1);
INSERT INTO `pais` VALUES (144, 'Mauritius', 'MU', 1);
INSERT INTO `pais` VALUES (145, 'Mayotte', 'YT', 1);
INSERT INTO `pais` VALUES (146, 'Mexico', 'MX', 1);
INSERT INTO `pais` VALUES (147, 'Micronesia', 'FM', 1);
INSERT INTO `pais` VALUES (148, 'Moldova', 'MD', 1);
INSERT INTO `pais` VALUES (149, 'Monaco', 'MC', 1);
INSERT INTO `pais` VALUES (150, 'Mongolia', 'MN', 1);
INSERT INTO `pais` VALUES (151, 'Montenegro', 'ME', 1);
INSERT INTO `pais` VALUES (152, 'Montserrat', 'MS', 1);
INSERT INTO `pais` VALUES (153, 'Morocco', 'MA', 1);
INSERT INTO `pais` VALUES (154, 'Mozambique', 'MZ', 1);
INSERT INTO `pais` VALUES (155, 'Myanmar', 'MM', 1);
INSERT INTO `pais` VALUES (156, 'Namibia', 'NA', 1);
INSERT INTO `pais` VALUES (157, 'Nauru', 'NR', 1);
INSERT INTO `pais` VALUES (158, 'Nepal', 'NP', 1);
INSERT INTO `pais` VALUES (159, 'Netherlands', 'NL', 1);
INSERT INTO `pais` VALUES (160, 'New Caledonia', 'NC', 1);
INSERT INTO `pais` VALUES (161, 'New Zealand', 'NZ', 1);
INSERT INTO `pais` VALUES (162, 'Nicaragua', 'NI', 1);
INSERT INTO `pais` VALUES (163, 'Niger', 'NE', 1);
INSERT INTO `pais` VALUES (164, 'Nigeria', 'NG', 1);
INSERT INTO `pais` VALUES (165, 'Niue', 'NU', 1);
INSERT INTO `pais` VALUES (166, 'Norfolk Island', 'NF', 1);
INSERT INTO `pais` VALUES (167, 'North Korea', 'KP', 1);
INSERT INTO `pais` VALUES (168, 'Northern Mariana Islands', 'MP', 1);
INSERT INTO `pais` VALUES (169, 'Norway', 'NO', 1);
INSERT INTO `pais` VALUES (170, 'Oman', 'OM', 1);
INSERT INTO `pais` VALUES (171, 'Pakistan', 'PK', 1);
INSERT INTO `pais` VALUES (172, 'Palau', 'PW', 1);
INSERT INTO `pais` VALUES (173, 'Palestinian Territory', 'PS', 1);
INSERT INTO `pais` VALUES (174, 'Panama', 'PA', 1);
INSERT INTO `pais` VALUES (175, 'Papua New Guinea', 'PG', 1);
INSERT INTO `pais` VALUES (176, 'Paraguay', 'PY', 1);
INSERT INTO `pais` VALUES (177, 'Peru', 'PE', 1);
INSERT INTO `pais` VALUES (178, 'Philippines', 'PH', 1);
INSERT INTO `pais` VALUES (179, 'Pitcairn', 'PN', 1);
INSERT INTO `pais` VALUES (180, 'Poland', 'PL', 1);
INSERT INTO `pais` VALUES (181, 'Portugal', 'PT', 1);
INSERT INTO `pais` VALUES (182, 'Puerto Rico', 'PR', 1);
INSERT INTO `pais` VALUES (183, 'Qatar', 'QA', 1);
INSERT INTO `pais` VALUES (184, 'Republic of the Congo', 'CG', 1);
INSERT INTO `pais` VALUES (185, 'Reunion', 'RE', 1);
INSERT INTO `pais` VALUES (186, 'Romania', 'RO', 1);
INSERT INTO `pais` VALUES (187, 'Russia', 'RU', 1);
INSERT INTO `pais` VALUES (188, 'Rwanda', 'RW', 1);
INSERT INTO `pais` VALUES (189, 'Saint Barthelemy', 'BL', 1);
INSERT INTO `pais` VALUES (190, 'Saint Helena', 'SH', 1);
INSERT INTO `pais` VALUES (191, 'Saint Kitts and Nevis', 'KN', 1);
INSERT INTO `pais` VALUES (192, 'Saint Lucia', 'LC', 1);
INSERT INTO `pais` VALUES (193, 'Saint Martin', 'MF', 1);
INSERT INTO `pais` VALUES (194, 'Saint Pierre and Miquelon', 'PM', 1);
INSERT INTO `pais` VALUES (195, 'Saint Vincent and the Grenadines', 'VC', 1);
INSERT INTO `pais` VALUES (196, 'Samoa', 'WS', 1);
INSERT INTO `pais` VALUES (197, 'San Marino', 'SM', 1);
INSERT INTO `pais` VALUES (198, 'Sao Tome and Principe', 'ST', 1);
INSERT INTO `pais` VALUES (199, 'Saudi Arabia', 'SA', 1);
INSERT INTO `pais` VALUES (200, 'Senegal', 'SN', 1);
INSERT INTO `pais` VALUES (201, 'Serbia', 'RS', 1);
INSERT INTO `pais` VALUES (202, 'Seychelles', 'SC', 1);
INSERT INTO `pais` VALUES (203, 'Sierra Leone', 'SL', 1);
INSERT INTO `pais` VALUES (204, 'Singapore', 'SG', 1);
INSERT INTO `pais` VALUES (205, 'Sint Maarten', 'SX', 1);
INSERT INTO `pais` VALUES (206, 'Slovakia', 'SK', 1);
INSERT INTO `pais` VALUES (207, 'Slovenia', 'SI', 1);
INSERT INTO `pais` VALUES (208, 'Solomon Islands', 'SB', 1);
INSERT INTO `pais` VALUES (209, 'Somalia', 'SO', 1);
INSERT INTO `pais` VALUES (210, 'South Africa', 'ZA', 1);
INSERT INTO `pais` VALUES (211, 'South Georgia and the South Sandwich Islands', 'GS', 1);
INSERT INTO `pais` VALUES (212, 'South Korea', 'KR', 1);
INSERT INTO `pais` VALUES (213, 'South Sudan', 'SS', 1);
INSERT INTO `pais` VALUES (214, 'Spain', 'ES', 1);
INSERT INTO `pais` VALUES (215, 'Sri Lanka', 'LK', 1);
INSERT INTO `pais` VALUES (216, 'Sudan', 'SD', 1);
INSERT INTO `pais` VALUES (217, 'Suriname', 'SR', 1);
INSERT INTO `pais` VALUES (218, 'Svalbard and Jan Mayen', 'SJ', 1);
INSERT INTO `pais` VALUES (219, 'Swaziland', 'SZ', 1);
INSERT INTO `pais` VALUES (220, 'Sweden', 'SE', 1);
INSERT INTO `pais` VALUES (221, 'Switzerland', 'CH', 1);
INSERT INTO `pais` VALUES (222, 'Syria', 'SY', 1);
INSERT INTO `pais` VALUES (223, 'Taiwan', 'TW', 1);
INSERT INTO `pais` VALUES (224, 'Taiwan*', 'TW', 1);
INSERT INTO `pais` VALUES (225, 'Tajikistan', 'TJ', 1);
INSERT INTO `pais` VALUES (226, 'Tanzania', 'TZ', 1);
INSERT INTO `pais` VALUES (227, 'Thailand', 'TH', 1);
INSERT INTO `pais` VALUES (228, 'Togo', 'TG', 1);
INSERT INTO `pais` VALUES (229, 'Tokelau', 'TK', 1);
INSERT INTO `pais` VALUES (230, 'Tonga', 'TO', 1);
INSERT INTO `pais` VALUES (231, 'Trinidad and Tobago', 'TT', 1);
INSERT INTO `pais` VALUES (232, 'Tunisia', 'TN', 1);
INSERT INTO `pais` VALUES (233, 'Turkey', 'TR', 1);
INSERT INTO `pais` VALUES (234, 'Turkmenistan', 'TM', 1);
INSERT INTO `pais` VALUES (235, 'Turks and Caicos Islands', 'TC', 1);
INSERT INTO `pais` VALUES (236, 'Tuvalu', 'TV', 1);
INSERT INTO `pais` VALUES (237, 'U.S. Virgin Islands', 'VI', 1);
INSERT INTO `pais` VALUES (238, 'Uganda', 'UG', 1);
INSERT INTO `pais` VALUES (239, 'Ukraine', 'UA', 1);
INSERT INTO `pais` VALUES (240, 'United Arab Emirates', 'AE', 1);
INSERT INTO `pais` VALUES (241, 'United Kingdom', 'GB', 1);
INSERT INTO `pais` VALUES (242, 'United States Minor Outlying Islands', 'UM', 1);
INSERT INTO `pais` VALUES (243, 'Uruguay', 'UY', 1);
INSERT INTO `pais` VALUES (244, 'US', 'US', 1);
INSERT INTO `pais` VALUES (245, 'Uzbekistan', 'UZ', 1);
INSERT INTO `pais` VALUES (246, 'Vanuatu', 'VU', 1);
INSERT INTO `pais` VALUES (247, 'Vatican', 'VA', 1);
INSERT INTO `pais` VALUES (248, 'Venezuela', 'VE', 1);
INSERT INTO `pais` VALUES (249, 'Vietnam', 'VN', 1);
INSERT INTO `pais` VALUES (250, 'Wallis and Futuna', 'WF', 1);
INSERT INTO `pais` VALUES (251, 'Western Sahara', 'EH', 1);
INSERT INTO `pais` VALUES (252, 'Yemen', 'YE', 1);
INSERT INTO `pais` VALUES (253, 'Zambia', 'ZM', 1);
INSERT INTO `pais` VALUES (254, 'Zimbabwe', 'ZW', 1);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for persona
-- ----------------------------
DROP TABLE IF EXISTS `persona`;
CREATE TABLE `persona`  (
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `apellidoPaterno` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `apellidoMaterno` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `fechaNacimiento` datetime(0) NULL DEFAULT NULL,
  `foto` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `estado` int(11) NULL DEFAULT NULL,
  `idpais` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '',
  `sobremi` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `enlace` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idPersona`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of persona
-- ----------------------------
INSERT INTO `persona` VALUES (1, 'Juan', 'Perez', 'Garcia', '2020-01-21 00:00:00', NULL, 1, '177', NULL, NULL);
INSERT INTO `persona` VALUES (6, 'juan', 'perez', 'perex', '2020-01-01 00:00:00', NULL, 1, '177', 'ddsdsd', 'sdfdsdf');
INSERT INTO `persona` VALUES (7, 'Miguel', 'Garcia', 'Carranza', NULL, NULL, 1, '5', NULL, NULL);
INSERT INTO `persona` VALUES (8, 'Junior', 'ddd', 'wwww', '2021-07-07 00:00:00', NULL, 1, '177', NULL, 'http://www.google.com');
INSERT INTO `persona` VALUES (9, 'Junior', NULL, NULL, NULL, NULL, 1, '', NULL, NULL);
INSERT INTO `persona` VALUES (10, 'Carlitos', NULL, NULL, NULL, NULL, 1, '', NULL, NULL);
INSERT INTO `persona` VALUES (11, 'Jorge', NULL, NULL, NULL, NULL, 1, '', NULL, NULL);
INSERT INTO `persona` VALUES (12, 'roberto', 'wwww', 'eeee', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `persona` VALUES (13, 'robertos', 'perez', 'eeeee', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `persona` VALUES (14, 'robertos3', NULL, NULL, NULL, NULL, 1, '', NULL, NULL);
INSERT INTO `persona` VALUES (15, 'xxx', NULL, NULL, NULL, NULL, 1, '', NULL, NULL);
INSERT INTO `persona` VALUES (16, 'vega', NULL, NULL, NULL, NULL, 1, '', NULL, NULL);
INSERT INTO `persona` VALUES (18, 'juan', 'perez', 'perez', '2021-10-10 00:00:00', NULL, 1, '177', NULL, NULL);
INSERT INTO `persona` VALUES (19, 'Miguel', 'Sanchez', 'Marino', '2021-03-06 00:00:00', NULL, 1, '177', NULL, NULL);
INSERT INTO `persona` VALUES (20, 'Marco', 'Smit', 'Martinez', '2021-03-06 00:00:00', NULL, 1, '177', NULL, NULL);

-- ----------------------------
-- Table structure for profesor
-- ----------------------------
DROP TABLE IF EXISTS `profesor`;
CREATE TABLE `profesor`  (
  `idProfesor` int(11) NOT NULL AUTO_INCREMENT,
  `idPersona` int(11) NULL DEFAULT NULL,
  `estado` int(11) NULL DEFAULT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `especialidad` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idProfesor`) USING BTREE,
  INDEX `fk_persona_profe`(`idPersona`) USING BTREE,
  CONSTRAINT `fk_persona_profe` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of profesor
-- ----------------------------
INSERT INTO `profesor` VALUES (1, 7, 1, 'Nuevo profesor', '30-year UX + Design Veteran; Consultant, Author & Speaker');

-- ----------------------------
-- Table structure for prospectos
-- ----------------------------
DROP TABLE IF EXISTS `prospectos`;
CREATE TABLE `prospectos`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `apellidoPaterno` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `apellidoMaterno` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipo_documento` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nro_documento` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `telefono` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `estado` int(255) NULL DEFAULT NULL,
  `fecha_registro` date NULL DEFAULT NULL,
  `compra_saldo` int(11) NULL DEFAULT NULL,
  `validado` int(11) NULL DEFAULT NULL,
  `usuario_registro` int(11) NULL DEFAULT NULL,
  `voucher` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `estado_voucher` int(255) NULL DEFAULT NULL,
  `monto_total` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `direccion` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `latitud` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `longitud` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `referencia` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of prospectos
-- ----------------------------
INSERT INTO `prospectos` VALUES (1, 'aaa', 'ssss', 'wwwwww', 'eeeee2@hotmail.com', 'D', '324234234', '986575551', 1, '2021-03-02', 1, 1, 3, '603ece8702efd.jpg', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `prospectos` VALUES (2, 'miguel', 'salas', 'salas', 'zzz@sss.aw', 'D', '58584848', '34232422', 1, '2021-03-02', 2, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `prospectos` VALUES (3, 'sssd', 'fsfdsf', 'sdfsdfsd', 'tello_jr2@hotmail.com', 'D', '324234234', '986575551', 1, '2021-03-02', 1, 1, 3, '603ecfb2a15b0.jpg', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `prospectos` VALUES (4, 'Carlos', 'GArcvia', 'garcia', 'aaa@fff.com', 'D', '12547895', '895498489', 1, '2021-03-06', 1, 1, 3, '6043e89a28331.png', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `prospectos` VALUES (5, 'xxxx', 'ssss', 'aaaa', 'zkated@gmail.comw', 'R', '22222222222', '968824886', 1, '2021-03-16', 1, 1, 3, '60524c47d8aa0.PNG', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `prospectos` VALUES (8, 'Jorge', 'Ulloa', 'Bellido', 'qqq@www.wco', 'D', '123123123', '933321233', 1, '2021-03-18', 2, 1, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `prospectos` VALUES (12, 'carmen', 'Martinez', 'Cabrera', 'cbabrera@sss.com', 'D', '11122221', '3222323232332', 1, '2021-03-28', 2, 1, 1, '', 0, NULL, 'Lima', '-12.06102939', '-77.04584241', 'Av caceres');
INSERT INTO `prospectos` VALUES (13, 'demo', 'demo', 'demo', 'deeeef2@ff.fff', 'R', '585848483', '3333', 1, '2021-05-21', 2, 1, 1, NULL, 0, NULL, '3333', '-11.96993813', '-76.99967086', 'www');

-- ----------------------------
-- Table structure for role_user
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` bigint(10) UNSIGNED NOT NULL,
  `user_id` bigint(10) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fkuser`(`user_id`) USING BTREE,
  INDEX `fkroles`(`role_id`) USING BTREE,
  CONSTRAINT `fkroles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fkuser` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of role_user
-- ----------------------------
INSERT INTO `role_user` VALUES (1, 1, 1, '2019-07-05 01:16:00', '2019-07-05 01:16:00');
INSERT INTO `role_user` VALUES (2, 2, 2, NULL, NULL);
INSERT INTO `role_user` VALUES (3, 2, 6, NULL, NULL);
INSERT INTO `role_user` VALUES (4, 2, 7, NULL, NULL);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `estado` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 'admin', 'Administrador', '2019-07-05 01:14:11', '2019-07-05 01:14:11', 1);
INSERT INTO `roles` VALUES (2, 'user', 'Personal', '2019-07-05 01:14:11', '2019-07-05 01:14:11', 0);
INSERT INTO `roles` VALUES (3, 'editor', 'Editor', '2019-07-05 01:14:11', '2019-07-05 01:14:11', 0);
INSERT INTO `roles` VALUES (4, 'vendedor', 'Jefe Personal', '2021-03-03 15:49:53', '2021-03-03 15:49:53', 1);
INSERT INTO `roles` VALUES (5, 'supervisor', 'Analista', '2021-03-03 15:49:53', '2021-03-03 15:49:53', 1);
INSERT INTO `roles` VALUES (6, 'cobrador', 'Personal Monitoreo', '2021-03-03 15:49:53', '2021-03-03 15:49:53', 1);

-- ----------------------------
-- Table structure for tbl_comprobante_facturacion
-- ----------------------------
DROP TABLE IF EXISTS `tbl_comprobante_facturacion`;
CREATE TABLE `tbl_comprobante_facturacion`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Gx_UsuEnc` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Gx_PasEnc` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Gx_Key` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `TipoDocSol` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `NroDocSol` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `DigVerSol` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `CorreoSol` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `TelefSol` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `TipoDocComprobante` char(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `NroDocumentoFAC` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ReferenceCode` varchar(35) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `SDT_TitMas` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `TipoDocCPT` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `NroDocCPT` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `TipoReporte` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Pago` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `json` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `FechaRegistro` datetime(0) NULL DEFAULT NULL,
  `idCliente` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tbl_comprobante_facturacion
-- ----------------------------
INSERT INTO `tbl_comprobante_facturacion` VALUES (3, NULL, NULL, NULL, 'D', '11122221', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'D', '46398244', 'R', NULL, '{\"SDT_TitMas_Out\":[],\"ID_Transaccion\":\"\",\"CodigoWS\":\"97\",\"FOnline\":{\"id\":0,\"des\":\"\"}}', '2021-04-08 19:09:19', 12);
INSERT INTO `tbl_comprobante_facturacion` VALUES (4, NULL, NULL, NULL, 'D', '11122221', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'D', '46398244', 'R', NULL, '{\"SDT_TitMas_Out\":[],\"ID_Transaccion\":\"\",\"CodigoWS\":\"97\",\"FOnline\":{\"id\":0,\"des\":\"\"}}', '2021-04-08 20:34:20', 12);
INSERT INTO `tbl_comprobante_facturacion` VALUES (5, NULL, NULL, NULL, 'D', '11122221', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'D', '11122221', 'R', NULL, '{\"SDT_TitMas_Out\":[],\"ID_Transaccion\":\"\",\"CodigoWS\":\"97\",\"FOnline\":{\"id\":0,\"des\":\"\"}}', '2021-04-08 20:36:48', 12);

-- ----------------------------
-- Table structure for tbl_historial_cliente
-- ----------------------------
DROP TABLE IF EXISTS `tbl_historial_cliente`;
CREATE TABLE `tbl_historial_cliente`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monto` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `voucher` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `idUsuario` int(11) NULL DEFAULT NULL,
  `idCliente` int(11) NULL DEFAULT NULL,
  `estado` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `fechaRegistro` datetime(0) NULL DEFAULT NULL,
  `idProspecto` int(11) NULL DEFAULT NULL,
  `validado` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tbl_historial_cliente
-- ----------------------------
INSERT INTO `tbl_historial_cliente` VALUES (1, '2', '605174eb94f6b.PNG', 3, 5, '1', NULL, NULL, 1);
INSERT INTO `tbl_historial_cliente` VALUES (2, '3', '605174eb94f6b.PNG', 3, 5, '1', '2021-03-17 00:00:00', NULL, 1);
INSERT INTO `tbl_historial_cliente` VALUES (3, '2', '605177a7ba3db.jpg', 3, 5, '1', '2021-03-17 00:00:00', NULL, 1);
INSERT INTO `tbl_historial_cliente` VALUES (4, '3', '60517e433ee18.PNG', 3, 4, '1', '2021-03-17 03:57:55', NULL, 1);
INSERT INTO `tbl_historial_cliente` VALUES (5, '2', '', 3, 8, '1', '2021-03-18 00:00:00', NULL, 1);
INSERT INTO `tbl_historial_cliente` VALUES (6, '3', '605177a7ba3db.jpg', NULL, 8, '1', '2021-03-17 00:00:00', NULL, 1);
INSERT INTO `tbl_historial_cliente` VALUES (7, '2', '6053c045b7b77.PNG', NULL, 8, '1', NULL, NULL, 1);
INSERT INTO `tbl_historial_cliente` VALUES (8, '2', '6053c05c3548f.PNG', NULL, 8, '1', NULL, NULL, 1);
INSERT INTO `tbl_historial_cliente` VALUES (9, '1', '6053c131b9a42.jpg', NULL, 8, '1', '2021-03-18 21:08:01', NULL, 1);
INSERT INTO `tbl_historial_cliente` VALUES (10, '3', '6053c1bba0e8c.jpg', NULL, 8, '1', '2021-03-18 21:10:19', NULL, 1);
INSERT INTO `tbl_historial_cliente` VALUES (11, '2', '6054c6843f31f.PNG', 3, 8, '1', '2021-03-19 15:43:00', NULL, 1);
INSERT INTO `tbl_historial_cliente` VALUES (12, '1', '6054c6fed03a5.PNG', 3, 8, '1', '2021-03-19 15:45:02', NULL, 1);
INSERT INTO `tbl_historial_cliente` VALUES (13, '1', '6054c8695e863.PNG', NULL, 8, '1', '2021-03-19 15:51:05', NULL, 1);
INSERT INTO `tbl_historial_cliente` VALUES (14, NULL, '', 1, 12, '1', '2021-03-28 00:00:00', NULL, NULL);
INSERT INTO `tbl_historial_cliente` VALUES (15, '2', '6060df207ce6f.PNG', NULL, 12, '1', '2021-03-28 19:55:12', NULL, 1);
INSERT INTO `tbl_historial_cliente` VALUES (16, NULL, '', 1, 13, '1', '2021-05-21 00:00:00', NULL, NULL);
INSERT INTO `tbl_historial_cliente` VALUES (17, '4', 'LIBRE', 1, 13, '1', '2021-05-22 02:42:24', NULL, 1);

-- ----------------------------
-- Table structure for tbl_montos
-- ----------------------------
DROP TABLE IF EXISTS `tbl_montos`;
CREATE TABLE `tbl_montos`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `montos` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `estado` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tbl_montos
-- ----------------------------
INSERT INTO `tbl_montos` VALUES (1, '10', '1', 'B');
INSERT INTO `tbl_montos` VALUES (2, '20', '1', 'D');
INSERT INTO `tbl_montos` VALUES (3, '25', '1', 'R');

-- ----------------------------
-- Table structure for tbl_tipo_documento
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tipo_documento`;
CREATE TABLE `tbl_tipo_documento`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Valor` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tbl_tipo_documento
-- ----------------------------
INSERT INTO `tbl_tipo_documento` VALUES (1, 'DNI', 'D');
INSERT INTO `tbl_tipo_documento` VALUES (2, 'RUC', 'R');
INSERT INTO `tbl_tipo_documento` VALUES (3, 'Carné de extranjeria', '4');
INSERT INTO `tbl_tipo_documento` VALUES (4, 'Pasaporte', '5');
INSERT INTO `tbl_tipo_documento` VALUES (5, 'Carnet Identidad FF PP', '6');
INSERT INTO `tbl_tipo_documento` VALUES (6, 'Carnet Identidad FF AA', '7');
INSERT INTO `tbl_tipo_documento` VALUES (7, 'Libreta Tributaria', '8');
INSERT INTO `tbl_tipo_documento` VALUES (8, 'Documento Provisional Identidad', '9');
INSERT INTO `tbl_tipo_documento` VALUES (9, 'Ced. Diplomatica de Identidad', 'A');
INSERT INTO `tbl_tipo_documento` VALUES (10, 'Doc. Trib. No Dom. Sin Ruc', 'B');
INSERT INTO `tbl_tipo_documento` VALUES (11, 'Doc. Identidad (Extranjero)', '3');
INSERT INTO `tbl_tipo_documento` VALUES (12, 'Permiso Temporal de Permanencia', 'T');

-- ----------------------------
-- Table structure for tbl_tipo_reporte
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tipo_reporte`;
CREATE TABLE `tbl_tipo_reporte`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Valor` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tbl_tipo_reporte
-- ----------------------------
INSERT INTO `tbl_tipo_reporte` VALUES (1, 'Resumido', 'R');
INSERT INTO `tbl_tipo_reporte` VALUES (2, 'Detallado', 'D');
INSERT INTO `tbl_tipo_reporte` VALUES (3, 'Básico', 'B');

-- ----------------------------
-- Table structure for temas
-- ----------------------------
DROP TABLE IF EXISTS `temas`;
CREATE TABLE `temas`  (
  `idTema` int(11) NOT NULL AUTO_INCREMENT,
  `idDetalleCurso` int(11) NULL DEFAULT NULL,
  `Titulo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Descripcion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Video` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Duracion` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Privado` int(11) NULL DEFAULT NULL,
  `Estado` int(11) NULL DEFAULT NULL,
  `FechaRegistro` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`idTema`) USING BTREE,
  INDEX `fk_detalle_tema`(`idDetalleCurso`) USING BTREE,
  CONSTRAINT `fk_detalle_tema` FOREIGN KEY (`idDetalleCurso`) REFERENCES `detallecurso` (`idDetalleCurso`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of temas
-- ----------------------------
INSERT INTO `temas` VALUES (1, 1, 'Introduction: a UXD Parable', '<p>sddsvdsv</p>', 'q', '22:00', 2, 1, NULL);
INSERT INTO `temas` VALUES (2, 1, 'What UXD Isn\'t', '<p>dvdvsdv</p>', 'q', '04:03', 2, 1, '2020-01-30 06:53:16');
INSERT INTO `temas` VALUES (3, 1, 'Why Should We Care About UXD?', '<p>ddddd</p>', 'q', '14:57', 1, 1, '2020-01-30 06:55:17');
INSERT INTO `temas` VALUES (4, 1, 'The Elements of User Experience', '<p>dsdsdsvdvdd</p>', 'q', '23:59', 1, 1, '2020-01-30 06:55:52');
INSERT INTO `temas` VALUES (5, 3, 'Generating Effective Requirements', '<p>xsddsdsd</p>', 'q', '22:56', 1, 1, '2020-01-30 06:58:22');
INSERT INTO `temas` VALUES (6, 3, 'Exploring Information Architecture', '<p>assasasasaassasa</p>', 'q', '16:19', 1, 1, '2020-01-30 06:59:03');
INSERT INTO `temas` VALUES (7, 3, 'Convention and Metaphor', '<p>sddssds</p>', 'q', '04:11', 1, 1, '2020-01-30 06:59:49');
INSERT INTO `temas` VALUES (8, 3, 'Contrast and Uniformity', '<p>dsddds</p>', 'a', '22:57', 1, 1, '2020-01-30 07:00:25');
INSERT INTO `temas` VALUES (9, 2, 'Curso responsive', '<p>demo</p>', '#', '22:57', 1, 1, '2020-01-31 14:38:29');
INSERT INTO `temas` VALUES (10, 6, '¡Bienvenido al Curso!', '<p>dcsdsdvsdvds</p>', '#', '01:58', 1, 1, '2020-03-31 02:55:51');
INSERT INTO `temas` VALUES (11, 6, 'Advertencia de Riesgo', '<p>dcsdcsdvsdvsdv</p>', '#', '19:19', 1, 1, '2020-03-31 02:56:55');
INSERT INTO `temas` VALUES (12, 9, 'Introducción', '<p>sasddsfvvdsvdf</p>', '#', '22:56', 1, 1, '2020-03-31 03:07:09');
INSERT INTO `temas` VALUES (13, 9, 'ESTRUCTURAS', '<p>sdvdfbdfbdfdvsdvdbfnbfbsdf</p>', '#', '22:58', 1, 1, '2020-03-31 03:07:37');
INSERT INTO `temas` VALUES (14, 9, 'ESTABLECER TENDENCIA GENERAL', '<p>dvdsvdsvdfbdfbdfbdfv</p>', '#', '02:57', 1, 1, '2020-03-31 03:08:07');
INSERT INTO `temas` VALUES (15, 10, 'ROTURAS DE ESTRUCTURA', '<p>xvddfbdbsb</p>', '#', '22:58', 1, 1, '2020-03-31 03:08:45');
INSERT INTO `temas` VALUES (16, 10, 'CONTINUACIONES DE TENDENCIA', '<p>assdvdfvsdbvaevbrefb</p>', '#', '22:57', 1, 1, '2020-03-31 03:09:40');
INSERT INTO `temas` VALUES (17, 11, 'ZONAS LIMPIAS O ZONAS DIFICILES', '<p>sddsvdfbdfbdfbf</p>', '#', '23:57', 2, 1, '2020-03-31 03:10:29');
INSERT INTO `temas` VALUES (18, 11, 'DONDE COLOCAR STOP LOSS Y TAKE PROFIT', '<p>sddfsgvrdvewfvewfew|</p>', '#', '22:57', 1, 1, '2020-03-31 03:11:13');
INSERT INTO `temas` VALUES (19, 12, 'SIMULACION', '<p>dvdfbdfbdfbsdvsdvsdv</p>', '#', '22:58', 2, 1, '2020-03-31 03:12:23');
INSERT INTO `temas` VALUES (20, 13, 'Introducción', '<p>dsdfsdfsdfsdfsdfsd</p>', '#', '22:58', 2, 1, '2020-03-31 03:15:10');
INSERT INTO `temas` VALUES (21, 14, 'Price Action', '<p>sdvdsdfgsdfsdssdsdfsfdsfdsfd</p>', '#', '21:57', 2, 1, '2020-03-31 03:16:29');
INSERT INTO `temas` VALUES (22, 14, 'Patrones de Continuación y Reversión - Parte 1', '<p>assdfsdfsdsd</p>', '#', '22:57', 2, 1, '2020-03-31 03:17:05');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `idUsuario` int(11) NULL DEFAULT NULL,
  `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `estado` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `autorizacion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Perez Perez admin', 'admin@profile.com', NULL, '$2y$10$tGXNkEofrIBARimWOo/s5.UBYnyPv8n5wjlMZRdHKgLcN3jbk4k8a', NULL, '2019-07-05 01:16:00', '2021-07-14 23:55:39', 1, 'admin', '2', '1');
INSERT INTO `users` VALUES (2, 'Ulloa Bellido Jorge', 'qqq@www.wco', NULL, '$2y$10$q108HS89bDsJcErfBK7Bt.vFacflE5m23PaZMFMoC0/iXxfiW37xe', NULL, '2021-03-18 15:28:48', '2021-03-18 15:28:48', 8, '123123123', '1', '1');
INSERT INTO `users` VALUES (6, 'Martinez Cabrera carmen', 'cbabrera@sss.com', NULL, '$2y$10$gphE/E8RZjZncYBivK.AdOsSi8AhVExknWB/wCaao8FLSrsRxLraq', NULL, '2021-03-28 18:18:00', '2021-03-28 18:18:00', 12, '11122221', '1', '1');
INSERT INTO `users` VALUES (7, 'demo demo demo', 'deeeef2@ff.fff', NULL, '$2y$10$zbCY53aPVRPs.Z64Jk2VEuvR5QlQn6fwgmoawp3vCjN0BOlx37yQS', NULL, '2021-05-21 22:14:16', '2021-05-21 22:14:16', 13, '585848483', '1', '1');

-- ----------------------------
-- Table structure for usuario_cursos
-- ----------------------------
DROP TABLE IF EXISTS `usuario_cursos`;
CREATE TABLE `usuario_cursos`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` int(11) NULL DEFAULT NULL,
  `idCurso` int(11) NULL DEFAULT NULL,
  `estado` int(11) NULL DEFAULT NULL,
  `fechaInscripcion` datetime(0) NULL DEFAULT NULL,
  `favorito` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of usuario_cursos
-- ----------------------------
INSERT INTO `usuario_cursos` VALUES (1, 6, 2, 1, '2020-03-28 19:28:24', 1);
INSERT INTO `usuario_cursos` VALUES (2, 6, 1, 1, '2020-03-30 02:36:59', NULL);
INSERT INTO `usuario_cursos` VALUES (3, 18, 2, 1, '2020-03-31 08:57:55', NULL);

-- ----------------------------
-- Table structure for valoracion
-- ----------------------------
DROP TABLE IF EXISTS `valoracion`;
CREATE TABLE `valoracion`  (
  `idCalificacion` int(11) NOT NULL AUTO_INCREMENT,
  `idCurso` int(11) NULL DEFAULT NULL,
  `valoracion` int(11) NULL DEFAULT NULL,
  `estado` int(11) NULL DEFAULT NULL,
  `fecha_registro` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`idCalificacion`) USING BTREE,
  INDEX `fk_valoracion`(`idCurso`) USING BTREE,
  CONSTRAINT `fk_valoracion` FOREIGN KEY (`idCurso`) REFERENCES `curso` (`idCurso`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of valoracion
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
