/*
Navicat MySQL Data Transfer

Source Server         : Mysql
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : asociacion

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-01-20 19:37:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `modulo`
-- ----------------------------
DROP TABLE IF EXISTS `modulo`;
CREATE TABLE `modulo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(200) DEFAULT NULL,
  `Descripcion` varchar(500) DEFAULT NULL,
  `Estado` int(11) DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `Icono` varchar(200) DEFAULT NULL,
  `Link` varchar(100) DEFAULT NULL,
  `LinkExterno` int(11) DEFAULT NULL,
  `Route` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of modulo
-- ----------------------------
INSERT INTO `modulo` VALUES ('1', 'Dasboard', 'Dasboard', '1', '1', '<i class=\"feather icon-home\"></i>', null, '0', 'admin/dashboard', null, null);
INSERT INTO `modulo` VALUES ('2', 'Titular', 'Titular', '1', '5', '<i class=\"feather icon-box\"></i>', null, '0', 'admin/titular', '2019-07-16 23:41:37', null);
INSERT INTO `modulo` VALUES ('3', 'Usuario', 'Usuario', '1', '3', '<i class=\"feather icon-settings\"></i>', 'http://google.com', '0', 'admin/usuario', '2019-07-16 23:41:50', null);
INSERT INTO `modulo` VALUES ('4', 'Configuraciòn', 'Modulo de configuración', '0', '2', '<i class=\"feather icon-settings\"></i>', null, '0', 'admin/configuracion', null, null);
INSERT INTO `modulo` VALUES ('7', 'Cuotas', 'Cuotas', '1', '7', '<i class=\"feather icon-box\"></i>', null, '0', 'admin/cuota', '2019-06-27 17:02:24', '2019-06-27 16:45:47');
INSERT INTO `modulo` VALUES ('8', 'Expediente', 'Expediente', '1', '4', '<i class=\"feather icon-sidebar\"></i>', '#', '0', 'admin/expediente', '2019-07-16 23:58:44', '2019-07-03 23:25:48');
INSERT INTO `modulo` VALUES ('9', 'Pagos', 'Pagos', '1', '8', '<i class=\"feather icon-sidebar\"></i>', null, '0', 'admin/pagos', '2019-07-03 23:25:48', '2019-07-03 23:25:48');
INSERT INTO `modulo` VALUES ('10', 'Reporte', 'Reporte', '1', '9', '<i class=\"feather icon-sidebar\"></i>', null, '0', 'admin/reporte', '2019-07-03 23:25:48', '2019-07-03 23:25:48');
INSERT INTO `modulo` VALUES ('11', 'Reuniones', 'Reuniones', '1', '6', '<i class=\"feather icon-home\"></i>', null, '0', 'admin/reuniones', '2019-08-08 18:31:31', '2019-08-08 18:31:31');
INSERT INTO `modulo` VALUES ('12', 'Backup', 'Copia de seguridad', '1', '10', '<i class=\"feather icon-settings\"></i>', null, '0', 'admin/backup', null, null);

-- ----------------------------
-- Table structure for `pagina`
-- ----------------------------
DROP TABLE IF EXISTS `pagina`;
CREATE TABLE `pagina` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Padre` char(5) DEFAULT NULL,
  `ModuloID` int(11) DEFAULT NULL,
  `Descripcion` varchar(200) DEFAULT NULL,
  `Ruta` varchar(200) DEFAULT NULL,
  `Slug` varchar(200) DEFAULT NULL,
  `Estado` int(11) DEFAULT NULL,
  `Visible` int(11) DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  `FechaIngreso` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Modulo` (`ModuloID`),
  CONSTRAINT `FK_Modulo` FOREIGN KEY (`ModuloID`) REFERENCES `modulo` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pagina
-- ----------------------------
INSERT INTO `pagina` VALUES ('1', '1', '1', 'Principal', 'dashboard', null, '1', '1', '1', '2019-06-24 23:05:15', null, null);
INSERT INTO `pagina` VALUES ('2', '1', '2', 'Listado', '-', '-', '0', '1', '2', '2019-06-24 23:05:12', null, null);
INSERT INTO `pagina` VALUES ('3', '1', '2', 'listado', 'titular/lista', '-', '1', '1', '3', null, '2019-07-17 01:09:11', null);
INSERT INTO `pagina` VALUES ('4', '0', '4', 'Menú', 'configuracion/menu', '-', '1', '1', '1', null, null, null);
INSERT INTO `pagina` VALUES ('6', null, '3', 'listado', 'usuario/lista', null, '1', null, '5', null, '2019-07-17 01:09:02', '2019-07-01 19:05:30');
INSERT INTO `pagina` VALUES ('7', null, '8', 'Listado', 'expediente/lista', null, '1', null, '6', null, '2019-07-17 01:09:26', '2019-07-03 23:26:17');
INSERT INTO `pagina` VALUES ('8', null, '7', 'Listado', 'cuota/lista', null, '1', null, '7', null, '2019-07-17 01:09:19', '2019-07-17 00:59:03');
INSERT INTO `pagina` VALUES ('9', null, '9', 'Buscar', 'pagos/pago/buscar', null, '1', null, '8', null, '2019-08-08 23:39:31', '2019-07-17 01:05:11');
INSERT INTO `pagina` VALUES ('10', null, '10', 'Lista de socios', 'reporte/lista', null, '1', null, '9', null, '2019-08-10 00:49:36', '2019-07-17 01:09:52');
INSERT INTO `pagina` VALUES ('11', null, '11', 'Listado', 'reuniones/lista', null, '1', null, '10', null, '2019-08-08 18:32:35', '2019-08-08 18:32:35');
INSERT INTO `pagina` VALUES ('12', null, '10', 'Historial de expediente', 'reporte/expediente', null, '1', null, '11', null, '2019-08-10 02:42:14', '2019-08-10 00:51:48');
INSERT INTO `pagina` VALUES ('13', null, '12', 'Backup', 'backup/lista', null, '1', null, '12', null, null, null);

-- ----------------------------
-- Table structure for `role_user`
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(10) unsigned NOT NULL,
  `user_id` bigint(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fkuser` (`user_id`) USING BTREE,
  KEY `fkroles` (`role_id`) USING BTREE,
  CONSTRAINT `fkroles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `fkuser` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of role_user
-- ----------------------------
INSERT INTO `role_user` VALUES ('1', '1', '1', '2019-07-05 01:16:00', '2019-07-05 01:16:00');

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'admin', 'PRESIDENTE', '2019-07-05 01:14:11', '2019-07-05 01:14:11');
INSERT INTO `roles` VALUES ('2', 'user', 'OTROS', '2019-07-05 01:14:11', '2019-07-05 01:14:11');
INSERT INTO `roles` VALUES ('3', 'editor', 'Editor', '2019-07-05 01:14:11', '2019-07-05 01:14:11');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Perez Perez admin', 'admin@profile.com', null, '$2y$10$tGXNkEofrIBARimWOo/s5.UBYnyPv8n5wjlMZRdHKgLcN3jbk4k8a', null, '2019-07-05 01:16:00', '2019-11-29 23:16:02', '1', 'admin');
