$(document).on('click','.alert-delete-activid',function(e){
    e.preventDefault();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var id = $(this).attr('data-id');
    swal({
        title: "Eliminar?",
        text: "Esta seguro que desea eliminar el registro?",
        type: "error",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Eliminar",
        cancelButtonText: "Cancelar",
    }, 
    function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                url: local.base+'/admin/actividades/delete/'+id,
                type: 'post',   
                dataType: 'json',
                data: {_token: CSRF_TOKEN},
                success: function(data){
                    if(data.status == true){
                        swal("Eliminado", "Se ha eliminado correctamente.", "success");
                        $('.selector-'+id).remove();
                    }else{
                        // swal('Ocurrio un error vuelva a intentarlo');
                        swal("Eliminado", "Ocurrio un error vuelva a intentarlo", "error");
                    }
                }
            });
        }
    });
});

$(document).on('click','.select-page',function(e){
    e.preventDefault();
    $('.table-verify').removeClass('active');
    let id =  $(this).data('sel');
    $('.selector-'+id).addClass('active');
    $('.panel-modulo').addClass('col-6');
    $('.panel-modulo').removeClass('col');
    $('.panel-pagina').show();
    $('.column_description').css('display','none');

    e.preventDefault();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    // var id = $(this).attr('data-id');
    $('#id_module').val(id);
    $.ajax({
        url: local.base+'/admin/actividades/get-actividades/'+id,
        type: 'GET',
        dataType: 'json',
        data: {},
        beforeSend: function(){
            var $this = $(this);
            $('.loader-cards').parents('.card').addClass("card-load");
            $('.loader-cards').parents('.card').append('<div class="card-loader"><i class="feather icon-radio rotate-refresh"></div>');
        },
        success: function(data){
            if(data.status == true){
                $('.code_page').text(data.person);
                $('.json').val(JSON.stringify(data.jsonActivity))
                $('.id_personal').val(id)
                $('#page-body-table').html("");
                let vhtml = "";
                // console.log(data.activity.length);
                if(data.activity.length != 0){
                    $.each(data.activity,function(k,v){
                        vhtml += '<tr>';
                        vhtml += '<td>'+(k+1)+'</td>';
                        vhtml += '<td>'+v.descripcion+'</td>';
                        if( v.concluido == 1 )
                        {
                            vhtml += '<td><label class="label label-success">Terminado</label></td>';
                            vhtml += '<td></td>';
                        }else{
                            vhtml += '<td><label class="label label-danger">Pendiente</label></td>';
                            vhtml += `<td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Acciones
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item alert-delete-page" href="#" data-id="`+v.ID+`" >
                                                    <i class="fa fa-trash-alt"></i>
                                                    Quitar
                                                </a>
                                            </div>
                                        </div>
                                    </td>`;
                        }
                        vhtml += '</tr>';
                    });

                }else{
                    vhtml = `
                    <tr>
                        <td colspan="4" class="not-row"><strong> No existen actividades asignadas</strong></td>
                    </tr>
                    `
                }
                $('#page-body-table').html(vhtml);

            }else{
                swal('Ocurrio un error vuelva a intentarlo');
            }
        },
        complete: function(){
            $('.loader-cards').parents('.card').children(".card-loader").remove();
            $('.loader-cards').parents('.card').removeClass("card-load");
        }
    });
});