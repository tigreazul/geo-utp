$(document).on('click','.curso_detalle',function(e){
    e.preventDefault();
    let url = local.base + '/inscripcion-curso';
    let data = $(this).data('slug');
    let i = 0;
    console.log(url);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: url,
        data: 'c='+data,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            $('.contenido_curso').hide();
            $('.loader').show();
            i++;
        },
        success: function(respuesta) {
            console.log(respuesta);
            if(respuesta.status == true ){
                $('.title-panel').text(respuesta.cursos.titulo);
                $('.contenido-panel').html(respuesta.cursos.descripcion);
                $('.slug-panel').text(respuesta.cursos.slug);
            }
        },
        error: function() {
            console.log("No se ha podido obtener la información");
        },
        complete: function() {
            i--;
            if (i <= 0) {
                // setTimeout(function(){ 
                    $('.loader').hide();
                    $('.contenido_curso').show();
                // }, 3000);
            }
        }
    });
});

$(document).on('click','#inscribir_curso',function(e){
    e.preventDefault();
    let url = local.base + '/registro-curso';
    let data = $('.slug-panel').text();
    let i = 0;
    // console.log(data);
    // return false;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: url,
        data: 'c='+data,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            $('.contenido_curso').hide();
            $('.loader').show();
            i++;
        },
        success: function(respuesta) {
            console.log(respuesta);
            if(respuesta.status == true ){
                location.reload();
                $('.title-panel').text(respuesta.cursos.titulo);
                $('.contenido-panel').html(respuesta.cursos.descripcion);
                $('.slug-panel').text(respuesta.cursos.descripcion);
            }
        },
        error: function() {
            console.log("No se ha podido obtener la información");
        },
        complete: function() {
            i--;
            if (i <= 0) {
                // setTimeout(function(){ 
                    $('.loader').hide();
                    $('.contenido_curso').show();
                // }, 3000);
            }
        }
    });
});


// $(document).on('click','.curso_detalle',function(e){
//     e.preventDefault();
//     let url = local.base + '/inscripcion-curso';
//     let data = $(this).data('slug');
//     let i = 0;
//     console.log(url);
//     $.ajaxSetup({
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         }
//     });
//     $.ajax({
//         url: url,
//         data: 'c='+data,
//         type: 'POST',
//         dataType: 'json',
//         beforeSend: function() {
//             $('.contenido_curso').hide();
//             $('.loader').show();
//             i++;
//         },
//         success: function(respuesta) {
//             console.log(respuesta);
//             if(respuesta.status == true ){
//                 $('.title-panel').text(respuesta.cursos.titulo);
//                 $('.contenido-panel').html(respuesta.cursos.descripcion);
//                 $('.slug-panel').text(respuesta.cursos.slug);
//             }
//         },
//         error: function() {
//             console.log("No se ha podido obtener la información");
//         },
//         complete: function() {
//             i--;
//             if (i <= 0) {
//                 // setTimeout(function(){ 
//                     $('.loader').hide();
//                     $('.contenido_curso').show();
//                 // }, 3000);
//             }
//         }
//     });
// });

$(document).on('click','.modal-course',function(e){
    e.preventDefault();
    let url = local.base + '/curso-id';
    let code = $(this).data('code');
    let data = '';
    let i = 0;
    // console.log(data);
    // return false;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: url,
        data: 'c='+code,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            $('.contenido_curso').hide();
            $('.loader').show();
            // $('.uk-modal-title').text('');
            // $('.uk-article').text('');
            i++;
        },
        success: function(respuesta) {
            // console.log(respuesta);
            if(respuesta.status == true ){
                $('#player').remove();
                $('#player-conteiner').html('<video id="player" controls></video>');
                var infoModal = jQuery('#lesson');
                infoModal.find('.uk-modal-title').html(respuesta.temas.titulo);
                // infoModal.find('.uk-modal-title').html(data[0].brand + ' ' + data[0].model);
                // htmlData = '<p>' + data[0].text + '</p>';
                infoModal.find('.uk-article').html(respuesta.temas.descripcion);
                const player = new Plyr('#player');
                player.source = {
                    type: 'video',
                    sources: [{
                    src: respuesta.video.codigo, // From the Vimeo video link
                    provider: respuesta.video.tipo,
                    }, ],
                };

                // $('#player-conteiner').html('<div id="player" data-plyr-provider="'+respuesta.video.tipo+'" data-plyr-embed-id="'+respuesta.video.codigo+'"></div>');
                // const player = new Plyr('#player', {});
                // Expose player so it can be used from the console
                // window.player = player;


                // var data = data.T.ads;
    
                UIkit.modal(infoModal).show();
                
            }
        },
        error: function() {
            console.log("No se ha podido obtener la información");
        },
        complete: function() {
            i--;
            if (i <= 0) {
                // setTimeout(function(){ 
                    $('.loader').hide();
                    $('.contenido_curso').show();
                // }, 3000);
            }
        }
    });
});



