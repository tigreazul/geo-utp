function mostrar(){
    var file = document.getElementById("aaa").files[0];
    console.log(file);
    var imagefile = file.type;
    var match= ["image/jpeg","image/png","image/jpg"];
    if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))){
        alert('Please select a valid image file (JPEG/JPG/PNG).');
        $("#imagen_preview").val('');
        return false;
    }else{
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        var formData = new FormData();
        // formData.append('section', 'general');
        formData.append('foto', $('#aaa')[0].files[0]);

        $.ajax({
            url        : app.base+'/image',
            type       : 'POST',
            dataType   : 'JSON', 
            contentType: false,
            processData: false,
            data       : formData,
            success    :function(data)
            {
                if(data.status== true){
                    $('#sin_imagen').hide();
                    $("#imagen_preview").html('');
                    $("#imagen_preview").attr('src',data.path);
                    $("#img_curso").val(data.name);
                }else{
                    alert("NO TIENE EL TAMAÑO VALIDO");
                }
            },
            error       :function(data)
            {
                console.log(data);
            }
        })

        // var reader = new FileReader();
        // if (file) {
        //     reader.readAsDataURL(file);
        //     reader.onloadend = function () {
        //         console.log(reader.result);
        //         $("#imagen_preview").html('');
        //         $("#imagen_preview").css('background',reader.result);
        //         // document.getElementById("imagen_preview").src = reader.result;
        //     }
        // }
    }
    // $("#imagen_preview").val('');

    // return false;
    


}