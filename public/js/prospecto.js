$(document).on('click','.solicitudes_estado',function(e){
    e.preventDefault();
    // let code = $(this).find(':selected').data('id');
    let estado = $(this).data('id');
    let id = $(this).data('code');
    let tipo = $(this).data('estado');
    let url = $('#url_proceso').val()
    // console.log(code); 
    // return false;

    swal({
        title: "¿APROBAR?",
        text: "Esta seguro que desea aprobar el prospecto?",
        type: "info",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Confirmar",
        cancelButtonText: "Cancelar",
    }, 
    function(isConfirm) {
        if (isConfirm) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url     : url,
                type    : 'POST',
                dataType: 'JSON',
                data    : {estado:estado,tipo:tipo,id:id},
                success : function(data){
                    swal("APROBADO", "Se ha aprobo el prospecto correctamente.", "success");
                    console.log(data);
                    if(data.status == true){
                        location.href = data.url;
                    }
                },
                error   : function(jqxhr, textStatus, error){
                    console.log(jqxhr.responseText);
                }
            });
        }
    });

    
});