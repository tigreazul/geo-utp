$(document).on('change','#grupo',function(e){
    e.preventDefault();
    // let code = $(this).find(':selected').data('id');
    let code = $(this).val();
    // console.log(code); 
    // return false;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url     : app.base+'/manzanas/'+code,
        type    : 'GET',
        dataType: 'JSON',
        data    : {},
        success : function(data){
            // $("#provincias").removeAttr('disabled');
            $('#mz').html("");
            let vhtml = "";
            // <option value="">[SELECCIONE]</option>
            vhtml += '<option value="">[SELECCIONE]</option>'
            $( data.data ).each(function( index, element ){
                vhtml += '<option value="'+element.idManzana+'" data-id="'+element.nomManzana+'">'+element.nomManzana+'</option>'
            });
            $('#mz').html(vhtml);
            // $('#distritos').html('<option value="">[SELECCIONE]</option>');
        },
        error   : function(jqxhr, textStatus, error){
            console.log(jqxhr.responseText);
        }
    });
});