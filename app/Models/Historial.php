<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Historial extends Model
{
    protected $table = 'tbl_historial_cliente';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
