<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Detallecurso extends Model
{
    protected $table = 'detallecurso';
    protected $primaryKey = 'idDetalleCurso';
    public $timestamps = false;
}
