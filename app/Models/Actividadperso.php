<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Actividadperso extends Model
{
	protected $table = 'actividadPersona';
	protected $primaryKey = 'id';
	public $timestamps = false;
}
