<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuariocurso extends Model
{
    protected $table = 'usuario_cursos';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
