<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comprobante extends Model
{
    protected $table = 'tbl_comprobante_facturacion';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
