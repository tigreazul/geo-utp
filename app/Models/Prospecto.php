<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prospecto extends Model
{
    protected $table = 'prospectos';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
