<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roladmin extends Model
{
    protected $table = 'admin_role';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
