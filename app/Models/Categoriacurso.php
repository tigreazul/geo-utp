<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categoriacurso extends Model
{
    protected $table = 'Categoriacurso';
    protected $primaryKey = 'idCategoria';
    // protected $primaryKey = 'idCurso';
    public $timestamps = false;
}
