<?php

namespace App\Providers;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // https://www2.sentinelperu.com/wsrest/rest/rws_senenc
        // https://jsonplaceholder.typicode.com
        $this->app->singleton('GuzzleHttp\Client',function(){
            return new Client([
                'base_uri' => 'https://www2.sentinelperu.com',
            ]);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
