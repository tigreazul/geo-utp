<?php

namespace App\Http\Controllers\Admin;

use App\Models\{
    Modulo, Pagina, Persona, Usuario, Actividades, Actividadperso
};
use App\Models\Role;
use App\User;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;

class ActividadesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);
        $actividades = DB::table('actividades')
        ->where(['actividades.estado'=>1])
        ->get();

        $a_data_page = array(
            'title' => 'Lista de Actividades',
            'actividades'=> $actividades
        );
        return \Views::admin('actividades.index',$a_data_page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $a_data_page = array(
            'title' => 'Registro de actividades'
        );
        return \Views::admin('actividades.create',$a_data_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'descripcion'   => "required",
            "estado"        => "required"
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }
        
        DB::beginTransaction();
        try{
            $actividad = new Actividades;
            $actividad->descripcion  = $request->input('descripcion');
            $actividad->estado       = 1;
            $actividad->save();
            DB::commit();
            \Session::flash('message', 'Registrado! Se registraron los datos correctamente');
            return redirect()->route('admin.actividad_list');

        } catch (\Exception $e) {
            DB::rollback();
            dd($e); die();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $actividades = DB::table('actividades')
        ->where(['actividades.estado'=>1])
        ->first();
        
        $a_data_page = array(
            'title' => 'Lista de Actividades',
            'actividades'=> $actividades
        );

        return \Views::admin('actividades.editar',$a_data_page);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'descripcion'  => "nullable",
            "estado"       => "nullable"
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }

        $usuario = Actividades::find($id);
        $usuario->descripcion   = $request->input('descripcion');
        $usuario->estado        = $request->input('estado');
        $usuario->save();
        \Session::flash('message', 'Actualizado! Se actualizaron los datos');

        return redirect()->route('admin.actividad_list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // dd($id);
        $usuario = Actividades::find($id);
        $usuario->estado = 2;
        $usuario->save();
        $data = array(
            'status' => true
        );

        echo json_encode($data);
    }

    public function show_activity_person(Request $request,$id)
    {
        $dPerson = '';
        $dActivity = array();

        $person = DB::table('users')
        ->select('persona.apellidoPaterno','persona.apellidoMaterno','persona.nombre','users.id')
        ->join('persona', 'persona.idPersona', '=', 'users.idUsuario') 
        ->where([
                'persona.estado' => 1,
                'persona.idPersona' => $id
            ])
        ->first();
        if($person){
            $dPerson = $person->apellidoPaterno.' '.$person->apellidoMaterno.', '.$person->nombre;
        }

        $activity = DB::table('actividadPersona')
        ->select('actividades.descripcion','actividadPersona.id','actividadPersona.estado','actividadPersona.concluido')
        ->join('users', 'users.id', '=', 'actividadPersona.idPersona')
        ->join('persona', 'persona.idPersona', '=', 'users.idUsuario') 
        ->join('actividades', 'actividades.id', '=', 'actividadPersona.idActividad')
        ->where([
                'actividadPersona.estado' => 1,
                'persona.idPersona' => $id
            ])
        ->orderBy('actividadPersona.estado','desc')
        ->get();
        if( count($activity) ){
            $dActivity = $activity;
        }


        
        // $retornoJson = $this->show_activity_person_actived($id);
        $dataJson = DB::select("SELECT * from actividades where id not in (select idActividad from actividadPersona WHERE estado = 1)");
        $jsonActivity = array();
        foreach ($dataJson as $value) {
            $jsonActivity[] = array(
                "city"  => $value->descripcion,
                "value" => $value->id,
            );
        }

        $arrResult = array(
            'status'    => true,
            'person'    => $dPerson,
            'codperso'  => $person->id,
            'activity'  => $dActivity,
            'jsonActivity' => $jsonActivity
        );

        return response()->json($arrResult);

    }


    public function show_activity_person_actived($id)
    {
        $dActivity = array();

        $activity = DB::table('actividades')
        ->where(['actividades.estado'=>1])
        ->get();

        

        $activityPerson = DB::table('actividadPersona')
        ->where([
                'actividadPersona.estado' => 1,
                'actividadPersona.idPersona' => $id
            ])
        ->get();

        // dd($activityPerson);
        foreach ($activity as $actived) {
            $activo = 0;
            foreach ($activityPerson as $activedPerson) {
                if( $actived->id == $activedPerson->idActividad )
                {
                    $activo = 1;
                }
            }
            if( $activo == 1 ){
                $dActivity[] = array(
                    "city"  => $actived->descripcion,
                    "value" => $actived->id,
                    "selected" => true
                );
            }else{
                $dActivity[] = array(
                    "city"  => $actived->descripcion,
                    "value" => $actived->id,
                );
            }
        }
        return $dActivity;
        // return response()->json($dActivity);
    }

    public function indexAsignacion(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);
        $actividades = DB::table('users')
        // ->distinct()
        ->select('users.*','persona.*')
        // ->join('users', 'users.id', '=', 'actividadPersona.idPersona')
        ->join('persona', 'persona.idPersona', '=', 'users.idUsuario') 
        ->where(['users.estado'=>1])
        ->get();

        $a_data_page = array(
            'title' => 'Lista de Actividades',
            'actividades'=> $actividades
        );
        return \Views::admin('asignacion.index',$a_data_page);
    }

    public function createAsignacion()
    {
        $a_data_page = array(
            'title' => 'Registro de Asignacion'
        );
        return \Views::admin('asignacion.create',$a_data_page);
    }
    
    public function activy_person_add(Request $request)
    {
        $data = $request->input('data');
        $perso = $request->input('id');

        // dd($request->input('data'));
        $arrDatos = array();

        foreach ($data as $value) {
            $arrDatos[] = array(
                'id' => (int)$value
            );
        }
        $usuario = DB::table('users')
        ->select('users.*')
        ->join('persona', 'persona.idPersona', '=', 'users.idUsuario') 
        ->where(['users.estado'=>1,'persona.idPersona'=>$perso])
        ->first();
        // dd($usuario);
        $activityPerson = DB::table('actividadPersona')
        ->where([
                'actividadPersona.estado' => 1,
                'actividadPersona.idPersona' => $usuario->id
            ])
        ->get();

        foreach ($activityPerson as $valPerson) {
            $arrDatos[] = array(
                'id' => $valPerson->idActividad
            );
        }

        // dd( $arrDatos );

        Actividadperso::where([ 'idPersona'=>$usuario->id, 'concluido' => 0 ])->update([
            'estado'   => 0
        ]);

        foreach ($data as $value) {
            $activity = new Actividadperso;
            $activity->idPersona = $usuario->id;
            $activity->idActividad = $value;
            $activity->estado = 1;
            $activity->fechaRegistro = date('Y-m-d H:i:s');
            $activity->concluido = 0;
            $activity->save();
        }

        $activityPersonList = DB::table('actividadPersona')
        ->select('actividades.descripcion','actividadPersona.id','actividadPersona.estado','actividadPersona.concluido')
        ->join('users', 'users.id', '=', 'actividadPersona.idPersona')
        ->join('persona', 'persona.idPersona', '=', 'users.idUsuario') 
        ->join('actividades', 'actividades.id', '=', 'actividadPersona.idActividad')
        ->where([
                'actividadPersona.estado' => 1,
                'persona.idPersona' => $perso
            ])
        ->orderBy('actividadPersona.estado','desc')
        ->get();

        $dataJson = DB::select("SELECT * from actividades where id not in (select idActividad from actividadPersona WHERE estado = 1)");
        $jsonActivity = array();
        foreach ($dataJson as $value) {
            $jsonActivity[] = array(
                "city"  => $value->descripcion,
                "value" => $value->id,
            );
        }

        $arr = array(
            'status'    => true,
            'activity'  => $activityPersonList,
            'dataJson'  => $jsonActivity
        );

        return response()->json($arr);

    }


}
