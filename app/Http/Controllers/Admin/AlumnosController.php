<?php

namespace App\Http\Controllers\Admin;

use App\Models\{
    Modulo, Pagina, Persona, Usuario, Roluser
};
use App\Models\Role;
use App\User;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;

class AlumnosController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);

        $usuarios = DB::table('role_user')->select('role_user.role_id AS rol','users.*','persona.*','roles.description AS descripcion_rol')
        ->join('users', 'users.id', '=', 'role_user.user_id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->join('persona', 'persona.idPersona', '=', 'users.idUsuario') 
        ->where(['users.estado'=>1,'roles.id'=>2,'persona.estado'=>1])
        ->get();

        $a_data_page = array(
            'title' => 'Lista de Alumnos',
            'usuarios'=> $usuarios
        );
        return \Views::admin('alumno.index',$a_data_page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rol = DB::table('roles')->get();
        $pais = DB::table('pais')->get();
        $a_data_page = array(
            'title' => 'Registro de paginas',
            'roles' => $rol,
            'pais'  => $pais
        );
        return \Views::admin('alumno.create',$a_data_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
            'username'      => "required",
            'nombre'        => "required",
            'apaterno'      => "required",
            'amaterno'      => "required",
            'email'         => "required|unique:users",
            "fnacimiento"   => "required",
            "username"      => "required|unique:users",
            "password"      => "required",
            "pais"          => "required",
            "estado"        => "required"
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }
        
        DB::beginTransaction();
        try{
            $perso = new Persona;
            $perso->nombre            = $request->input('nombre');
            $perso->apellidoPaterno   = $request->input('apaterno');
            $perso->apellidoMaterno   = $request->input('amaterno');
            $perso->fechaNacimiento   = $request->input('fnacimiento');
            $perso->idPais            = $request->input('pais');
            $perso->estado            = 1;
            $perso->save();
            $persoId = $perso->idPersona;

            $user = new User;
            $user->name       = $request->input('apaterno').' '.$request->input('amaterno').' '.$request->input('nombre');
            $user->email      = $request->input('email');
            $user->password   = Hash::make($request->input('password'));
            $user->idPersona  = $persoId;
            $user->username   = $request->input('username');
            $user->estado     = 1;
            $user->autorizacion= 1;
            $user->save();
            $usuar = $user->id;
            
            $roluser = new Roluser;
            $roluser->role_id  = 2;
            $roluser->user_id = $usuar;
            $roluser->save();
            DB::commit();
            
            \Session::flash('message', 'Registrado! Se registraron los datos correctamente');
            return redirect()->route('admin.alumno_list');

        } catch (\Exception $e) {
            DB::rollback();
            dd($e); die();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuarios = DB::table('users')
        ->join('persona', 'users.idUsuario', '=', 'persona.idPersona')
        ->where(['users.id'=> $id])
        ->first();
        // $cargo = \Views::diccionario('idCargo');
        $pais = DB::table('pais')->get();
        $a_data_page = array(
            'title' => 'Editar de usuario',
            'usuarios' => $usuarios,
            'pais' => $pais
        );

        return \Views::admin('alumno.editar',$a_data_page);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'username'      => "nullable",
            'nombre'        => "nullable",
            'apaterno'      => "nullable",
            'amaterno'      => "nullable",
            "fnacimiento"   => "nullable",
            "enlace"        => "nullable",
            "pais"          => "nullable",
            "sobremi"       => "nullable"
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }

        $usuario = User::find($id);
        $usuario->username     = $request->input('username');
        // $usuario->idCargo           = $request->input('cargo');
        // $usuario->anioGestion       = $request->input('agestion');
        $usuario->save();

        // User::where(['idUsuario'=>$usuario->idUsuario ])->update([
        //     'name'     => $request->input('apaterno').' '.$request->input('amaterno').' '.$request->input('nombre'),
        //     'email'    => $request->input('usuario').'@profile.com'
        // ]);
        // dd($usuario->idUsuario);

        $perso = Persona::find($usuario->idUsuario);
        $perso->nombre            = $request->input('nombre');
        $perso->apellidoPaterno   = $request->input('apaterno');
        $perso->apellidoMaterno   = $request->input('amaterno');
        $perso->fechaNacimiento   = $request->input('fnacimiento');
        $perso->enlace            = $request->input('enlace');
        $perso->idpais            = $request->input('pais');
        $perso->sobremi           = $request->input('sobremi');
        $perso->save();
        
        // $persoId = $perso->idPersona;
        // if($request->input('cargo') == 1){
        //     $role_id   = 1;
        // }else{
        //     $role_id   = 2;
        // }
        
        // $users = DB::select('SELECT id FROM users WHERE idUsuario = ?',[$usuario->idUsuario]);
        // dd($users[0]->id);

        // Roluser::where(['user_id'=>$users[0]->id ])->update([
        //     'role_id'     => $role_id,
        // ]);

        
        \Session::flash('message', 'Actualizado! Se actualizaron los datos');

        return redirect()->route('admin.alumno_list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // dd($id);
        $usuario = User::find($id);
        $usuario->estado = 2;
        $usuario->save();

        $perso = Persona::find($usuario->idPersona);
        $perso->estado = 3;
        $perso->save();
        $data = array(
            'status' => true
        );

        echo json_encode($data);
    }
}
