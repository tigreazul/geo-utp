<?php

namespace App\Http\Controllers\Admin;

use App\Models\{
    Persona, Curso, Categoria, Categoriacurso, Detallecurso, Tema
};
use App\Models\Role;
use App\User;
use App\Admin;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;

use Illuminate\Support\Arr;

class CursoController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('admin');
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);

        $curso = DB::table('curso')->select('curso.*','categoria.nombreCat AS Categoria','categoriacurso.estado')
        ->join('categoriacurso', 'categoriacurso.idCurso', '=', 'curso.idCurso')
        ->join('categoria', 'categoria.idCat', '=', 'categoriacurso.idCategoria')
        ->whereIn('categoriacurso.estado',[1,2])
        ->whereIn('curso.estado',[1,2])
        ->get();
        $a_data_page = array(
            'title' => 'Lista de Curso',
            'curso'=> $curso
        );
        return \Views::admin('curso.index',$a_data_page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profesor = DB::table('profesor')->select('persona.*','profesor.descripcion AS DescripcionProfesor','profesor.idProfesor')
        ->join('persona', 'profesor.idPersona', '=', 'persona.idPersona')
        ->whereIn('profesor.estado',[1,2])
        ->get();

        $categoria = DB::table('categoria')
        ->whereIn('categoria.estadoCat',[1,2])
        ->get();

        $a_data_page = array(
            'title'     => 'Registro de Curso',
            'profesor'  => $profesor,
            'categoria' => $categoria
        );
        return \Views::admin('curso.create',$a_data_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
            "titulo"            => "required",
            "code_curso"        => "required",
            "precio"            => "required",
            "precioPromocio"    => "required",
            "docente"           => "required",
            "descripcion"       => "required",
            "adicional"         => "required",
            "requerimiento"     => "required",
            "categoria"         => "required",
            "estado"            => "required",
            "img_curso"         => "required"
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }
        
        DB::beginTransaction();
        try{

            // if ($request->hasFile('imagen')) {
            //     $imagen = $request->file('imagen');
            //     $extension = $imagen->getClientOriginalExtension();
            //     $fileName = uniqid().md5(date('Y-m-d H:i:s')) . '.' . $extension;
            //     Storage::disk('imagen')->put($fileName,  \File::get($imagen));
            // }else{
            //     $fileName = '';
            // }

            $curso = new Curso;
            $curso->titulo          = $request->input('titulo');
            $curso->slug            = \Str::slug($request->input('titulo'));
            $curso->codigoCurso     = $request->input('code_curso');
            $curso->precioPrincipal = $request->input('precio');
            $curso->precioPromocion = $request->input('precioPromocio');
            $curso->ImagenCurso     = $request->input('img_curso');
            $curso->idProfesor      = $request->input('docente');
            // $curso->ImagenCurso     = $fileName;
            $curso->descripcion     = $request->input('descripcion');
            $curso->adicional       = $request->input('adicional');
            $curso->requerimiento   = $request->input('requerimiento');
            $curso->fecha_registro  = date('Y-m-d H:i:s');
            $curso->estado          = $request->input('estado');
            $curso->save();
            $cursoId = $curso->idCurso;
            
            $catCurso = new Categoriacurso;
            $catCurso->idCategoria  = $request->input('categoria');
            $catCurso->idCurso      = $cursoId;
            $catCurso->estado       = $request->input('estado');
            $catCurso->fechaRegistro= date('Y-m-d H:i:s');
            $catCurso->save();
            
            DB::commit();
            
            \Session::flash('message', 'Registrado! Se registraron los datos correctamente');
            return redirect()->route('admin.curso_list');

        } catch (\Exception $e) {
            DB::rollback();
            dd($e); die();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profesor = DB::table('profesor')->select('persona.*','profesor.descripcion AS DescripcionProfesor','profesor.idProfesor')
        ->join('persona', 'profesor.idPersona', '=', 'persona.idPersona')
        ->whereIn('profesor.estado',[1,2])
        ->get();
        
        $categoria = DB::table('categoria')
        ->whereIn('categoria.estadoCat',[1,2])
        ->get();
        
        $curso = DB::table('curso')->select('curso.*','categoria.nombreCat AS Categoria','categoriacurso.estado AS estadoCat','categoriacurso.idCategoria')
        ->join('categoriacurso', 'categoriacurso.idCurso', '=', 'curso.idCurso')
        ->join('categoria', 'categoria.idCat', '=', 'categoriacurso.idCategoria')
        ->where('curso.idCurso',[$id])
        ->first();
        
        $a_data_page = array(
            'title'     => 'Editar de Curso',
            'curso'     => $curso,
            'profesor'  => $profesor,
            'categoria' => $categoria
        );
        // dd($a_data_page);
        return \Views::admin('curso.editar',$a_data_page);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            "titulo"            => "required",
            "code_curso"        => "required",
            "precio"            => "required",
            "precioPromocio"    => "required",
            "docente"           => "required",
            "descripcion"       => "required",
            "adicional"         => "required",
            "requerimiento"     => "required",
            "categoria"         => "required",
            "estado"            => "required",
            "img_curso"         => "required"
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }

        DB::beginTransaction();
        try{
            $curso = Curso::find($id);
            $curso->titulo          = $request->input('titulo');
            $curso->codigoCurso     = $request->input('code_curso');
            $curso->slug            = \Str::slug($request->input('titulo'));
            $curso->precioPrincipal = $request->input('precio');
            $curso->precioPromocion = $request->input('precioPromocio');
            $curso->ImagenCurso     = $request->input('img_curso');
            $curso->idProfesor      = $request->input('docente');
            $curso->descripcion     = $request->input('descripcion');
            $curso->adicional       = $request->input('adicional');
            $curso->requerimiento   = $request->input('requerimiento');
            $curso->estado          = $request->input('estado');
            $curso->save();
            
            // $catCurso = Categoriacurso::find($id);
            // $catCurso->idCategoria  = $request->input('categoria');
            // $catCurso->idCurso      = $id;
            // $catCurso->estado       = $request->input('estado');
            // $catCurso->save();
            
            DB::commit();
            
            \Session::flash('message', 'Actualizado! Se registraron los datos correctamente');
            return redirect()->route('admin.curso_list');

        } catch (\Exception $e) {
            DB::rollback();
            dd($e); die();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // dd($id);
        $curso = Curso::find($id);
        $curso->estado = 3;
        $curso->save();
        $data = array(
            'status' => true
        );

        echo json_encode($data);
    }


    ## CATEGORIA

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexCategoria(Request $request)
    {
        // dd($request->user());
        // $request->admin()->authorizeRoles(['admin']);

        $categoria = DB::table('categoria')->whereIn('estadoCat',[1,2])->get();

        // dd($usuarios);

        $a_data_page = array(
            'title' => 'Lista de Categoria',
            'usuarios'=> $categoria
        );
        return \Views::admin('categoria.index',$a_data_page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCategoria()
    {
        $a_data_page = array(
            'title' => 'Registro de categoria'
        );
        return \Views::admin('categoria.create',$a_data_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCategoria(Request $request)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
           'descripcion'  => "required",
           'estado'       => "required",
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }
        
        DB::beginTransaction();
        try{
            $perso = new Categoria;
            $perso->nombreCat            = $request->input('descripcion');
            $perso->estadoCat            = $request->input('estado');
            $perso->save();
            DB::commit();
            \Session::flash('message', 'Registrado! Se registraron los datos correctamente');
            return redirect()->route('admin.categoria_list');

        } catch (\Exception $e) {
            DB::rollback();
            dd($e); die();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editCategoria($id)
    {
        $categoria = DB::table('categoria')->where(['idCat' => $id])->whereIn('estadoCat',[1,2])->first();

        $a_data_page = array(
            'title'     => 'Editar de Categoria',
            'categoria' => $categoria
        );

        return \Views::admin('categoria.editar',$a_data_page);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateCategoria(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'descripcion'         => "required",
            'estado'       => "required"
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }

        $perso = Categoria::find($id);
        $perso->nombreCat            = $request->input('descripcion');
        $perso->estadoCat            = $request->input('estado');
        $perso->save();

        
        \Session::flash('message', 'Actualizado! Se actualizaron los datos');

        return redirect()->route('admin.categoria_list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteCategoria($id)
    {
        // dd($id);
        // $usuario = Categoria::find($id);

        $perso = Categoria::find($id);
        $perso->estado = 3;
        $perso->save();
        $data = array(
            'status' => true
        );

        echo json_encode($data);
    }





    ## DETALLE CURSO

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexDetalle(Request $request,$id)
    {
        $request->user()->authorizeRoles(['admin']);
        $curso = DB::table('curso')->where('idCurso',[$id])->first();
        // dd($curso);

        $detalleCategoria = DB::table('detallecurso')->where('idCurso',[$id])->whereIn('estado',[0,1,2])->get();
        // Detallecurso
        $a_data_page = array(
            'title' => 'Lista de Detalle Curso',
            'curso'=> $curso,
            'detalle'=> $detalleCategoria
        );
        // dd($a_data_page);
        return \Views::admin('curso.detalle',$a_data_page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createDetalle(Request $request, $id)
    {
        $a_data_page = array(
            'title' => 'Registro de detalle',
            'idCurso' => $id
        );
        return \Views::admin('curso.create_detalle',$a_data_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeDetalle(Request $request)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
           'grupo'      => "required",
           'idcurso'    => "required",
           'estado'     => "required",
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }
        
        DB::beginTransaction();
        try{
            $idcurso = $request->input('idcurso');
            $detalle = new Detallecurso;
            $detalle->titulo    = $request->input('grupo');
            $detalle->idCurso   = $idcurso;
            $detalle->fechaRegistro = date('Y-m-d H:i:s');
            $detalle->estado    = $request->input('estado');
            $detalle->save();

            DB::commit();
            \Session::flash('message', 'Registrado! Se registraron los datos correctamente');
            return redirect()->route('admin.detalle_curso_list', [ 'id' => $idcurso ]);

        } catch (\Exception $e) {
            DB::rollback();
            dd($e); die();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editDetalle($id, $curso)
    {
        $detalleCategoria = DB::table('detallecurso')->where('idDetalleCurso',[$id])->first();

        $a_data_page = array(
            'title'         => 'Editar de Categoria',
            'detalleCurso' => $detalleCategoria,
            'curso'         => $curso
        );
        // dd($a_data_page);
        return \Views::admin('curso.editar_detalle',$a_data_page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateDetalle(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'grupo'      => "required",
            'idcurso'    => "required",
            'estado'     => "required",
         ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }

        DB::beginTransaction();
        try{
            $idcurso = $request->input('idcurso');

            $detalle = Detallecurso::find($id);
            $detalle->titulo    = $request->input('grupo');
            $detalle->estado    = $request->input('estado');
            // dd($detalle);
            $detalle->save();
            DB::commit();

            \Session::flash('message', 'Registrado! Se registraron los datos correctamente');
            return redirect()->route('admin.detalle_curso_list', [ 'id' => $idcurso ]);

        } catch (\Exception $e) {
            DB::rollback();
            dd($e); die();
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteDetalle($id)
    {
        // dd($id);
        // $usuario = Categoria::find($id);
        $detalle = Detallecurso::find($id);
        $detalle->estado = 3;
        $detalle->save();
        
        $data = array(
            'status' => true
        );

        echo json_encode($data);
    }




    ## DETALLE TEMA

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexTema(Request $request,$id)
    {
        $request->user()->authorizeRoles(['admin']);
        $detcurso = DB::table('detallecurso')->where('idDetalleCurso',[$id])->first();
        
        $temas = DB::table('temas')->where('idDetalleCurso',[$id])->whereIn('estado',[0,1,2])->get();
        // Detallecurso
        $a_data_page = array(
            'title' => 'Lista de Detalle Tema',
            'detcurso'=> $detcurso,
            'tema'=> $temas
        );
        // dd($a_data_page);
        return \Views::admin('curso.tema',$a_data_page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createTema(Request $request, $id)
    {
        $a_data_page = array(
            'title' => 'Registro de Tema',
            'idDetalleCurso' => $id
        );
        return \Views::admin('curso.tema_create',$a_data_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeTema(Request $request)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
           'titulo'      => "required",
           'idDetalleCurso'    => "required",
           'video'     => "required",
           'duracion'   => 'required',
           'privado'   => 'required',
           'estado'   => 'required',
           'descripcion'   => 'required'
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }
        
        DB::beginTransaction();
        try{
            $idDetalleCurso = $request->input('idDetalleCurso');
            $tema = new Tema;
            $tema->titulo           = $request->input('titulo');
            $tema->idDetalleCurso   = $idDetalleCurso;
            $tema->video            = $request->input('video');
            $tema->duracion         = $request->input('duracion');
            $tema->privado          = $request->input('privado');
            $tema->estado           = $request->input('estado');
            $tema->descripcion      = $request->input('descripcion');
            $tema->FechaRegistro      = date('Y-m-d H:i:s');
            $tema->save();

            DB::commit();
            \Session::flash('message', 'Registrado! Se registraron los datos correctamente');
            return redirect()->route('admin.tema_curso_list', [ 'id' => $idDetalleCurso ]);

        } catch (\Exception $e) {
            DB::rollback();
            dd($e); die();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editTema($id, $idDetalleCurso)
    {
        // $categoria = DB::table('categoria')->where(['idCat' => $id])->whereIn('estadoCat',[1,2])->first();
        // $detalleCategoria = DB::table('detallecurso')->where('idDetalleCurso',[$id])->first();
        // $detcurso = DB::table('detallecurso')->where('idDetalleCurso',[$id])->first();

        $temas = DB::table('temas')->where('idTema',[$id])->first();

        $a_data_page = array(
            'title'     => 'Editar de Tema',
            'idDetalleCurso' => $idDetalleCurso,
            'temas' => $temas
        );
        // dd($a_data_page);
        return \Views::admin('curso.tema_editar',$a_data_page);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateTema(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'titulo'      => "required",
            'idDetalleCurso'    => "required",
            'video'     => "required",
            'duracion'   => 'required',
            'privado'   => 'required',
            'estado'   => 'required',
            'descripcion'   => 'required'
         ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }

        $idDetalleCurso = $request->input('idDetalleCurso');
        $tema = Tema::find($id);
        $tema->titulo           = $request->input('titulo');
        $tema->idDetalleCurso   = $idDetalleCurso;
        $tema->video            = $request->input('video');
        $tema->duracion         = $request->input('duracion');
        $tema->privado          = $request->input('privado');
        $tema->estado           = $request->input('estado');
        $tema->descripcion      = $request->input('descripcion');
        $tema->save();

        DB::commit();
        \Session::flash('message', 'Actualizado! Se actualizaron los datos');
        return redirect()->route('admin.tema_curso_list', [ 'id' => $idDetalleCurso ]);

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteTema($id)
    {
        $tema = Tema::find($id);
        $tema->Estado = 3;
        $tema->save();

        $data = array(
            'status' => true
        );

        echo json_encode($data);
    }
}
