<?php

namespace App\Http\Controllers\Admin;

use App\Models\{
    Modulo, Pagina, Persona, Usuario, Actividades, Actividadperso
};
use App\Models\Role;
use App\User;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;

class MonitoreoController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);
        $actividades = DB::table('actividades')
        ->where(['actividades.estado'=>1])
        ->get();

        $monitoreo = DB::select("select p.nombre,p.apellidoMaterno,p.apellidoPaterno, a.latitud,a.longitud ,count(1) 'actividades' 
                from actividadpersona a
                INNER JOIN users u on a.idPersona = u.id
                INNER JOIN persona p on u.idUsuario = p.idPersona
                where a.concluido = 1
                GROUP BY p.nombre,p.apellidoMaterno,p.apellidoPaterno, a.latitud,a.longitud");

                // dd($monitoreo); 
        $a_data_page = array(
            'title' => 'Lista de Actividades',
            'actividades'=> $actividades,
            'puntosreferencia'    =>  $monitoreo
        );


        return \Views::admin('actividades.monitoreo',$a_data_page);
    }


}
