<?php

namespace App\Http\Controllers\Admin;

use App\Models\{
    Modulo, Pagina, Reunion, Detallereunion, Asistencia, Backups
};

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use DB;

class BackupController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);
        
        $mes = \Views::diccionario('idMes');
        
        $bks = DB::table('backups')
        ->where([
            'estado'   => 1
        ])->orderBy('fecha_registro','DESC')->get();

        // dd($cuota);
        $a_data_page = array(
            'title' => 'Lista de Backups',
            'backups' => $bks
        );

        return \Views::admin('backup.index',$a_data_page);
    }

    public function generar(Request $request){
        date_default_timezone_set('America/Lima');
        $path       = public_path('bat\bk.bat ');
        $pathapp    = " ".public_path('app\\');
        $path2      = " ".public_path('backup');
        $dbhost     = " ".env('DB_HOST', '');
        $dbname     = " ".env('DB_DATABASE', '');
        $dbuser     = " ".env('DB_USERNAME', '');

        $filename = ' bk_'.date('Ymd_his').'_'.uniqid();
        
        $output     = array(
            'status'=> false,
            'data' => ''
        );

        if($dbhost != '' || $dbname != '' || $dbuser != ''){
            exec($path . $path2. $pathapp . $dbhost . $dbname . $dbuser . $filename,$output2);

            $bks = new Backups;
            $bks->descripcion      = "Backups generado: ".date("d-m-Y h:i:s");
            $bks->fecha_registro   = date("Y-m-d h:i:s");
            $bks->nombreArchivo    = trim($filename.'.sql');
            $bks->estado   = 1;
            $bks->save();

            $output = array(
                'status'=> true,
                'data' => $output2
            );
        }

        // ACCESO A BD
        echo json_encode($output);
        
    }


    public function restaurar(Request $request){
        $name = $request->input('name');

        date_default_timezone_set('America/Lima');
        $path       = public_path('bat\rt.bat ');

        $pathapp    = " ".public_path('app\\');
        $path2      = " ".public_path('backup');
        $dbhost     = " ".env('DB_HOST', '');
        $dbname     = " ".env('DB_DATABASE', '');
        $dbuser     = " ".env('DB_USERNAME', '');

        $filename = " $name";
        

        $output     = array(
            'status'=> false,
            'data' => ''
        );
        // dd($path , $path2, $pathapp,$dbhost);
        // die();

        // dd($path , $path2, $pathapp , $dbhost , $dbname , $dbuser , $filename);

        if( $dbname != '' || $dbuser != ''){
            exec($path . $path2. $pathapp . $dbhost . $dbname . $dbuser . $filename,$output2);

            // $bks = new Backups;
            // $bks->descripcion      = "Backups generado: ".date("d-m-Y h:i:s");
            // $bks->fecha_registro   = date("Y-m-d h:i:s");
            // $bks->nombreArchivo    = trim($filename.'.sql');
            // $bks->estado   = 1;
            // $bks->save();

            $output = array(
                'status'=> true,
                'data' => $output2
            );
        }

        // ACCESO A BD
        echo json_encode($output);
        
    }


}
