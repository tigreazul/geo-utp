<?php

namespace App\Http\Controllers\Admin;

use App\Models\{
    Modulo, Pagina, Pago
};

use Pdfempresa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;

class PagosController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['user', 'admin']);
        
        $frontend =  DB::table('frontend')
        ->where([
            'Estado'   => 1
        ])
        ->orderBy('ID')
        ->get();

        


        $a_data_page = array(
            'title' => 'Lista de Pagos',
            'pagina'=> $frontend
        );

        return \Views::admin('pagos.index',$a_data_page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $prefencia = \Views::diccionario('idPreferencia');
        $reuniones  = array();
        $cuota  = array();
        $legal  =array();
        $filtro = '';
        $dato = '';

        $a_data_page = array(
            'title'     => 'Registro de paginas',
            'reuniones'   => $reuniones,
            'cuota'       => $cuota,
            'legal'       => $legal,
            'filtro'       => $filtro,
            'dato'       => $dato,
            'prefencia'   => $prefencia
        );

        return \Views::admin('pagos.create',$a_data_page);
    }


    public function validaBusqueda(Request $request){
        // dd($request); die();
        $validator = Validator::make($request->all(), [
            'buscador' => "required"
        ]);
 
        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }

        $search     =  $request->input('buscador');
        $selector   =  $request->input('selector');

        // $vSearch  = DB::select("SELECT u.idUsuario, p.nombre, p.apellidoPaterno, p.apellidoMaterno,e.nomDireccion,g.nomGrupo
        // FROM usuario u
        // INNER JOIN persona p ON u.idPersona = p.idPersona
        // INNER JOIN expediente e ON e.idUsuario = e.idUsuario
        // INNER JOIN manzanas m ON m.idManzana = e.idManzana
        // INNER JOIN grupo g ON g.idGrupo = m.idGrupo
        // where p.dni = '$search'");

        if($selector == 1){
            $buscar = "AND p.dni = '$search'";
        }

        if($selector == 2){
            $buscar = "AND e.nroExpediente = '$search'";
        }

        $vSearch  = DB::select("SELECT 
            p.nombre, p.apellidoPaterno, p.apellidoMaterno, p.dni
            ,e.nomDireccion
            ,g.nomGrupo
            ,e.idManzana
            ,t.estadoSocio
        FROM titular t
        INNER JOIN persona p ON t.idPersona = p.idPersona
        INNER JOIN expediente e ON e.nroExpediente = t.nroExpediente
        INNER JOIN manzanas m ON m.idManzana = e.idManzana
        INNER JOIN grupo g ON g.idGrupo = m.idGrupo
        WHERE t.estadoSocio = 'ACTIVO' $buscar");

        // dd($vSearch);

        if(!empty($vSearch)){
            return redirect()->route('admin.pagos_create_search',[$search,$selector]);
        }else{
            \Session::flash('message', 'El numero ingresado no existe ');
            return redirect()->route('admin.pagos_create');
        }
        
    }


    public function busqueda(Request $request,$id,$filtro)
    {

        // $persona  = DB::select("SELECT u.idUsuario, p.nombre, p.apellidoPaterno, p.apellidoMaterno,e.nomDireccion,g.nomGrupo
        //     FROM usuario u
        //     INNER JOIN persona p ON u.idPersona = p.idPersona
        //     INNER JOIN expediente e ON e.idUsuario = e.idUsuario
        //     INNER JOIN manzanas m ON m.idManzana = e.idManzana
        //     INNER JOIN grupo g ON g.idGrupo = m.idGrupo
        //     where p.dni = '$id'");

        if($filtro == 1){
            $buscar = "AND p.dni = '$id'";
        }

        if($filtro == 2){
            $buscar = "AND e.nroExpediente = '$id'";
        }

        $persona  = DB::select("SELECT
            p.nombre, p.apellidoPaterno, p.apellidoMaterno, p.dni
            ,e.nomDireccion
            ,g.nomGrupo
            ,e.idManzana
            ,t.estadoSocio
        FROM titular t
        INNER JOIN persona p ON t.idPersona = p.idPersona
        INNER JOIN expediente e ON e.nroExpediente = t.nroExpediente
        INNER JOIN manzanas m ON m.idManzana = e.idManzana
        INNER JOIN grupo g ON g.idGrupo = m.idGrupo
        WHERE t.estadoSocio = 'ACTIVO' $buscar");
        
        // dd($persona);

        $prefencia = \Views::diccionario('idPreferencia');

        $reuniones  = DB::select("SELECT 
            u.idUsuario,r.idTipoReunion,
            (SELECT valor FROM diccionario WHERE ubicacion = 'idTipoReunion' AND codigo = r.idTipoReunion ) AS 'tipo' ,
            dc.multa,r.fecha,MONTH(r.fecha) AS MES,dc.idDetalleReunion
            FROM usuario u
            LEFT JOIN persona p ON u.idPersona = p.idPersona
            INNER JOIN titular t ON p.idPersona = t.idPersona
            INNER JOIN expediente e ON e.nroExpediente = t.nroExpediente
            LEFT JOIN detalle_reunion dc ON dc.idUsuario = u.idUsuario
            LEFT JOIN reunion r ON r.idReunion = dc.idReunion
            WHERE dc.idDetalleReunion NOT IN (SELECT p.idCodigo FROM pago p where p.idCodigo = dc.idDetalleReunion and idUsuario = dc.idUsuario) $buscar");

        $cuota  = DB::select("SELECT dc.*,c.idTipoCuota,c.fechaRegistro,
            (SELECT valor FROM diccionario WHERE ubicacion = 'idTipoCuota' AND codigo = c.idTipoCuota ) AS 'tipo',
            (SELECT d.valor FROM diccionario d WHERE d.ubicacion = 'idMes' AND d.codigo = dc.idMes ) AS 'mes'
            FROM usuario u
            LEFT JOIN persona p ON u.idPersona = p.idPersona
            INNER JOIN titular t ON p.idPersona = t.idPersona
            INNER JOIN expediente e ON e.nroExpediente = t.nroExpediente
            LEFT JOIN detalle_cuota dc ON dc.idUsuario = u.idUsuario
            LEFT JOIN cuota c ON c.idCuota = dc.idCuota
            WHERE c.idTipoCuota IN (2,3) $buscar 
            AND dc.idCuotaDetalle NOT IN (SELECT p.idCodigo FROM pago p where p.idCodigo = dc.idCuotaDetalle and idUsuario = dc.idUsuario)");

        
        $legal  = DB::select("SELECT dc.*,c.idTipoCuota,c.fechaRegistro,
            (SELECT valor FROM diccionario WHERE ubicacion = 'idTipoCuota' AND codigo = c.idTipoCuota ) AS 'tipo',
            (SELECT d.valor FROM diccionario d WHERE d.ubicacion = 'idMes' AND d.codigo = dc.idMes ) AS 'mes'
            FROM usuario u
            LEFT JOIN persona p ON u.idPersona = p.idPersona
            INNER JOIN titular t ON p.idPersona = t.idPersona
            INNER JOIN expediente e ON e.nroExpediente = t.nroExpediente
            LEFT JOIN detalle_cuota dc ON dc.idUsuario = u.idUsuario
            LEFT JOIN cuota c ON c.idCuota = dc.idCuota
            WHERE c.idTipoCuota IN (1) $buscar 
            AND dc.idCuotaDetalle NOT IN (SELECT p.idCodigo FROM pago p where p.idCodigo = dc.idCuotaDetalle and idUsuario = dc.idUsuario)");

        \Session::flash('resultado', 'ok');

        $a_data_page = array(
            'title'         => 'Registro de paginas',
            'persona'       => $persona,
            'reuniones'     => $reuniones,
            'cuota'         => $cuota,
            'legal'         => $legal,
            'dato'          => $id,
            'filtro'        => $filtro,
            'prefencia'     => $prefencia
        );

        return \Views::admin('pagos.create',$a_data_page);
    }


    public function justificar(Request $request){
        // dd($request); die();

        $data = $request->validate([
            'idUsuario'     => 'required',
            'idtipo_j'      => 'required',
            'precencia'     => 'required',
            'idjustifica'   => 'required',
            'fecha_tipot'   => 'required',
            'motivo'        => 'required',
            'identifica'        => 'required',
            'origin'        => 'required'
        ], [
            'idUsuario'     => 'Campo requerido',
            'idtipo_j'      => 'Campo requerido',
            'precencia'     => 'Campo requerido',
            'idjustifica'   => 'Campo requerido',
            'fecha_tipot'   => 'Campo requerido',
            'motivo'        => 'Campo requerido'
        ]);

        $pago = new Pago;
        $pago->idUsuario   = $data['idUsuario'];
        $pago->Tipo        = $data['idtipo_j'];
        $pago->Presencia   = $data['precencia'];
        // $pago->monto       = $data['idjustifica'];
        $pago->FechaTipo   = $data['fecha_tipot'];
        $pago->FechaPago   = date('Y-m-d');
        $pago->motivo      = $data['motivo'];
        $pago->estado      = 1;
        $pago->idCodigo    = $data['idjustifica'];
        $pago->identificador    = $data['identifica'];
        $pago->origen    = $data['origin'];
        $pago->save();

        return response()->json(['msg'=> true], 200);


    }


    public function pagar(Request $request){
        // dd($request); die();

        // "idUsuario" => "7"
        // "idtipo_p" => "3"
        // "fecha_tipot" => "2019-08-06"
        // "monto" => "345"
        // "idPago" => "3"
        // "identifica" => "PAGO"
        // "origin" => "R"


        $data = $request->validate([
            'idUsuario'     => 'required',
            'idtipo_p'      => 'required',
            'fecha_tipot'     => 'required',
            'monto'   => 'required',
            'idPago'   => 'required',
            'identifica'        => 'required',
            'origin'        => 'required'
        ], [
            'idUsuario'     => 'Campo requerido',
            'idtipo_p'      => 'Campo requerido',
            'fecha_tipot'     => 'Campo requerido',
            'monto'   => 'Campo requerido',
            'idPago'   => 'Campo requerido',
            'identifica'        => 'Campo requerido',
            'origin'        => 'Campo requerido'
        ]);

        $pago = new Pago;
        $pago->idUsuario        = $data['idUsuario'];
        $pago->Tipo             = $data['idtipo_p'];
        $pago->FechaTipo        = $data['fecha_tipot'];
        $pago->monto            = $data['monto'];
        $pago->FechaPago        = date('Y-m-d');
        $pago->idCodigo         = $data['idPago'];
        $pago->estado           = 1;
        $pago->identificador    = $data['identifica'];
        $pago->origen           = $data['origin'];
        $pago->save();

        return response()->json(['msg'=> true], 200);


    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $front = new Front;
        $front->Titulo              = $request->titulo;
        $front->Categoria           = $request->categoria;
        $front->Estado              = 1;
        $front->Tag                 = $request->tag;
        $front->FechaIngreso        = date('Y-m-d');
        $front->Imagen_principal    = $request->images;
        $front->Contenido           = $request->contentenido;
        $front->save();

        return redirect()->route('admin.front_list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function imprimirPago(Request $request,$id,$filtro){
        header('Content-type: application/pdf');
        if($filtro == 1){
            $buscar = "AND p.dni = '$id'";
        }

        if($filtro == 2){
            $buscar = "AND e.nroExpediente = '$id'";
        }

        $persona  = DB::select("SELECT
            p.nombre, p.apellidoPaterno, p.apellidoMaterno, p.dni
            ,e.nomDireccion
            ,g.nomGrupo
            ,e.idManzana
            ,t.estadoSocio
        FROM titular t
        INNER JOIN persona p ON t.idPersona = p.idPersona
        INNER JOIN expediente e ON e.nroExpediente = t.nroExpediente
        INNER JOIN manzanas m ON m.idManzana = e.idManzana
        INNER JOIN grupo g ON g.idGrupo = m.idGrupo
        WHERE t.estadoSocio = 'ACTIVO' $buscar");
        
        // dd($persona);

        $prefencia = \Views::diccionario('idPreferencia');

        $reuniones  = DB::select("SELECT 
            u.idUsuario,r.idTipoReunion,
            (SELECT valor FROM diccionario WHERE ubicacion = 'idTipoReunion' AND codigo = r.idTipoReunion ) AS 'tipo' ,
            dc.multa,r.fecha,MONTH(r.fecha) AS MES,dc.idDetalleReunion
            FROM usuario u
            LEFT JOIN persona p ON u.idPersona = p.idPersona
            INNER JOIN titular t ON p.idPersona = t.idPersona
            INNER JOIN expediente e ON e.nroExpediente = t.nroExpediente
            LEFT JOIN detalle_reunion dc ON dc.idUsuario = u.idUsuario
            LEFT JOIN reunion r ON r.idReunion = dc.idReunion
            WHERE dc.idDetalleReunion NOT IN (SELECT p.idCodigo FROM pago p where p.idCodigo = dc.idDetalleReunion and idUsuario = dc.idUsuario) $buscar");

        $cuota  = DB::select("SELECT dc.*,c.idTipoCuota,c.fechaRegistro,
            (SELECT valor FROM diccionario WHERE ubicacion = 'idTipoCuota' AND codigo = c.idTipoCuota ) AS 'tipo',
            (SELECT d.valor FROM diccionario d WHERE d.ubicacion = 'idMes' AND d.codigo = dc.idMes ) AS 'mes'
            FROM usuario u
            LEFT JOIN persona p ON u.idPersona = p.idPersona
            INNER JOIN titular t ON p.idPersona = t.idPersona
            INNER JOIN expediente e ON e.nroExpediente = t.nroExpediente
            LEFT JOIN detalle_cuota dc ON dc.idUsuario = u.idUsuario
            LEFT JOIN cuota c ON c.idCuota = dc.idCuota
            WHERE c.idTipoCuota IN (2,3) $buscar 
            AND dc.idCuotaDetalle NOT IN (SELECT p.idCodigo FROM pago p where p.idCodigo = dc.idCuotaDetalle and idUsuario = dc.idUsuario)");

        
        $legal  = DB::select("SELECT dc.*,c.idTipoCuota,c.fechaRegistro,
            (SELECT valor FROM diccionario WHERE ubicacion = 'idTipoCuota' AND codigo = c.idTipoCuota ) AS 'tipo',
            (SELECT d.valor FROM diccionario d WHERE d.ubicacion = 'idMes' AND d.codigo = dc.idMes ) AS 'mes'
            FROM usuario u
            LEFT JOIN persona p ON u.idPersona = p.idPersona
            INNER JOIN titular t ON p.idPersona = t.idPersona
            INNER JOIN expediente e ON e.nroExpediente = t.nroExpediente
            LEFT JOIN detalle_cuota dc ON dc.idUsuario = u.idUsuario
            LEFT JOIN cuota c ON c.idCuota = dc.idCuota
            WHERE c.idTipoCuota IN (1) $buscar 
            AND dc.idCuotaDetalle NOT IN (SELECT p.idCodigo FROM pago p where p.idCodigo = dc.idCuotaDetalle and idUsuario = dc.idUsuario)");



        // dd($vSearch);
        // use Pdfempresa;
        $pdf = new Pdfempresa;
        $pdf->AliasNbPages();
		$pdf->SetFont('Arial','',10);
		$pdf->AddPage();
		$pdf->Ln(4);

		//****************************************
		//****************************************

        $pdf->SetFont('Arial','B',9);
        $pdf->SetFillColor(205,205,205);    
        $pdf->Cell(185,8,utf8_decode('DATOS DE TITULAR'),1,0,'L',true);
        $pdf->SetFillColor(205,205,205);
        $pdf->Ln();


        $pdf->SetFillColor(205,205,205);//color de fondo tabla
        $pdf->SetTextColor(10);
        $pdf->SetDrawColor(153,153,153);
        $pdf->SetLineWidth(.3);
        $pdf->SetFont('Arial','B',9);
        $pdf->SetFillColor(205,205,205);
        $pdf->Cell(45,6,'DATOS PERSONALES',1,0,'L',true);
        $pdf->SetFillColor(255,255,255);
        $pdf->MultiCell( 140, 6, utf8_decode($persona[0]->nombre.' '.$persona[0]->apellidoPaterno.' '.$persona[0]->apellidoMaterno), 1,'L',true); 
        $pdf->SetFont('Arial','B',9);
        $pdf->SetFillColor(255,255,255);

        $pdf->SetFillColor(205,205,205);//color de fondo tabla
        $pdf->SetTextColor(10);
        $pdf->SetDrawColor(153,153,153);
        $pdf->SetLineWidth(.3);
        $pdf->SetFont('Arial','B',9);
        $pdf->SetFillColor(205,205,205);
        $pdf->Cell(45,6,utf8_decode('DIRECCIÓN'),1,0,'L',true);
        $pdf->SetFillColor(255,255,255);
        $pdf->MultiCell( 140, 6, utf8_decode($persona[0]->nomDireccion), 1,'L',true); 
        $pdf->SetFont('Arial','B',9);
        $pdf->SetFillColor(255,255,255);

        $pdf->SetFillColor(205,205,205);//color de fondo tabla
        $pdf->SetTextColor(10);
        $pdf->SetDrawColor(153,153,153);
        $pdf->SetLineWidth(.3);
        $pdf->SetFont('Arial','B',9);
        $pdf->SetFillColor(205,205,205);
        $pdf->Cell(45,6,'GRUPO',1,0,'L',true);
        $pdf->SetFillColor(255,255,255);
        $pdf->MultiCell( 140, 6, utf8_decode($persona[0]->nomGrupo), 1,'L',true); 
        $pdf->SetFont('Arial','B',9);
        $pdf->SetFillColor(255,255,255);

        // REUNIONES
        $pdf->Ln();
        $pdf->SetFont('Arial','B',9);
        $pdf->SetFillColor(205,205,205);    
        $pdf->Cell(185,8,utf8_decode('REUNIÓN CON DEUDAS'),1,0,'L',true);
        $pdf->SetFillColor(205,205,205);
        $pdf->Ln();
        $pdf->SetFont('Arial','B',8);
        $w = array(85,20,80);
        $pdf->Cell($w[0],6,utf8_decode('TIPO'),1,0,'L',true);
        $pdf->Cell($w[1],6,utf8_decode('MES'),1,0,'L',true);
        $pdf->Cell($w[2],6,utf8_decode('MONTO'),1,0,'L',true);
        $pdf->Ln();
        // Restauración de colores y fuentes
        $pdf->SetFillColor(224,235,255);
        $pdf->SetTextColor(0);
        $pdf->SetFont('Arial','',7);
        // // Datos
        $fill = false;
        $contador = 0;
        $total = 0;
        foreach ($reuniones as $multa) {
            $contador++;
            $linea = 0;
            
            if($contador == 21){
                $pdf->Cell(array_sum($w),0,'','T');
                $pdf->AddPage();
                $linea = 0;
            }else{
                $linea++;
            }
                        
            if($linea!= 0 && $linea%58==0 ){
                $pdf->Cell(array_sum($w),0,'','T');
                $pdf->AddPage();
            }
    

            $pdf->Cell($w[0],5,$multa->tipo,'LR',0,'L');
            $pdf->Cell($w[1],5,$multa->fecha,'LR',0,'L');
            $pdf->Cell($w[2],5,$multa->multa,'LR',0,'L');
            $pdf->Ln();
            $fill = !$fill;

            $total = $total + $multa->multa; 
        }
        // // Línea de cierre
        $pdf->Cell(array_sum($w),0,'','T');
        $pdf->Ln();

        $pdf->SetFillColor(205,205,205);
        $pdf->SetTextColor(10);
        $pdf->SetDrawColor(153,153,153);
        $pdf->SetLineWidth(.3);
        $pdf->SetFont('Arial','B',9);
        $pdf->SetFillColor(205,205,205);
        $pdf->Cell(105,6,'TOTAL',1,0,'L',true);
        $pdf->SetFillColor(255,255,255);
        $pdf->MultiCell( 80, 6, $total, 1,'L',true); 
        $pdf->SetFont('Arial','B',9);
        $pdf->SetFillColor(255,255,255);
        $pdf->Ln(5);


        // CUOTA LEGAL
        $pdf->Ln();
        $pdf->SetFont('Arial','B',9);
        $pdf->SetFillColor(205,205,205);    
        $pdf->Cell(185,8,utf8_decode('CUOTAS DE TEMA LEGAL'),1,0,'L',true);
        $pdf->SetFillColor(205,205,205);
        $pdf->Ln();
        $pdf->SetFont('Arial','B',8);
        $w = array(85,20,80);
        $pdf->Cell($w[0],6,utf8_decode('TIPO'),1,0,'L',true);
        $pdf->Cell($w[1],6,utf8_decode('MES'),1,0,'L',true);
        $pdf->Cell($w[2],6,utf8_decode('MONTO'),1,0,'L',true);
        $pdf->Ln();
        // Restauración de colores y fuentes
        $pdf->SetFillColor(224,235,255);
        $pdf->SetTextColor(0);
        $pdf->SetFont('Arial','',7);
        // // Datos
        $fill = false;
        $contador = 0;
        $total = 0;
        foreach ($legal as $leg) {
            $contador++;
            $linea = 0;
            
            if($contador == 21){
                $pdf->Cell(array_sum($w),0,'','T');
                $pdf->AddPage();
                $linea = 0;
            }else{
                $linea++;
            }
                        
            if($linea!= 0 && $linea%58==0 ){
                $pdf->Cell(array_sum($w),0,'','T');
                $pdf->AddPage();
            }
    

            $pdf->Cell($w[0],5,$leg->tipo,'LR',0,'L');
            $pdf->Cell($w[1],5,$leg->mes,'LR',0,'L');
            $pdf->Cell($w[2],5,$leg->monto,'LR',0,'L');
            $pdf->Ln();
            $fill = !$fill;
            $total = $total + $leg->monto;
        }
        // Línea de cierre
        $pdf->Cell(array_sum($w),0,'','T');
        $pdf->Ln();

        $pdf->SetFillColor(205,205,205);
        $pdf->SetTextColor(10);
        $pdf->SetDrawColor(153,153,153);
        $pdf->SetLineWidth(.3);
        $pdf->SetFont('Arial','B',9);
        $pdf->SetFillColor(205,205,205);
        $pdf->Cell(105,6,'TOTAL',1,0,'L',true);
        $pdf->SetFillColor(255,255,255);
        $pdf->MultiCell( 80, 6, $total, 1,'L',true); 
        $pdf->SetFont('Arial','B',9);
        $pdf->SetFillColor(255,255,255);
        $pdf->Ln(5);


        // ADMINISTRATIVA
        $pdf->Ln();
        $pdf->SetFont('Arial','B',9);
        $pdf->SetFillColor(205,205,205);    
        $pdf->Cell(185,8,utf8_decode('CUOTAS ADM. Y EXTRAORDINARIAS'),1,0,'L',true);
        $pdf->SetFillColor(205,205,205);
        $pdf->Ln();
        $pdf->SetFont('Arial','B',8);
        $w = array(85,20,80);
        $pdf->Cell($w[0],6,utf8_decode('TIPO'),1,0,'L',true);
        $pdf->Cell($w[1],6,utf8_decode('MES'),1,0,'L',true);
        $pdf->Cell($w[2],6,utf8_decode('MONTO'),1,0,'L',true);
        $pdf->Ln();
        // Restauración de colores y fuentes
        $pdf->SetFillColor(224,235,255);
        $pdf->SetTextColor(0);
        $pdf->SetFont('Arial','',7);
        // Datos
        $fill = false;
        $contador = 0;
        $total = 0;
        foreach ($cuota as $cuo) {
            $contador++;
            $linea = 0;
            
            if($contador == 21){
                $pdf->Cell(array_sum($w),0,'','T');
                $pdf->AddPage();
                $linea = 0;
            }else{
                $linea++;
            }
                        
            if($linea!= 0 && $linea%58==0 ){
                $pdf->Cell(array_sum($w),0,'','T');
                $pdf->AddPage();
            }
    

            $pdf->Cell($w[0],5,$cuo->tipo,'LR',0,'L');
            $pdf->Cell($w[1],5,$cuo->mes,'LR',0,'L');
            $pdf->Cell($w[2],5,$cuo->monto,'LR',0,'L');
            $pdf->Ln();
            $fill = !$fill;
            $total = $total + $leg->monto;
        }
        // Línea de cierre
        $pdf->Cell(array_sum($w),0,'','T');
        $pdf->Ln();

        $pdf->SetFillColor(205,205,205);
        $pdf->SetTextColor(10);
        $pdf->SetDrawColor(153,153,153);
        $pdf->SetLineWidth(.3);
        $pdf->SetFont('Arial','B',9);
        $pdf->SetFillColor(205,205,205);
        $pdf->Cell(105,6,'TOTAL',1,0,'L',true);
        $pdf->SetFillColor(255,255,255);
        $pdf->MultiCell( 80, 6, $total, 1,'L',true); 
        $pdf->SetFont('Arial','B',9);
        $pdf->SetFillColor(255,255,255);
        $pdf->Ln(5);;

		
		//****************************************
		// DATOS 
		//****************************************
		//TITULO
		// $pdf->SetFont('Arial','B',9);
		// $pdf->SetFillColor(205,205,205);	
		// $pdf->Cell(185,8,utf8_decode('LISTA DE SOCIOS'),1,0,'L',true);
		// $pdf->SetFillColor(205,205,205);
		// $pdf->Ln();
		
		// $pdf->SetFont('Arial','B',8);
		// $w = array(7, 25,60,60,33);
		// $pdf->Cell($w[0],6,utf8_decode('#'),1,0,'L',true);
		// $pdf->Cell($w[1],6,utf8_decode('EXPEDIENTE'),1,0,'L',true);
		// $pdf->Cell($w[2],6,utf8_decode('NOMBRE Y APELLIDOS'),1,0,'L',true);
		// $pdf->Cell($w[3],6,utf8_decode('DIRECCIÓN'),1,0,'L',true);
		// $pdf->Cell($w[4],6,utf8_decode('MANZANA'),1,0,'L',true);
		// $pdf->Ln();
		
		// // Restauración de colores y fuentes
		// $pdf->SetFillColor(224,235,255);
		// $pdf->SetTextColor(0);
		// $pdf->SetFont('Arial','',7);
		// // // Datos
		// $fill = false;
  //       $contador = 0;
  //       foreach ($vSearch as $search) {
		// 	$contador++;
  //           $linea = 0;
            
		// 	if($contador == 21){
		// 		$pdf->Cell(array_sum($w),0,'','T');
		// 		$pdf->AddPage();
		// 		$linea = 0;
		// 	}else{
		// 		$linea++;
		// 	}
						
		// 	if($linea!= 0 && $linea%58==0 ){
		// 		$pdf->Cell(array_sum($w),0,'','T');
		// 		$pdf->AddPage();
		// 	}
	
		// 	$pdf->Cell($w[0],5,$contador,'LR',0,'L');
		// 	$pdf->Cell($w[1],5,$search->nroExpediente,'LR',0,'L');
  //           $pdf->Cell($w[2],5,$search->apellidoPaterno.' '.$search->apellidoMaterno.' '.$search->nombre,'LR',0,'L');
		// 	$pdf->Cell($w[3],5,$search->nomDireccion,'LR',0,'L');
		// 	$pdf->Cell($w[4],5,$search->nomManzana,'LR',0,'L');
		// 	$pdf->Ln();
		// 	$fill = !$fill;
  //       }
		// // // Línea de cierre
		// $pdf->Cell(array_sum($w),0,'','T');
		// $pdf->Ln();
		
        $modo="I"; 
        $nombre_archivo = "LISTA_SOCIOS.pdf"; 
        $pdf->Output($nombre_archivo,$modo);
        exit;
    }

}
