<?php

namespace App\Http\Controllers\Admin;

use App\Models\{
    Modulo, Pagina, Persona, Usuario, Roladmin
};
use App\Models\Role;
use App\User;
use App\Admin;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;

class UsuarioController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);
        
        $usuarios = DB::table('admin_role')->select('admin_role.role_id AS rol','admins.*','persona.*','roles.description AS descripcion_rol')
            ->join('admins', 'admins.id', '=', 'admin_role.admin_id')
            ->join('roles', 'roles.id', '=', 'admin_role.role_id')
            ->join('persona', 'persona.idPersona', '=', 'admins.idPersona') 
        ->where(['admins.estado'=>1,'persona.estado'=>1])
        ->get();

        // dd($usuarios);

        $a_data_page = array(
            'title' => 'Lista de Usuario',
            'usuarios'=> $usuarios
        );
        return \Views::admin('usuario.index',$a_data_page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $rol = DB::table('roles')->get();
        $rol = DB::table('roles')->where('estado',1)->get();
        $pais = DB::table('pais')->get();
        $a_data_page = array(
            'title' => 'Registro de paginas',
            'roles' => $rol,
            'pais'  => $pais
        );
        return \Views::admin('usuario.create',$a_data_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
            'username'      => "required",
            'nombre'        => "required",
            'apaterno'      => "required",
            'amaterno'      => "required",
            "email"         => "required|unique:admins",
            "fnacimiento"   => "required",
            "username"      => "required|unique:admins",
            "password"      => "required",
            "pais"          => "required",
            "estado"        => "required",
            "rol"           => "required",
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }
        
        DB::beginTransaction();
        try{
            $perso = new Persona;
            $perso->nombre            = $request->input('nombre');
            $perso->apellidoPaterno   = $request->input('apaterno');
            $perso->apellidoMaterno   = $request->input('amaterno');
            $perso->fechaNacimiento   = $request->input('fnacimiento');
            $perso->idPais            = $request->input('pais');
            $perso->estado            = 1;
            $perso->save();
            $persoId = $perso->idPersona;

            $user = new Admin;
            $user->name       = $request->input('apaterno').' '.$request->input('amaterno').' '.$request->input('nombre');
            $user->email      = $request->input('email');
            $user->password   = Hash::make($request->input('password'));
            $user->idPersona  = $persoId;
            $user->username   = $request->input('username');
            $user->estado     = 1;
            $user->autorizacion= 1;
            $user->save();
            $usuar = $user->id;
            
            $roluser = new Roladmin;
            $roluser->role_id  = $request->input('rol');
            $roluser->admin_id = $usuar;
            $roluser->save();
            DB::commit();
            
            \Session::flash('message', 'Registrado! Se registraron los datos correctamente');
            return redirect()->route('admin.user_list');

        } catch (\Exception $e) {
            DB::rollback();
            dd($e); die();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $usuarios = DB::table('admins')
        // ->join('persona', 'admins.idPersona', '=', 'persona.idPersona')
        // ->where(['admins.id'=> $id])
        // ->first();

        $usuarios = DB::table('admin_role')->select('admin_role.role_id AS rol','admins.*','persona.*','roles.description AS descripcion_rol')
            ->join('admins', 'admins.id', '=', 'admin_role.admin_id')
            ->join('roles', 'roles.id', '=', 'admin_role.role_id')
            ->join('persona', 'persona.idPersona', '=', 'admins.idPersona') 
        ->where(['admins.estado'=>1,'persona.estado'=>1])
        ->where(['admins.id'=> $id])
        ->first();

        $rol = DB::table('roles')->where('estado',1)->get();

        // $cargo = \Views::diccionario('idCargo');
        $pais = DB::table('pais')->get();
        $a_data_page = array(
            'title' => 'Editar de usuario',
            'usuarios' => $usuarios,
            'roles' => $rol,
            'pais' => $pais
        );

        return \Views::admin('usuario.editar',$a_data_page);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'username'      => "required",
            'nombre'        => "required",
            'apaterno'      => "required",
            'amaterno'      => "required",
            "email"         => "required",
            "fnacimiento"   => "required",
            "email"         => "required",
            "password"      => "nullable",
            "pais"          => "required",
            "rol"          => "required",
            // "estado"        => "required"
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }

        $usuario = Admin::find($id);
        $usuario->email     = $request->input('email');
        $usuario->username  = $request->input('username');
        $usuario->name      = $request->input('apaterno').' '. $request->input('amaterno').', '. $request->input('nombre');
        if($request->input('password')){
            $usuario->password  = Hash::make($request->input('password'));
        }
        $usuario->save();

        Roladmin::where(['admin_id'=>$id ])->update([
            'role_id'   => $request->input('rol')
        ]);

        $perso = Persona::find($usuario->idPersona);
        $perso->nombre            = $request->input('nombre');
        $perso->apellidoPaterno   = $request->input('apaterno');
        $perso->apellidoMaterno   = $request->input('amaterno');
        $perso->fechaNacimiento   = $request->input('fnacimiento');
        $perso->idpais            = $request->input('pais');
        $perso->save();
        
        \Session::flash('message', 'Actualizado! Se actualizaron los datos');

        return redirect()->route('admin.user_list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // dd($id);
        $usuario = Admin::find($id);
        $usuario->estado = 2;
        $usuario->save();

        $perso = Persona::find($usuario->idPersona);
        $perso->estado = 3;
        $perso->save();
        $data = array(
            'status' => true
        );

        echo json_encode($data);
    }
}
