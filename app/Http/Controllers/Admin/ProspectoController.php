<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use App\Models\{
    Prospecto, Historial, User, Roluser
};
use Illuminate\Support\Facades\Validator;

class ProspectoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd($request->user()->id);
        $request->user()->authorizeRoles(['admin','vendedor']);

        $prospecto = DB::table('prospectos')->select('prospectos.*','tbl_tipo_documento.Descripcion',DB::raw("(select count(1) from tbl_historial_cliente where idCliente = prospectos.id and validado = 0) as 'pendiente'"), DB::raw("(select count(validado) from tbl_historial_cliente where idCliente = prospectos.id and validado = 1) as 'validados'"))
        ->join('tbl_tipo_documento', 'tbl_tipo_documento.Valor', '=', 'prospectos.tipo_documento')
        ->where([
            'prospectos.estado' => 1,
            'prospectos.usuario_registro' => $request->user()->id
        ])
        ->orderBy('prospectos.id','desc')
        ->get();
        // dd($prospecto);
        $montos = DB::table('tbl_montos')->get();
        $a_data_page = array(
            'title' => 'Lista de Prospectos',
            'prospecto'=> $prospecto,
            'montos'=> $montos
        );
        return \Views::admin('prospectos.index',$a_data_page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd(\Auth::user()->id);
        $tdocumento = DB::table('tbl_tipo_documento')->get();
        $montos = DB::table('tbl_montos')->get();
        $a_data_page = array(
            'title' => 'Registro de Prospecto',
            'documento' => $tdocumento,
            'montos' => $montos,
        );
        return \Views::admin('prospectos.create',$a_data_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->user());
        $validator = Validator::make($request->all(), [
            'nombre'         => "required",
            'apaterno'       => "required",
            'amaterno'       => "required",
            'email'          => "required|unique:prospectos",
            'tdocumento'     => "required",
            "nrodocumento"   => "required",
            "telefono"       => "required",
            "comprasaldo"    => "required",
            "estado"         => "required",
            "saldos"         => "nullable",
            "direccion"      => "required",
            "referencia"     => "required",
            "txtLat"         => "required",
            "txtLng"         => "required"
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }
        
        DB::beginTransaction();
        try{

            if ($request->hasFile('voucher')) {
                $archivo = $request->file('voucher');
                $extension = $archivo->getClientOriginalExtension();
                $fileName = uniqid() . '.' . $extension;
                Storage::disk('voucher')->put($fileName,  \File::get($archivo));
            }else{
                $fileName = '';
            }

            $perso = new Prospecto;
            $perso->nombre              = $request->input('nombre');
            $perso->apellidoPaterno     = $request->input('apaterno');
            $perso->apellidoMaterno     = $request->input('amaterno');
            $perso->email               = $request->input('email');
            $perso->tipo_documento      = $request->input('tdocumento');
            $perso->nro_documento       = $request->input('nrodocumento');
            $perso->telefono            = $request->input('telefono');
            $perso->estado              = $request->input('estado');
            $perso->fecha_registro      = date('Y-m-d');
            $perso->compra_saldo        = $request->input('comprasaldo');
            $perso->validado            = 0;
            $perso->estado_voucher      = 0;
            $perso->direccion           = $request->input('direccion');
            $perso->referencia          = $request->input('referencia');
            $perso->latitud             = $request->input('txtLat');
            $perso->longitud            = $request->input('txtLng');
            $perso->usuario_registro    = $request->user()->id;
            $perso->save();

            $user = new User;
            $user->name     = $request->input('apaterno') . ' '. $request->input('amaterno') . ' ' . $request->input('nombre');
            $user->email    = $request->input('email');
            $user->idUsuario= $perso->id;
            $user->username     = $request->input('nrodocumento');
            $user->estado       = 1;
            $user->autorizacion = 1;
            $user->password = Hash::make($request->input('nrodocumento'));
            $user->save();

            $rol = new Roluser;
            $rol->role_id = 2;
            $rol->user_id = $user->id;
            $rol->save();

            $historia = new Historial;
            $historia->monto        = $request->input('saldos');
            $historia->idUsuario    = \Auth::user()->id;
            $historia->idCliente    = $perso->id;
            $historia->voucher      = $fileName;
            $historia->fechaRegistro= date('Y-m-d');
            $historia->estado       = 1;
            $historia->save();
            
            DB::commit();
            \Session::flash('message', 'Registrado! Se registraron los datos correctamente');
            return redirect()->route('admin.prospecto_list');

        } catch (\Exception $e) {
            DB::rollback();
            dd($e); die();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prospecto = DB::table('prospectos')
        ->select('prospectos.*','tbl_tipo_documento.Descripcion')
        ->join('tbl_tipo_documento', 'tbl_tipo_documento.Valor', '=', 'prospectos.tipo_documento')
        ->where(
            [
                'prospectos.estado' => 1,
                'prospectos.id'     => $id
            ]
        )->first();

        $historial = DB::table('tbl_historial_cliente')
        ->where('idCliente',$id)
        ->first();
        // dd($historial);
        $montos = DB::table('tbl_montos')->get();
        $tdocumento = DB::table('tbl_tipo_documento')->get();
        
        $a_data_page = array(
            'title' => 'Editar de Prospecto',
            'prospecto' => $prospecto,
            'montos'    => $montos,
            'documento' => $tdocumento,
            'historial' => $historial
        );

        return \Views::admin('prospectos.editar',$a_data_page);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'nombre'         => "required",
            'apaterno'       => "required",
            'amaterno'       => "required",
            'email'          => "required",
            'tdocumento'     => "required",
            "nrodocumento"   => "required",
            "telefono"       => "required",
            "comprasaldo"    => "required",
            "historial"      => "required",
            "estado"         => "required",
            "direccion"      => "required",
            "referencia"     => "required",
            "txtLat"         => "required",
            "txtLng"         => "required"
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }

        DB::beginTransaction();
        try{
            if ($request->hasFile('voucher')) {
                $archivo = $request->file('voucher');
                $extension = $archivo->getClientOriginalExtension();
                $fileName = uniqid() . '.' . $extension;
                Storage::disk('voucher')->put($fileName,  \File::get($archivo));
            }else{
                $fileName = '';
            }
            // dd($request);
            $perso = Prospecto::find($id);
            $perso->nombre              = $request->input('nombre');
            $perso->apellidoPaterno     = $request->input('apaterno');
            $perso->apellidoMaterno     = $request->input('amaterno');
            $perso->email               = $request->input('email');
            $perso->tipo_documento      = $request->input('tdocumento');
            $perso->nro_documento       = $request->input('nrodocumento');
            $perso->telefono            = $request->input('telefono');
            $perso->estado              = $request->input('estado');
            $perso->voucher             = $fileName;
            $perso->compra_saldo        = $request->input('comprasaldo');
            $perso->direccion           = $request->input('direccion');
            $perso->referencia          = $request->input('referencia');
            $perso->latitud             = $request->input('txtLat');
            $perso->longitud            = $request->input('txtLng');
            $perso->save();
            
            $historia = Historial::find($request->input('historial'));
            $historia->monto        = $request->input('saldos');
            $historia->voucher      = $fileName;
            $historia->save();
            
            DB::commit();
            \Session::flash('message', 'Actualizado! Se actualizaron los datos');

            return redirect()->route('admin.prospecto_list');
        } catch (\Exception $e) {
            DB::rollback();
            dd($e); die();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $usuario = Prospecto::find($id);
        $usuario->estado = 2;
        $usuario->save();
        $data = array(
            'status' => true
        );
        echo json_encode($data);
    }


    public function pendientes(Request $request)
    {
        $request->user()->authorizeRoles(['admin','vendedor','cobrador']);

        // $prospecto = DB::table('prospectos')->select('prospectos.*','tbl_tipo_documento.Descripcion')
        // ->join('tbl_tipo_documento', 'tbl_tipo_documento.Valor', '=', 'prospectos.tipo_documento')
        // ->whereIn('prospectos.validado',array(0,1))
        // ->whereIn('prospectos.estado_voucher',array(0))
        // ->where(['prospectos.estado'=>1])
        // ->get();
        
        $prospecto = DB::table('prospectos')->select('prospectos.*','tbl_tipo_documento.Descripcion',DB::raw("(select count(1) from tbl_historial_cliente where idCliente = prospectos.id and validado = 0) as 'pendiente'"), DB::raw("(select count(validado) from tbl_historial_cliente where idCliente = prospectos.id and validado = 1) as 'validados'"))
        ->join('tbl_tipo_documento', 'tbl_tipo_documento.Valor', '=', 'prospectos.tipo_documento')
        ->where([
            'prospectos.estado' => 1
        ])
        ->whereIn('prospectos.validado',array(0,1))
        // ->whereIn('prospectos.estado_voucher',array(0))
        ->orderBy('prospectos.id','desc')
        ->get();

        $a_data_page = array(
            'title' => 'Lista de Prospectos',
            'prospecto'=> $prospecto
        );


        return \Views::admin('solicitudes.index',$a_data_page);
    }

    public function ver_prospectos(Request $request,$id)
    {
        $request->user()->authorizeRoles(['admin','vendedor','cobrador']);

        $prospecto = DB::table('prospectos')
        ->select('prospectos.*','tbl_tipo_documento.Descripcion')
        ->join('tbl_tipo_documento', 'tbl_tipo_documento.Valor', '=', 'prospectos.tipo_documento')
        ->where(
            [
                'prospectos.estado' => 1,
                'prospectos.id'     => $id
            ]
        )->first();

        $historial = DB::table('tbl_historial_cliente')->select('tbl_historial_cliente.*','tbl_montos.montos')
        ->join('tbl_montos', 'tbl_montos.id', '=', 'tbl_historial_cliente.monto')
        ->where([
            'tbl_historial_cliente.estado' => 1,
            'idCliente' => $prospecto->id
        ])
        ->get();
            // dd($historial);
        $a_data_page = array(
            'title' => 'Lista de Prospectos',
            'prospecto'=> $prospecto,
            'historial'=> $historial
        );
        return \Views::admin('solicitudes.ver_registro',$a_data_page);
    }
    
    public function estado_pendientes(Request $request,$id)
    {
        $request->user()->authorizeRoles(['admin','vendedor','cobrador']);

        $validator = Validator::make($request->all(), [
            "estado"    => "required",
            "id"        => "nullable",
            "tipo"      => "required"
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }

        DB::beginTransaction();
        try{

            $estado = $request->input('estado');
            $tipo   = $request->input('tipo');
            // dd($estado,$id);

            if($tipo == 'solicitud'){
                $perso = Prospecto::find($id);
                $perso->validado = $estado;

                $historia = new Historial;
                $historia->monto        = 4;
                $historia->idUsuario    = \Auth::user()->id;
                $historia->idCliente    = $id;
                $historia->voucher      = 'LIBRE';
                $historia->fechaRegistro= date('Y-m-d H:i:s');
                $historia->estado       = 1;
                $historia->validado     = 1;
                $historia->save();

            }
            if($tipo == 'voucher'){
                $perso = Historial::find($request->input('id'));
                $perso->validado = $estado;
            }
            // dd($perso);
            $perso->save();

            DB::commit();
            \Session::flash('message', 'Actualizado! Se actualizaron los datos');
            $data = array(
                'status' => true,
                'url'    => route('admin.prospecto_pendientes')
            );
            echo json_encode($data);

            // return redirect()->route('admin.prospecto_pendientes');
        } catch (\Exception $e) {
            DB::rollback();
            dd($e); die();
        }

    }
    
    public function terminado(Request $request)
    {
        $request->user()->authorizeRoles(['admin','vendedor','cobrador']);

        // $prospecto = DB::table('prospectos')->select('prospectos.*','tbl_tipo_documento.Descripcion')
        // ->join('tbl_tipo_documento', 'tbl_tipo_documento.Valor', '=', 'prospectos.tipo_documento')
        // ->whereIn('prospectos.validado',array(1,2))
        // ->whereIn('prospectos.estado_voucher',array(1,2))
        // ->where(['prospectos.estado'=>1])
        // ->get();

        $prospecto = DB::table('prospectos')->select('prospectos.*','tbl_tipo_documento.Descripcion',DB::raw("(select count(1) from tbl_historial_cliente where idCliente = prospectos.id and validado = 0) as 'pendiente'"), DB::raw("(select count(validado) from tbl_historial_cliente where idCliente = prospectos.id and validado = 1) as 'validados'"))
        ->join('tbl_tipo_documento', 'tbl_tipo_documento.Valor', '=', 'prospectos.tipo_documento')
        ->where([
            'prospectos.estado' => 1
        ])
        ->whereIn('prospectos.validado',array(1,2))
        ->whereIn('prospectos.estado_voucher',array(1,2))
        ->get();

        $a_data_page = array(
            'title' => 'Lista de Prospectos',
            'prospecto'=> $prospecto
        );
        return \Views::admin('solicitudes.index',$a_data_page);
    }

    public function recarga(Request $request, $id)
    {
        $request->user()->authorizeRoles(['admin','vendedor']);

        $prospecto = DB::table('prospectos')
        ->select('prospectos.*','tbl_tipo_documento.Descripcion')
        ->join('tbl_tipo_documento', 'tbl_tipo_documento.Valor', '=', 'prospectos.tipo_documento')
        ->where(
            [
                'prospectos.estado' => 1,
                'prospectos.id'     => $id
            ]
        )->first();


        $historial = DB::table('tbl_historial_cliente')
        ->join('tbl_montos', 'tbl_montos.id', '=', 'tbl_historial_cliente.monto')
        ->where([
            'tbl_historial_cliente.estado' => 1,
            'idCliente' => $prospecto->id
        ])
        ->get();

        $a_data_page = array(
            'title'     => 'Recarga de saldo',
            'prospecto' => $prospecto,
            'historial' => $historial
        );

        return \Views::admin('prospectos.saldo',$a_data_page);
    }

    public function datosPersonal(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id' => "required"
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }

        $prospecto = DB::table('prospectos')
        ->select('prospectos.*','tbl_tipo_documento.Descripcion')
        ->join('tbl_tipo_documento', 'tbl_tipo_documento.Valor', '=', 'prospectos.tipo_documento')
        ->where(
            [
                'prospectos.estado' => 1,
                'prospectos.id'     => $request->input('id')
            ]
        )->first();

        return response()->json($prospecto,200);
    }

    public function enviar_recarga(Request $request)
    {

        $data = $request->validate([
            'saldo'     => 'required',
            'voucher'    => 'required',
            'code'       => 'required'
        ], [
            'saldo.required'    => 'El campo saldo es obligatorio',
            'voucher.required'  => 'El campo voucher es obligatorio',
            'code.required'     => 'El campo code es obligatorio'
        ]);

        $dataImage = request()->file('voucher');
        if (!$dataImage->isValid()) {
            return response()->json($dataImage->getErrorMessage(), 200); 
        }

        if ($request->hasFile('voucher')) {
            $archivo = $request->file('voucher');
            $extension = $archivo->getClientOriginalExtension();
            $fileName = uniqid() . '.' . $extension;
            Storage::disk('voucher')->put($fileName,  \File::get($archivo));
        }else{
            $fileName = '';
        }
        
        $historia = new Historial;
        $historia->monto        = $request->input('saldo');
        $historia->idUsuario    = \Auth::user()->id;
        $historia->idCliente    = $request->input('code');
        $historia->voucher      = $fileName;
        $historia->fechaRegistro= date('Y-m-d H:i:s');
        $historia->estado       = 1;
        $historia->validado     = 0;
        $historia->save();

        $status = array(
            'status' => true
        );
        return response()->json($status,200);
    }

    
}
