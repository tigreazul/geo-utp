<?php

namespace App\Http\Controllers\Admin;

use App\Models\{
    Modulo, Pagina
};

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;
use Pdfprincipal;

class ReporteController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['user', 'admin']);
        
        $frontend =  DB::table('frontend')
        ->where([
            'Estado'   => 1
        ])
        ->orderBy('ID')
        ->get();

        // $grupo = \Views::diccionario('idGrupo');
        $grupo =  DB::table('grupo')->get();

        $a_data_page = array(
            'title' => 'Lista de Socios',
            'buscardor'=> $frontend,
            'lstgrupo' => $grupo
        );

        return \Views::admin('reporte.index',$a_data_page);
    }

    public function expediente(Request $request)
    {
        $request->user()->authorizeRoles(['user', 'admin']);

        $tcuota = \Views::diccionario('idTipoCuota');
        $buscardor = array();
        
        $a_data_page = array(
            'title' => 'Lista de Socios',
            // 'dato'  => '',
            'buscador'=> $buscardor
        );

        return \Views::admin('reporte.expediente',$a_data_page);
    }

    public function validaBusquedaSocio(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'mz' => "nullable",
            "grupo"    => "nullable"
        ]);
 
        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        }

        $manzanas   =  $request->input('mz');
        $grupo      =  $request->input('grupo');

        // dd($grupo,$manzanas);
        if( !is_null($manzanas) && $manzanas != '' && $grupo == '' ){
            $buscar = "manzanas.idManzana = '$manzanas'";
        }

        if( !is_null($grupo) && $grupo != '' && $manzanas == '' ){
            $buscar = "grupo.idGrupo = '$grupo'";
        }


        if( $grupo != '' && $manzanas != '' ){
            $buscar = "grupo.idGrupo = '$grupo' AND manzanas.idManzana = '$manzanas'";
        }

        if( empty($manzanas) ){
            $manzanas = "-";
        }

        $vSearch  = DB::select("SELECT expediente.nroExpediente,persona.nombre,persona.apellidoPaterno,persona.apellidoMaterno,expediente.nomDireccion, grupo.idGrupo,grupo.nomGrupo,manzanas.nomManzana
        FROM usuario
        INNER JOIN persona ON usuario.idPersona = persona.idPersona
        INNER JOIN titular ON persona.idPersona = titular.idPersona
        INNER JOIN expediente ON expediente.nroExpediente = titular.nroExpediente
        INNER JOIN manzanas ON manzanas.idManzana = expediente.idManzana
        INNER JOIN grupo ON manzanas.idGrupo = grupo.idGrupo
        WHERE $buscar");
        // dd($vSearch);
        if(!empty($vSearch)){
            return redirect()->route('admin.socio_create_search', [$grupo,$manzanas]);
        }else{
            \Session::flash('message', 'El numero ingresado no existe ');
            return redirect()->route('admin.report_list');
        }

        if(empty($grupo) || empty($manzanas)){
            \Session::flash('message', 'El numero ingresado no existe ');
            return redirect()->route('admin.report_list');
        }

    }

    public function busquedaSocio(Request $request,$grupo,$manzanas)
    {   

        if( !is_null($manzanas) && $manzanas != '' && $grupo == '' ){
            $buscar = "manzanas.idManzana = '$manzanas'";
        }

        if( !is_null($grupo) && $grupo != '' && $manzanas == '-' ){
            $buscar = "grupo.idGrupo = '$grupo'";
        }

        if( $grupo != '' && $manzanas != '-' ){
            $buscar = "grupo.idGrupo = '$grupo' AND manzanas.idManzana = '$manzanas'";
        }


        $vSearch  = DB::select("SELECT expediente.idExpediente,expediente.nroExpediente,persona.nombre,persona.apellidoPaterno,
            persona.apellidoMaterno,expediente.nomDireccion,
            grupo.idGrupo,grupo.nomGrupo,manzanas.nomManzana
            FROM usuario
            INNER JOIN persona ON usuario.idPersona = persona.idPersona
            INNER JOIN titular ON persona.idPersona = titular.idPersona
            INNER JOIN expediente ON expediente.nroExpediente = titular.nroExpediente
            INNER JOIN manzanas ON manzanas.idManzana = expediente.idManzana
            INNER JOIN grupo ON manzanas.idGrupo = grupo.idGrupo
            WHERE $buscar");

        // $grupo = \Views::diccionario('idGrupo');
        $lstgrupo =  DB::table('grupo')->get();


        $a_data_page = array(
            'title'     => 'Registro de paginas',
            'manzanas'  => $manzanas,
            'grupx'  => $grupo,
            'lstgrupo'     => $lstgrupo,
            'buscardor' => $vSearch
        );
        // dd($a_data_page);

        return \Views::admin('reporte.index',$a_data_page);
    }


    public function validaBusqueda(Request $request){
        // dd($request); die();
        $validator = Validator::make($request->all(), [
            'grupo' => "nullable",
            'manzana' => "nullable"
        ]);
 
        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }


        $grupo      =  $request->input('grupo');
        $manzana    =  $request->input('manzana');


        if(!is_null($grupo)){

            $vSearch  = DB::select("SELECT u.idUsuario, p.dni, p.nombre, p.apellidoPaterno, p.apellidoMaterno,
            e.nomDireccion,g.nomGrupo,g.idGrupo,e.nroExpediente
            FROM usuario u
            INNER JOIN persona p ON u.idPersona = p.idPersona
            INNER JOIN expediente e ON u.idUsuario = e.idUsuario
            INNER JOIN manzanas m ON m.idExpediente = e.idExpediente
            INNER JOIN grupo g ON g.idGrupo = m.idGrupo
            WHERE 
            g.idGrupo = $grupo");
        }

        if(!is_null($manzana)){

            $vSearch  = DB::select("SELECT u.idUsuario, p.dni, p.nombre, p.apellidoPaterno, p.apellidoMaterno,
            e.nomDireccion,g.nomGrupo,g.idGrupo,e.nroExpediente
            FROM usuario u
            INNER JOIN persona p ON u.idPersona = p.idPersona
            INNER JOIN expediente e ON u.idUsuario = e.idUsuario
            INNER JOIN manzanas m ON m.idExpediente = e.idExpediente
            INNER JOIN grupo g ON g.idGrupo = m.idGrupo
            WHERE 
            m.nomManzana LIKE '$manzana'");
        }


        $vSearch  = DB::select("SELECT u.idUsuario, p.nombre, p.apellidoPaterno, p.apellidoMaterno,e.nomDireccion,g.nomGrupo
        FROM usuario u
        LEFT JOIN persona p ON u.idPersona = p.idPersona
        LEFT JOIN expediente e ON e.idUsuario = e.idUsuario
        LEFT JOIN manzanas m ON m.idExpediente = e.idExpediente
        LEFT JOIN grupo g ON g.idGrupo = m.idGrupo
        where p.dni = '$search'");

        if(!empty($vSearch)){
            return redirect()->route('admin.pagos_create_search', $search);
        }else{
            \Session::flash('message', 'El numero ingresado no existe ');
            return redirect()->route('admin.pagos_create');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {

        $validator = Validator::make($request->all(), [
            'buscador' => "nullable",
        ]);
 
        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }

        $buscador   =  $request->input('buscador');

        if(!is_null($buscador))
        {
            $vSearch  = DB::select("SELECT expediente.nroExpediente,persona.nombre,persona.apellidoPaterno,persona.apellidoMaterno,expediente.nomDireccion,
            grupo.idGrupo,grupo.nomGrupo,manzanas.nomManzana
            FROM expediente
            INNER JOIN usuario ON usuario.idUsuario = expediente.idUsuario
            INNER JOIN persona ON persona.idPersona = usuario.idPersona
            INNER JOIN manzanas ON manzanas.idExpediente = expediente.idExpediente
            INNER JOIN grupo ON manzanas.idGrupo = grupo.idGrupo
            WHERE expediente.nroExpediente = '$buscador'");
            if(!empty($vSearch)){
                return redirect()->route('admin.exp_create_search', $buscador);
            }else{
                \Session::flash('message', 'El numero ingresado no existe ');
                return redirect()->route('admin.report_exp');
            }
        }

        if(empty($grupo) || empty($manzanas)){
            \Session::flash('message', 'El numero ingresado no existe ');
            return redirect()->route('admin.report_exp');
        }

    }

    public function busquedaExpediente(Request $request,$id)
    {

        $vSearch  = DB::table('titular')
        ->select('persona.*','titular.*','expediente.*',DB::raw('SUM(detalle_cuota.monto) As monto'))
        ->join('usuario','titular.idPersona','=','usuario.idPersona')
        ->join('detalle_cuota','detalle_cuota.idUsuario','=','usuario.idUsuario')
        ->join('cuota','cuota.idCuota','=','detalle_cuota.idCuota')
        ->join('persona','persona.idPersona','=','titular.idPersona')
        ->leftJoin('expediente','expediente.nroExpediente','=','titular.nroExpediente')
        ->where(['titular.nroExpediente'=>$id,'titular.estadoSocio'=>'DESACTIVADO'])
        ->groupBy('titular.idPersona')
        ->get();

        // $grupo = \Views::diccionario('idGrupo');


        $a_data_page = array(
            'title'     => 'Registro de paginas',
            'dato'      => $id,
            // 'grupo'     => $grupo,
            'buscador' => $vSearch
        );

        return \Views::admin('reporte.expediente',$a_data_page);
    }

    public function reporteSocio(Request $request,$grupo,$manzanas){
        header('Content-type: application/pdf');
        if( !is_null($manzanas) && $manzanas != '' && $grupo == '' ){
            $buscar = "manzanas.idManzana = '$manzanas'";
        }

        if( !is_null($grupo) && $grupo != '' && $manzanas == '-' ){
            $buscar = "grupo.idGrupo = '$grupo'";
        }

        if( $grupo != '' && $manzanas != '-' ){
            $buscar = "grupo.idGrupo = '$grupo' AND manzanas.idManzana = '$manzanas'";
        }

        $vSearch  = DB::select("SELECT expediente.idExpediente,expediente.nroExpediente,persona.nombre,persona.apellidoPaterno,
            persona.apellidoMaterno,expediente.nomDireccion,
            grupo.idGrupo,grupo.nomGrupo,manzanas.nomManzana
            FROM usuario
            INNER JOIN persona ON usuario.idPersona = persona.idPersona
            INNER JOIN titular ON persona.idPersona = titular.idPersona
            INNER JOIN expediente ON expediente.nroExpediente = titular.nroExpediente
            INNER JOIN manzanas ON manzanas.idManzana = expediente.idManzana
            INNER JOIN grupo ON manzanas.idGrupo = grupo.idGrupo
            WHERE $buscar");

        // dd($vSearch);
        $pdf = new Pdfprincipal;
        $pdf->AliasNbPages();
		$pdf->SetFont('Arial','',10);
		$pdf->AddPage();
		$pdf->Ln(4);

		//****************************************
		//****************************************

		$pdf->SetFillColor(205,205,205);//color de fondo tabla
            $pdf->SetTextColor(10);
            $pdf->SetDrawColor(153,153,153);
            $pdf->SetLineWidth(.3);
            $pdf->SetFont('Arial','B',9);
            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,6,'GRUPO',1,0,'L',true);        
            $pdf->SetFillColor(255,255,255);    
            $pdf->MultiCell( 140, 6, utf8_decode($vSearch[0]->nomGrupo), 1,'L',true); 
            $pdf->SetFont('Arial','B',9);
            $pdf->SetFillColor(255,255,255);    
            
        $pdf->Ln(3);
		
		//****************************************
		// DATOS 
		//****************************************
		//TITULO
		$pdf->SetFont('Arial','B',9);
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(185,8,utf8_decode('LISTA DE SOCIOS'),1,0,'L',true);
		$pdf->SetFillColor(205,205,205);
		$pdf->Ln();
		
		$pdf->SetFont('Arial','B',8);
		$w = array(7, 25,60,60,33);
		$pdf->Cell($w[0],6,utf8_decode('#'),1,0,'L',true);
		$pdf->Cell($w[1],6,utf8_decode('EXPEDIENTE'),1,0,'L',true);
		$pdf->Cell($w[2],6,utf8_decode('NOMBRE Y APELLIDOS'),1,0,'L',true);
		$pdf->Cell($w[3],6,utf8_decode('DIRECCIÓN'),1,0,'L',true);
		$pdf->Cell($w[4],6,utf8_decode('MANZANA'),1,0,'L',true);
		$pdf->Ln();
		
		// Restauración de colores y fuentes
		$pdf->SetFillColor(224,235,255);
		$pdf->SetTextColor(0);
		$pdf->SetFont('Arial','',7);
		// // Datos
		$fill = false;
        $contador = 0;
        foreach ($vSearch as $search) {
			$contador++;
            $linea = 0;
            
			if($contador == 21){
				$pdf->Cell(array_sum($w),0,'','T');
				$pdf->AddPage();
				$linea = 0;
			}else{
				$linea++;
			}
						
			if($linea!= 0 && $linea%58==0 ){
				$pdf->Cell(array_sum($w),0,'','T');
				$pdf->AddPage();
			}
	
			$pdf->Cell($w[0],5,$contador,'LR',0,'L');
			$pdf->Cell($w[1],5,$search->nroExpediente,'LR',0,'L');
            $pdf->Cell($w[2],5,$search->apellidoPaterno.' '.$search->apellidoMaterno.' '.$search->nombre,'LR',0,'L');
			$pdf->Cell($w[3],5,$search->nomDireccion,'LR',0,'L');
			$pdf->Cell($w[4],5,$search->nomManzana,'LR',0,'L');
			$pdf->Ln();
			$fill = !$fill;
        }
		// // Línea de cierre
		$pdf->Cell(array_sum($w),0,'','T');
		$pdf->Ln();
		
        $modo="I"; 
        $nombre_archivo = "LISTA_SOCIOS.pdf"; 
        $pdf->Output($nombre_archivo,$modo);
        exit;
    }

    public function validaBusquedaExpediente(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "expediente"    => "nullable"
        ]);
 
        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }

        $expediente =  $request->input('expediente');
        
        if(!is_null($expediente))
        {
            $vSearch  = DB::table('titular')
            ->select('persona.*','titular.*','expediente.*',DB::raw('SUM(detalle_cuota.monto) As monto'))
            ->join('usuario','titular.idPersona','=','usuario.idPersona')
            ->join('detalle_cuota','detalle_cuota.idUsuario','=','usuario.idUsuario')
            ->join('cuota','cuota.idCuota','=','detalle_cuota.idCuota')
            ->join('persona','persona.idPersona','=','titular.idPersona')
            ->leftJoin('expediente','expediente.nroExpediente','=','titular.nroExpediente')
            ->where(['titular.nroExpediente'=>$expediente,'titular.estadoSocio'=>'DESACTIVADO'])
            ->groupBy('titular.idPersona')
            ->get();;

           
            if(!empty($vSearch)){
                return redirect()->route('admin.exp_create_search', $expediente);
            }else{
                \Session::flash('message', 'El numero ingresado no existe ');
                return redirect()->route('admin.report_exp');
            }
        }

        if(empty($expediente)){
            \Session::flash('message', 'El expediente no existe');
            return redirect()->route('admin.report_exp');
        }
    }

    public function viewFecha(Request $request){
        $request->user()->authorizeRoles(['user', 'admin']);
        

        $grupo = \Views::diccionario('idGrupo');

        $a_data_page = array(
            'title' => 'Lista de fecha',
            'buscador' => array()
        );

        return \Views::admin('reporte.fpago',$a_data_page);
    }
    public function validarBuscarFecha(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "finicio" => "nullable",
            "ffin"    => "nullable"
        ]);
 
        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }

        $finicio =  $request->input('finicio');
        $ffin    =  $request->input('ffin');
        
        if(!is_null($finicio) && !is_null($ffin))
        {
            $vSearch  = DB::table('titular')
            ->select('persona.*','titular.*','expediente.*',DB::raw('SUM(detalle_cuota.monto) As monto'))
            ->join('usuario','titular.idPersona','=','usuario.idPersona')
            ->join('detalle_cuota','detalle_cuota.idUsuario','=','usuario.idUsuario')
            ->join('cuota','cuota.idCuota','=','detalle_cuota.idCuota')
            ->join('persona','persona.idPersona','=','titular.idPersona')
            ->leftJoin('expediente','expediente.nroExpediente','=','titular.nroExpediente')
            // ->where(['titular.nroExpediente'=>$expediente,'titular.estadoSocio'=>'DESACTIVADO'])
            ->whereBetween('cuota.fechaRegistro', [$finicio, $ffin])
            ->groupBy('titular.idPersona')
            ->get();;
            // dd($vSearch);
           
            if(!empty($vSearch)){
                return redirect()->route('admin.fecha_csearch', [$finicio,$ffin]);
            }else{
                \Session::flash('message', 'El numero ingresado no existe ');
                return redirect()->route('admin.indx_fecha');
            }
        }

        if(empty($expediente)){
            \Session::flash('message', 'El expediente no existe');
            return redirect()->route('admin.indx_fecha');
        }
    }

    public function buscarFecha(Request $request,$fini,$ffin)
    {

        $vSearch  = DB::table('titular')
        ->select('persona.*','titular.*','expediente.*',DB::raw('SUM(detalle_cuota.monto) As monto'))
        ->join('usuario','titular.idPersona','=','usuario.idPersona')
        ->join('detalle_cuota','detalle_cuota.idUsuario','=','usuario.idUsuario')
        ->join('cuota','cuota.idCuota','=','detalle_cuota.idCuota')
        ->join('persona','persona.idPersona','=','titular.idPersona')
        ->leftJoin('expediente','expediente.nroExpediente','=','titular.nroExpediente')
        ->whereBetween('cuota.fechaRegistro', [$fini, $ffin])
        ->groupBy('titular.idPersona')
        ->get();;


        $a_data_page = array(
            'title'     => 'Registro de paginas',
            'fini'      => $fini,
            'ffin'     => $ffin,
            'buscador' => $vSearch
        );

        return \Views::admin('reporte.fpago',$a_data_page);
    }


    public function viewTitular(Request $request){
        $request->user()->authorizeRoles(['user', 'admin']);
        

        $grupo = \Views::diccionario('idGrupo');

        $a_data_page = array(
            'title' => 'Lista de fecha',
            'buscador' => array()
        );

        return \Views::admin('reporte.titular',$a_data_page);
    }

    public function validarBuscarTitular(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "nombre" => "nullable"
        ]);
 
        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }

        $nombre =  $request->input('nombre');
        
        if(!is_null($nombre))
        {
            // $vSearch  = DB::table('titular')
            // ->select('persona.*','titular.*','expediente.*',DB::raw('SUM(detalle_cuota.monto) As monto'))
            // ->join('usuario','titular.idPersona','=','usuario.idPersona')
            // ->join('detalle_cuota','detalle_cuota.idUsuario','=','usuario.idUsuario')
            // ->join('cuota','cuota.idCuota','=','detalle_cuota.idCuota')
            // ->join('persona','persona.idPersona','=','titular.idPersona')
            // ->leftJoin('expediente','expediente.nroExpediente','=','titular.nroExpediente')
            // ->where('persona.nombre','LIKE',$nombre)
            // ->orWhere('persona.apellidoMaterno','LIKE',$nombre)
            // ->orWhere('persona.apellidoPaterno','LIKE',$nombre)
            // // ->whereBetween('cuota.fechaRegistro', [$finicio, $ffin])
            // ->groupBy('titular.idPersona')
            // ->get();;
            // dd($vSearch);

            $vSearch  = DB::select("SELECT u.idUsuario, p.nombre, p.apellidoPaterno, p.apellidoMaterno,e.nomDireccion,g.nomGrupo
            FROM usuario u
            LEFT JOIN persona p ON u.idPersona = p.idPersona
            LEFT JOIN expediente e ON e.idUsuario = e.idUsuario
            LEFT JOIN manzanas m ON m.idExpediente = e.idExpediente
            LEFT JOIN grupo g ON g.idGrupo = m.idGrupo
            where (p.nombre LIKE '%$nombre%' OR p.apellidoMaterno LIKE '%$nombre%' OR p.apellidoPaterno LIKE '%$nombre%' )
            ");

            // $vSearch  = DB::table('titular')
            // ->select('persona.*','titular.*','expediente.*',DB::raw('SUM(detalle_cuota.monto) As monto'))
            // ->join('usuario','titular.idPersona','=','usuario.idPersona')
            // ->join('detalle_cuota','detalle_cuota.idUsuario','=','usuario.idUsuario')
            // ->join('cuota','cuota.idCuota','=','detalle_cuota.idCuota')
            // ->join('persona','persona.idPersona','=','titular.idPersona')
            // ->leftJoin('expediente','expediente.nroExpediente','=','titular.nroExpediente')
            // ->where('persona.nombre','LIKE',$nombre)
            // ->orWhere('persona.apellidoMaterno','LIKE',$nombre)
            // ->orWhere('persona.apellidoPaterno','LIKE',$nombre)

        

           
            if(!empty($vSearch)){
                return redirect()->route('admin.titular_csearch', [$nombre]);
            }else{
                \Session::flash('message', 'El numero ingresado no existe ');
                return redirect()->route('admin.indx_fecha');
            }
        }

        if(empty($expediente)){
            \Session::flash('message', 'El expediente no existe');
            return redirect()->route('admin.indx_fecha');
        }
    }


    public function BuscarTitular(Request $request,$nombre)
    {

        // $vSearch  = DB::table('titular')
        // ->select('persona.*','titular.*','expediente.*',DB::raw('SUM(detalle_cuota.monto) As monto'))
        // ->join('usuario','titular.idPersona','=','usuario.idPersona')
        // ->join('detalle_cuota','detalle_cuota.idUsuario','=','usuario.idUsuario')
        // ->join('cuota','cuota.idCuota','=','detalle_cuota.idCuota')
        // ->join('persona','persona.idPersona','=','titular.idPersona')
        // ->leftJoin('expediente','expediente.nroExpediente','=','titular.nroExpediente')
        // ->where('persona.nombre','LIKE',$nombre)
        // ->orWhere('persona.apellidoMaterno','LIKE',$nombre)
        // ->orWhere('persona.apellidoPaterno','LIKE',$nombre)
        // ->groupBy('titular.idPersona')
        // ->get();;


        $persona  = DB::select("SELECT u.idUsuario, p.nombre, p.apellidoPaterno, p.apellidoMaterno,e.nomDireccion,g.nomGrupo
            FROM usuario u
            LEFT JOIN persona p ON u.idPersona = p.idPersona
            LEFT JOIN expediente e ON e.idUsuario = e.idUsuario
            LEFT JOIN manzanas m ON m.idExpediente = e.idExpediente
            LEFT JOIN grupo g ON g.idGrupo = m.idGrupo
            where (p.nombre LIKE '%$nombre%' OR p.apellidoMaterno LIKE '%$nombre%' OR p.apellidoPaterno LIKE '%$nombre%' )
            ");

        $reuniones  = DB::select("SELECT 
            u.idUsuario,r.idTipoReunion,
            (SELECT valor FROM diccionario WHERE ubicacion = 'idTipoReunion' AND codigo = r.idTipoReunion ) AS 'tipo' ,
            dc.multa,r.fecha,MONTH(r.fecha) AS MES,dc.idDetalleReunion
            FROM usuario u
            LEFT JOIN persona p ON u.idPersona = p.idPersona
            LEFT JOIN detalle_reunion dc ON dc.idUsuario = u.idUsuario
            LEFT JOIN reunion r ON r.idReunion = dc.idReunion
            
            WHERE (p.nombre LIKE '%$nombre%' OR p.apellidoMaterno LIKE '%$nombre%' OR p.apellidoPaterno LIKE '%$nombre%' )
            AND dc.idDetalleReunion NOT IN (SELECT p.idCodigo FROM pago p where p.idCodigo = dc.idDetalleReunion and idUsuario = dc.idUsuario)");

        // dd($reuniones);

        $cuota  = DB::select("SELECT dc.*,c.idTipoCuota,c.fechaRegistro,
            (SELECT valor FROM diccionario WHERE ubicacion = 'idTipoCuota' AND codigo = c.idTipoCuota ) AS 'tipo',
            (SELECT d.valor FROM diccionario d WHERE d.ubicacion = 'idMes' AND d.codigo = dc.idMes ) AS 'mes'
            FROM usuario u
            LEFT JOIN persona p ON u.idPersona = p.idPersona
            LEFT JOIN detalle_cuota dc ON dc.idUsuario = u.idUsuario
            LEFT JOIN cuota c ON c.idCuota = dc.idCuota
            
            WHERE (p.nombre LIKE '%$nombre%' OR p.apellidoMaterno LIKE '%$nombre%' OR p.apellidoPaterno LIKE '%$nombre%' )
            AND c.idTipoCuota IN (2,3)
            AND dc.idCuotaDetalle NOT IN (SELECT p.idCodigo FROM pago p where p.idCodigo = dc.idCuotaDetalle and idUsuario = dc.idUsuario)");

        $legal  = DB::select("SELECT dc.*,c.idTipoCuota,c.fechaRegistro,
            (SELECT valor FROM diccionario WHERE ubicacion = 'idTipoCuota' AND codigo = c.idTipoCuota ) AS 'tipo',
            (SELECT d.valor FROM diccionario d WHERE d.ubicacion = 'idMes' AND d.codigo = dc.idMes ) AS 'mes'
            FROM usuario u
            LEFT JOIN persona p ON u.idPersona = p.idPersona
            LEFT JOIN detalle_cuota dc ON dc.idUsuario = u.idUsuario
            LEFT JOIN cuota c ON c.idCuota = dc.idCuota
            
            WHERE (p.nombre LIKE '%$nombre%' OR p.apellidoMaterno LIKE '%$nombre%' OR p.apellidoPaterno LIKE '%$nombre%' )
            AND c.idTipoCuota IN (1)
            AND dc.idCuotaDetalle NOT IN (SELECT p.idCodigo FROM pago p where p.idCodigo = dc.idCuotaDetalle and idUsuario = dc.idUsuario)");


        $a_data_page = array(
            'title'     => 'Registro de paginas',
            'persona'       => $persona,
            'reuniones'     => $reuniones,
            'cuota'         => $cuota,
            'legal'         => $legal
        );

        return \Views::admin('reporte.titular',$a_data_page);
    }

    

}
