<?php

namespace App\Http\Controllers\Admin;

use App\Models\{
    Modulo, Pagina, Persona, Usuario, Roladmin, Profesor
};
use App\Models\Role;
use App\User;
use App\Admin;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;

class DocenteController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);

        $usuarios = DB::table('role_admin')->select('role_admin.role_id AS rol','admins.*','persona.*','roles.description AS descripcion_rol')
        ->join('admins', 'admins.id', '=', 'role_admin.admin_id')
        ->join('roles', 'roles.id', '=', 'role_admin.role_id')
        ->join('persona', 'persona.idPersona', '=', 'admins.idPersona') 
        ->join('profesor', 'profesor.idPersona', '=', 'persona.idPersona') 
        ->where(['admins.estado'=>1,'roles.id'=>3,'persona.estado'=>1])
        ->get();

        $a_data_page = array(
            'title' => 'Lista de Docente',
            'usuarios'=> $usuarios
        );
        return \Views::admin('docente.index',$a_data_page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rol = DB::table('roles')->get();
        $pais = DB::table('pais')->get();
        $a_data_page = array(
            'title' => 'Registro de paginas',
            'roles' => $rol,
            'pais'  => $pais
        );
        return \Views::admin('docente.create',$a_data_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
            'username'         => "required",
            'nombre'         => "required",
            'apaterno'       => "required",
            'amaterno'       => "required",
            'email'          => "required|unique:admins",
            "fnacimiento"   => "required",
            "username"      => "required|unique:admins",
            "password"      => "required",
            "pais"           => "required",
            "estado"        => "required",
            "facebook"        => "nullable",
            "instagram"        => "nullable"
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }
        
        DB::beginTransaction();
        try{
            $perso = new Persona;
            $perso->nombre            = $request->input('nombre');
            $perso->apellidoPaterno   = $request->input('apaterno');
            $perso->apellidoMaterno   = $request->input('amaterno');
            $perso->fechaNacimiento   = $request->input('fnacimiento');
            $perso->idPais            = $request->input('pais');
            $perso->estado            = 1;
            $perso->save();
            $persoId = $perso->idPersona;

            $user = new Admin;
            $user->name       = $request->input('apaterno').' '.$request->input('amaterno').' '.$request->input('nombre');
            $user->email      = $request->input('email');
            $user->password   = Hash::make($request->input('password'));
            $user->idPersona  = $persoId;
            $user->username   = $request->input('username');
            $user->estado     = 1;
            $user->autorizacion= 1;
            $user->save();
            $usuar = $user->id;
            

            $profesor = new Profesor;
            $profesor->idPersona  = $persoId;
            $profesor->estado     = 1;
            $profesor->especialidad = $request->input('especialidad');
            $profesor->descripcion  = $request->input('descripcion');
            $profesor->facebook     = $request->input('facebook');
            $profesor->instagram    = $request->input('instagram');
            $profesor->save();

            $roluser = new Roladmin;
            $roluser->role_id  = 3;
            $roluser->admin_id = $usuar;
            $roluser->save();
            DB::commit();
            
            \Session::flash('message', 'Registrado! Se registraron los datos correctamente');
            return redirect()->route('admin.docente_list');

        } catch (\Exception $e) {
            DB::rollback();
            dd($e); die();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $usuarios = DB::table('admins')
        // ->join('persona', 'admins.idPersona', '=', 'persona.idPersona')
        // ->where(['users.id'=> $id])
        // ->first();

        $usuarios = DB::table('role_admin')->select('role_admin.role_id AS rol','admins.*','persona.*','roles.description AS descripcion_rol','profesor.*')
        ->join('admins', 'admins.id', '=', 'role_admin.admin_id')
        ->join('roles', 'roles.id', '=', 'role_admin.role_id')
        ->join('persona', 'persona.idPersona', '=', 'admins.idPersona') 
        ->join('profesor', 'profesor.idPersona', '=', 'persona.idPersona') 
        ->where(['admins.estado'=>1,'roles.id'=>3,'persona.estado'=>1,'admins.id'=> $id])
        ->first();

        // $cargo = \Views::diccionario('idCargo');
        $pais = DB::table('pais')->get();
        $a_data_page = array(
            'title' => 'Editar de docente',
            'usuarios' => $usuarios,
            'pais' => $pais
        );

        return \Views::admin('docente.editar',$a_data_page);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'username'      => "nullable",
            'nombre'        => "nullable",
            'apaterno'      => "nullable",
            'amaterno'      => "nullable",
            "fnacimiento"   => "nullable",
            "enlace"        => "nullable",
            "pais"          => "nullable",
            "sobremi"       => "nullable",
            "descripcion"   => "nullable",
            'email'         => "required",
            "password"      => "nullable"
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }

        $user = Admin::find($id);
        $user->name       = $request->input('apaterno').' '.$request->input('amaterno').' '.$request->input('nombre');
        $user->email      = $request->input('email');
        if($request->input('password')){
            $user->password  = $request->input('password');
        }
        $user->username   = $request->input('username');
        $user->estado     = 1;
        $user->autorizacion= 1;
        $user->save();
        $usuar = $user->id;
        
        $perso = Persona::find($user->idPersona);
        $perso->nombre            = $request->input('nombre');
        $perso->apellidoPaterno   = $request->input('apaterno');
        $perso->apellidoMaterno   = $request->input('amaterno');
        $perso->fechaNacimiento   = $request->input('fnacimiento');
        $perso->idPais            = $request->input('pais');
        $perso->sobremi           = $request->input('sobremi');
        $perso->estado            = 1;
        $perso->save();
        $persoId = $perso->idPersona;

        Profesor::where(['idPersona'=>$user->idPersona ])->update([
            'especialidad'   => $request->input('especialidad'),
            'descripcion'    => $request->input('descripcion'),
            'facebook'       => $request->input('facebook'),
            'instagram'      => $request->input('instagram')
        ]);
        
        \Session::flash('message', 'Actualizado! Se actualizaron los datos');

        return redirect()->route('admin.docente_list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // dd($id);
        $usuario = Admin::find($id);
        $usuario->estado = 2;
        $usuario->save();

        $perso = Persona::find($usuario->idPersona);
        $perso->estado = 3;
        $perso->save();

        Profesor::where(['idPersona'=>$user->idPersona ])->update([
            'estado'   => 2
        ]);

        $data = array(
            'status' => true
        );

        echo json_encode($data);
    }
}
