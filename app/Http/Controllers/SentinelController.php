<?php

namespace App\Http\Controllers;

use App\Repositories\Sentinel;
use Illuminate\Http\Request;
use App\Models\{
    Comprobante
};

class SentinelController extends Controller
{
    
    protected $sentinel;

    public function __construct(Sentinel $sentinel){
        $this->sentinel = $sentinel;
    }

    public function index()
    {
        // $lista = $this->sentinel->all();
        // dd($lista);
    }

    public function ver(Request $request)
    {
        return view('admin.formulario');
    }

    public function encryp(Request $request)
    {
        // $user  = $request->input('user');
        $user = 'DB4F19DA91095D3000367F54CF218ECB';
        $clave = $request->input('clave');
        $arr = array(
            'keysentinel' => $user,
            'parametro'   => $clave
        );
        $lista = $this->sentinel->encryptar( $arr );
        // $dd = json_decode( $lista );
        // dd($dd->encriptado);
        echo $lista;
        // return json_decode( $lista );
    }


    public function encrypta(Request $request, $usuario)
    {
        $privada = 'DB4F19DA91095D3000367F54CF218ECB'; // Llave Privada
        $arr = array(
            'keysentinel' => $privada,
            'parametro'   => $usuario
        );
        $lista = $this->sentinel->encryptar( $arr );
        $encriptar = json_decode( $lista );
        // dd($encriptar);
        return $encriptar->encriptado;
        // return json_decode( $lista );
    }

    public function consulta_reporte(Request $request)
    {
        $permiso = session()->get('permisos');

        // dd($request);
        

        $usuario  = '09998644';
        $password = 'Manolo74%';
        $key = '73D905D008C600F7FD90B1CBEBDE864F'; // Llave Publica

        $comprobante = Comprobante::max('id');
        // dd($comprobante);
        $tipoDocumento      = $request->input('tdocumento');
        $nroDocumento       = $request->input('nroDocumento');
        $digVerSol          = '';
        $email              = $request->input('email');
        $telefono           = $request->input('telefono');
        $tipoComprobante    = $request->input('tipoComprobante');
        $nroComprobante     = $request->input('nroDocumentoComprobante');
        $codigoReferencia   = str_pad( ($comprobante + 1), 13, '0', STR_PAD_LEFT);
        // dd($codigoReferencia);
        // Array de consultantes
        $tipoDocumentoPara  = $request->input('tipoDocumentoPara');
        $nroDocumentoPara   = $request->input('nroDocumentoPara');
        $tipoReportePara    = $request->input('tipoConsulta');
        $pagoReporte        = 'S';

        $montosTot = \Views::montototal();
        
        $montos = \DB::table('tbl_montos')
        ->where('tipo','=',$tipoReportePara)
        ->first();

        // dd($montos->montos,$montosTot);

        if($montos->montos >= $montosTot )
        {
            \Session::flash('message', 'Tu saldo no es suficiente');
            return redirect()->route('solicitar_reporte');
            // dd('monto mayor');
        }

        $encriptadoUsuario = $this->encrypta($request, $usuario);
        $encriptadoClave   = $this->encrypta($request, $password);
        if($encriptadoUsuario == '' || $encriptadoClave == '')
        {
            \Session::flash('message', 'Los usuarios de validación no son correctos');
            return redirect()->route('solicitar_reporte');
        }
        

        $detalle[] = array(
            'TipoDocCPT'    => $tipoDocumentoPara,
            'NroDocCPT'     => $nroDocumentoPara,
            'TipoReporte'   => $tipoReportePara,
            'Pago'          => $pagoReporte
        );

        $arr = array(
            'Gx_UsuEnc'             => $encriptadoUsuario,
            'Gx_PasEnc'             => $encriptadoClave,
            'Gx_Key'                => $key,
            'TipoDocSol'            => $tipoDocumento,
            'NroDocSol'             => $nroDocumento,
            'DigVerSol'             => $digVerSol,
            'CorreoSol'             => $email,
            'TelefSol'              => $telefono,
            'TipoDocComprobante'    => $tipoComprobante,
            'NroDocumentoFAC'       => $nroComprobante,
            'ReferenceCode'         => $codigoReferencia,
            'SDT_TitMas'            => $detalle
        );
        
        // dd($arr);
        $lista = $this->sentinel->consultaReporte( $arr );
        $respuesta = json_decode( $lista );
        date_default_timezone_set('America/Lima');
        $comprob = new Comprobante;
        $comprob->TipoDocSol            = $tipoDocumento;
        $comprob->NroDocSol             = $nroDocumento;
        if($respuesta->CodigoWS == 0){
            \Session::flash('messageOk', 'Tu reporte ha sido enviado, revise su correo electronico');
            $comprob->Gx_UsuEnc             = $encriptadoUsuario;
            $comprob->Gx_PasEnc             = $encriptadoClave;
            $comprob->Gx_Key                = $key;
            $comprob->DigVerSol             = $digVerSol;
            $comprob->CorreoSol             = $email;
            $comprob->TelefSol              = $telefono;
            $comprob->TipoDocComprobante    = $tipoComprobante;
            $comprob->NroDocumentoFAC       = $nroComprobante;
            $comprob->ReferenceCode         = $codigoReferencia;
            // $comprob->SDT_TitMas            = array;
            $comprob->Pago                  = $pagoReporte;
        }else{
            \Session::flash('message', 'No se pudo procesar la información, vuelva a intentarlo');
        }
        $comprob->TipoDocCPT            = $tipoDocumentoPara;
        $comprob->idCliente             = $permiso->id;
        $comprob->NroDocCPT             = $nroDocumentoPara;
        $comprob->TipoReporte           = $tipoReportePara;
        $comprob->FechaRegistro         = date('Y-m-d H:i:s');
        $comprob->json = $lista;
        $comprob->save();
        return redirect()->route('solicitar_reporte');
        // dd($arr,$lista);
    }

}
