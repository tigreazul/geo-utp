<?php

namespace App\Http\Controllers\Portal;

use App\Models\{
    Curso, Usuariocurso
};

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use DB;

class PortalController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        // $curso = Curso::where([
        //     'estado'   => 1
        // ])->orderBy('fecha_registro','DESC')->get();
        
        $curso = DB::table('curso')->select('curso.*','persona.nombre','persona.apellidoPaterno','persona.apellidoMaterno')
        ->join('profesor', 'curso.idProfesor', '=', 'profesor.idProfesor')
        ->join('persona', 'persona.idPersona', '=', 'profesor.idPersona')
        ->where('curso.estado',[1])
        ->get();

        $categoria = DB::table('categoria')
        ->where('estadoCat',1)
        // ->join('profesor', 'curso.idProfesor', '=', 'profesor.idProfesor')
        ->get();
        // dd($curso);


        $a_data_page = array(
            'title' => 'Inicio',
            'curso' => $curso,
            'categoria' => $categoria
        );

        return \Views::portal('inicio',$a_data_page);
    }

    public function showCurso(Request $request, $slugCurso){

        // if (!session()->exists('permisos')) {
        //     dd('ss');
        //     $user = session()->get('permisos');
        //     $vCursos = DB::table('usuario_cursos')->select('users.*','usuario_cursos.*','curso.*')
        //     ->join('users', 'usuario_cursos.idUsuario', '=', 'users.id') 
        //     ->join('curso', 'usuario_cursos.idCurso', '=', 'curso.idCUrso') 
        //     ->where(['users.id'=>$user->id,'users.estado'=>1,'users.autorizacion'=>1])->get();
        // }
        // $user = session()->get('permisos');

        $curso = DB::table('curso')->select('curso.*','persona.nombre','persona.apellidoPaterno','persona.apellidoMaterno','profesor.descripcion AS descripcionProfesor','profesor.especialidad')
        ->join('profesor', 'curso.idProfesor', '=', 'profesor.idProfesor')
        ->join('persona', 'persona.idPersona', '=', 'profesor.idPersona')
        ->where(['curso.slug' => $slugCurso,'curso.estado' => 1])->first();

        $categoria = DB::table('categoriacurso')->select('categoria.*','categoriacurso.*')
        ->join('categoria', 'categoria.idCat', '=', 'categoriacurso.idCategoria')
        ->where(['categoriacurso.idCurso' =>$curso->idCurso,'categoria.estadoCat'=>1])->get();
        // dd($categoria);

        $listCurso = DB::table('curso')->select('curso.*','persona.nombre','persona.apellidoPaterno','persona.apellidoMaterno')
        ->join('profesor', 'curso.idProfesor', '=', 'profesor.idProfesor')
        ->join('persona', 'persona.idPersona', '=', 'profesor.idPersona')
        ->where('curso.estado','=', 1)
        ->whereNotIn('curso.idCurso',[$curso->idCurso])
        ->inRandomOrder()
        ->get();

        $cursosInscrito = $this->validaRegistroCurso($curso->idCurso);
        // dd($cursosInscrito);

        $detalle = DB::table('detallecurso')->where(['idCurso'=> $curso->idCurso,'estado'=>1])->get();
        foreach($detalle as $det){
            $detCurso[] = array(
                'contenido' => $det->titulo,
                'temas'     => DB::table('temas')->where(['idDetalleCurso'=> $det->idDetalleCurso,'Estado'=>1])->get()
            );
        }
        $a_data_page = array(
            'title' => $slugCurso,
            'curso' => $curso,
            'detcurso' => $detCurso,
            'cursosInscrito' => $cursosInscrito,
            'categoria' => $categoria,
            'listCurso' => $listCurso
        );
        return \Views::portal('detalle_curso',$a_data_page);
    }

    public function listCursos(Request $request){

        $curso = DB::table('curso')->select('curso.*','persona.nombre','persona.apellidoPaterno','persona.apellidoMaterno','categoria.nombreCat')
        ->join('categoriacurso', 'curso.idCurso', '=', 'categoriacurso.idCurso')
        ->join('categoria', 'categoria.idCat', '=', 'categoriacurso.idCategoria')
        ->join('profesor', 'curso.idProfesor', '=', 'profesor.idProfesor')
        ->join('persona', 'persona.idPersona', '=', 'profesor.idPersona')
        ->where('curso.estado','=', 1)
        ->get();
        
        $a_data_page = array(
            'title' => 'Lista de Cursos',
            'curso' => $curso
        );
        return \Views::portal('lista_curso',$a_data_page);
    }

    public function searchCursos(Request $request,$data){
        
        $curso = DB::table('curso')->select('curso.*','persona.nombre','persona.apellidoPaterno','persona.apellidoMaterno','categoria.nombreCat')
        ->join('categoriacurso', 'curso.idCurso', '=', 'categoriacurso.idCurso')
        ->join('categoria', 'categoria.idCat', '=', 'categoriacurso.idCategoria')
        ->join('profesor', 'curso.idProfesor', '=', 'profesor.idProfesor')
        ->join('persona', 'persona.idPersona', '=', 'profesor.idPersona')
        ->where("curso.titulo","LIKE","%{$data}%")
        ->where('curso.estado',1)
        ->get();
        // dd($curso);
        $a_data_page = array(
            'title' => 'Lista de Cursos',
            'curso' => $curso
        );
        return \Views::portal('search_curso',$a_data_page);
    }

    public function busqueda(Request $request)
    {
        $buscar = $request->input('buscar');
        // dd($buscar);
        return redirect()->route('search-cursos',$buscar);


        // dd($request);
        // $input = $request->all();

        // if($request->get('busqueda')){
            // $noticias = Noticia::where("noticiero_turno", "LIKE", "%{$request->get('busqueda')}%")
        //         ->paginate(5);
        // }else{
        //     $noticias = Noticia::paginate(5);
        // }

        // return response($noticias);
    }

    public function validaRegistroCurso($curso)
    {
        $valido = array(
            'status' =>false
        );

        $user = session()->get('permisos');
        if ( !is_null($user) ) {
            // dd($user->id);
            $vCursos = DB::table('usuario_cursos')->select('users.*','usuario_cursos.*','curso.*')
            ->join('users', 'usuario_cursos.idUsuario', '=', 'users.id') 
            ->join('curso', 'usuario_cursos.idCurso', '=', 'curso.idCUrso') 
            ->where(['users.id'=>$user->id,'users.estado'=>1,'users.autorizacion'=>1,'usuario_cursos.idCurso'=> $curso])->first();
            if( !empty($vCursos) )
            {
                $valido = array(
                    'status' => true,
                    'cursos' => $vCursos
                );
            }
        }
        return $valido;
    }

    public function datosCurso(Request $request)
    {
        $valido = array(
            'status' =>false
        );
        $user = session()->get('permisos');
        if ( !is_null($user) ) {
            $slug = $request->input('c');
            $vCursos = DB::table('curso')
                ->select('curso.titulo','curso.descripcion','curso.slug')
                ->where(['curso.estado'=>1,'curso.slug'=> $slug])->first();
            if( !empty($vCursos) )
            {
                $valido = array(
                    'status' => true,
                    'cursos' => $vCursos
                );
            }
        }
        return $valido;
    }

    public function registroCursos(Request $request)
    {
        $valido = array(
            'status' =>false
        );
        $user = session()->get('permisos');
        if ( !is_null($user) ) {
            $slug = $request->input('c');
            $vCursos = DB::table('curso')
                ->select('curso.titulo','curso.descripcion','curso.slug', 'curso.idCurso')
                ->where(['curso.estado'=>1,'curso.slug'=> $slug])->first();
            if( !empty($vCursos) )
            {
                // dd($user, $vCursos);
                $registro = new Usuariocurso; 
                $registro->idUsuario = $user->id;
                $registro->idCurso = $vCursos->idCurso;
                $registro->estado = 1;
                $registro->fechaInscripcion = date('Y-m-d H:i:s');
                $registro->save();

                $valido = array(
                    'status' => true,
                    'cursos' => $vCursos
                );
                // return redirect()->route('search-cursos',$buscar);
            }
        }
        return $valido;
        
    }
    
    public function cursoid(Request $request)
    {
        $valido = array(
            'status' =>false
        );
        $id = $request->input('c');
        // $temas = DB::table('temas')->find($id)->first();
        $temas = DB::table('temas')->where(['Estado'=>1,'idTema'=> $id])->first();
        if( !empty($temas) )
        {
            $tempVideo = $this->datoVideo($temas->Video);
            // dd($tempVideo);
            // die();
            // $url = explode('//',$temas->Video);
            $valido = array(
                'status' => true,
                'temas' => array(
                    'id'            => $temas->idTema,
                    'titulo'        => $temas->Titulo,
                    'descripcion'   => $temas->Descripcion,
                    'video'         => $temas->Video
                ),
                'video' => $tempVideo
            );
        }
        echo json_encode($valido);
    }

    public function datoVideo($video){
        // <!-- https://www.youtube.com/watch?v=l6eQPY3aZVI -->
        // <!-- https://vimeo.com/265045525 -->
        // <!-- <iframe src="https://player.vimeo.com/video/126241629
        $dataYoutube = strpos($video, 'youtube');
        $dataVimeo = strpos($video, 'vimeo');
        $arrDatos = array();
        if ($dataYoutube !== false) {
            $tipo = 'youtube';
            $tCodigo = explode('v=',$video);
            $codigo = $tCodigo[1];
            $arrDatos = array(
                'tipo' => $tipo,
                'codigo'=> $codigo
            );
        }else if($dataVimeo !== false){
            $tipo = 'vimeo';
            $remplace = str_replace('video/','',$video);
            $tCodigo = explode('/',$remplace);
            $codigo = $tCodigo[3];
            $arrDatos = array(
                'tipo' => $tipo,
                'codigo'=> $codigo
            );
        }else{
            $arrDatos = array(
                'tipo' => 'video',
                'codigo'=> $video
            );
        }
        return $arrDatos;
    }

    public function videos_curso(Request $request,$tema,$curso){
        $listacurso = DB::table('curso')->where(['curso.idCurso' => $curso,'estado'=>1])->first();
        $listaTema = DB::table('detallecurso')->where(['idCurso'=> $curso,'estado'=>1])->get();
        foreach($listaTema as $det){
            $detCurso[] = array(
                'contenido' => $det->titulo,
                'idDetalleCurso' => $det->idDetalleCurso,
                'temas'     => DB::table('temas')->where(['idDetalleCurso'=> $det->idDetalleCurso,'Estado'=>1])->get()
            );
        }

        $firstTema = DB::table('temas')->where(['idTema'=> $tema,'Estado'=>1])->first();
        // dd($firstTema->idDetalleCurso);
        $tempVideo = $this->datoVideo($firstTema->Video);
        return view('portal.curso_video')->with('tema_detalle',$firstTema)
                    ->with('detcurso',$detCurso)
                    ->with('slug',$listacurso->slug)
                    ->with('cursoid',$curso)
                    ->with('video',$tempVideo);
        // 
    }
}
