<?php

namespace App\Http\Controllers\Portal;

use App\Models\{
    Curso, Persona, Prospectos, Historial
};

use App\{
    User 
};

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use DB;
use Illuminate\Support\Facades\Hash;

class PerfilController extends Controller
{
    public $datos;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->datos = session()->get('permisos');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = session()->get('permisos');
        $mUser = DB::table('users')->select('users.*','prospectos.*')
        ->join('prospectos', 'prospectos.id', '=', 'users.idUsuario') 
        ->where(['users.idUsuario'=>$user->id,'users.estado'=>1,'prospectos.validado'=>1])->first();
        
        // dd($mUser);

        $pais = DB::table('pais')->orderBy('id', 'ASC')->get();
        $a_data_page = array(
            'title'     => 'Inicio',
            'pais'      => $pais,
            'perfil'    => $mUser
        );
        // dd($a_data_page);
        return \Views::portal('profile',$a_data_page);
    }

    public function profileUpdate(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'nombre'        => "required",
            'apaterno'      => "required",
            'amaterno'      => "required",
            "fnacimiento"   => "nullable",
            "email"         => "required"
        ]);

        if ($validator->fails()) {
            return redirect()->route('profile')
                        ->withErrors($validator)
                        ->withInput();
        }
        // if ($validator->fails()) {    
        //     return response()->json($validator->messages(), 200);
        // }

        $usuario = User::find($id);
        $usuario->email    = $request->input('email');
        $usuario->name     = $request->input('apaterno') . ' '. $request->input('amaterno') . ' ' . $request->input('nombre');
        // if( !empty($request->input('password')) ){
        //     $usuario->password      = Hash::make($request->input('password'));
        // }
        $usuario->save();

        $persona = Prospectos::find($usuario->idUsuario);
        $persona->nombre            = $request->input('nombre');
        $persona->apellidoPaterno   = $request->input('apaterno');
        $persona->apellidoMaterno   = $request->input('amaterno');
        $persona->fechaNacimiento   = $request->input('fnacimiento');
        $persona->email     = $request->input('email');
        $persona->save();

        // User::where(['idUsuario'=>$usuario->idUsuario ])->update([
        //     'name'     => $request->input('apaterno').' '.$request->input('amaterno').' '.$request->input('nombre'),
        //     'email'    => $request->input('usuario').'@profile.com'
        // ]);

        \Session::flash('message', 'Actualizado! Se actualizaron los datos');
        return redirect()->route('profile');
    }

    public function cuentaUpdate(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            "password"        => "required",
            "newpassword"     => "required",
            "renewpassword"   => "required",
        ]);

        if ($validator->fails()) {
            return redirect()->route('cuenta')->withErrors($validator)->withInput();
        }

        $password       = $request->input('password');
        $newpassword    = $request->input('newpassword');
        $renewpassword  = $request->input('renewpassword');
        
        $user = session()->get('permisos');
        $usuario = DB::table('users')->where('id',$user->id)->first();
        
        if (!Hash::check($password, $usuario->password)) {
            return redirect()->route('cuenta')->withInput()->withErrors([ 'La conraseña ingresada no pertenece a la cuenta']);
        }
        
        // dd($password,$newpassword,$renewpassword);
        if($newpassword !== $renewpassword){
            return redirect()->route('cuenta')->withInput()->withErrors([ 'Las contraseñas ingresadas no coinciden']);
        }
        
        $usuario = User::find($id);
        if( !empty( $newpassword ) ){
            $usuario->password      = Hash::make($newpassword);
        }
        $usuario->save();

        \Session::flash('message', 'Se cambio la contraseña correctamente');
        return redirect()->route('cuenta');
    }

    public function cuenta(Request $request)
    {
        $user = session()->get('permisos');
        $mUser = DB::table('users')->select('users.*','prospectos.*')
        ->join('prospectos', 'prospectos.id', '=', 'users.idUsuario') 
        ->where(['users.idUsuario'=>$user->id,'users.estado'=>1,'prospectos.validado'=>1])->first();

        $a_data_page = array(
            'title'     => 'Inicio',
            'perfil'    => $mUser
        );

        return \Views::portal('cuenta',$a_data_page);
    }

    public function mis_cursos(Request $request){
        $user = session()->get('permisos');
        // dd($user);
        $vCursos = DB::table('usuario_cursos')->select('users.*','usuario_cursos.*','curso.*')
        ->join('users', 'usuario_cursos.idUsuario', '=', 'users.id') 
        ->join('curso', 'usuario_cursos.idCurso', '=', 'curso.idCUrso') 
        ->where(['users.id'=>$user->id,'users.estado'=>1,'users.autorizacion'=>1])->get();

        $a_data_page = array(
            'title'     => 'Inicio',
            'cursos'    => $vCursos
        );

        return \Views::portal('mis_cursos',$a_data_page);
    }


    public function recarga_saldo(Request $request)
    {
        $user = session()->get('permisos');
        $mUser = DB::table('users')->select('users.*','prospectos.*')
        ->join('prospectos', 'prospectos.id', '=', 'users.idUsuario') 
        ->where(['users.idUsuario'=>$user->id,'users.estado'=>1,'prospectos.validado'=>1])->first();

        $montos = DB::table('tbl_montos')->get();

        $a_data_page = array(
            'title'     => 'Inicio',
            'perfil'    => $mUser,
            'montos'    => $montos
        );

        return \Views::portal('recarga',$a_data_page);
    }

    public function historial_recarga(Request $request)
    {
        $user = session()->get('permisos');
        $mUser = DB::table('users')->select('users.*','prospectos.*')
        ->join('prospectos', 'prospectos.id', '=', 'users.idUsuario') 
        ->where(['users.idUsuario'=>$user->id,'users.estado'=>1,'prospectos.validado'=>1])->first();

        $historial = DB::table('tbl_historial_cliente')->select('tbl_historial_cliente.*','tbl_montos.montos')
        ->join('tbl_montos', 'tbl_montos.id', '=', 'tbl_historial_cliente.monto')
        ->where([
            'tbl_historial_cliente.estado' => 1,
            'idCliente' => $user->id
        ])
        ->get();
        // dd($historial);
        $montos = DB::table('tbl_montos')->get();

        $a_data_page = array(
            'title'     => 'Inicio',
            'perfil'    => $mUser,
            'historial' => $historial
        );
        // dd($historial);

        return \Views::portal('historial',$a_data_page);
    }
    

    public function recargar(Request $request)
    {

        $validator = Validator::make($request->all(), [
            "saldos"      => "required"
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);
        }

        DB::beginTransaction();
        try{
            if ($request->hasFile('voucher')) {
                $archivo = $request->file('voucher');
                $extension = $archivo->getClientOriginalExtension();
                $fileName = uniqid() . '.' . $extension;
                Storage::disk('voucher')->put($fileName,  \File::get($archivo));
            }else{
                $fileName = '';
            }
            // dd($request);
            $user = session()->get('permisos');
            date_default_timezone_set('America/Lima');
            $historia = new Historial;
            $historia->monto        = $request->input('saldos');
            $historia->voucher      = $fileName;
            $historia->estado       = 1;
            $historia->validado     = 0;
            $historia->fechaRegistro= date('Y-m-d H:i:s');
            $historia->idCliente    = $user->id;
            $historia->save();
            DB::commit();
            \Session::flash('message', 'Registrado! Se envio la solicitud correctamente');
            return redirect()->route('historia_recarga');
        } catch (\Exception $e) {
            DB::rollback();
            dd($e); die();
        }
    }


    public function reporte_solicitud(Request $request)
    {
        $user = session()->get('permisos');
        $mUser = DB::table('users')->select('users.*','prospectos.*')
        ->join('prospectos', 'prospectos.id', '=', 'users.idUsuario') 
        ->where(['users.idUsuario'=>$user->id,'users.estado'=>1,'prospectos.validado'=>1])->first();
        
        // dd($mUser);
        $a_data_page = array(
            'title'     => 'Inicio',
            'perfil'    => $mUser
        );
        // dd($a_data_page);
        return \Views::portal('recarga.solicitud',$a_data_page);
    }

    public function reporte_solicitud_terceros(Request $request)
    {
        $user = session()->get('permisos');
        $mUser = DB::table('users')->select('users.*','prospectos.*')
        ->join('prospectos', 'prospectos.id', '=', 'users.idUsuario') 
        ->where(['users.idUsuario'=>$user->id,'users.estado'=>1,'prospectos.validado'=>1])->first();

        $tdocumento = DB::table('tbl_tipo_documento')->get();
        // dd($mUser);
        $a_data_page = array(
            'title'     => 'Inicio',
            'perfil'    => $mUser,
            'documento' => $tdocumento
        );
        // dd($a_data_page);
        return \Views::portal('recarga.solicitud_tercero',$a_data_page);
    }

    public function reporte_mi_solicitud(Request $request)
    {
        $user = session()->get('permisos');
        $mUser = DB::table('users')->select('users.*','prospectos.*')
        ->join('prospectos', 'prospectos.id', '=', 'users.idUsuario') 
        ->where(['users.idUsuario'=>$user->id,'users.estado'=>1,'prospectos.validado'=>1])->first();

        $tdocumento = DB::table('tbl_tipo_documento')->get();
        // dd($mUser);
        $a_data_page = array(
            'title'     => 'Inicio',
            'perfil'    => $mUser,
            'documento' => $tdocumento
        );
        // dd($a_data_page);
        return \Views::portal('recarga.mi_solicitud',$a_data_page);
    }

    public function mis_reportes(Request $request)
    {
        $user = session()->get('permisos');
        $mUser = DB::table('users')->select('users.*','prospectos.*')
        ->join('prospectos', 'prospectos.id', '=', 'users.idUsuario') 
        ->where(['users.idUsuario'=>$user->id,'users.estado'=>1,'prospectos.validado'=>1])->first();

        $historial = DB::table('tbl_comprobante_facturacion')
        // ->select('tbl_comprobante_facturacion.*','tbl_montos.*')
        ->join('tbl_montos', 'tbl_montos.tipo', '=', 'tbl_comprobante_facturacion.TipoReporte')
        ->join('tbl_tipo_reporte', 'tbl_tipo_reporte.Valor', '=', 'tbl_comprobante_facturacion.TipoReporte')
        ->where([
            // 'tbl_historial_cliente.estado' => 1,
            // 'idCliente' => $user->id,
            'tbl_comprobante_facturacion.Pago' => 'S'
        ])
        ->get();
        // dd($historial);

        $a_data_page = array(
            'title'     => 'Inicio',
            'perfil'    => $mUser,
            'historial' => $historial
        );
        // dd($historial);

        return \Views::portal('mi_reporte',$a_data_page);
    }


    public function actividades_asignadas()
    {
        $user = session()->get('permisos');
        $activityPersonList = DB::table('actividadPersona')
        ->select('actividades.descripcion','actividadPersona.id','actividadPersona.estado','actividadPersona.concluido','actividadPersona.fechaConclusion')
        ->join('users', 'users.id', '=', 'actividadPersona.idPersona')
        ->join('persona', 'persona.idPersona', '=', 'users.idUsuario') 
        ->join('actividades', 'actividades.id', '=', 'actividadPersona.idActividad')
        ->where([
                'actividadPersona.estado' => 1,
                'persona.idPersona' => $user->id
            ])
        ->orderBy('actividadPersona.estado','desc')
        ->get();
        // dd($activityPersonList);

        $a_data_page = array(
            'title'     => 'Actividades',
            'activity' => $activityPersonList
        );

        return \Views::portal('mi_activity',$a_data_page);

    }

}
