<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Session;
// use App\Models\Usuario;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:web')->except('logout');
    }


    protected function authenticated(Request $request, $user)
    {
        // $mUser = DB::table('role_user')->select('role_user.role_id AS rol','users.*','persona.*')
        // ->join('users', 'users.id', '=', 'role_user.user_id')
        // ->join('roles', 'roles.id', '=', 'role_user.role_id')
        // ->join('persona', 'persona.idPersona', '=', 'users.idPersona') 
        // ->where(['users.username'=>$request->username,'users.estado'=>1,'users.autorizacion'=>1])->first();

        $mUser = DB::table('role_user')->select('role_user.role_id AS rol','users.*','prospectos.*')
        ->join('users', 'users.id', '=', 'role_user.user_id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->join('prospectos', 'prospectos.id', '=', 'users.idUsuario') 
        ->where(['users.username'=>$request->username,'users.estado'=>1,'prospectos.validado'=>1])->first();


        session()->put('permisos',$mUser);  
    }


    protected function attemptLogin(Request $request)
    {
        // $mUser = DB::table('users')->where(['username'=>$request->username,'estado'=>1,'autorizacion'=>1])->first();

        $mUser = DB::table('role_user')->select('role_user.role_id AS rol','users.*','prospectos.*')
        ->join('users', 'users.id', '=', 'role_user.user_id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->join('prospectos', 'prospectos.id', '=', 'users.idUsuario') 
        ->where(['users.username'=>$request->username,'users.estado'=>1,'prospectos.validado'=>1])->first();

        // $mUser = DB::table('prospectos')->select('prospectos.*','tbl_tipo_documento.Descripcion')
        // ->join('tbl_tipo_documento', 'tbl_tipo_documento.Valor', '=', 'prospectos.tipo_documento')
        // ->where([
        //     'prospectos.estado' => 1,
        //     'prospectos.nro_documento' => $request->username,
        //     'prospectos.validado' => 1
        // ])
        // ->first();

        // dd($mUser);
        if(!$mUser){
            session()->flash('message', 'No tiene autorizacion para ingresar al modulo');
            return false;
        }
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }
    
    public function username()
    {
        return 'username';
    }

    public function logout(Request $request) {
        Auth::logout();
        Session::flush(); 
        return redirect('/');
      }

}
