<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\{
    Modulo, Pagina, Persona, Usuario, Roluser
};

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'username' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }


    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    // public function showRegistrationForm()
    // {
    //     return view('auth.register');
    // }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    // protected function register(Request $request)
    {
        // dd($request->input('name'));
        // $name       = $request->input('name');
        // $email      = $request->input('email');
        // $password   = $request->input('password');
        // $username   = $request->input('username');

        $name       = $data['name'];
        $email      = $data['email'];
        $password   = $data['password'];
        $username   = $data['username'];
        
        $perso = new Persona;
        $perso->nombre   = $name;
        $perso->estado   = 1;
        $perso->save();
        $persoId = $perso->idPersona;
        
        $usuario = User::create([
            'name'          => $name,
            'email'         => $email,
            'password'      => Hash::make($password),
            'idPersona'     => $persoId,
            'username'      => $username,
            'estado'        => 1,
            'autorizacion'  => 1
        ]);
        $usuar = $usuario->id;
        
        $roluser = new Roluser;
        $roluser->role_id  = 2;
        $roluser->user_id = $usuar;
        $roluser->save();
        
        // return redirect()->intended('/login');

        $mUser = DB::table('role_user')->select('role_user.role_id AS rol','users.*','persona.*')
        ->join('users', 'users.id', '=', 'role_user.user_id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->join('persona', 'persona.idPersona', '=', 'users.idPersona') 
        ->where(['users.username'=>$data['username'],'users.estado'=>1,'users.autorizacion'=>1])->first();
        session()->put('permisos',$mUser);  

        return $usuario;
    }
}
