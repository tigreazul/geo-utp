<?php

namespace App\Repositories;
use GuzzleHttp\Client;


class GuzzleHttpRequest 
{

    protected $client;

    public function __construct(Client $client){
        $this->client = $client;
    }

    protected function get($url)
    {
        $response = $this->client->request('GET', $url);
        return json_encode( $response->getBody()->getContents() );
    }

    protected function post($url, $params)
    {
        $response = $this->client->request( 'POST', $url, 
            [
                // 'headers' => ['Content-Type' => 'application/json'],
                'json' => $params
            ]
        );
        return $response->getBody()->getContents();
        // return json_encode( $response->getBody()->getContents() );
    }

}