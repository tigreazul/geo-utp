<?php

namespace App\Repositories;


class Sentinel extends GuzzleHttpRequest
{

    
    public function all()
    {
        return $this->get('posts');
    }

    public function encryptar($data)
    {
        return $this->post('wsrest/rest/rws_senenc',$data);
    }


    public function consultaReporte($data)
    {
        return $this->post('wsrest/rest/RWS_Tda_SolConTitMasFac',$data);
    }

}